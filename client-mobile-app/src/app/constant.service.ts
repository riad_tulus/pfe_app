import { Injectable } from "@angular/core";

@Injectable()
export class ConstantService {
  API_ENDPOINT: String;
  Login_Auth: String;
  Socket_Url: String;

  constructor() {
    this.Login_Auth = "http://10.188.70.194:8000/";
    this.API_ENDPOINT = "http://10.188.70.194:8000/api/";
    this.Socket_Url = "http://10.188.70.194:8000/";

    // this.Socket_Url = "https://restaurantsass.herokuapp.com/";
    // this.Login_Auth = "https://restaurantsass.herokuapp.com/";
    // this.API_ENDPOINT = "https://restaurantsass.herokuapp.com/api/";
  }
}
