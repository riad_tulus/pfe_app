import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Events
} from "ionic-angular";
import { RestaurantDetailService } from "./restaurant-detail.service";

@IonicPage()
@Component({
  selector: "page-restaurant-details",
  templateUrl: "restaurant-details.html",
  providers: [RestaurantDetailService]
})
export class RestaurantDetailsPage {
  searchBoxVisible = false;
  btnsVisible = true;
  visible = false;
  public cartItemCount: number = 0;
  public contactNumber: number;
  public noOfProduct: number = 0;
  public restaurantId: any;
  public rating: number = 0;
  public productList: any[] = [
    {
      variants: [
        {
          price: ""
        }
      ]
    }
  ];
  public freeDelivery: boolean = false;
  public restaurant: any = {};
  public deliveryInfo: any = {};
  public contactPerson: any;
  public workingHours: any = {};
  public message: string = "";
  public isShopOpen: boolean = false;
  public isProductAvailable: boolean = false;
  public timeSchedule: any[] = [];
  public weekday: any[] = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  public location: any = {};
  private restaurantDetail: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestaurantDetailService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private events: Events
  ) {}

  ionViewWillEnter() {
    this.events.subscribe("restInfo", (res: any) => {
      this.deliveryInfo = res.delivery;
      this.restaurantDetail = res.restaurant;
    });
    if (localStorage.getItem("cartItem") != null) {
      this.cartItemCount = JSON.parse(localStorage.getItem("cartItem")).length;
    }
  }

  async ionViewWillLoad() {
    this.restaurantId = this.navParams.get("id");
    if (this.restaurantId != null) {
      await this.getAllProduct(this.restaurantId);
      this.getLocationOpeningTImings(this.restaurantId);
    }
  }

  // Used to check resturatbt is open or closed and its working hours
  getLocationOpeningTImings(id) {
    this.restService.getlocationById(id).subscribe((res: any) => {
      this.contactPerson = res.contactPerson;
      this.contactNumber = res.contactNumber;
      this.rating = res.rating ? res.rating : 0;
      if (res.deliveryInfo == undefined) {
        this.deliveryInfo.areaAthority = true;
        this.deliveryInfo.freeDelivery = true;
        this.freeDelivery = true;
      } else {
        this.deliveryInfo = res.deliveryInfo.deliveryInfo;
      }
      this.location._id = res._id;
      this.location.locationName = res.locationName;
      if (res.workingHours) {
        this.workingHours = res.workingHours;
        let today = this.weekday[new Date().getDay()];
        let hour = Number(new Date().getHours());
        let minute = Number(new Date().getMinutes());

        if (!res.workingHours.isAlwaysOpen) {
          let indexOfDay = this.workingHours.daySchedule.findIndex(
            item => item.day == today
          );
          if (indexOfDay != -1) {
            this.timeSchedule = this.workingHours.daySchedule[
              indexOfDay
            ].timeSchedule;
            if (this.timeSchedule != null) {
              for (let i = 0; i < this.timeSchedule.length; i++) {
                let shopOpenTime = this.timeSchedule[i].openTime.split(":");
                let shopOpenHour = Number(shopOpenTime[0]);
                // let shopOpenMinute = Number(shopOpenTime[1]);
                let shopCloseTime = this.timeSchedule[i].closingTime.split(":");
                let shopCloseHour = Number(shopCloseTime[0]);
                // let shopCloseMinute = Number(shopCloseTime[1]);
                if (hour >= shopOpenHour && hour <= shopCloseHour) {
                  if (minute > 0 && hour < shopCloseHour) {
                    this.message = "Restaurant is Open now";
                    this.isShopOpen = true;
                  } else {
                    this.isShopOpen = false;
                    this.message = "Restaurant is Closed now";
                  }
                  break;
                } else {
                  this.isShopOpen = false;
                  this.message = "Restaurant is Closed now";
                }
              }
            }
          } else {
            this.message = "Restaurant is Open now";
            this.isShopOpen = true;
          }
        } else {
          this.message = "Restaurant is Open now";
          this.isShopOpen = true;
        }
      } else {
        this.message = "Restaurant is Open now";
        this.isShopOpen = true;
      }
    });
  }

  // Used for get all product list of that restaurant
  getAllProduct(id) {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.restService.getProductListByRestId(id).subscribe(
      (res: any) => {
        if (res.message) {
          this.isProductAvailable = true;
          loader.dismiss();
        } else {
          loader.dismiss();
          this.noOfProduct = res.categorydata.length;
          this.restaurant = res.restaurant;
          this.productList = res.categorydata;
          if (res.restaurant.taxInfo == undefined) {
            this.restaurant.taxInfo = { taxName: "Tax", taxRate: 0 };
          }
        }
      },
      error => {
        loader.dismiss();
      }
    );
  }

  // Used to goto product detail page
  OnGoToProduct_detailsPage(item) {
    let restaurantInfo = {
      restaurantID: this.restaurant._id,
      restaurantName: this.restaurant.restaurantName,
      location: this.location,
      taxInfo: this.restaurant.taxInfo
    };
    this.navCtrl.push("ProductDetailsPage", {
      product: item,
      restaurantName: this.restaurant.restaurantName,
      delivery: this.deliveryInfo,
      restaurantData: restaurantInfo
    });
  }
  searchVisible() {
    this.searchBoxVisible = true;
    this.btnsVisible = false;
  }
  btnVisible() {
    this.searchBoxVisible = false;
    this.btnsVisible = true;
  }

  favToggle() {
    this.visible = !this.visible;
  }

  onClickInfo() {
    this.showAlert(
      "You can reach us by contacting us on " + this.contactNumber
    );
  }

  // Go To chat with restaurant
  gotoChat() {
    if (localStorage.getItem("token") != null) {
      const loader = this.loadingCtrl.create({
        content: "Please wait..."
      });
      loader.present();
      this.restService.checkValidToken().subscribe(
        (res: any) => {
          if (res) {
            this.navCtrl.push("ChatPage", {
              managerId: this.contactPerson,
              restId: this.restaurantId
            });
          }
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          this.showAlert(
            "You are not authorized. Please login again to send messages"
          );
        }
      );
    } else {
      this.showAlert("Please login first to start chat!");
    }
  }

  // show alert message
  showAlert(message) {
    let alert = this.alertCtrl.create({
      title: "Message",
      subTitle: message,
      buttons: ["Ok"]
    });
    alert.present();
  }

  // Event fire and listen in appcomponent
  goToCart() {
    this.events.subscribe("restInfo", (res: any) => {
      this.deliveryInfo = res.delivery;
      this.restaurantDetail = res.restaurant;
    });
    if (localStorage.getItem("cartItem") != null) {
      this.navCtrl.push("CartPage", {
        delivery: this.deliveryInfo,
        restaurant: this.restaurantDetail
      });
    } else {
      this.navCtrl.push("CartPage");
    }
  }
}
