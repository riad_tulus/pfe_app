import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstantService } from "../../app/constant.service";

@Injectable()
export class RestaurantDetailService {
  constructor(
    public http: HttpClient,
    public constantService: ConstantService
  ) {}

  getProductListByRestId(id: any) {
    const headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    // return this.http.get(
    //   this.constantService.API_ENDPOINT + "products/location/" + id,
    //   {
    //     headers: headers
    //   }
    // );
    return this.http.get(
      this.constantService.API_ENDPOINT + "locations/all/category/data/" + id,
      {
        headers: headers
      }
    );
  }

  getlocationById(id: any) {
    //let authToken = localStorage.getItem("token");

    const headers = new HttpHeaders().set("Content-Type", "application/json");
    //  .set("Authorization", authToken);
    return this.http.get(
      this.constantService.API_ENDPOINT + "locations/" + id,
      {
        headers: headers
      }
    );
  }

  checkValidToken() {
    let authToken = localStorage.getItem("token");
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", authToken);
    return this.http.get(
      this.constantService.API_ENDPOINT + "users/verify/token",
      {
        headers: headers
      }
    );
  }
}
