import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  IonicPage,
  LoadingController,
  AlertController
} from "ionic-angular";
import { HomeService } from "./home.service";

@IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html",
  providers: [HomeService]
})
export class HomePage {
  event: string;
  searchBoxVisible = false;
  btnsVisible = true;
  public visible = false;
  public restaurantListByPosition: any[] = [];
  public restaurantListByRating: any[] = [];
  public restaurantListAsNewlyArrived: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public homeService: HomeService,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
  ) {
    this.getRestaurantAsNewlyArrived();
    this.getRestaurantsByRating();
  }

  ionViewWillLoad() {
    if (localStorage.getItem("position") != null) {
      const prevPosition = JSON.parse(localStorage.getItem("position"));
      const index = prevPosition.length;
      const singlePosition = prevPosition[index - 1];
      this.getRestaurantsByPosition(singlePosition.lat, singlePosition.long);
    } else {
      this.getRestaurantsByPosition("12.917096599999999", "77.58980509999999"); //static location will be used in  case when dynamic location is not available
    }
  }

  // Used for get restaurant list by latitude and longitude
  private getRestaurantsByPosition(lat, lng) {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.homeService.getRestaurantsByPosition(lat, lng).subscribe(
      (res: any) => {
        if (res.message) {
          loader.dismiss();
        } else {
          this.restaurantListByPosition = res;
          loader.dismiss();
        }
      },
      error => {
        loader.dismiss();
      }
    );
  }

  // Used for get restaurant list by Rating
  private getRestaurantsByRating() {
    this.homeService.getRestaurantsByRating().subscribe(
      (res: any) => {
        if (res.message) {
        } else {
          this.restaurantListByRating = res;
        }
      },
      error => { }
    );
  }

  // Used for get restaurant list newly arrived
  private getRestaurantAsNewlyArrived() {
    this.homeService.getRestaurantAsNewlyArrived().subscribe(
      (res: any) => {
        if (res.message) {
        } else {
          this.restaurantListAsNewlyArrived = res;
        }
      },
      error => { }
    );
  }

  /*Used for selecting restaurant and move to next page RestaurantDetailsPage if resturant already selected 
  than changed by removing all cart item 
  */
  onSelectRestaurant(restaurantID: any) {
    if (localStorage.getItem("cartItem") == null) {
      localStorage.setItem("rid", restaurantID);
      this.navCtrl.push("RestaurantDetailsPage", {
        id: restaurantID
      });
    } else {
      const tempRid = localStorage.getItem("rid");
      if (tempRid == restaurantID) {
        this.navCtrl.push("RestaurantDetailsPage", {
          id: restaurantID
        });
      } else {
        let alert = this.alertCtrl.create({
          title: "Are you sure?",
          message: "To change restaurant you have to clear your cart!",
          buttons: [
            {
              text: "Nope",
              role: "cancel",
              handler: () => {
                //handle if dont want to clear cart
              }
            },
            {
              text: "Sure",
              handler: () => {
                localStorage.removeItem("cartItem");
                localStorage.setItem("rid", restaurantID);
                this.navCtrl.push("RestaurantDetailsPage", {
                  id: restaurantID
                });
              }
            }
          ]
        });
        alert.present();
      }
    }
  }

  searchVisible() {
    this.searchBoxVisible = true;
    this.btnsVisible = false;
  }
  btnVisible() {
    this.searchBoxVisible = false;
    this.btnsVisible = true;
  }
  toggle() {
    this.visible = !this.visible;
  }

  goToLocationChangerPage() {
    this.navCtrl.push("LocationChangerPage");
  }

  goToAllRestaurantPage(pageValue) {
    this.navCtrl.push("AllRestaurantPage", { pageValue: pageValue });
  }

  //used in slider in homepage
  offers = [
    {
      img: "assets/imgs/bg1.jpg",
      title: "Buy 1 & Get 1 Free",
      describe: "Veg Paneer Roll at just Rs.25",
      tagline: "Tap here to see Offer"
    },
    {
      img: "assets/imgs/bg6.jpg",
      title: "Kitchen Offers",
      describe: "Veg Paneer Roll at just Rs.25",
      tagline: "Order now...! Hurry up"
    },
    {
      img: "assets/imgs/bg.jpg",
      title: "Book a Table with 50% Off",
      describe: "Veg Paneer Roll at just Rs.25",
      tagline: "Grab it...!!!"
    }
  ];
}
