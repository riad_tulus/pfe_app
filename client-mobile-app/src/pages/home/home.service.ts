import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ConstantService } from "../../app/constant.service";

@Injectable()
export class HomeService {
  constructor(
    public http: HttpClient,
    public constantService: ConstantService
  ) {}

  //API call to get all Restaurant list based on latitude and longitude
  getRestaurantsByPosition(latitude?, longitude?) {
    const body = {
      latitude: latitude,
      longitude: longitude,
      page: 1
    };

    const headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    return this.http.post(
      this.constantService.API_ENDPOINT + "locations/map/distance",
      body,
      {
        headers: headers
      }
    );
  }

  //API call to get all Restaurant list based on Ratings
  getRestaurantsByRating() {
    const headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    return this.http.get(
      this.constantService.API_ENDPOINT + "locations/toprated/restaurant/list",
      {
        headers: headers
      }
    );
  }

  //API call to get all Restaurant list, newly arrived
  getRestaurantAsNewlyArrived() {
    const headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    return this.http.get(
      this.constantService.API_ENDPOINT +
        "locations/newlyadded/restaurant/list",
      {
        headers: headers
      }
    );
  }
}
