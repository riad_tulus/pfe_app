'use strict';
 //enableAllCategoryList
var express = require('express');
var controller = require('./category.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Gets a list of Categories of a single restaurant
router.get('/restaurant/:id',auth.isAuthenticated(), controller.index);

//menuItem GRAPH
router.get('/menuitem/',auth.isAuthenticated(), controller.noOfMenuItem);

//Get Category by location Id
router.get('/location/:id', controller.byLocation);

//Get Category by category id
router.get('/:id',auth.isAuthenticated(),controller.show);

// Creates a new Category in the DB
router.post('/', auth.isAuthenticated(),controller.create);

// Update a Category
router.put('/:id', auth.isAuthenticated(),controller.upsert);

// Delete a Category
router.delete('/:id', auth.isAuthenticated(),controller.destroy);

//Custom search by category name
router.post('/by/name',auth.isAuthenticated(),controller.customCategory);

//Front-end
//Get all Categories by location id
router.get('/all/:id', controller.byLocAllCat);

//all category list that is enabled by location id
router.get('/all/enable/categorylist/bylocation/:id', controller.enableAllCategoryList);

//all category list that is enabled by restaurant id
router.get('/all/enable/categorylist/byrestaurant/:id', controller.allCategoryListByrestaurant);

module.exports = router;
