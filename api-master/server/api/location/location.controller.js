/**show topRatedRestaurantlist mapDistance allDataMapDistance allTopRatedRestaurantlist
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/locations     tax         ->  index locCatAndSubCat
 * POST    /api/locations              ->  create allDataMapDistance
 * GET     /api/locations/:id          ->  show categoryData restaurantlistOncreatedOrder
 * PUT     /api/locations/:id          ->  upsert locCategoryData
 allLocationRestaurant mapDistance restaurantlistOncreatedOrder
 
 */

'use strict'; 

import jsonpatch from 'fast-json-patch';
import Location from './location.model';
import CuisineLocation from '../cuisineLocation/cuisineLocation.model';
import User from '../user/user.model';
import Category from '../category/category.model';
import Product from '../product/product.model';
import Subcategory from '../subcategory/subcategory.model';
var  multiparty = require('multiparty');
var path = require("path");
var cloudinary = require('cloudinary');
let geoLib = require('geo-lib');

var async = require("async");
var crypto = require("crypto");
//Cloudinary Image Upload Config
cloudinary.config({
  cloud_name: 'dgcs',
  api_key: '682777964494331',
  api_secret: 'VNzzLF7r6WQMq2vPfkUNVUz_K2A'
});
function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
      return res.status(statusCode).json(entity);
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}



// Gets a single Location from the DB 
export function show(req, res) {
  return Location.findById(req.params.id,{'__v':false}).populate('contactPerson','name').populate('restaurantID','restaurantName name logo role').exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

//get all restaurant list
export function listOfAllrestaurant(req, res) {
  return Location.find({},'contactPerson locationName').populate('restaurantID','restaurantName name logo role').exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

//get all location based on cuisine
// export function getLocationByCuisine(req, res) {
//   return Location.find({'cuisine':req.params.cuisineName},{}).exec()
//     .then(handleEntityNotFound(res))
//     .then(respondWithResult(res))
//     .catch(handleError(res));
// }


// export function getByFilter(req, res) {
// //   // console.log('hieee');
// // //  var response = {}
// // //  for (var key in req.body) {
// // //    if (typeof(req.body) != 'function') {
// // //      if(key == 'cuisineName' ){
// // //        response['cuisine'] ={ "$in":req.body.cuisineName};
// // //      }
// // //     }
// // //   }
// // // var query = response;
// console.log('response........'+JSON.stringify(req.body));
//  Location.find({'cuisine':{"$in":req.body.cuisineId}},{}).exec(function (err, location) {
//    if (err) {
//      return handleError(res, err);
//    }
//    if (location.length==0) {
//      return res.status(200).send({message:'No location found matching this criteria.'});
//    }
//    return res.json(location);
//  });
// }


export function getlistOfAllrestaurant(req, res) {
  return Location.find().populate('restaurantID').exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

//display all locations depends on restaurantType
export function LocationByRestoType(req, res) {
  return Location.find({'restaurantType':req.body.restaurantType},{}).populate('restaurantID').exec(function(err,location){
    if(err){
      return handleError(res);
    }
    if(!location){
      res.json([]);
    }
    res.json(location);
  })
}

// Creates a new Location in the DB
export function create(req, res) {
  console.log("location"+JSON.stringify(req.body))
  let location = new Location(req.body);
  return Location.create(location)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
} 


// Get all location of a restaurant all LocationRestaurant
export function allLocationRestaurant(req, res) {
  console.log("hitting........"+req.params.id);
  return Location.find({'restaurantID':req.params.id},{}).populate('contactPerson','name').exec()
  .then(respondWithResult(res))
  .catch(handleError(res));
}


//all retauren count by restaurent id
export function allLocationRestaurantCount(req, res) {
  return Location.find({'restaurantID':req.params.id},{}).count().exec()
  .then(respondWithResult(res))
  .catch(handleError(res));
}

// Upserts the given Location in the DB at the specified ID
export function upsert(req, res) {
  console.log("dfghjklfghj"+JSON.stringify(req.body))
  if(req.body._id) {
    delete req.body._id;
  }
 // if(req.body.cuisinedata!=null){
    console.log('inif')
  // let category = new Category(req.body);
  console.log(JSON.stringify(req.body.cuisinedata))
  //slet cuisinelocation=new CuisineLocation(req.body.cuisinedata);
  // cuisinelocation.cuisineId=req.body.cuisineId;
  // cuisinelocation.location=req.body.locationId;
  CuisineLocation.create(req.body.cuisinedata).then(() => {console.log('finished populating Coupan')});
return Location.findOneAndUpdate({_id: req.params.id}, req.body,
  {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()
  .then(respondWithResult(res))
  .catch(handleError(res));
    
//     if(req.body.cuisinedata.cuisineId==undefined){
//       console.log('secondif')
//     return Location.findOneAndUpdate({_id: req.params.id}, req.body,
//   {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()
//   .then(respondWithResult(res))
//   .catch(handleError(res)); 
// }
}


//updating delivery info
export function deliveryInfo(req,res) {
  var locationId=req.body.id;
  //finding location data
  Location.findById(locationId).exec(function(err,deliverydata){
    if(err){
      //error occoured
      return handleError(res)
    }
    if(!deliverydata){
      //when there is no delivery data by locationId
      return res.status(404).send({message:'data not found'})
    }
    deliverydata.deliveryInfo= req.body;
    //save delivery data
    deliverydata.save(function(err,done){
      if(err){
        //handle error
        return handleError(res);
      }
      else{
        //sending response
        res.json(deliverydata);
      }
    })
  })
}

// Deletes a Location from the DB
export function destroy(req, res) {
  let locationId = req.params.id;
  //Getting categories of a location
  Category.find({'location':locationId},{}).exec(function(err,category){
    if(err){
     return handleError(res);
    }     
    //If category exist can't delete location                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
    if(category.length>0){
      res.status(200).send({
        message:'There are Some Categories related to this Restaurant!'
      })
    }
    if(category.length==0){
      //find public Id
      Location.findById(locationId, function (err, location) {
        let imagePath = location.publicId;
        // Delete Image
        cloudinary.uploader.destroy(imagePath, function(result) {
           return Location.findById(req.params.id).exec()
           .then(handleEntityNotFound(res))
           .then(removeEntity(res))
           .catch(handleError(res));
        });
      });
    }
  });
}

//Front-End Api--

// Gets a list of Locations by user lat&log
export function mapDistance(req, res) {
  let user_lat = req.body.latitude;
  let user_lan = req.body.longitude;
  //All location data
  Location.find().lean().populate('restaurantID','restaurantName logo publicId').exec(function(err,locations){
    //error handling
    if(err){
      return handleError(res);
    }
    else{
      let arr_Data = [];
      let i;
      let len =locations.length;
      //traversing all location one by one 
      for(i=0;i<len;i++)
      { 
        //Getting distance between two lat&long
        let result = geoLib.distance({
            p1: { lat: user_lat, lon: user_lan },
            p2: { lat: locations[i].latitude, lon:locations[i].longitude }
        });
        let obj={
          restaurantName          : locations[i].restaurantID.restaurantName,
          restaurantLogo          : locations[i].restaurantID.logo,
          restaurantPublicId      : locations[i].restaurantID.publicId,
          hasDelivery             : locations[i].hasDelivery,
          restaurantID            : locations[i]._id,
          distance                : result.distance,
          address                 : locations[i].address,
          city                    : locations[i].city,
          contactNumber           : locations[i].contactNumber,
          rating                  : locations[i].rating
        }
        arr_Data.push(obj);
      }
      if(arr_Data.length === locations.length){
        //Sorting all data on the basis of distance
        arr_Data.sort(function(a, b) {
          return a.distance - b.distance;
        });
        let final_data = [];
        let page = req.body.page-1;
        //Here,static value 10 is one page data(10 data)
        let lower_limit = page *4;
        let upper_limit = lower_limit + 4;
        let flag=0;
        if(lower_limit>arr_Data.length ||page<0 ){
          flag=1;
          let array_null = [];
          res.json(array_null);
        }
        if(upper_limit>arr_Data.length){
          upper_limit = arr_Data.length;
        }
        for(let i=lower_limit;i<upper_limit;i++){
          final_data.push(arr_Data[i]);
        }
        if(flag ==0)
        {
        res.json(final_data);
        }
      }
    }
  })
}

// //mapping all location data basis on distance
export function allDataMapDistance(req, res) {
  let user_lat = req.body.latitude;
  let user_lan = req.body.longitude;
  //All location data
  Location.find().lean().populate('restaurantID','restaurantName logo publicId').exec(function(err,locations){
    //error handling
    if(err){
      return handleError(res);
    }
    else{
      let arr_Data = [];
      let i;
      let len =locations.length;
      //traversing all location one by one 
      for(i=0;i<len;i++)
      { 
        //Getting distance between two lat&long
        let result = geoLib.distance({
            p1: { lat: user_lat, lon: user_lan },
            p2: { lat: locations[i].latitude, lon:locations[i].longitude }
        });
        let obj={
          _id:     locations[i]._id,
          restaurantID :{
            _id           : locations[i].restaurantID._id,
            restaurantName: locations[i].restaurantID.restaurantName,
            logo          : locations[i].restaurantID.logo,
            publicId      : locations[i].restaurantID.publicId
          },
          contactNumber   :locations[i].contactNumber,
          locationName    :locations[i].locationName,
          deliveryInfo    :locations[i].deliveryInfo,
          rating          :locations[i].rating
        }
        arr_Data.push(obj);
      }
      if(arr_Data.length === locations.length){
        //Sorting all data on the basis of distance
        arr_Data.sort(function(a, b) {
          return a.distance - b.distance;
        });
        res.send(arr_Data)
      }
    }
  })
}

//Get Location Info as well as all categorories and sub-categories locCatAndSubCat
export function locCatAndSubCat(req,res){
  let locId = req.params.id;
  //Getting location info
  Location.findById(locId).populate('restaurantID','restaurantName logo publicId').exec(function(err,locdata){
    if(err){
      return handleError(res);
    }
    else{
      //Getting category data
      Category.find({'location':locId},{}).lean().exec(function(err,catdata){
        if(err){
          return handleError(res);
        }
        else{
          //Getting sub categories data
          Subcategory.find({'location':locId},{}).lean().exec(function(err,subdata){
            if(err){
              return handleError(res);
            }
            else{
              let result = {};
              result = {
                locationdata:locdata,
                categorydata:catdata,
                subcategorydata:subdata
              }
              res.json(result);
            }
          })
        }
      })
    }
  })
}

//get all categories with their products
export function categoryData(req,res){
  var obj_data={};
  var arr_data=[];
  // finding location data
  User.findOne({'location':req.params.id},{}).populate('restaurantID').exec(function(err,restaurant){
    if(err){
      //error handling
      return handleError(res)
    }
    if(!restaurant){
      //when there is no such location with this id
      res.status().send({message:'there is no such location with this id.'})
    }
    //get all categories of a location
    Product.find({'location':req.params.id},{}).distinct('category').exec(function(err,categoryData){
    if(err){
      //error handling
      return handleError(res)
    }
    if(categoryData.length==0){
      //while there is no products in a location
      res.status(200).send({message:'There is no products in a location.'})
    }
    //iterating category ids
    for(let i=0;i<categoryData.length;i++){
      //find all products of a category
      Product.find({'category':categoryData[i]},{}).exec(function(err,productData){
        if(err){
          // handle error
          return handleError(res)
        }
        obj_data={
          categoryTitle:productData[0].categoryTitle,
          product:productData
        }
        //push all category in an array
        arr_data.push(obj_data)
        if(arr_data.length==categoryData.length){
          //while all products and category title pushed 
          //into array
          //response object
          let obj2={
            restaurant:restaurant.restaurantID,
            categorydata:arr_data
          }
          //send response
          res.send(obj2)
        }
      })
    }
    })
  })
}


//top rated rastaurant list only 10
export function topRatedRestaurantlist(req,res){
  //getting all location
  Location.find({},'address city rating').populate('restaurantID','restaurantName logo publicId').sort({rating: -1}).limit(4).exec(function(err,restaurantlist){
    if(err){
      //error occoured
      return handleError(res)
    }
    if(restaurantlist.length==0){
      //while there is no location
      res.status(200).send({message:'restaurant not found'})
    }
    else{
      let arr_Data = [];
      //iterating all location or restaurant
      for(let i=0;i<restaurantlist.length;i++){
        //creating raw object
      let obj={
          restaurantName          : restaurantlist[i].restaurantID.restaurantName,
          restaurantLogo          : restaurantlist[i].restaurantID.logo,
          restaurantPublicId      : restaurantlist[i].restaurantID.publicId,
          hasDelivery             : restaurantlist[i].hasDelivery,
          restaurantID            : restaurantlist[i]._id,
          address                 :restaurantlist[i].address,
          city                    :restaurantlist[i].city,
          contactNumber           :restaurantlist[i].contactNumber,
          rating                  :restaurantlist[i].rating
        }
        //pushing objects in an array
        arr_Data.push(obj);
      }
      if(restaurantlist.length==arr_Data.length){
        //when all done,
        //send response
        res.send(arr_Data)
      }
    }
  })
}


//all top rated rastaurant list all 
export function allTopRatedRestaurantlist(req,res){
  //sorting all locations on basis of their ratings
  Location.find({},'_id contactPerson locationName deliveryInfo rating').populate('restaurantID','restaurantName logo publicId').sort({rating: -1}).exec(function(err,restaurantlist){
    if(err){
      //while error occoured
      return handleError(res)
    }
    if(restaurantlist.length==0){
      //when there is no restaurant
      res.status(200).send({message:'restaurant not found'})
    }
    else{
      //while got restaurant list
      //sending them as response
      res.send(restaurantlist)
    }
  })
}

//**************only 10 restaurant list on newly added basis
export function restaurantlistOncreatedOrder(req,res){
  //getting all locations
  Location.find({},'address city rating').populate('restaurantID','restaurantName logo publicId').sort({createdAt: -1}).limit(4).exec(function(err,restaurantlist){
    if(err){
      return handleError(res)
    }
    if(restaurantlist.length==0){
      //while there is no restaurant
      res.status(200).send({message:'restaurant not found'})
    }
    
    else{
      console.log('asdfghjk'+JSON.stringify(restaurantlist))
      let arr_Data = [];
      //iterating all restaurant
      for(let i=0;i<restaurantlist.length;i++){
        //creating raw object
        let obj={
          restaurantName          : restaurantlist[i].restaurantID.restaurantName,
          restaurantLogo          : restaurantlist[i].restaurantID.logo,
          restaurantPublicId      : restaurantlist[i].restaurantID.publicId,
          hasDelivery             : restaurantlist[i].hasDelivery,
          restaurantID            : restaurantlist[i]._id,
          address                 :restaurantlist[i].address,
          city                    :restaurantlist[i].city,
          contactNumber           :restaurantlist[i].contactNumber,
          rating                  :restaurantlist[i].rating
        }
        arr_Data.push(obj);
      }
      //when done
      if(restaurantlist.length==arr_Data.length){
        //send response
        res.send(arr_Data)
      }
    }
  })
}

//**************get restaurant list basis of newly added
export function allRestaurantlistOncreatedOrder(req,res){
  //applying query critetia
  Location.find({},'_id contactPerson locationName deliveryInfo rating').populate('restaurantID','restaurantName logo publicId').sort({createdAt: -1}).exec(function(err,restaurantlist){
    if(err){
      //handling error
      return handleError(res)
    }
    if(restaurantlist.length==0){
      //when no result found
      res.status(200).send({message:'restaurant not found'})
    }
    else{
      //sending response
      res.send(restaurantlist)
    }
  })
}
