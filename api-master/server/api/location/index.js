'use strict';

var express = require('express');
var controller = require('./location.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Gets a single Location from the DB
router.get('/:id',controller.show);

// get all locations few details
router.get('/all/restaurant/list',controller.listOfAllrestaurant);

//get all locations all details
router.get('/get/all/restaurant/list',controller.getlistOfAllrestaurant);

//get locations based on restaurantType
router.post('/by/restaurant/type',controller.LocationByRestoType);

// router.post('/by/filter',controller.getByFilter);

// get all categories with their products
router.get('/all/category/data/:id', controller.categoryData);

//Get all categories and Subcategories of a location 
//as well as location info
router.get('/cat/subcat/:id', controller.locCatAndSubCat);

// Get all location of a restaurant
router.get('/all/data/:id',controller.allLocationRestaurant);

//location count by restaurent id
router.get('/all/countdata/:id',controller.allLocationRestaurantCount);

//Create a location
router.post('/', controller.create);

//Update a location
router.put('/:id',controller.upsert);

//Delete a Location
router.delete('/:id', auth.isAuthenticated(),controller.destroy);

//*****************list of restaurant*************************************************

//Map Location restaurant as per user lat&long
router.post('/map/distance',controller.mapDistance);

// mapping all location data basis on distance
router.post('/all/map/distance',controller.allDataMapDistance);

// 10 top Rated Restaurantlist 
router.get('/toprated/restaurant/list',controller.topRatedRestaurantlist);

// all top Rated Restaurantlist all 
router.get('/toprated/all/restaurant/list',controller.allTopRatedRestaurantlist);

//**10 restaurant list on newly added  basis
router.get('/newlyadded/restaurant/list',controller.restaurantlistOncreatedOrder);

//**all restaurant list on newly added  basis
router.get('/newlyadded/all/restaurant/list',controller.allRestaurantlistOncreatedOrder);

//******************************************************************************************

//add deliveryInfo with pincode
router.post('/add/deliveryinfo/data/',controller.deliveryInfo);

module.exports = router;
