'use strict'; 

var express = require('express');
var controller = require('./setting.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

// Gets a list of Settings
router.get('/', auth.hasRole('Owner'), controller.index);

// Gets a single Setting from the DB based on restaurent iD
router.get('/:id', controller.show);

// Creates a new Setting in the DB
router.post('/', auth.hasRole('Owner'), controller.create);

// Upserts the given Setting in the DB at the specified ID
router.put('/:id', auth.hasRole('Owner'), controller.upsert);

// Deletes a Setting from the DB
router.delete('/:id', auth.hasRole('Owner'), controller.destroy);

// Gets a single Setting from the DB based on location
router.get('/loyalty/:id', controller.loyalty);

module.exports = router;
