'use strict';

var express = require('express');
var controller = require('./coupan.controller');

var router = express.Router();

// get all coupans
router.get('/', controller.index);

// get single coupan
router.get('/single/:id', controller.singleCoupan);

// get all coupans of a location
router.get('/:id', controller.show);

// creates coupan
router.post('/', controller.create);

// update coupan
router.put('/:id', controller.upsert);

// delete coupan
router.delete('/:id', controller.destroy);

// get  valid coupan for user applcable or not
router.get('/validcoupon/bycurrenttimestamp/:id', controller.vaildCoupanData);
//router.post('/timestamp/', controller.create1);
module.exports = router;
