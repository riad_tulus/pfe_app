'use strict';

var express = require('express');
var controller = require('./product.controller');
var auth 	   = require('../../auth/auth.service');
var router = express.Router();

// Get a list of products by restaurant id
router.get('/restaurant', auth.isAuthenticated(),controller.index);

// get a product list by serched by product name
router.post('/productname/', auth.isAuthenticated(),controller.productSerchByName);

//get a list of products of a restaurant
router.get('/restaurant/:id', auth.isAuthenticated(),controller.index);

//menuItem graph data 
router.get('/menuitem/', controller.noOfMenuItem);



//Get a list of products by location id
router.get('/location/:id',controller.byLocation);

//Get a list of products by category id
router.get('/category/:id',controller.byCategory);

//Get a list of products by subcategory id
router.get('/subcategory/:id', controller.bySubcategory);

//Get a single Product  
router.get('/:id',controller.show);

//get data by filter
router.post('/data/:id', auth.isAuthenticated(),controller.productData);

//Create Product
router.post('/', auth.isAuthenticated(),controller.create);

//Update Product
router.put('/:id', auth.isAuthenticated(),controller.upsert);

//Delete a single product
router.delete('/:id', auth.isAuthenticated(),controller.destroy);

//Get a list of product by Sub-Category 
router.get('/subcat/:id',controller.bySubCat);

module.exports = router;
