/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/products/restaurant/:id     ->  index 
 * POST    /api/products                    ->  create
 * GET     /api/products/:id                ->  show
 * PUT     /api/products/:id/:flag          ->  upsert
 * DELETE  /api/products/:id                ->  destroy
 */

'use strict'; 
 
import jsonpatch from 'fast-json-patch';
import Product from './product.model';
import Order from '../order/order.model';
import User from '../user/user.model';
import Location from '../location/location.model';

var multiparty = require('multiparty');
var path       = require("path");
var cloudinary = require('cloudinary');

//Cloudinary Image Upload Config
cloudinary.config({
  cloud_name: 'dgcs',
  api_key: '682777964494331',
  api_secret: 'VNzzLF7r6WQMq2vPfkUNVUz_K2A'
});
function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Products by restaurant Id
export function index(req, res) {
  //getting a list of products by restaurant id
  Product.find({'restaurantID':req.params.id}).populate('restaurantID location category','name locationName categoryName').exec(function(err,product){
    if(err){
      //error occoured
      return handleError(res);
    }
    if(product.length==0){
      return res.status(200).send({message:"no product found"})
    }
    else{
      //wrapping for response
      let resobj={
        productdata:product,
        productcount:product.length
      }
      //sending response
      res.json(resobj);
    }
  })
}

export function productSerchByName(req, res) {
  //searching products by their name using regular expression
  return Product.find({title: { $regex:req.body.title,$options: 'i' },'restaurantID':req.body.id}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}
// Gets a list of Products by location Id byLocation
export function byLocation(req, res) {
  //getting a list of products of a location
  Product.find({'location':req.params.id},{}).populate('location','locationName').exec(function(err,product){
    if(err){
      //error occoured
      return handleError(res);
    }
    if(!product || product== undefined || product== null || product.length ==0){
      //while no products found
      return res.send({message:'No product in this location'})
    }
    else{
      var id=product[0].restaurantID
      //getting restaurant info by their if
      User.findById(id,'-password -salt').exec(function(err,restaurantdata){
        if(err){
          //error occoured
          return handleError(res);
        }
        if(!restaurantdata){
          //when no data found
          return res.send({message:'No data'})
        }
        else{
          //getting location info by location id
          Location.findById(req.params.id).exec(function(err,deliverydata){
            if(err){
              //error occoured
              return handleError(res);
            }
            if(!deliverydata){
              //while no location found
              return res.status(404).send({message:"data not found"})
            }
            //framing data in object
            let resobj={
              restaurant:restaurantdata,
              productdata:product,
              location:product[0].location,
              deliveryInfo:deliverydata.deliveryInfo,
              productDataLength:product.length
            }
            //sending response
            res.json(resobj);
          })
        }
      })
    }
  })
}
//***************

// Gets a list of Products by category Id
export function byCategory(req, res) {
  return Product.find({'category':req.params.id}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Products by Subcategory Id
export function bySubcategory(req, res) {
  return Product.find({'subcategory':req.params.id}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Product from the DB
export function show(req, res) {
  return Product.findById(req.params.id,{'__v':false}).populate('location category','categoryName locationName').exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Product in the DB
export function create(req, res) {
  //creating product instance
  let product = new Product(req.body);
  //saving product instance
  product.save(function(err){
    if(err){
      //error occoured
      return handleError(res);
    }
    else{
      //sending response
      res.json(product);
    }
  })
}

// Upserts the given Product in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  //if only field data has to update
  //flag 0 by body
   if(req.body.flag == 0)
  {
    return Product.findOneAndUpdate({_id: req.params.id}, req.body,
      {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
  }
  //if file data also has to update
  //flag 1 by body
  else
  {
    let id = req.params.id;
    let count = 0;
    let compareValue = 0;
    //find a product by product id
    Product.findById(id, function (err, product) {
      let publicId ;
      if(req.body.deletePublicId != undefined)
      {
        publicId =  product.publicId;
        count++;
      }
      //Destroying cloudinary image
      if(publicId != undefined)
      {
        cloudinary.uploader.destroy(publicId, function(result) {
        });
        compareValue++;
        complete();
      }
      function complete() {
        if(compareValue === count) {
          //updating field data
          return Product.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()
          .then(respondWithResult(res))
          .catch(handleError(res));
        }
      }
    });
  }
}

export function productData(req, res) {
  //params id when 0

  if(req.params.id == 0)
  {
    let query = {};
    query['$and']=[];
    //if filter with categoryId
    if(req.body.categoryId!=undefined)
    {
      query['$and'].push({category:{$in:req.body.categoryId}});
    }
    //if filter with subCategoryId
    if(req.body.subCategoryId!=undefined)
    {
      query['$and'].push({subcategory:{$in:req.body.subCategoryId}});
    }
    //if filter with title
    if(req.body.title!=undefined)
    {
      query['$and'].push({title:{'$regex': req.body.title,"$options": "i"}});
    }
    //getting products data as per  query
    Product.find(query).exec(function(err,products){
      if(err){
        //error occoured
        return handleError(res);
      }
      else{
        //sending response
        res.json(products);
      }
    })
  }
  //only title field needed(with id )
  //case-insensitive 
  //else part when only title from client side
  //params id except 0.
  else{
    Product.find( { title: { $regex:req.body.title,$options: 'i' } } ).exec(function(err,data){
      if(err){
      //error occoured
       return handleError(res);
      }
      else{
        //sending response
        res.json(data);
      }
    })
  }
}


// Deletes a Product from the DB
export function destroy(req, res) {
  //getting product id 
  let productId = req.params.id;
  //getting order data of a product
  Order.find({'Product':productId},{}).exec(function(err,order){
    if(err){
      //error occoured
      return handleError(res);
    }
    if(order.length>0){
      //while got some orders of a productId
      res.status(200).send({
        message:'There are Some Orders related to this Product!'
      })
    }
    if(order.length==0){
      //while no order found
      Product.findById(productId, function (err, product) {
        let imagePath = product.publicId;
        // Delete Image
        cloudinary.uploader.destroy(imagePath, function(result) {
           return Product.findById(req.params.id).exec()
           .then(handleEntityNotFound(res))
           .then(removeEntity(res))
           .catch(handleError(res));
        })
      })
    }
    else{
      //deleting product
      return Product.findById(productId).exec()
      .then(handleEntityNotFound(res))
      .then(removeEntity(res))
      .catch(handleError(res));
    }
  })
}
//**************menuitem graph*********
//getting all categories's menuItem count
export function noOfMenuItem(req, res) {
 var i; 
 var rawData = [];
 var categoryName = [];
 var length;
 var rawDataInObj = {};
 var rawDataInObjInArray = [];
 var result = {};
 //grouping all categories basis on having products count
  Product.aggregate([
    { $group : {
     _id :'$categoryTitle',
     data: { $sum: 1 },
    }},
  ]).
  exec(function (err, menuItem) {
    if (err) {
      //error occoured
      return handleError(res, err);
    }
    length = menuItem.length;
    //iterating categories
    for(i=0;i<length;i++)
    {
      categoryName.push(menuItem[i]._id);
      rawData.push(menuItem[i].data);
    }
    rawDataInObj = {
      data:rawData
    }
    rawDataInObjInArray.push(rawDataInObj);
    //framing into object
    result = {
      labels:categoryName,
      datasets:rawDataInObjInArray
    }
    //sending response
    res.json(result);
  })
}

// Gets a list of Products by SubCategory Id
export function bySubCat(req, res) {
  return Product.find({'subcategory':req.params.id,'enable':1}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}