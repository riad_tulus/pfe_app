'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './product.events';
import {Schema} from 'mongoose';
var ProductSchema = new mongoose.Schema({
  restaurantID:{
    type:Schema.ObjectId,
    ref:'User'
  },
  location:{
    type:Schema.ObjectId,
    ref:'Location'
  },
  category:{
  	type:Schema.ObjectId,
  	ref:'Category'
  },
  //new field added post data 
  categoryTitle:{
    type:String
  },
  subcategory:{
  	type:Schema.ObjectId,
  	ref:'Subcategory'
  },
  //added to calculate average rating 
  rating:{
    type:Number,
    default:0
  },
  ratingCount:{
    type:Number,
    default:0
  },
  title:{
  	type:String
  },
  brand:{
  	type:String
  },
  description:{
  	type:String
  },
  tags:[{
    id:{
      type:String
    },
    text:{
      type:String
    }
  }],
  imageUrl:{
  	type:String
  },
  publicId:{
  	type:String
  },
  enable:{
  	type:Boolean,
    default:1 
  },
  extraIngredients:[{

  }],
  //variants array has been updated
  variants:[{
    size:{
      type:String
    },
    MRP:{
      type:Number
    },
    Discount:{
      type:Number
    },
    price:{
      type:Number
    }
  }],
  createdAt:{
  	type:Date,
  	default:Date.now
  },
  updatedAt:{
  	type:Date
  }
},{
  usePushEach: true
});

registerEvents(ProductSchema);
export default mongoose.model('Product', ProductSchema);
