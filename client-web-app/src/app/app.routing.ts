import {Routes, RouterModule} from'@angular/router';

const APP_ROUTES: Routes = [

  { path: '', loadChildren: 'app/pages/pages.module#PagesModule' },
];
export const Routing = RouterModule.forRoot(APP_ROUTES);
