import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpHandler} from '@angular/common/http';
import {ConstantService} from './constant.service';

@Injectable()
export class CrudService extends ConstantService {
  constructor(private http: HttpClient) {
    super();  // points to Constant Service class
  }

  // get api call
  public get(api: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.API_ENDPOINT + api, {
      headers: headers
    });
  }

  // gets user details
  public getUserDetails(api: string) {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.get(this.API_ENDPOINT + api, {
      headers: headers
    });
  }

  // returns users particular details such as orders,wishlist etc
  public getUserDetailsById(api: string, id: string) {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.get(this.API_ENDPOINT + api + '/' + id, {
      headers: headers
    });
  }

  // getting a data based on id
  public getOne(api: string, id: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.get(this.API_ENDPOINT + api + '/' + id, {
      headers: headers
    });
  }

  // post api call
  public post(api: string, data: any) {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.post(this.API_ENDPOINT + api, data, {
      headers: headers
    });
  }

  // post api call
  public filterRestaurants(api: string, data: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.API_ENDPOINT + api, data, {
      headers: headers
    });
  }

  // put api call
  public put(api: string, id: string, data: any) {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.put(this.API_ENDPOINT + api + '/' + id, data, {
      headers: headers
    });
  }

  // delete api call
  public delete(api: string, id: string) {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.delete(this.API_ENDPOINT + api + '/' + id, {
      headers: headers
    });
  }

}
