import {Injectable} from '@angular/core';
import {CrudService} from '../../services/crud.service';

@Injectable()
export class BannerService {
  constructor(private crud: CrudService) {
  }

  // returns a list of all restaurants
  public getAllRestaurants() {
    return this.crud.get('users/restaurant/list');
  }

  // returns all restaurants
  public getAllLocations() {
    return this.crud.get('locations/get/all/restaurant/list');
  }

  // returns top rated restaurants
  public getTopRatedRestaurants() {
    return this.crud.get('locations/toprated/all/restaurant/list');
  }

  // returns newly added restaurants
  public getNewlyAddedRestaurants() {
    return this.crud.get('locations/newlyadded/all/restaurant/list');
  }

  // returns all cuisines
  public getCuisines() {
    return this.crud.get('cuisines');
  }

}
