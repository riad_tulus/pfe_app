import {Component, ViewChild, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {ToastrService} from 'ngx-toastr';
import {BannerService} from '../banner.service';
import swal from 'sweetalert2';
import * as fromCart from '../../store/cart.reducer';
import * as CartActions from '../../store/cart.action';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {Store} from '@ngrx/store';

const cuisine = ['Gobi Munchurian', 'Baby Corn Manchurian', 'Chinese Noodles', 'Gobi Munchurian', 'Baby Corn Manchurian', 'Chinese Noodles',
  'Gobi Munchurian', 'Baby Corn Manchurian', 'Chinese Noodles', 'Gobi Munchurian', 'Baby Corn Manchurian'
];
const area = [
  'Indiranagar', 'Jayanagar', 'Malleshwaram', 'Chickpet', 'Indiranagar', 'Jayanagar', 'Malleshwaram', 'Chickpet', 'Chickpet',
  'Indiranagar', 'Jayanagar', 'Malleshwaram', 'Chickpet',
  'Indiranagar', 'Jayanagar', 'Malleshwaram', 'Chickpet', 'Indiranagar', 'Jayanagar', 'Malleshwaram', 'Chickpet', 'Chickpet',
  'Indiranagar', 'Jayanagar', 'Malleshwaram', 'Chickpet',
];

const city = [
  'Bangalore', 'Mysore', 'Mandya', 'Kolegal', 'Chennai', 'Bangalore', 'Mysore', 'Mandya', 'Kolegal', 'Chennai',
  'Bangalore', 'Mysore', 'Mandya', 'Kolegal', 'Chennai', 'Bangalore', 'Mysore', 'Mandya', 'Kolegal', 'Chennai',
  'Bangalore', 'Mysore', 'Mandya', 'Kolegal', 'Chennai', 'Bangalore', 'Mysore', 'Mandya', 'Kolegal', 'Chennai',
  'Bangalore', 'Mysore', 'Mandya', 'Kolegal', 'Chennai', 'Bangalore', 'Mysore', 'Mandya', 'Kolegal', 'Chennai',
]


@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css'],
  providers: [BannerService]
})
@Injectable()
export class BannerComponent {
  model: any;
  public restaurants: Array<any>; // contains all the restaurants
  public topRatedRestaurants: Array<any>;   // contains top rated restaurants
  public newlyAddedRestaurants: Array<any>;
  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  private cartObservable: Observable<fromCart.State>;
  private cart: Array<any>;
  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200).distinctUntilChanged()
      .merge(this.focus$)
      .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => (term === '' ? cuisine : cuisine.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10));

  search1 = (text$: Observable<string>) =>
    text$
      .debounceTime(200).distinctUntilChanged()
      .merge(this.focus$)
      .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => (term === '' ? area : area.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10));

  search2 = (text$: Observable<string>) =>
    text$
      .debounceTime(200).distinctUntilChanged()
      .merge(this.focus$)
      .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => (term === '' ? city : city.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10));


  constructor(private restService: BannerService, private toastr: ToastrService, private router: Router, private store: Store<fromCart.CartState>) {
    this.cartObservable = this.store.select('cart');
    this.cartObservable.subscribe(cart => {
      this.cart = cart.cart;
    });
    this.getRestaurants();
    this.getPopularRestaurants();
    this.getNewlyAddedRestaurants();
  }

  // called from constructor, gets a list of restaurants
  private getRestaurants() {
    this.restService.getAllRestaurants().subscribe((res: any) => {
      this.restaurants = res;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // returns popular restaurants
  private getPopularRestaurants() {
    this.restService.getTopRatedRestaurants().subscribe((res: any) => {
      this.topRatedRestaurants = res.slice(0, 4);
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // returns newly added restaurants
  private getNewlyAddedRestaurants() {
    this.restService.getNewlyAddedRestaurants().subscribe((res: any) => {
      this.newlyAddedRestaurants = res.slice(0, 4);
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // navigates to restaurant view page
  public vewRestaurant(id: string) {
    let locationId = localStorage.getItem('locationId');
    // checks whether location was selected or not and performs specifed actions based on location id
    if (locationId === null) {
      localStorage.setItem('locationId', id);
      this.router.navigate(['restaurant-page/' + id]);
    } else {
      if (locationId === id) {
        this.router.navigate(['restaurant-page/' + id]);
      } else {
        if (this.cart.length === 0) {
          localStorage.setItem('locationId', id);
          this.router.navigate(['restaurant-page/' + id]);
        } else {
          swal({
            title: 'Are you sure?',
            text: "The restaurant you have selected is of different location, Your cart will be cleared",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
              this.cart = [];
              this.store.dispatch(new CartActions.AddToCart({cart: this.cart}));
              localStorage.removeItem('cart');
              localStorage.removeItem('taxInfo');
              localStorage.removeItem('subTotal');
              localStorage.removeItem('grandTotal');
              localStorage.removeItem('locationInformation');
              localStorage.removeItem('restaurantDeliveryInfo');
              localStorage.removeItem('address');
              localStorage.removeItem('couponDetails');
              localStorage.removeItem('couponApplied');
              localStorage.removeItem('restaurantID');
              localStorage.removeItem('loyaltyApplied');
              localStorage.removeItem('loyaltyPoints');
              localStorage.setItem('locationId', id);
              this.router.navigate(['restaurant-page/' + id]);
            } else if (
              // Read more about handling dismissals
              result.dismiss === swal.DismissReason.cancel
            ) {
              swal({
                title: 'Cancelled',
                text: 'Your cart item is safe',
                type: 'info',
                background: '#fff',
                showConfirmButton: false,
                width: 300,
                timer: 2000
              })
            }
          });
        }
      }
    }
  }

  offers = [
    {
      img: "assets/img/chick.jpg"
    },
    {
      img: "assets/img/pizza.jpg"
    },
    {
      img: "assets/img/1.jpg"
    }
  ];

  popular = [
    {
      img: "assets/img/chick.jpg",
      title: "Barbeque",
      name: "WOW Kitchen",
      place: "BTM"
    },
    {
      img: "assets/img/chick7.jpg",
      title: "Italian",
      name: "WOW Kitchen",
      place: "BTM"
    },
    {
      img: "assets/img/4.jpeg",
      title: "Chinese",
      name: "WOW Kitchen",
      place: "BTM"
    },
    {
      img: "assets/img/biryani.jpg",
      title: "Indian",
      name: "WOW Kitchen",
      place: "BTM"
    }
  ];
}
