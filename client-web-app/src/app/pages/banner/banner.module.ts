import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BannerComponent} from './banner/banner.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


export const routes = [
  {path: '', component: BannerComponent, pathMatch: 'full'},
  {path: 'home', component: BannerComponent}

];

@NgModule({
  imports: [
    NgbModule.forRoot(),
    CommonModule,
    RouterModule.forChild(routes),
  ],

  declarations: [
    BannerComponent,
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class BannerModule {
}
