import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ThankYouPageComponent} from './thank-you/thank-you-page/thank-you-page.component';
import {AuthGuardService} from '../services/auth-guard.service';
// import {UserProfileModule} from './user-profile/user-profile.module';
// import { CheckoutComponent } from './common-side-menu/checkout/checkout.component';
// import { OrdersSideMenuComponent } from './orders-side-menu/orders-side-menu.component';


export const routes: Routes = [
  {path: '', loadChildren: 'app/pages/banner/banner.module#BannerModule'},
  {path: '', loadChildren: 'app/pages/login/login.module#LoginModule'},
  {path: '', loadChildren: 'app/pages/restaurant-details/restaurant-details.module#RestaurantDetailsModule'},
  {path: 'thank_you-page', component: ThankYouPageComponent, canActivate: [AuthGuardService]}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],

  declarations: [ThankYouPageComponent],
  schemas: [NO_ERRORS_SCHEMA],
})

export class PagesModule {
}
