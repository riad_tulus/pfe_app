import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../login.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [LoginService]
})
export class SignupComponent implements OnInit {
  @ViewChild('registrationForm') registrationForm: FormGroup;

  constructor(private restService: LoginService, private toastr: ToastrService, private formBuilder: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.buildRegistrationForm();
  }

  // builds user registration form
  private buildRegistrationForm() {
    this.registrationForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required])],
      contactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.pattern('[0-9]+')])],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  // registers a new user
  public createUser() {
    this.registrationForm.value.contactNumber = Number(this.registrationForm.value.contactNumber);
    this.restService.registerUser(this.registrationForm.value).subscribe(res => {
      console.log(res);
      this.toastr.success('User Registered Successfully', 'Success', { timeOut: 3000 });
      this.router.navigate(['login']);
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

}
