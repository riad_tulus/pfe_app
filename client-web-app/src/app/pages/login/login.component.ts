import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angular4-social-login';
import { LoginService } from './login.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService, AuthService]
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: FormGroup;
  public isRememberMe: boolean = false;  // used to save cookie if true
  public loading: boolean = false;  // displays loader when true
  constructor(private restService: LoginService, private auth: AuthService, private _formBuilder: FormBuilder, private toastr: ToastrService, private cookieService: CookieService, private router: Router) {

  }

  ngOnInit() {
    this.buildLoginForm();
    this.getCookies();
  }

  // gets cookies
  private getCookies() {
    let user: any = this.cookieService.getObject('user');
    if (user !== undefined && user !== null) {
      this.loginForm.get('email').setValue(user.email);
      this.loginForm.get('password').setValue(user.password);
    }
  }

  // creates user login form
  private buildLoginForm() {
    this.loginForm = this._formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  // submits user credentials to server
  public login() {
    this.loading = true;
    if (this.isRememberMe) {
      this.cookieService.putObject('user', this.loginForm.value);
    }
    this.restService.login(this.loginForm.value).subscribe((res: any) => {
      localStorage.setItem('token', 'Bearer ' + res.token);
      this.restService.getUserDetails().subscribe((user: any) => {
        if (user.role !== 'User') return this.toastr.error('You are not authorized to login', 'Error', { timeOut: 3000 });
        localStorage.setItem('id', user._id);
        localStorage.setItem('auth', 'yes');
        this.loginForm.reset();
        this.loading = false;
        this.toastr.success('Connexion réussie', 'Success', { timeOut: 3000 });
        if (localStorage.getItem('fromCart') !== null && localStorage.getItem('fromCart') === 'yes') {
          localStorage.removeItem('fromCart');
          this.router.navigate(['checkout-page']);
        } else {
          this.router.navigate(['home']);
        }
      }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // opens facebook login popup
  public loginWithFacebook() {
    this.auth.signIn(FacebookLoginProvider.PROVIDER_ID).then(res => {
      let facebookData = {
        id: res.id,
        email: res.email,
        name: res.name,
        imageId: res.photoUrl
      };
      this.restService.facebookLogin(facebookData).subscribe((res: any) => {
        localStorage.setItem('token', 'Bearer ' + res.token);
        this.restService.getUserDetails().subscribe((user: any) => {
          localStorage.setItem('id', user._id);
          localStorage.setItem('auth', 'yes');
          this.loginForm.reset();
          this.loading = false;
          this.toastr.success('Connexion réussie', 'Success', { timeOut: 3000 });
          this.router.navigate(['home']);
        }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
      }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
    }).catch(error => console.log(error));
  }

  // opens google login popup
  public loginWithGoogle() {
    this.auth.signIn(GoogleLoginProvider.PROVIDER_ID).then(res => {
      let googleData = {
        imageId: res.photoUrl,
        name: res.name,
        email: res.email,
        googleId: res.id
      };
      this.restService.googleLogin(googleData).subscribe((res: any) => {
        localStorage.setItem('token', 'Bearer ' + res.token);
        this.restService.getUserDetails().subscribe((user: any) => {
          localStorage.setItem('id', user._id);
          localStorage.setItem('auth', 'yes');
          this.loginForm.reset();
          this.loading = false;
          this.toastr.success('Connexion réussie', 'Success', { timeOut: 3000 });
          this.router.navigate(['home']);
        }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
      }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
    }).catch(error => console.log(error));
  }

}
