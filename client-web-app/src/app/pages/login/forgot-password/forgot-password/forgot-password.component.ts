import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {LoginService} from '../../login.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers: [LoginService]
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private router: Router, private toastr: ToastrService, private restService: LoginService) {
  }

  ngOnInit() {
  }

  // verifies email with server
  public verifyEmail(form: NgForm) {
    console.log(form.value);
    this.restService.verifyEmail(form.value).subscribe((res: any) => {
      localStorage.setItem('emailtoken', 'Bearer ' + res.token);
      this.router.navigate(['otp-generation']);
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

}
