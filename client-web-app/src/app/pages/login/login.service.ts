import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConstantService} from '../../services/constant.service';
import {CrudService} from '../../services/crud.service';

@Injectable()
export class LoginService {
  constructor(private constantService: ConstantService, private http: HttpClient, private crud: CrudService) {
  }

  // submits users credentials to server and returns its response
  public login(data: any) {
    return this.http.post(this.constantService.LOGIN_AUTH + 'auth/local', data);
  }

  // saves facebook logi data
  public facebookLogin(data: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.constantService.API_ENDPOINT + 'users/auth/facebook', data, {
      headers: headers
    });
  }

  // saves google login data
  public googleLogin(data: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post(this.constantService.API_ENDPOINT + 'users/auth/google', data, {
      headers: headers
    });
  }

  // gets current user details
  public getUserDetails() {
    let token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });

    return this.http.get(this.constantService.API_ENDPOINT + 'users/me', {
      headers: headers
    });
  }

  // registers new user
  public registerUser(data) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.constantService.API_ENDPOINT + 'users', data, {
      headers: headers
    });
  }

  // verfies email with server
  public verifyEmail(data) {
    return this.http.post(this.constantService.API_ENDPOINT + 'users/password/otp', data);
  }

  // submits otp
  public verifyOTP(data) {
    const token = localStorage.getItem('emailtoken');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.post(this.constantService.API_ENDPOINT + 'users/password/verification', data, {
      headers: headers
    });
  }

  // resets password
  public resetPassword(data) {
    const token = localStorage.getItem('otpToken');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.post(this.constantService.API_ENDPOINT + 'users/password/reset', data, {
      headers: headers
    });
  }

}
