import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import { UserProfileService } from '../user-profile.service';
import swal from 'sweetalert2';
import * as CartActions from '../../../store/cart.action';
import * as fromCart from '../../../store/cart.reducer';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css'],
  providers: [UserProfileService]
})
export class FavoritesComponent implements OnInit {
  private cartObservable: Observable<fromCart.State>;
  private locationId: string  // contains selected location id
  public wishlists: Array<any>;  // contains all wishlist of user of a particular location
  public userDetails: any = {};  // contains logged in user details
  private modalRef: NgbModalRef;  // contains reference of NgbModal
  public selectedProduct: any = null;  // contains the selected product
  public selectedQuantity: number = 1;  // contains selected quantity
  private quantitySelected: boolean = false;  // set to true if quantity is changed
  public closeResult: string;  // closes modal with reason
  public grandTotal: number = 0;  // contains grand total
  private initialTotal: number = 0;  // contains product price
  private selectedVariant: any = null;  // contains selected variant information
  private variantSelected: boolean = false;  // checks whether user has selected a variant or not
  private selectedIngredient: any = null;   // contains the selected ingredient
  private extraIngredientSelected: boolean = false;  // checks whether user has selected the ingredient or not
  private locationAndRestaurantDetails: any = {}; // contains location and restaurant details
  private cart: Array<any>;  // contains cart data
  constructor(private restService: UserProfileService, private toastr: ToastrService, private modalService: NgbModal, private store: Store<fromCart.CartState>) {
    if (localStorage.getItem('locationId') !== null) {
      this.locationId = localStorage.getItem('locationId');
      this.getWishlistData();
      this.getUserDetails();
      this.cartObservable = this.store.select('cart');
      this.cartObservable.subscribe(cart => {
        this.cart = cart.cart;
      });
    } else {
      this.toastr.info('Please select a restaurant to see your wishlist');
    }
  }

  ngOnInit() {
  }

  // called from constructor to get all wishlist of user
  private getWishlistData() {
    this.restService.getWishlistData(this.locationId).subscribe((res: any) => {
      this.wishlists = res;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // called from constructor to get logged in user details
  private getUserDetails() {
    this.restService.getUserDetails().subscribe(res => {
      this.userDetails = res;
    }, error => this.toastr.error(error.error.message, 'Errror', { timeOut: 3000 }));
  }

  // removes item from wishlist
  public removeFromWishlist(item) {
    swal({
      title: 'Are you sure?',
      text: "Do you want remove " + item.product.title + " from favorites",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.restService.removeFromWishlist(item._id).subscribe(res => {
          swal({
            title: 'Success',
            text: item.product.title + ' removed from wishlist',
            type: 'success',
            background: '#fff',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          }).then(() => {
          }, () => {
          });
          this.getWishlistData();
        }, error => {
          swal({
            title: 'Error',
            text: error.error.message,
            type: 'error',
            background: '#fff',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          }).then(() => {
          }, () => {
          });
        });
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
      }
    }).then(() => {
    }, () => {
    });
  }

  // opens ngb modal
  public open(content, product) {
    this.selectedProduct = product.product;
    this.locationAndRestaurantDetails = product;
    this.grandTotal = this.selectedProduct.variants[0].price === this.selectedProduct.variants[0].MRP ? this.selectedProduct.variants[0].MRP : this.selectedProduct.variants[0].price;
    this.grandTotal = this.grandTotal * this.selectedQuantity;
    this.initialTotal = this.grandTotal;
    this.getWishlistData();
    this.modalRef = this.modalService.open(content, { size: 'lg' });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public selectVariantEvent(event) {
    this.selectedVariant = this.selectedProduct.variants.find(data => {
      return data._id === event.target.value
    });
    this.variantSelected = true;
    this.initialTotal = this.selectedVariant.price === this.selectedVariant.MRP ? this.selectedVariant.MRP : this.selectedVariant.price;
    if (!this.quantitySelected && !this.extraIngredientSelected) {
      this.grandTotal = this.initialTotal;
    } else if (this.quantitySelected && !this.extraIngredientSelected) {
      this.grandTotal = this.initialTotal;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
    } else if (this.quantitySelected && this.extraIngredientSelected) {
      this.initialTotal = this.initialTotal + this.selectedIngredient.price;
      this.grandTotal = this.initialTotal;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
    }

  }

  // this method is called when user selects quantity
  public quantityChangedEvent(event) {
    this.quantitySelected = true;
    this.selectedQuantity = event.target.value;
    if (!this.extraIngredientSelected) {
      this.grandTotal = this.initialTotal;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
    } else {
      this.grandTotal = this.initialTotal;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
    }
  }

  // this method is called when user adds extra ingredient
  public extraIngredientsEvent(event) {
    this.selectedIngredient = this.selectedProduct.extraIngredients.find(data => {
      return data.name === event.target.value
    });
    this.extraIngredientSelected = true;
    if (!this.variantSelected) {
      this.grandTotal = this.initialTotal;
      this.grandTotal = (this.grandTotal + this.selectedIngredient.price) * this.selectedQuantity;
    } else {
      this.grandTotal = this.initialTotal;
      this.grandTotal = (this.grandTotal + this.selectedIngredient.price) * this.selectedQuantity;
      this.initialTotal = this.grandTotal;
    }
  }

  // this method is called when user adds item to cart
  public addToCart() {
    if (this.selectedVariant === null) {
      return swal({
        title: 'Size not selected',
        text: 'Please select a size',
        type: 'info',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    }
    if (this.cart.length === 0) {
      let name = this.selectedProduct.title;
      let productDetails = {
        totalPrice: this.grandTotal,
        restaurant: this.locationAndRestaurantDetails.restaurantID.restaurantName,
        title: this.selectedProduct.title,
        brand: this.selectedProduct.brand,
        productId: this.selectedProduct._id,
        Quantity: Number(this.selectedQuantity),
        imageUrl: this.selectedProduct.imageUrl,
        restaurantID: this.locationAndRestaurantDetails.restaurantID._id,
        extraIngredients: this.selectedIngredient !== null ? this.selectedIngredient : null,
        variant: this.selectedVariant,
        location: this.locationId,
        MRP: this.selectedVariant.MRP,
        price: this.selectedVariant.price,
        Discount: this.selectedVariant.Discount
      }
      localStorage.setItem('restaurantDeliveryInfo', JSON.stringify(this.locationAndRestaurantDetails.location.deliveryInfo.deliveryInfo));
      localStorage.setItem('taxInfo', JSON.stringify(this.locationAndRestaurantDetails.restaurantID.taxInfo));
      let cart: Array<any> = [];
      cart.push(productDetails);
      localStorage.setItem('cart', JSON.stringify(cart));
      this.store.dispatch(new CartActions.AddToCart({ cart: cart }));
      this.closeProductModel();
      swal({
        title: 'Cool',
        text: name + ' added to cart',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      }).catch(() => {
      });
    } else {
      let index = this.cart.findIndex(data => data.productId === this.selectedProduct._id);
      if (index >= 0) {
        let cart: Array<any> = JSON.parse(localStorage.getItem('cart'));
        cart.splice(index, 1);
        let name = this.selectedProduct.title;
        let productDetails = {
          totalPrice: this.grandTotal,
          restaurant: this.locationAndRestaurantDetails.restaurantID.restaurantName,
          title: this.selectedProduct.title,
          brand: this.selectedProduct.brand,
          productId: this.selectedProduct._id,
          Quantity: Number(this.selectedQuantity),
          imageUrl: this.selectedProduct.imageUrl,
          restaurantID: this.locationAndRestaurantDetails.restaurantID._id,
          extraIngredients: this.selectedIngredient !== null ? this.selectedIngredient : null,
          variant: this.selectedVariant,
          location: this.locationId,
          MRP: this.selectedVariant.MRP,
          price: this.selectedVariant.price,
          Discount: this.selectedVariant.Discount
        }
        localStorage.setItem('restaurantDeliveryInfo', JSON.stringify(this.locationAndRestaurantDetails.location.deliveryInfo.deliveryInfo));
        localStorage.setItem('taxInfo', JSON.stringify(this.locationAndRestaurantDetails.restaurantID.taxInfo));
        cart.push(productDetails);
        localStorage.setItem('cart', JSON.stringify(cart));
        this.store.dispatch(new CartActions.UpdateCart({ id: this.selectedProduct._id, cart: productDetails }));
        this.closeProductModel();
        swal({
          title: 'Cool',
          text: ' Cart item updated',
          type: 'success',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => {
        }, () => {
        }).catch(() => {
        });
      } else {
        let name = this.selectedProduct.title;
        let cart: Array<any> = JSON.parse(localStorage.getItem('cart'));
        let productDetails = {
          totalPrice: this.grandTotal,
          restaurant: this.locationAndRestaurantDetails.restaurantID.restaurantName,
          title: this.selectedProduct.title,
          brand: this.selectedProduct.brand,
          productId: this.selectedProduct._id,
          Quantity: Number(this.selectedQuantity),
          imageUrl: this.selectedProduct.imageUrl,
          restaurantID: this.locationAndRestaurantDetails.restaurantID._id,
          extraIngredients: this.selectedIngredient !== null ? this.selectedIngredient : null,
          variant: this.selectedVariant,
          location: this.locationId,
          MRP: this.selectedVariant.MRP,
          price: this.selectedVariant.price,
          Discount: this.selectedVariant.Discount
        }
        localStorage.setItem('restaurantDeliveryInfo', JSON.stringify(this.locationAndRestaurantDetails.location.deliveryInfo.deliveryInfo));
        localStorage.setItem('taxInfo', JSON.stringify(this.locationAndRestaurantDetails.restaurantID.taxInfo));
        cart.push(productDetails);
        localStorage.setItem('cart', JSON.stringify(cart));
        this.store.dispatch(new CartActions.AddToCart({ cart: cart }));
        this.closeProductModel();
        swal({
          title: 'Cool',
          text: name + ' added to cart',
          type: 'success',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => {
        }, () => {
        }).catch(() => {
        });
      }
    }
  }

  // closes product modal
  public closeProductModel() {
    this.modalRef.close();
    this.selectedProduct = null;
    this.selectedQuantity = 1;
    this.selectedVariant = null;
    this.selectedIngredient = null;
    this.extraIngredientSelected = false;
    this.variantSelected = false;
    this.grandTotal = 0;
    this.initialTotal = 0;
    this.quantitySelected = false;
  }

}
