import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class UserProfileService {
  constructor(private crud: CrudService) {
  }

  // gets logged in user details
  public getUserDetails() {
    return this.crud.getUserDetails('users/me');
  }

  //updates user details
  public updateUser(id: string, data: any) {
    return this.crud.put('users', id, data);
  }

  // returns particular product rating
  public getProductRatings(data) {
    return this.crud.post('productratings/rating/by/product/order', data);
  }

  // returns user wishlist data
  public getWishlistData(id: string) {
    return this.crud.getUserDetailsById('favourites/user/fav/list', id);
  }

  // gets user orders
  public getUserOrders() {
    let id = localStorage.getItem('id');
    return this.crud.getUserDetailsById('orders/user', id);
  }

  // gets user addresses
  public getUserAddresses() {
    return this.crud.getUserDetails('users/newaddress/address');
  }

  // updates user password
  public updatePassword(data) {
    return this.crud.post('users/password/update', data);
  }

  // saves users new address
  public saveAddress(data) {
    return this.crud.post('users/add/address', data);
  }

  // saves product ratings
  public saveProductRatings(data) {
    return this.crud.post('productratings', data);
  }

  // deletes the address
  public deleteAddress(index: any) {
    return this.crud.delete('users/address', index);
  }

  // removes item from wishlist
  public removeFromWishlist(id: string) {
    return this.crud.delete('favourites', id);
  }

  // returns particular order details
  public getOrderDetails(id: string) {
    return this.crud.getUserDetailsById('orders', id);
  }

  // returns location details
  public getLocationDetails(id: string) {
    return this.crud.getOne('locations', id);
  }
}
