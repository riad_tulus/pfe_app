import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {UserProfileService} from '../user-profile.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [UserProfileService]
})
export class OrdersComponent implements OnInit {
  public userDetails: any = {};  // contains logged in user details
  public orders: Array<any>;  //contains users orders
  constructor(private restService: UserProfileService, private toastr: ToastrService, private router: Router) {
    this.getUserDetails();
    this.getUserOrders();
  }

  ngOnInit() {
  }

  // gets logged in user details
  private getUserDetails() {
    this.restService.getUserDetails().subscribe(res => {
      this.userDetails = res;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // gets users all orders
  private getUserOrders() {
    this.restService.getUserOrders().subscribe((res: any) => {
      this.orders = res;
    }), error => this.toastr.error(error.error.message);
  }

  // navigates to order history page
  public viewOrderHistory(id: string) {
    this.router.navigate(['history/' + id]);
  }

  //navigates to order tracking page
  public trackOrder(id: string) {
    this.router.navigate(['order-tracking/' + id]);
  }

}
