import {Component, OnInit, ViewChild} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import {UserProfileService} from '../user-profile.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css'],
  providers: [UserProfileService]
})
export class AddressComponent implements OnInit {
  public addresses: Array<any>; // contains all addresses of user
  public userDetails: any = {};  // contains loggedmin user details
  private modalRef: NgbModalRef;  // contains modal reference
  public closeResult: string;  // closed modal with reason
  constructor(private restService: UserProfileService, private toastr: ToastrService, private modalService: NgbModal) {
    this.getUserDetails();
    this.getUserAddresses();
  }

  ngOnInit() {
  }

  // gets users details
  private getUserDetails() {
    this.restService.getUserDetails().subscribe(res => {
      this.userDetails = res;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // called from constructor to get user addresses
  private getUserAddresses() {
    this.restService.getUserAddresses().subscribe((res: any) => {
      this.addresses = res;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // opens add address modal
  public open(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // closes modal with reason
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // saves new address
  public saveAddress(addAddressForm: NgForm) {
    addAddressForm.value.zip = Number(addAddressForm.value.zip);
    addAddressForm.value.contactNumber = Number(addAddressForm.value.contactNumber);
    this.restService.saveAddress(addAddressForm.value).subscribe(res => {
      swal({
        title: 'Success',
        text: 'Address saved successfully',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      })
      addAddressForm.reset();
      this.closeAddAddressModal();
    }, error => {
      swal(
        'Error',
        error.error.message,
        'error'
      );
    });
  }

  // deleted user address
  public deleteAddress(index: number) {
    console.log(index);
    swal({
      title: 'Are you sure?',
      text: "Do you want to delete this address",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.restService.deleteAddress(index).subscribe(res => {
          swal({
            title: 'Success',
            text: 'Address deleted successfully',
            type: 'success',
            background: '#fff',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          });
          this.getUserAddresses();
        }, error => {
          swal({
            title: 'Error',
            text: error.error.message,
            type: 'error',
            background: '#fff',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          })
        });
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
      }
    })
  }

  // closes add address modal
  public closeAddAddressModal() {
    this.getUserAddresses();
    this.modalRef.close();
  }

}
