import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../user-profile.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { CloudinaryUploader, CloudinaryOptions } from 'ng2-cloudinary';
import { ConstantService } from '../../../../services/constant.service';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
  providers: [UserProfileService]
})
export class EditProfileComponent implements OnInit {
  public userDetails: any = {};  // contains logged in user details
  public passwordData = {    // contains old password and new password, used when user wants to update his/her password
    oldPassword: null,
    newPassword: null
  }
  public confirmPass: string = null;  // used to check if user confirm password matches new password
  public doesPasswordMatch: boolean;  // set to false if password dows not match
  public uploader: CloudinaryUploader = new CloudinaryUploader(new CloudinaryOptions(this._constantService.cloudinaryConfig));  // initializes an cloudinary uploader with the config
  private isFileSelected: boolean = false;   // set to true when user selects a file to upload
  private imageURL: string = null;   // contains the URL of the selected file
  public imageUploaded: boolean = false;  // set to true when upload is successful
  constructor(private restService: UserProfileService, private toastr: ToastrService, private router: Router, private _constantService: ConstantService) {
    this.getUserDetails();
  }

  // called from constructor to get logged in user details
  private getUserDetails() {
    this.restService.getUserDetails().subscribe(res => {
      this.userDetails = res;
    },
      error => console.log(error)
      //  error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 })
    );
  }

  ngOnInit() {
  }

  // this method is called when user selects an image
  public readURL(event) {
    if (event.target.files[0] !== undefined) {
      let reader = new FileReader();
      reader.onload = (event: any) => {
        this.isFileSelected = true;
        this.imageURL = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  // updates user's information
  private updateUserDetails(form: NgForm) {
    if (!this.isFileSelected) {
      this.restService.updateUser(this.userDetails._id, form.value).subscribe(res => {
        this.toastr.success('User details updated successfully', 'Success', { timeOut: 3000 });
        this.getUserDetails();
      }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
    } else {
      this.imageUploaded = true;
      console.log('file selected');
      if (this.userDetails.userImage === undefined) {
        this.uploader.uploadAll();
        this.uploader.onSuccessItem = (item: any, response: any, status: number, headers: any): any => {
          const res = JSON.parse(response);
          console.log(res);
          this.imageUploaded = false;
          const body = {
            name: form.value.name,
            contactNumber: form.value.contactNumber,
            email: form.value.email,
            publicId: res.public_id,
            userImage: res.secure_url,
            userImagePublicId: res.public_id
          }
          this.restService.updateUser(this.userDetails._id, body).subscribe(res => {
            swal({
              title: 'Success',
              text: 'User details updated successfully',
              type: 'success',
              background: '#fff',
              showConfirmButton: false,
              width: 300,
              timer: 3000
            }).then(() => { }, () => { });
            this.getUserDetails();
          }, error => {
            swal({
              title: 'Error',
              text: error.error.message,
              type: 'error',
              background: '#fff',
              showConfirmButton: false,
              width: 300,
              timer: 3000
            }).then(() => { }, () => { });
          });
        }
      } else {
        this.uploader.uploadAll();
        this.uploader.onSuccessItem = (iitem: any, response: any, status: number, headers: any): any => {
          const res = JSON.parse(response);
          console.log(res);
          this.imageUploaded = false;
          const body = {
            name: form.value.name,
            contactNumber: form.value.contactNumber,
            email: form.value.email,
            publicId: res.public_id,
            userImage: res.secure_url,
            userImagePublicId: res.public_id,
            deletePublicId: this.userDetails.publicId
          }
          this.restService.updateUser(this.userDetails._id, body).subscribe(res => {
            swal({
              title: 'Success',
              text: 'User details updated successfully',
              type: 'success',
              background: '#fff',
              showConfirmButton: false,
              width: 300,
              timer: 3000
            }).then(() => { }, () => { });
            this.getUserDetails();
          }, error => {
            swal({
              title: 'Error',
              text: error.error.message,
              type: 'error',
              background: '#fff',
              showConfirmButton: false,
              width: 300,
              timer: 3000
            }).then(() => { }, () => { });
          });
        }
      }
    }
  }

  // updates user's password
  public updatePassword(form: NgForm) {
    if (form.value.confirmPass === this.passwordData.newPassword) {
      this.doesPasswordMatch = true;
      this.restService.updatePassword(this.passwordData).subscribe(res => {
        swal({
          title: 'Success',
          text: 'Password changed successfully',
          type: 'success',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => { }, () => { });
        this.router.navigate(['login']);
      }, error => {
        swal({
          title: 'Error',
          text: error.error.message,
          type: 'error',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => { }, () => { });
      });
    } else {
      this.doesPasswordMatch = false;
    }
  }

}
