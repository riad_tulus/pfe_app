import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../user-profile.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css'],
  providers: [UserProfileService]
})
export class OrderHistoryComponent implements OnInit {
  public userDetails: any = {};  // contains logged in user details
  public orderDetails: any = {};  // contains order details
  private modalRef: NgbModalRef;  // modal reference
  public closeResult: string; // contains modal dismission reason
  public ratingData = {     //  contains ptoduct rating details
    rating: 0,
    comment: null,
    restaurantID: null,
    location: null,
    order: null,
    product: null,
    user: null
  };
  public hasRatings: boolean = false;  // checks whether the user has already rated the product
  private selectedProduct: any = {};  // contains the product details selected for rating

  constructor(private route: ActivatedRoute, private toastr: ToastrService, private restService: UserProfileService, private modalService: NgbModal) {
    route.params.map(params => params.id).subscribe(Id => {
      if (Id !== undefined && Id !== null) {
        this.getUserDetails();
        this.getOrderDetails(Id);
      }
    });
  }

  ngOnInit() {
  }

  // gets logged in user details
  private getUserDetails() {
    this.restService.getUserDetails().subscribe(res => {
      this.userDetails = res;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // gets particular order details
  private getOrderDetails(id) {
    this.restService.getOrderDetails(id).subscribe(res => {
      this.orderDetails = res;
      console.log(this.orderDetails);
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // opens rating modal
  public open(productRating, item) {
    this.selectedProduct = item;
    this.getProductRating();
    this.modalRef = this.modalService.open(productRating);
    this.modalRef.result.then((result) => {
      this.closeResult = `closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // gets product ratings
  private getProductRating() {
    const body = {
      productId: this.selectedProduct.productId,
      orderId: this.orderDetails._id
    };
    this.restService.getProductRatings(body).subscribe((res: any) => {
      if (res !== null) {
        this.ratingData = res;
      }
      this.hasRatings = res !== null ? true : false;
    }, error => this.toastr.error(error.err));
  }

  // dismisses modal with a reason
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // submits product rating
  public submitRating() {
    this.ratingData.location = this.orderDetails.location;
    this.ratingData.order = this.orderDetails._id;
    this.ratingData.restaurantID = this.orderDetails.restaurantID;
    this.ratingData.user = localStorage.getItem('id');
    this.ratingData.product = this.selectedProduct.productId;
    this.restService.saveProductRatings(this.ratingData).subscribe(() => {
      swal({
        title: 'Thank You',
        text: 'Product rating successful',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
      this.closeRatingModal();
    }, error => {
      swal({
        title: 'Error',
        text: error.error.message,
        type: 'error',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    });
  }

  // closes rating modal
  public closeRatingModal() {
    this.modalRef.close();
  }

}
