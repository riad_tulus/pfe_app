import {Component, OnInit} from '@angular/core';
import {UserProfileService} from '../user-profile.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-order-tracking',
  templateUrl: './order-tracking.component.html',
  styleUrls: ['./order-tracking.component.css'],
  providers: [UserProfileService]
})
export class OrderTrackingComponent implements OnInit {
  public userDetails: any = {};  // contains logged in user details
  public orderDetails: any = {};  // contains order details
  constructor(private route: ActivatedRoute, private restService: UserProfileService, private toastr: ToastrService) {
    route.params.map(params => params.id).subscribe(Id => {
      if (Id !== undefined && Id !== null) {
        this.getUserDetails();
        this.getOrderDetails(Id);
      }
    });
  }

  ngOnInit() {
  }

  // gets user details
  private getUserDetails() {
    this.restService.getUserDetails().subscribe(res => {
      this.userDetails = res;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // gets order details
  private getOrderDetails(id: string) {
    this.restService.getOrderDetails(id).subscribe(res => {
      this.orderDetails = res;
      this.getRestaurantDeliveryInformation(this.orderDetails.location);
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // gets restaurant delivery information
  private getRestaurantDeliveryInformation(id: string) {
    this.restService.getLocationDetails(id).subscribe((res: any) => {
      this.orderDetails['deliveryDetails'] = res.deliveryInfo.deliveryInfo;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

}
