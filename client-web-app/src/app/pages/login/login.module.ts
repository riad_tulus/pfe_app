import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoginComponent} from './login.component';
import {OrdersSideMenuComponent} from '../common-side-menu/orders-side-menu/orders-side-menu.component';
import {OrderTrackingComponent} from './user-profile/order-tracking/order-tracking.component';
import {OrderHistoryComponent} from './user-profile/order-history/order-history.component';
import {OrdersComponent} from './user-profile/orders/orders.component';
import {FavoritesComponent} from './user-profile/favorites/favorites.component';
import {AddressComponent} from './user-profile/address/address.component';
import {EditProfileComponent} from './user-profile/edit-profile/edit-profile.component';
import {OffersComponent} from './user-profile/offers/offers.component';
import {SignupComponent} from './signup/signup/signup.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password/forgot-password.component';
import {OtpGenerationComponent} from './otp/otp-generation/otp-generation.component';
import {ResetPasswordComponent} from './reset-password/reset-password/reset-password.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {AuthGuardService} from '../../services/auth-guard.service';
import {Ng2CloudinaryModule} from 'ng2-cloudinary';
import {FileUploadModule} from 'ng2-file-upload';


export const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'order-tracking/:id', component: OrderTrackingComponent, canActivate: [AuthGuardService]},
  {path: 'history/:id', component: OrderHistoryComponent, canActivate: [AuthGuardService]},
  {path: 'orders', component: OrdersComponent, canActivate: [AuthGuardService]},
  {path: 'favorites', component: FavoritesComponent, canActivate: [AuthGuardService]},
  {path: 'add-address', component: AddressComponent, canActivate: [AuthGuardService]},
  {path: 'edit-profile', component: EditProfileComponent, canActivate: [AuthGuardService]},
  {path: 'offers', component: OffersComponent, canActivate: [AuthGuardService]},
  {path: 'signup', component: SignupComponent},
  {path: 'forgot-password', component: ForgotPasswordComponent},
  {path: 'otp-generation', component: OtpGenerationComponent},
  {path: 'reset-password', component: ResetPasswordComponent}


];

@NgModule({
  imports: [
    NgbModule.forRoot(),
    CommonModule,
    FormsModule,
    Ng2CloudinaryModule,
    FileUploadModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],

  declarations: [
    LoginComponent,
    OrdersSideMenuComponent,
    OrderTrackingComponent,
    OrderHistoryComponent,
    OrdersComponent,
    FavoritesComponent,
    AddressComponent,
    EditProfileComponent,
    OffersComponent,
    SignupComponent,
    ForgotPasswordComponent,
    OtpGenerationComponent,
    ResetPasswordComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LoginModule {
}
