import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {LoginService} from '../../login.service';
import {ToastrService} from 'ngx-toastr';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-otp-generation',
  templateUrl: './otp-generation.component.html',
  styleUrls: ['./otp-generation.component.css'],
  providers: [LoginService]
})
export class OtpGenerationComponent implements OnInit {
  public count: number = 300;  // displays token expiration timer
  public counter: any;  // subscribes the count values
  constructor(private restService: LoginService, private toastr: ToastrService, private router: Router) {
    this.counter = Observable.timer(0, 1000).take(this.count).map(() => --this.count);
  }

  ngOnInit() {
  }

  // submits the OTP entered by user
  public verifyOTP(form: NgForm) {
    this.restService.verifyOTP(form.value).subscribe((res: any) => {
      localStorage.setItem('otpToken', 'Bearer ' + res.token);
      this.router.navigate(['reset-password']);
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

}
