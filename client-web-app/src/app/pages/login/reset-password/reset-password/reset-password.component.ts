import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {LoginService} from '../../login.service';
import {Router} from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
  providers: [LoginService]
})
export class ResetPasswordComponent implements OnInit {

  constructor(private restService: LoginService, private router: Router) {
  }

  ngOnInit() {
  }

  // reset password
  public resetPassword(form: NgForm) {
    this.restService.resetPassword(form.value).subscribe(res => {
      swal({
        title: 'Success',
        text: 'Password changed successfully',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
      localStorage.clear();
      this.router.navigate(['login']);
    }, error => {
      swal({
        title: 'Error',
        text: error.error.message,
        type: 'error',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    })
  }

}
