import { Action } from '@ngrx/store';

export const ADD_TO_CART = 'ADD_TO_CART';
export const UPDATE_CART = 'UPDATE_CART';
export const DELETE_CART_ITEM = 'DELETE_CART_ITEM';

export class AddToCart implements Action {
    readonly type = ADD_TO_CART;
    constructor(public payload: { cart: Array<any> }) { }
}

export class UpdateCart implements Action {
    readonly type = UPDATE_CART;
    constructor(public payload: { id: string, cart: any }) { }
}

export class DeleteCartItem implements Action {
    readonly type = DELETE_CART_ITEM;
    constructor(public payload: string) { }
}

export type CartActions = AddToCart | UpdateCart | DeleteCartItem;