import * as CartActions from './cart.action';

// interface to define cart type  
export interface CartState {
    cart: State
}

export interface State {
    cart: Array<any>;
}

// gets cart data from localstorage if cart is not present returns null
const cartData = JSON.parse(localStorage.getItem('cart'));

// used to initialize the cart with default state
const inititalState: State = {
    cart: (cartData !== null) ? cartData : []
}

// based on the action type cart state is modified and new state is returned
export function cartReducer(state = inititalState, action: CartActions.CartActions) {
    switch (action.type) {
        case CartActions.ADD_TO_CART:
            return {
                ...state,
                cart: [...action.payload.cart]
            };
        case CartActions.UPDATE_CART:
            const cartInfo = state.cart.find(data => data.productId === action.payload.id);
            const updatedCart = {
                ...cartInfo,
                ...action.payload.cart
            };

            const cartData = [...state.cart];
            const index = cartData.findIndex(data => data.productId === action.payload.id);
            cartData[index] = updatedCart;
            return {
                ...state,
                cart: cartData
            };
        case CartActions.DELETE_CART_ITEM:
            const oldCart = [...state.cart];
            const deleteIndex = oldCart.findIndex(data => data.productId === action.payload);
            oldCart.splice(deleteIndex, 1);
            return {
                ...state,
                cart: oldCart
            }
        default: return state;
    }
}