import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { RestaurantDetailsService } from '../../restaurant-details.service';
import { ToastrService } from 'ngx-toastr';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';

declare let paypal: any;
import * as CartActions from '../../../store/cart.action';
import * as fromCart from '../../../store/cart.reducer';

import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-checkout-page',
  templateUrl: './checkout-page.component.html',
  styleUrls: ['./checkout-page.component.css'],
  providers: [RestaurantDetailsService]
})
export class CheckoutPageComponent implements OnInit {
  @ViewChild('addUserAddressForm') addUserAddressForm: FormGroup;  // reference of form element
  @ViewChild('updateUpdateForm') updateUpdateForm: FormGroup;
  @ViewChild('closeModal') closeModal: ElementRef;  // contains reference of DOM element
  @ViewChild('closeUpdateModal') closeUpdateModal: ElementRef // contains reference of DOM element
  public addresses: Array<any>;  // contains delivery addresses of user
  public selectedAddress: any = null;  // contains the address selected by user
  private deliveryInfo: any = {};  // contains restaurant delivery information
  private locationInformation: any = {};  // contains restaurant location information
  private subTotal: number;  // contains cart sub total amount
  private grandTotal: number;  // contains user payable amount
  private taxInfo: any = {};  // contains tax related information
  private cartObservable: Observable<fromCart.State>;  // subscribes to cart state
  public cart: Array<any>;  // contains cart data
  public closeResult: string;  // uses to close modal
  private modalRef: NgbModalRef; // contains reference of ng bootstrap modal;
  private updateAddressIndex: number;  // contains the index of address you want to update
  public paypalSelected: boolean = false;  // renders paypal button when true
  private gateWayTransactionDetails: any = null;   // contains gateway transactions response details
  public loading: boolean = false;  // displays loader when true
  private couponDetails: any = null;  // contains applied coupon details
  public savedUserCards: Array<any>;  // contains all the saved card of users
  public isCardSelected: boolean = false;  // set to true when user selects a card
  private selectedCardDetails: any = {}  // contains details of selected card
  private selectedCardIndex: number  // contains index of selected card
  private loyaltyStatus: boolean;  // set to true if user has applied loyalty
  private loyaltyPoints: number;  // contains the loyalty points applied by user
  public stripeCardInfo = {
    month: null,
    year: null,
    cardNumber: null,
    cvc: null,
    isSaved: false
  }

  constructor(private restService: RestaurantDetailsService, private toastr: ToastrService,
    private modalService: NgbModal, private formBuilder: FormBuilder, private router: Router, private store: Store<fromCart.CartState>) {
    this.getUserAddresses();
    this.cartObservable = this.store.select('cart');
    this.cartObservable.subscribe(cart => {
      this.cart = cart.cart;
    })
    this.deliveryInfo = JSON.parse(localStorage.getItem('restaurantDeliveryInfo'));
    this.locationInformation = JSON.parse(localStorage.getItem('locationInformation'));
    console.log("location info" + JSON.stringify(this.locationInformation))
    this.taxInfo = JSON.parse(localStorage.getItem('taxInfo'));
    this.subTotal = Number(localStorage.getItem('subTotal'));
    this.grandTotal = Number(localStorage.getItem('grandTotal'));
    if (localStorage.getItem('address') !== null) {
      this.selectedAddress = JSON.parse(localStorage.getItem('address'));
    }
    if (localStorage.getItem('couponApplied') !== null) {
      this.couponDetails = JSON.parse(localStorage.getItem('couponDetails'));
    } else {
      this.couponDetails = null;
    }

    this.loyaltyStatus = (localStorage.getItem('loyaltyApplied') !== null && localStorage.getItem('loyaltyApplied') === 'yes') ? true : false;
    if (this.loyaltyStatus == true) {
      this.loyaltyPoints = Number(localStorage.getItem('loyaltyPoints'));
    }
    this.getUserSavedCards();
  }

  // called from constructor to get all saved cards of user
  private getUserSavedCards() {
    this.restService.getSavedCards().subscribe((res: any) => {
      this.savedUserCards = res;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  ngOnInit() {
    this.buildAddAddressForm();
    this.buildUpdateAddressForm();
  }

  // builds add address form
  private buildAddAddressForm() {
    this.addUserAddressForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required])],
      city: [null, Validators.compose([Validators.required, Validators.minLength(4)])],
      zip: [null, Validators.compose([Validators.required, Validators.minLength(4)])],
      locationName: [null, Validators.compose([Validators.required])],
      contactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10)])],
      address: [null, Validators.compose([Validators.required])]
    });
  }

  // builds update address form
  private buildUpdateAddressForm() {
    this.updateUpdateForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required])],
      city: [null, Validators.compose([Validators.required, Validators.minLength(4)])],
      zip: [null, Validators.compose([Validators.required, Validators.minLength(4)])],
      locationName: [null, Validators.compose([Validators.required])],
      contactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10)])],
      address: [null, Validators.compose([Validators.required])]
    });
  }

  // called from constructor to get all addresses of user
  private getUserAddresses() {
    this.restService.getUserAddresses().subscribe((res: any) => {
      this.addresses = res;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // this method is called when user selects an event
  public addressSelectionEvent(event, address) {
    this.selectedAddress = address;
    localStorage.setItem('address', JSON.stringify(this.selectedAddress));
  }

  // saves new address
  public saveUserAddress() {
    this.addUserAddressForm.value.zip = Number(this.addUserAddressForm.value.zip);
    this.addUserAddressForm.value.contactNumber = Number(this.addUserAddressForm.value.contactNumber);
    this.restService.saveUserAddress(this.addUserAddressForm.value).subscribe(res => {
      this.closeModal.nativeElement.click();
      swal(
        'Success',
        'Address saved successfully',
        'success'
      );
      this.getUserAddresses();
      this.addUserAddressForm.reset();
    }, error => swal(
      'Error',
      error.error.message,
      'error'
    ));
  }

  // updates user address
  public updateUserAddress() {
    this.updateUpdateForm.value.zip = Number(this.updateUpdateForm.value.zip);
    this.updateUpdateForm.value.contactNumber = Number(this.updateUpdateForm.value.contactNumber);
    const body = {
      addAddress: this.updateUpdateForm.value
    }
    this.restService.updateAddress(this.updateAddressIndex, body).subscribe(res => {
      swal({
        title: 'Success',
        text: 'Address updated successfully',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      });
      this.selectedAddress = null;
      this.closeUpdateAddressModal();
    }, error => {
      swal({
        title: 'Error',
        text: error.error.message,
        type: 'error',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      });
    })
  }

  // deletes user address
  public deleteAddress(index) {
    swal({
      title: 'Are you sure?',
      text: "Do you want to delete this address",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.restService.deleteAddress(index).subscribe(res => {
          swal({
            title: 'Success',
            text: 'Address deleted successfully',
            type: 'success',
            background: '#fff',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          });
          this.addresses.splice(index, 1);
        }, error => {
          swal({
            title: 'Error',
            text: error.error.message,
            type: 'error',
            background: '#fff',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          });
        });
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal(
          'Cancelled',
          'Address is not deleted',
          'error'
        )
      }
    })
  }

  // places order with COD as paymant option
  public payWithCOD() {
    console.log("Location info" + JSON.stringify(this.locationInformation))
    let orderDetails = {
      deliveryCharge: this.deliveryInfo.freeDelivery === true ? (this.grandTotal >= this.deliveryInfo.amountEligibility ? 'free' : this.deliveryInfo.deliveryCharges) : this.deliveryInfo.deliveryCharges,
      shippingAddress: this.selectedAddress,
      restaurantID: this.locationInformation.restaurantID._id,
      restaurantName: this.locationInformation.restaurantID.restaurantName,
      location: this.locationInformation._id,
      locationName: this.locationInformation.locationName,
      grandTotal: this.grandTotal,
      subTotal: this.subTotal,
      charges: this.taxInfo.taxRate,
      productDetails: JSON.parse(localStorage.getItem('cart')),
      status: "Pending",
      coupon: {
        couponApplied: this.couponDetails !== null ? true : false,
        offPrecentage: this.couponDetails !== null ? this.couponDetails.offPrecentage : 0,
        couponName: this.couponDetails !== null ? this.couponDetails.couponName : null
      },
      loyaltyData: {
        isApplied: this.loyaltyStatus,
        loyaltyPoints: this.loyaltyStatus === true ? this.loyaltyPoints : 0,
      },
      payableAmount: this.grandTotal,
      orderType: 'Home Delivery',
      position: {
        lat: 12.917096599999999,
        long: 77.58980509999999,
        name: "Current Position"
      },
      paymentOption: 'COD'
    }
    this.restService.placeOrder(orderDetails).subscribe(res => {
      swal(
        'Succès',
        'Commande passée',
        'success'
      );
      localStorage.removeItem('cart');
      localStorage.removeItem('taxInfo');
      localStorage.removeItem('subTotal');
      localStorage.removeItem('grandTotal');
      localStorage.removeItem('locationInformation');
      localStorage.removeItem('restaurantDeliveryInfo');
      localStorage.removeItem('address');
      localStorage.removeItem('couponDetails');
      localStorage.removeItem('couponApplied');
      localStorage.removeItem('restaurantID');
      localStorage.removeItem('loyaltyApplied');
      localStorage.removeItem('loyaltyPoints');
      this.store.dispatch(new CartActions.AddToCart({ cart: [] }));
      this.router.navigate(['thank_you-page']);
      this.playAudio();
    }, error => swal(
      'Error',
      error.error.message,
      'error'
    ));
  }

  // opens update address modal
  public open(content, address, index) {
    this.updateAddressIndex = index;
    this.updateUpdateForm.get('name').setValue(address.name);
    this.updateUpdateForm.get('city').setValue(address.city);
    this.updateUpdateForm.get('contactNumber').setValue(address.contactNumber);
    this.updateUpdateForm.get('zip').setValue(address.zip);
    this.updateUpdateForm.get('locationName').setValue(address.locationName);
    this.updateUpdateForm.get('address').setValue(address.address);
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // paypal selection event
  public paypalSelectionEvent(event) {
    if (event.target.checked) {
      this.paypalSelected = true;
      this.openPaypalPayment();
    }
  }

  //enders paypal button
  private openPaypalPayment() {
    let amount = this.grandTotal;
    let that = this;
    paypal.Button.render({

      env: 'sandbox', // sandbox | production

      style: {
        label: 'pay',
        size: 'medium',    // small | medium | large | responsive
        shape: 'pill',     // pill | rect
        color: 'gold',     // gold | blue | silver | black
        tagline: false
      },

      client: {
        sandbox: 'A9RDK3ihUrZj-R7cqWGDu71e5Q8FAO3SCGwPRzwC7I8vO2Wkc-Yp.5Dt',
      },

      payment: function (data, actions) {
        return actions.payment.create({
          payment: {
            transactions: [
              {
                amount: { total: amount, currency: 'USD' }
              }
            ]
          }
        });
      },

      onAuthorize: function (data, actions) {
        return actions.payment.execute().then(function (payment) {
          that.gateWayTransactionDetails = payment;
          that.placeOrderWithPaypal();
        });
      },
      onCancel: function (data, actiona) {
        swal({
          title: 'Payment Cancelled',
          text: 'Order could not be placed since user cancelled the transaction',
          type: 'info',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        });
      }

    }, '#paypal-button-container');
  }

  // places order once paypal transaction is successful
  private placeOrderWithPaypal() {
    let orderDetails = {
      deliveryCharge: this.deliveryInfo.freeDelivery === true ? (this.grandTotal >= this.deliveryInfo.amountEligibility ? 'free' : this.deliveryInfo.deliveryCharges) : this.deliveryInfo.deliveryCharges,
      shippingAddress: this.selectedAddress,
      restaurantID: this.locationInformation.restaurantID._id,
      restaurantName: this.locationInformation.restaurantID.restaurantName,
      location: this.locationInformation._id,
      locationName: this.locationInformation.locationName,
      grandTotal: this.grandTotal,
      subTotal: this.subTotal,
      charges: this.taxInfo.taxRate,
      productDetails: JSON.parse(localStorage.getItem('cart')),
      status: "Pending",
      coupon: {
        couponApplied: this.couponDetails !== null ? true : false,
        offPrecentage: this.couponDetails !== null ? this.couponDetails.offPrecentage : 0,
        couponName: this.couponDetails !== null ? this.couponDetails.couponName : null
      },
      loyaltyData: {
        isApplied: this.loyaltyStatus,
        loyaltyPoints: this.loyaltyStatus === true ? this.loyaltyPoints : 0,
      },
      payableAmount: this.grandTotal,
      orderType: 'Home Delivery',
      position: {
        lat: 12.917096599999999,
        long: 77.58980509999999,
        name: "Current Position"
      },
      paymentOption: 'PayPal',
      payment: {
        transactionId: this.gateWayTransactionDetails.id,
        paymentType: 'PayPal',
        paymentStatus: true
      }
    }
    this.restService.placeOrder(orderDetails).subscribe(res => {
      swal({
        title: 'Success',
        text: 'commande passée successfully',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
      localStorage.removeItem('cart');
      localStorage.removeItem('taxInfo');
      localStorage.removeItem('subTotal');
      localStorage.removeItem('grandTotal');
      localStorage.removeItem('locationInformation');
      localStorage.removeItem('restaurantDeliveryInfo');
      localStorage.removeItem('address');
      localStorage.removeItem('couponDetails');
      localStorage.removeItem('couponApplied');
      localStorage.removeItem('restaurantID');
      localStorage.removeItem('loyaltyApplied');
      localStorage.removeItem('loyaltyPoints');
      this.store.dispatch(new CartActions.AddToCart({ cart: [] }));
      this.router.navigate(['thank_you-page']);
      this.playAudio();
    }, error => {
      swal({
        title: 'Error',
        text: error.error.message,
        type: 'error',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    });
  }

  // process card details using stripe as payment gateway
  public stripePayment() {
    this.loading = true;
    this.stripeCardInfo.month = Number(this.stripeCardInfo.month);
    this.stripeCardInfo.year = Number(this.stripeCardInfo.year);
    this.stripeCardInfo.cvc = Number(this.stripeCardInfo.cvc);
    this.stripeCardInfo.isSaved = confirm('Do you want to save card details');
    this.restService.submitCardDetails(this.stripeCardInfo).subscribe(res => {
      const body = {
        amount: this.grandTotal
      }
      this.restService.finishStripePayment(body).subscribe(res => {
        this.gateWayTransactionDetails = res;
        this.placeOrderWithStripe();
      })
    }, error => {
      swal({
        title: 'Error',
        text: error.error.message,
        type: 'error',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    });
  }

  // places order when stripe payment is completed
  private placeOrderWithStripe() {
    let orderDetails = {
      deliveryCharge: this.deliveryInfo.freeDelivery === true ? (this.grandTotal >= this.deliveryInfo.amountEligibility ? 'free' : this.deliveryInfo.deliveryCharges) : this.deliveryInfo.deliveryCharges,
      shippingAddress: this.selectedAddress,
      restaurantID: this.locationInformation.restaurantID._id,
      restaurantName: this.locationInformation.restaurantID.restaurantName,
      location: this.locationInformation._id,
      locationName: this.locationInformation.locationName,
      grandTotal: this.grandTotal,
      subTotal: this.subTotal,
      charges: this.taxInfo.taxRate,
      productDetails: JSON.parse(localStorage.getItem('cart')),
      status: "Pending",
      coupon: {
        couponApplied: this.couponDetails !== null ? true : false,
        offPrecentage: this.couponDetails !== null ? this.couponDetails.offPrecentage : 0,
        couponName: this.couponDetails !== null ? this.couponDetails.couponName : null
      },
      loyaltyData: {
        isApplied: this.loyaltyStatus,
        loyaltyPoints: this.loyaltyStatus === true ? this.loyaltyPoints : 0,
      },
      payableAmount: this.grandTotal,
      orderType: 'Home Delivery',
      position: {
        lat: 12.917096599999999,
        long: 77.58980509999999,
        name: "Current Position"
      },
      paymentOption: 'Stripe',
      payment: {
        transactionId: this.gateWayTransactionDetails.transactionId,
        paymentType: 'Stripe',
        paymentStatus: true
      }
    }
    this.restService.placeOrder(orderDetails).subscribe(res => {
      swal({
        title: 'Success',
        text: 'commande passée successfully',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
      this.loading = false;
      localStorage.removeItem('cart');
      localStorage.removeItem('taxInfo');
      localStorage.removeItem('subTotal');
      localStorage.removeItem('grandTotal');
      localStorage.removeItem('locationInformation');
      localStorage.removeItem('restaurantDeliveryInfo');
      localStorage.removeItem('address');
      localStorage.removeItem('couponDetails');
      localStorage.removeItem('couponApplied');
      localStorage.removeItem('restaurantID');
      localStorage.removeItem('loyaltyApplied');
      localStorage.removeItem('loyaltyPoints');
      this.store.dispatch(new CartActions.AddToCart({ cart: [] }));
      this.router.navigate(['thank_you-page']);
      this.playAudio();
    }, error => {
      swal({
        title: 'Error',
        text: error.error.message,
        type: 'error',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    });
  }

  // this method is called when user selects a card
  public selectCardEvent(event) {
    this.isCardSelected = true;
    this.selectedCardDetails = this.savedUserCards.find(card => card.lastFourDigit === event.target.value);
    this.selectedCardIndex = this.savedUserCards.findIndex(card => card.lastFourDigit === event.target.value);
  }

  // this method is called when user deletes saved card
  public deleteCard(form: NgForm) {
    const body = {
      index: this.selectedCardIndex,
      userId: localStorage.getItem('id')
    };
    swal({
      title: 'Are you sure?',
      text: "Do you want to delete this card",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.restService.deleteCard(body).subscribe(res => {
          swal({
            title: 'Success',
            text: 'Card deleted successfully',
            type: 'success',
            background: '#fff',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          }).then(() => {
          }, () => {
          });
          form.reset();
          this.getUserSavedCards();
        }, error => {
          swal({
            title: 'Error',
            text: error.error.message,
            type: 'error',
            background: '#fff',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          }).then(() => {
          }, () => {
          });
        });
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
      }
    });
  }

  // places order with saved card
  public placeOrderBySavedCard(form: NgForm) {
    this.loading = true;
    const cardData = {
      userId: localStorage.getItem('id'),
      index: this.selectedCardIndex,
      cvc: Number(form.value.cvc),
      amount: this.grandTotal
    }
    this.restService.submitSavedCardDetails(cardData).subscribe((res: any) => {
      if (res.errorMessage) return this.toastr.error(res.errorMessage, 'Error', { timeOut: 3000 });
      this.gateWayTransactionDetails = res;
      let orderDetails = {
        deliveryCharge: this.deliveryInfo.freeDelivery === true ? (this.grandTotal >= this.deliveryInfo.amountEligibility ? 'free' : this.deliveryInfo.deliveryCharges) : this.deliveryInfo.deliveryCharges,
        shippingAddress: this.selectedAddress,
        restaurantID: this.locationInformation.restaurantID._id,
        restaurantName: this.locationInformation.restaurantID.restaurantName,
        location: this.locationInformation._id,
        locationName: this.locationInformation.locationName,
        grandTotal: this.grandTotal,
        subTotal: this.subTotal,
        charges: this.taxInfo.taxRate,
        productDetails: JSON.parse(localStorage.getItem('cart')),
        status: "Pending",
        coupon: {
          couponApplied: this.couponDetails !== null ? true : false,
          offPrecentage: this.couponDetails !== null ? this.couponDetails.offPrecentage : 0,
          couponName: this.couponDetails !== null ? this.couponDetails.couponName : null
        },
        loyaltyData: {
          isApplied: this.loyaltyStatus,
          loyaltyPoints: this.loyaltyStatus === true ? this.loyaltyPoints : 0,
        },
        payableAmount: this.grandTotal,
        orderType: 'Home Delivery',
        position: {
          lat: 12.917096599999999,
          long: 77.58980509999999,
          name: "Current Position"
        },
        paymentOption: 'Stripe',
        payment: {
          transactionId: this.gateWayTransactionDetails.transactionId,
          paymentType: 'Stripe',
          paymentStatus: true
        }
      }
      this.restService.placeOrder(orderDetails).subscribe(res => {
        swal({
          title: 'Success',
          text: 'commande passée successfully',
          type: 'success',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => {
        }, () => {
        });
        this.loading = false;
        localStorage.removeItem('cart');
        localStorage.removeItem('taxInfo');
        localStorage.removeItem('subTotal');
        localStorage.removeItem('grandTotal');
        localStorage.removeItem('locationInformation');
        localStorage.removeItem('restaurantDeliveryInfo');
        localStorage.removeItem('address');
        localStorage.removeItem('couponDetails');
        localStorage.removeItem('couponApplied');
        localStorage.removeItem('restaurantID');
        localStorage.removeItem('loyaltyApplied');
        localStorage.removeItem('loyaltyPoints');
        this.store.dispatch(new CartActions.AddToCart({ cart: [] }));
        this.router.navigate(['thank_you-page']);
        this.playAudio();
      }, error => {
        swal({
          title: 'Error',
          text: error.error.message,
          type: 'error',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => {
        }, () => {
        });
      });
    }, error => {
      swal({
        title: 'Error',
        text: error.error.message,
        type: 'error',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    });
  }

  // closes update address modal
  public closeUpdateAddressModal() {
    this.modalRef.close();
    this.updateUpdateForm.reset();
    this.getUserAddresses();
  }

  // dismisses modal with reason
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // plays sound when order is placed
  private playAudio() {
    let audio = new Audio('../../../../../assets/sound/sound.mp3');
    audio.play();
  }

}
