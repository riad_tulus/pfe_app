import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { RestaurantDetailsService } from '../restaurant-details.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import * as CartActions from '../../store/cart.action';
import * as fromCart from '../../store/cart.reducer';

@Component({
  selector: 'app-restaurant-page',
  templateUrl: './restaurant-page.component.html',
  styleUrls: ['./restaurant-page.component.css'],
  providers: [RestaurantDetailsService]
})
export class RestaurantPageComponent implements OnInit {
  modelReference: NgbModalRef;
  public loading: boolean = true;  // displays loader when true
  public locationId: string;  // contains location id from route
  public locationDetails: any = {};
  private cartObservable: Observable<fromCart.State>;  //
  public cart: Array<any>;  //
  public restaurantProducts: any = {};  // stores restaurants products of a location
  public closeResult: string;  // used to close model
  public selectedProduct: any = {};  // contains selected product
  public selectedQuantity: number = 1;  // contains selected quantity
  private quantitySelected: boolean = false;  // set to true if quantity is changed
  public grandTotal: number = 0;  // contains grand total
  private initialTotal: number = 0;  // contains product price
  private selectedVariant: any = null;  // contains selected variant information
  private variantSelected: boolean = false;  // checks whether user has selected a variant or not
  private selectedIngredient: any = null;   // contains the selected ingredient
  private extraIngredientSelected: boolean = false;  // checks whether user has selected the ingredient or not
  public isFavorite: boolean; // checks whether user has added the product to wishlist or not
  private wishlistData: any = {};  // contains wishlist data
  public isRestaurantOpen: boolean;   // checks whether restaurant is open or not
  public message: string;  // displays message if restaurant is closes
  private weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];  // contains all days ina week
  public timeSchedule: Array<any>;  // contains restaurant timings

  constructor(private route: ActivatedRoute,
    private modalService: NgbModal,
    private restService: RestaurantDetailsService,
    private toastr: ToastrService, private store: Store<fromCart.CartState>, private router: Router) {
    route.params.map(params => params.id).subscribe(Id => {
      if (Id !== undefined && Id !== null) {
        this.locationId = Id;    // contains location id from route
        this.getRestaurantsProducts(this.locationId);   // gets products based on location id
        this.getLocationInformation(this.locationId);
      }
    });
    this.cartObservable = this.store.select('cart');
    this.cartObservable.subscribe(cart => {
      this.cart = cart.cart;
    });
  }

  ngOnInit() {
  }

  // called from constructor to get restaurant products
  private getRestaurantsProducts(id: string) {
    this.restService.getRestaurantProductsByLocation(id).subscribe((res: any) => {
      this.restaurantProducts = res;
      console.log("REst Product" + JSON.stringify(res));
      this.loading = false;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // gets location information by location id
  private getLocationInformation(id: string) {
    this.restService.getLocationDetails(id).subscribe(data => {
      this.locationDetails = data;
      let today = this.weekDays[new Date().getDay() - 1];
      let hour = new Date().getHours();
      let minutes = new Date().getMinutes();
      if (this.locationDetails.workingHours !== undefined) {
        if (this.locationDetails.workingHours.isAlwaysOpen === true) {
          this.isRestaurantOpen = true;
          this.message = 'Restaurant is open';
        } else if (this.locationDetails.workingHours.isAlwaysOpen === false) {
          let indexOfToday = this.locationDetails.workingHours.daySchedule.findIndex(day => day.day === today);
          if (indexOfToday >= 0) {
            this.timeSchedule = this.locationDetails.workingHours.daySchedule[indexOfToday].timeSchedule;
            if (this.timeSchedule !== null && this.timeSchedule !== undefined) {
              this.timeSchedule.forEach(time => {
                if (hour >= Number(time.openTime.split(':')[0]) && hour <= Number(time.closingTime.split(':')[0])) {
                  if (minutes > 0 && hour < Number(time.closingTime.split(':')[0])) {
                    this.isRestaurantOpen = true;
                    this.message = 'Restaurant is open';
                  } else {
                    this.isRestaurantOpen = false;
                    this.message = 'Restaurant is closed';
                  }
                } else {
                  this.isRestaurantOpen = false;
                  this.message = 'Restaurant is closed';
                }
              });
            } else {
              this.isRestaurantOpen = true;
              this.message = 'Restaurant is open';
            }
          } else {
            this.isRestaurantOpen = true;
            this.message = 'Restaurant is open';
          }
        }
      } else {
        this.isRestaurantOpen = true;
        this.message = 'Restaurant is open';
      }
      localStorage.setItem('restaurantID', this.locationDetails.restaurantID._id);
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // opens product modal
  public open(content, product) {
    if (this.isRestaurantOpen) {
      this.selectedProduct = product;
      console.log(this.selectedProduct);
      this.grandTotal = this.selectedProduct.variants[0].price === this.selectedProduct.variants[0].MRP ? this.selectedProduct.variants[0].MRP : this.selectedProduct.variants[0].price;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
      this.initialTotal = this.grandTotal;
      this.getWishlistData();
      this.modelReference = this.modalService.open(content, { size: 'lg' });
      this.modelReference.result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    } else {
      swal({
        title: 'Closed',
        text: 'Restaurant is closed',
        type: 'info',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    }
  }

  // gets wishlist data
  private getWishlistData() {
    let auth = localStorage.getItem('auth');
    if (auth === 'yes') {
      const body = {
        location: this.locationId,
        product: this.selectedProduct._id
      };
      this.restService.getWishlistData(body).subscribe(res => {
        this.wishlistData = res;
        this.isFavorite = this.wishlistData === null ? false : true;
      }, error => console.log(error.error.message));
    } else {
      this.isFavorite = false;
    }
  }

  // this method is called when user selects a variant
  public selectVariantEvent(event) {
    this.selectedVariant = this.selectedProduct.variants.find(data => {
      return data._id === event.target.value;
    });
    this.variantSelected = true;
    this.initialTotal = this.selectedVariant.price === this.selectedVariant.MRP ? this.selectedVariant.MRP : this.selectedVariant.price;
    if (!this.quantitySelected && !this.extraIngredientSelected) {
      this.grandTotal = this.initialTotal;
    } else if (this.quantitySelected && !this.extraIngredientSelected) {
      this.grandTotal = this.initialTotal;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
    } else if (this.quantitySelected && this.extraIngredientSelected) {
      this.initialTotal = this.initialTotal + this.selectedIngredient.price;
      this.grandTotal = this.initialTotal;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
    }

  }

  // this method is called when user selects quantity
  public quantityChangedEvent(event) {
    this.quantitySelected = true;
    this.selectedQuantity = event.target.value;
    if (!this.extraIngredientSelected) {
      this.grandTotal = this.initialTotal;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
    } else {
      this.grandTotal = this.initialTotal;
      this.grandTotal = this.grandTotal * this.selectedQuantity;
    }
  }

  // this method is called when user adds extra ingredient
  public extraIngredientsEvent(event) {
    this.selectedIngredient = this.selectedProduct.extraIngredients.find(data => {
      return data.name === event.target.value;
    });
    this.extraIngredientSelected = true;
    if (!this.variantSelected) {
      this.grandTotal = this.initialTotal;
      this.grandTotal = (this.grandTotal + this.selectedIngredient.price) * this.selectedQuantity;
    } else {
      this.grandTotal = this.initialTotal;
      this.grandTotal = (this.grandTotal + this.selectedIngredient.price) * this.selectedQuantity;
      this.initialTotal = this.grandTotal;
    }
  }

  // adds a menu item to wishlist
  public addToWishlist() {
    let auth = localStorage.getItem('auth');
    if (auth === 'yes') {
      const wishlistData = {
        user: localStorage.getItem('id'),
        restaurantID: this.restaurantProducts.restaurant._id,
        location: this.locationId,
        product: this.selectedProduct._id
      };
      this.restService.addToWishlist(wishlistData).subscribe(res => {
        swal({
          title: 'Cool',
          text: this.selectedProduct.title + ' added to wishlist',
          type: 'success',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => {
        }, () => {
        });
        this.getWishlistData();
      }, error => {
        swal({
          title: 'Error',
          text: error.error.message,
          type: 'error',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => {
        }, () => {
        });
      });
    } else {
      swal({
        title: 'Authentication Required',
        text: 'Please login to add item to wishlist',
        type: 'info',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
      this.closeProductModel();
      this.router.navigate(['login']);
    }

  }

  // removes menu item from wishlist
  public removeFromWishlist() {
    this.restService.removeFromWishlist(this.wishlistData._id).subscribe(res => {
      swal({
        title: 'Success',
        text: this.selectedProduct.title + ' removed from wishlist',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      });
      this.getWishlistData();
    }, error => {
      swal({
        title: 'Error',
        text: error.error.message,
        type: 'error',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      });
    });
  }

  // this method is called when user adds item to cart
  public addToCart() {
    if (this.selectedVariant === null) {
      return swal({
        title: 'Size not selected',
        text: 'Please select a size',
        type: 'info',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
    }
    if (this.cart.length === 0) {
      let name = this.selectedProduct.title;
      let productDetails = {
        totalPrice: this.grandTotal,
        restaurant: this.locationDetails.restaurantID.restaurantName,
        title: this.selectedProduct.title,
        brand: this.selectedProduct.brand,
        productId: this.selectedProduct._id,
        Quantity: Number(this.selectedQuantity),
        imageUrl: this.selectedProduct.imageUrl,
        restaurantID: this.locationDetails.restaurantID._id,
        extraIngredients: this.selectedIngredient !== null ? this.selectedIngredient : null,
        variant: this.selectedVariant,
        location: this.locationId,
        MRP: this.selectedVariant.MRP,
        price: this.selectedVariant.price,
        Discount: this.selectedVariant.Discount
      };
      localStorage.setItem('restaurantDeliveryInfo', JSON.stringify(this.locationDetails.deliveryInfo.deliveryInfo));
      localStorage.setItem('taxInfo', JSON.stringify(this.restaurantProducts.restaurant.taxInfo));
      let cart: Array<any> = [];
      cart.push(productDetails);
      localStorage.setItem('cart', JSON.stringify(cart));
      this.store.dispatch(new CartActions.AddToCart({ cart: cart }));
      this.closeProductModel();
      swal({
        title: 'Cool',
        text: name + ' added to cart',
        type: 'success',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      }).catch(() => {
      });
    } else {
      let index = this.cart.findIndex(data => data.productId === this.selectedProduct._id);
      if (index >= 0) {
        let cart: Array<any> = JSON.parse(localStorage.getItem('cart'));
        cart.splice(index, 1);
        let name = this.selectedProduct.title;
        let productDetails = {
          totalPrice: this.grandTotal,
          restaurant: this.locationDetails.restaurantID.restaurantName,
          title: this.selectedProduct.title,
          brand: this.selectedProduct.brand,
          productId: this.selectedProduct._id,
          Quantity: Number(this.selectedQuantity),
          imageUrl: this.selectedProduct.imageUrl,
          restaurantID: this.locationDetails.restaurantID._id,
          extraIngredients: this.selectedIngredient !== null ? this.selectedIngredient : null,
          variant: this.selectedVariant,
          location: this.locationId,
          MRP: this.selectedVariant.MRP,
          price: this.selectedVariant.price,
          Discount: this.selectedVariant.Discount
        };
        localStorage.setItem('restaurantDeliveryInfo', JSON.stringify(this.locationDetails.deliveryInfo.deliveryInfo));
        localStorage.setItem('taxInfo', JSON.stringify(this.restaurantProducts.restaurant.taxInfo));
        cart.push(productDetails);
        localStorage.setItem('cart', JSON.stringify(cart));
        this.store.dispatch(new CartActions.UpdateCart({ id: this.selectedProduct._id, cart: productDetails }));
        this.closeProductModel();
        swal({
          title: 'Cool',
          text: ' Cart item updated',
          type: 'success',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => {
        }, () => {
        }).catch(() => {
        });
      } else {
        let name = this.selectedProduct.title;
        let cart: Array<any> = JSON.parse(localStorage.getItem('cart'));
        let productDetails = {
          totalPrice: this.grandTotal,
          restaurant: this.locationDetails.restaurantID.restaurantName,
          title: this.selectedProduct.title,
          brand: this.selectedProduct.brand,
          productId: this.selectedProduct._id,
          Quantity: Number(this.selectedQuantity),
          imageUrl: this.selectedProduct.imageUrl,
          restaurantID: this.locationDetails.restaurantID._id,
          extraIngredients: this.selectedIngredient !== null ? this.selectedIngredient : null,
          variant: this.selectedVariant,
          location: this.locationId,
          MRP: this.selectedVariant.MRP,
          price: this.selectedVariant.price,
          Discount: this.selectedVariant.Discount
        };
        localStorage.setItem('restaurantDeliveryInfo', JSON.stringify(this.locationDetails.deliveryInfo.deliveryInfo));
        localStorage.setItem('taxInfo', JSON.stringify(this.restaurantProducts.restaurant.taxInfo));
        cart.push(productDetails);
        localStorage.setItem('cart', JSON.stringify(cart));
        this.store.dispatch(new CartActions.AddToCart({ cart: cart }));
        this.closeProductModel();
        swal({
          title: 'Cool',
          text: name + ' added to cart',
          type: 'success',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        }).then(() => {
        }, () => {
        }).catch(() => {
        });
      }
    }
  }

  // dismisses the product modal with reason
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // closes product modal
  public closeProductModel() {
    this.modelReference.close();
    this.selectedProduct = null;
    this.selectedQuantity = 1;
    this.selectedVariant = null;
    this.selectedIngredient = null;
    this.extraIngredientSelected = false;
    this.variantSelected = false;
    this.grandTotal = 0;
    this.isFavorite = false;
    this.initialTotal = 0;
    this.quantitySelected = false;
  }

  // navigates to checkout page
  public goToCheckout() {
    let auth = localStorage.getItem('auth');
    if (auth !== 'yes') {
      swal({
        title: 'Sorry',
        text: 'Pleae login',
        type: 'info',
        background: '#fff',
        showConfirmButton: false,
        width: 300,
        timer: 3000
      }).then(() => {
      }, () => {
      });
      localStorage.setItem('locationInformation', JSON.stringify(this.locationDetails));
      this.router.navigate(['login']);
      localStorage.setItem('fromCart', 'yes');

    } else {
      localStorage.setItem('locationInformation', JSON.stringify(this.locationDetails));
      this.router.navigate(['checkout-page']);
    }
  }

}
