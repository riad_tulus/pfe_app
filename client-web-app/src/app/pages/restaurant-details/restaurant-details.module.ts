import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RestaurantPageComponent} from './restaurant-page/restaurant-page.component';


import {PopularRestaurantsComponent} from './popular-restaurants/popular-restaurants.component';
import {CheckoutPageComponent} from './restaurant-page/checkout-page/checkout-page.component';
import {CheckoutComponent} from '../common-side-menu/checkout/checkout.component';
import {ViewAllRestaurantsComponent} from './view-restaurants/view-all-restaurants/view-all-restaurants.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthGuardService} from '../../services/auth-guard.service';


export const routes: Routes = [
  {path: 'restaurant-page/:id', component: RestaurantPageComponent},

  {path: 'restaurants', component: PopularRestaurantsComponent},

  {path: 'checkout-page', component: CheckoutPageComponent, canActivate: [AuthGuardService]},
  {path: 'view-all-restaurants', component: ViewAllRestaurantsComponent}

];

@NgModule({
  imports: [
    NgbModule.forRoot(),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],

  declarations: [
    RestaurantPageComponent,


    PopularRestaurantsComponent,

    CheckoutPageComponent,
    CheckoutComponent,
    ViewAllRestaurantsComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class RestaurantDetailsModule {
}
