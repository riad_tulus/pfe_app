import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { RestaurantDetailsService } from '../../restaurant-details.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';
import * as CartActions from '../../../store/cart.action';
import * as fromCart from '../../../store/cart.reducer';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-view-all-restaurants',
  templateUrl: './view-all-restaurants.component.html',
  styleUrls: ['./view-all-restaurants.component.css'],
  providers: [RestaurantDetailsService]
})
export class ViewAllRestaurantsComponent implements OnInit {
  @ViewChild('resTypeFil') resTypeFil: ElementRef;
  public loading: boolean = true;  // displays loader when true
  public restaurants: Array<any>;  // contains all the restaurants
  public cuisines: Array<any>;  // contains all cuisines
  private cartObservable: Observable<fromCart.State>;  // subscribes to changes to cart
  private cart: Array<any>;  // contains cart data
  public restaurantsTypes: Array<any>;  // contains types of restaurant
  private cuisineId: Array<any> = [];  // contains cuisine id's for sorting restaurant by cuisine id
  public isRestaurantTypeSelecetd: boolean = false;  // checks whether restaurant type is selected
  public isCuisineSelected: boolean = false;  // checks whether cuisine is selected or not
  constructor(private restService: RestaurantDetailsService, private toastr: ToastrService, private config: NgbRatingConfig, private router: Router, private store: Store<fromCart.CartState>) {
    this.cartObservable = this.store.select('cart');
    this.cartObservable.subscribe(cart => {
      this.cart = cart.cart;
    });
    config.max = 5;
    config.readonly = true;
    this.getAllRestaurants();
    this.getCuisines();
    this.getAllRestaurantsTypes();
  }

  ngOnInit() {
  }

  // called from constructor to get all restaurants list
  private getAllRestaurants() {
    this.loading = true;
    this.restService.getAllRestaurants().subscribe((res: any) => {
      this.restaurants = res;
      this.loading = false;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  public getAllRestaurantsTypes() {
    this.restaurantsTypes = [
      {
        restaurantType: 'Veg'
      },
      {
        restaurantType: 'Non-Veg'
      },
      {
        restaurantType: 'Both'
      }
    ];
  }

  // called from constructor to get all cuisines
  private getCuisines() {
    this.restService.getCuisines().subscribe((res: any) => {
      this.cuisines = res;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // filter restaurant by cuisines
  public filterByCuisine(event, cuisine, index) {
    if (event.target.checked) {
      this.isCuisineSelected = true;
      if (this.isRestaurantTypeSelecetd) {
        this.isRestaurantTypeSelecetd = false;
        this.getAllRestaurantsTypes();
      }
      this.cuisineId.push(cuisine._id);
      let body = {
        cuisineId: this.cuisineId
      };
      this.restService.filterByCuisines(body).subscribe((res: any) => {
        this.restaurants = (!res.message) ? res : [];
      }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
    } else {
      if (this.cuisineId.length > 1) {
        this.cuisineId.splice(index, 1);
        let body = {
          cuisineId: this.cuisineId
        };
        this.restService.filterByCuisines(body).subscribe((res: any) => {
          this.restaurants = (!res.message) ? res : [];
        }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
      } else {
        this.cuisineId = [];
        this.getAllRestaurants();
      }
    }
  }

  // filters restaurants by restauranttype
  public filterByRestaurantType(event, type) {
    if (this.isCuisineSelected) {
      this.cuisineId = [];
      this.getCuisines();
    }
    this.isRestaurantTypeSelecetd = true;
    this.loading = true;
    let data = {
      restaurantType: type.restaurantType
    };
    this.restService.filterByRestaurantType(data).subscribe((res: any) => {
      this.loading = false;
      this.restaurants = res;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // clears restaurant type filters
  public resetRestaurantTypes() {
    this.isRestaurantTypeSelecetd = false;
    this.getAllRestaurants();
    this.getAllRestaurantsTypes();
  }

  // navigates to restaurant page
  public viewRestaurant(id: string) {
    let locationId = localStorage.getItem('locationId');
    // checks whether location was selected or not and performs specifed actions based on location id
    if (locationId === null) {
      localStorage.setItem('locationId', id);
      this.router.navigate(['restaurant-page/' + id]);
    } else {
      if (locationId === id) {
        this.router.navigate(['restaurant-page/' + id]);
      } else {
        if (this.cart.length === 0) {
          localStorage.setItem('locationId', id);
          this.router.navigate(['restaurant-page/' + id]);
        } else {
          swal({
            title: 'Are you sure?',
            text: 'The restaurant you have selected is of different location, Your cart will be cleared',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
              this.cart = [];
              this.store.dispatch(new CartActions.AddToCart({ cart: this.cart }));
              localStorage.removeItem('cart');
              localStorage.removeItem('taxInfo');
              localStorage.removeItem('subTotal');
              localStorage.removeItem('grandTotal');
              localStorage.removeItem('locationInformation');
              localStorage.removeItem('restaurantDeliveryInfo');
              localStorage.removeItem('address');
              localStorage.removeItem('couponDetails');
              localStorage.removeItem('couponApplied');
              localStorage.removeItem('restaurantID');
              localStorage.removeItem('loyaltyApplied');
              localStorage.removeItem('loyaltyPoints');
              localStorage.setItem('locationId', id);
              this.router.navigate(['restaurant-page/' + id]);
            } else if (
              // Read more about handling dismissals
              result.dismiss === swal.DismissReason.cancel
            ) {
              swal({
                title: 'Cancelled',
                text: 'Your cart item is safe',
                type: 'info',
                background: '#fff',
                showConfirmButton: false,
                width: 300,
                timer: 2000
              }).then(() => {
              }, () => {
              });
            }
          });
        }
      }
    }
  }

}
