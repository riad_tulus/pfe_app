import { Component, OnInit } from '@angular/core';
import { RestaurantDetailsService } from '../restaurant-details.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-popular-restaurants',
  templateUrl: './popular-restaurants.component.html',
  styleUrls: ['./popular-restaurants.component.css'],
  providers: [RestaurantDetailsService]
})
export class PopularRestaurantsComponent implements OnInit {
  public restaurants: Array<any>;
  constructor(private restService: RestaurantDetailsService, private toastr: ToastrService) {
    this.getRestaurants();
  }

  ngOnInit() {
  }

  // gets popular restaurants
  private getRestaurants() {
    this.restService.getPopularRestaurants().subscribe((res: any) => {
      this.restaurants = res;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  popular = [
    {
      img: "assets/img/chick.jpg",
      title: "Barbeque",
      name: "WOW Kitchen",
      place: "BTM",
      time: "20-30 min"
    }
  ];

}
