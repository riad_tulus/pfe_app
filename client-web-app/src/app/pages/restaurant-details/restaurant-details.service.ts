import {Injectable} from '@angular/core';
import {CrudService} from '../../services/crud.service';

@Injectable()
export class RestaurantDetailsService {
  constructor(private crud: CrudService) {
  }

  // returns restaurants products by location id
  public getRestaurantProductsByLocation(id: string) {
    return this.crud.getOne('locations/all/category/data', id);
  }

  // returns coupons by location id
  public getCoupons(id: string) {
    return this.crud.getOne('coupons', id);
  }

  // returns all restaurants
  public getAllRestaurants() {
    return this.crud.get('locations/get/all/restaurant/list');
  }

  // filters restaurant by type
  public filterByRestaurantType(data) {
    return this.crud.filterRestaurants('locations/by/restaurant/type', data);
  }

  // filters restaurant by cuisines
  public filterByCuisines(data) {
    return this.crud.filterRestaurants('cuisineLocations/by/cuisins', data);
  }


  // returns all cuisines
  public getCuisines() {
    return this.crud.get('cuisines');
  }

  // returns user details
  public getUserDetails() {
    return this.crud.getUserDetails('users/me');
  }

  //returns popular restaurants
  public getPopularRestaurants() {
    return this.crud.get('locations/toprated/all/restaurant/list');
  }

  // returns location data by location id
  public getLocationDetails(id: string) {
    return this.crud.getOne('locations', id);
  }

  // returns restaurant settings
  public getRestaurantSettings(id: string) {
    return this.crud.getOne('settings', id);
  }

  // returns delivery addresses of user
  public getUserAddresses() {
    return this.crud.getUserDetails('users/newaddress/address');
  }

  //returns wishlist based on product id and location id
  public getWishlistData(data) {
    return this.crud.post('favourites/user/fav/location/product', data);
  }

  // returns all saved cards of user
  public getSavedCards() {
    let id = localStorage.getItem('id');
    return this.crud.getOne('users/card/info/data', id);
  }

  //adds product to favorites
  public addToWishlist(data) {
    return this.crud.post('favourites', data);
  }

  //removes from wishlist
  public removeFromWishlist(id: string) {
    return this.crud.delete('favourites', id);
  }

  // submits card details
  public submitCardDetails(data) {
    return this.crud.post('users/stripe/card/info', data);
  }

  // processes stripe payment
  public finishStripePayment(amount) {
    return this.crud.post('users/stripe/payment', amount);
  }

  // process paymant by saved card
  public submitSavedCardDetails(data) {
    return this.crud.post('users/savedcard/stripe/payment', data);
  }

  // saves new address
  public saveUserAddress(data) {
    return this.crud.post('users/add/address', data);
  }

  // updates user address
  public updateAddress(index, data: any) {
    return this.crud.put('users/update/address', index, data);
  }

  // delete user order
  public deleteAddress(index) {
    return this.crud.delete('users/address', index);
  }

  // deletes user's card
  public deleteCard(data) {
    return this.crud.post('users/savedcard/delete', data);
  }

  // places order
  public placeOrder(data) {
    return this.crud.post('orders', data);
  }
}
