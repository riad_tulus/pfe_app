import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as fromCart from '../../store/cart.reducer';
import * as CartActions from '../../store/cart.action';
import swal from 'sweetalert2';
import { RestaurantDetailsService } from '../../restaurant-details/restaurant-details.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
  providers: [RestaurantDetailsService]
})
export class CheckoutComponent implements OnInit {
  private cartObservable: Observable<fromCart.State>;   // listens to changes in cart state
  public cart: Array<any>;  // contains the cart information
  public cartCopy: Array<any>;  // contains copy of cart items
  public subtotal: number = 0;  // contains subtotal of cart items
  public taxInfo: any = {};  // contains restaurant tax details
  public grandTotal: number;  // contains grand total or user payable amount
  public deliveryInformation: any = {};   // contains restaurant delivery information
  public deliveryCharges: number;  // contains delivery charges of restaurant
  public coupons: Array<any>  // contains list of coupons of a particular restaurant
  private selectedCoupon: any = {};  // contains the coupon applied by user
  public isCouponApplied: boolean = false;  // checks whether user has applied coupon or not
  private initialTotal: number = 0;  // contains copy of grand total , used when user changes coupon;
  private restaurantId: string;  // contains the restaurant id
  public restaurantSettings: any = {}; // contains the restaurant settings
  public isLoyaltyApplied: boolean = false;  // checks whether loyalty is applied or not
  public userDetails: any = {};   // contains user details
  public isAuthenticated: boolean = false;  // checks whether user is authenticated or not
  private appliedPoints: number; // contains number of loyalty points used
  constructor(private store: Store<fromCart.CartState>, private restService: RestaurantDetailsService, private toastr: ToastrService) {
    this.cartObservable = this.store.select('cart');  // listens to cart state
    this.cartObservable.subscribe(cart => {
      // subscribes the changes everytime cart is updated
      this.cart = cart.cart;
      this.cartCopy = this.cart;
      this.subtotal = 0;
      this.grandTotal = 0;
      this.cart.forEach(item => {
        this.subtotal += item.totalPrice;
        localStorage.setItem('subTotal', this.subtotal.toString());
      });
      this.taxInfo = JSON.parse(localStorage.getItem('taxInfo'));
      console.log("TaxInfo" + JSON.stringify(this.taxInfo));
      console.log("Delivery Info" + JSON.stringify(this.deliveryInformation));
      this.deliveryInformation = JSON.parse(localStorage.getItem('restaurantDeliveryInfo'));
      if (this.taxInfo !== null && this.deliveryInformation.freeDelivery !== undefined) {
        this.grandTotal = this.subtotal + this.taxInfo.taxRate;
        this.initialTotal = this.grandTotal;
        localStorage.setItem('grandTotal', this.grandTotal.toString());
        if (this.deliveryInformation.freeDelivery === true) {
          if (this.grandTotal >= this.deliveryInformation.amountEligibility) {
            this.deliveryCharges = 0;
          } else {
            this.deliveryCharges = Number(this.deliveryInformation.deliveryCharges);
            this.grandTotal = this.grandTotal + this.deliveryCharges;
            this.initialTotal = this.grandTotal;
            localStorage.setItem('grandTotal', this.grandTotal.toString());
          }
        } else {
          this.deliveryCharges = Number(this.deliveryInformation.deliveryCharges);
          this.grandTotal = this.grandTotal + this.deliveryCharges;
          this.initialTotal = this.grandTotal;
          localStorage.setItem('grandTotal', this.grandTotal.toString());
        }

      }
      // else {
      //   // here this set as default value for tax
      //   this.taxInfo = Object.assign({ taxName: null, taxRate: null });
      //   this.taxInfo.taxName = 'VAT';
      //   this.taxInfo.taxRate = 10;
      // }
      if (localStorage.getItem('couponApplied') !== null) {
        this.isCouponApplied = true;
        this.selectedCoupon = JSON.parse(localStorage.getItem('couponDetails'));
        this.initialTotal = this.grandTotal;
        this.grandTotal = this.grandTotal - (this.grandTotal * (this.selectedCoupon.offPrecentage / 100));
        localStorage.setItem('grandTotal', this.grandTotal.toString());
      }

      if (localStorage.getItem('loyaltyApplied') !== null) {
        this.isLoyaltyApplied = true;
        this.appliedPoints = Number(localStorage.getItem('loyaltyPoints'));
        this.grandTotal = this.grandTotal - this.appliedPoints;
        localStorage.setItem('grandTotal', this.grandTotal.toString());
      }
    });
    this.getCoupons();
    if (localStorage.getItem('auth') !== null) {
      this.getUserDetails();
    }
  }

  // called from constructor to get all coupons
  private getCoupons() {
    let locationId = localStorage.getItem('locationId');
    this.restService.getCoupons(locationId).subscribe((res: any) => {
      this.coupons = res;
      if (localStorage.getItem('restaurantID') !== null) {
        this.restaurantId = localStorage.getItem('restaurantID');
        this.getRestaurantSettings();
      }
    }, error => this.toastr.error(error.error.message));
  }

  // gets logged in user details
  private getUserDetails() {
    this.restService.getUserDetails().subscribe(res => {
      this.userDetails = res;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // gets restaurant settings data
  private getRestaurantSettings() {
    this.restService.getRestaurantSettings(this.restaurantId).subscribe(res => {
      this.restaurantSettings = res;
      let auth = localStorage.getItem('auth');
      this.isAuthenticated = auth === 'yes' ? true : false;
    }, error => this.toastr.error(error.error.message, 'Error', { timeOut: 3000 }));
  }

  // this method is called when user applies coupon
  public couponAppliedEvent(event) {
    this.selectedCoupon = this.coupons.find(coupon => coupon._id === event.target.value);
    this.isCouponApplied = true;
    this.grandTotal = this.initialTotal;
    this.grandTotal = this.grandTotal - (this.grandTotal * (this.selectedCoupon.offPrecentage / 100));
    localStorage.setItem('grandTotal', this.grandTotal.toString());
    localStorage.setItem('couponApplied', 'true');
    localStorage.setItem('couponDetails', JSON.stringify(this.selectedCoupon));
  }

  // this method is called when user applies loyalty
  public applyLoyaltyEvent() {
    this.isLoyaltyApplied = true;
    this.grandTotal = this.grandTotal - this.userDetails.totalLoyaltyPoints;
    localStorage.setItem('grandTotal', this.grandTotal.toString());
    localStorage.setItem('loyaltyApplied', 'yes');
    localStorage.setItem('loyaltyPoints', this.userDetails.totalLoyaltyPoints.toString());
  }

  // undo the changes done after applying coupon
  public removeCoupon() {
    this.isCouponApplied = false;
    this.grandTotal = this.initialTotal;
    localStorage.removeItem('couponApplied');
    localStorage.removeItem('couponDetails');
    localStorage.setItem('grandTotal', this.grandTotal.toString());
  }

  // increases item quantity
  public increaseQuantity(item, index) {
    let product = item;
    product.Quantity += 1;
    if (product.price === product.MRP) {
      if (product.extraIngredients === null) {
        product.totalPrice = product.MRP * product.Quantity;
      } else {
        let mrp: number = product.MRP
        mrp += product.extraIngredients.price;
        product.totalPrice = mrp * product.Quantity;
      }
    } else {
      if (product.extraIngredients === null) {
        product.totalPrice = product.price * product.Quantity;
      } else {
        let price = product.price;
        price += product.extraIngredients.price;
        product.totalPrice = price * product.Quantity;
      }
    }
    this.cartCopy.splice(index, 1);
    this.cartCopy.push(product)
    localStorage.setItem('cart', JSON.stringify(this.cartCopy));
    this.store.dispatch(new CartActions.UpdateCart({ id: product.productId, cart: product }));
  }

  // reduces item quantity
  public reduceQuantity(item, index) {
    let product = item;
    if (product.Quantity > 1) {
      product.Quantity -= 1;
      if (product.price === product.MRP) {
        if (product.extraIngredients === null) {
          product.totalPrice = product.MRP * product.Quantity;
        } else {
          let mrp: number = product.MRP;
          mrp += product.extraIngredients.price;
          product.totalPrice = mrp * product.Quantity;
        }
      } else {
        if (product.extraIngredients === null) {
          product.totalPrice = product.price * product.Quantity;
        } else {
          let price = product.price;
          price += product.extraIngredients.price;
          product.totalPrice = price * product.Quantity;
        }
      }
      this.cartCopy.splice(index, 1);
      this.cartCopy.push(product);
      localStorage.setItem('cart', JSON.stringify(this.cartCopy));
      this.store.dispatch(new CartActions.UpdateCart({ id: product.productId, cart: product }));
    }
  }

  // removes item from cart
  public removeItemFromCart(item) {
    let cart: Array<any> = JSON.parse(localStorage.getItem('cart'));
    let index = cart.findIndex(data => data.productId === item.productId);
    swal({
      title: 'Are you sure?',
      text: 'Do you want to remove ' + item.title + ' from cart',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.store.dispatch(new CartActions.DeleteCartItem(item.productId));
        cart.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify(cart));
        swal({
          title: 'Success',
          text: item.title + ' removed from cart',
          type: 'success',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        });
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal({
          title: 'Cancelled',
          text: item.title + ' is not removed',
          type: 'info',
          background: '#fff',
          showConfirmButton: false,
          width: 300,
          timer: 3000
        });
      }
    });
  }

  ngOnInit() {
  }

}
