import {Component} from '@angular/core';
import {CrudService} from './services/crud.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CrudService]
})
export class AppComponent {
  title = 'app';

  constructor(private crud: CrudService, private router: Router) {
    let token = localStorage.getItem('token');
    if (token !== undefined && token !== null) {
      this.crud.getUserDetails('users/me').subscribe(() => {

      }, () => {
        localStorage.removeItem('token');
        localStorage.removeItem('id');
        localStorage.removeItem('auth');
        this.router.navigate(['login']);
      });
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('id');
      localStorage.removeItem('auth');
    }
  }
}
