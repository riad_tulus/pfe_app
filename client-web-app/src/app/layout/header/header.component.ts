import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as CartActions from '../../pages/store/cart.action';
import * as fromCart from '../../pages/store/cart.reducer';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  private cartObservable: Observable<fromCart.State>;
  public cart: any[] = [];
  public noOfCartItems: number;
  constructor(private toastr: ToastrService, private router: Router, private store: Store<fromCart.CartState>) {
    this.cartObservable = store.select('cart');
    this.cartObservable = this.store.select('cart');
    this.cartObservable.subscribe(cart => {
      this.cart = cart.cart;
      this.noOfCartItems = this.cart.length;
      console.log('cart length', this.noOfCartItems);
    });
  }

  // used to alter menu based on user authentication status
  public check() {
    let auth = localStorage.getItem('auth');
    if (auth === 'yes') {
      return true;
    } else {
      return false;
    }
  }

  // log's out current user
  logout() {
    this.store.dispatch(new CartActions.AddToCart({ cart: [] }));
    localStorage.clear();
    // this.cart = [];
    this.toastr.success('Logout Successful', 'Success', { timeOut: 3000 });
    this.router.navigate(['login']);
  }

  onCartSeelect() {
    console.log('clicked')
    if (localStorage.getItem('locationId')) {
      const locationId = localStorage.getItem('locationId');
      this.router.navigate(['/restaurant-page', locationId])
    } else {
      this.toastr.info('Please Select Restaurant First', 'Info', { timeOut: 3000 });
    }
  }
}
