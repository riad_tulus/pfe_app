import {Component, OnInit} from '@angular/core';
import {BannerService} from '../../pages/banner/banner.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  providers: [BannerService]
})
export class FooterComponent implements OnInit {
  public popularlocations: Array<any>;  // contains popular locations
  public popularRestaurants: Array<any>;  // contains popular restaurants
  public cuisines: Array<any>;   // contains popular cuisines

  constructor(private restService: BannerService, private toastr: ToastrService, private router: Router) {
    this.getPopulatLocations();
    this.getPopularRestaurants();
    this.getPopularCuisines();
  }

  // called from constructor to get all popular locations
  private getPopulatLocations() {
    this.restService.getAllLocations().subscribe((res: any) => {
      this.popularlocations = res;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // called from constructor to get all popular restaurants
  private getPopularRestaurants() {
    this.restService.getTopRatedRestaurants().subscribe((res: any) => {
      this.popularRestaurants = res;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // called from constructor to get all cuisines
  private getPopularCuisines() {
    this.restService.getCuisines().subscribe((res: any) => {
      this.cuisines = res;
    }, error => this.toastr.error(error.error.message, 'Error', {timeOut: 3000}));
  }

  // navigates to restaurant page
  public viewRestaurant(id: string) {
    this.router.navigate(['restaurant-page/' + id]);
  }

  ngOnInit() {
  }

}
