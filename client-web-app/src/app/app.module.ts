import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {Routing} from './app.routing';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {LaddaModule} from 'angular2-ladda';
import {CookieModule} from 'ngx-cookie';
import {Ng2CloudinaryModule} from 'ng2-cloudinary';
import {FileUploadModule} from 'ng2-file-upload';
import {SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider} from 'angular4-social-login';
import {AppComponent} from './app.component';
import {HeaderComponent} from './layout/header/header.component';
import {FooterComponent} from './layout/footer/footer.component';
import {CrudService} from './services/crud.service';
import {ConstantService} from './services/constant.service';
import {AuthGuardService} from './services/auth-guard.service';
import {cartReducer} from './pages/store/cart.reducer';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('249166393283-a3mcd3haceekh8s9b9geg9fr8f2cfgkg.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('270336200169297')
  }
]);

export function socialConfig() {
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    CookieModule.forRoot(),
    Ng2CloudinaryModule,
    FileUploadModule,
    SocialLoginModule,
    StoreModule.forRoot({'cart': cartReducer}),
    LaddaModule,
    NgbModule.forRoot(),
    RouterModule,
    Routing,
    HttpClientModule
  ],
  providers: [CrudService, AuthGuardService, ConstantService, {
    provide: AuthServiceConfig,
    useFactory: socialConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
