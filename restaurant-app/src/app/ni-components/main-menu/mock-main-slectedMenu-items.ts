import {MainMenuItem} from './main-menu-item';

export const MAINMENUITEMS2: MainMenuItem[] = [
  {
    title: 'Principale',
    icon: '',
    active: true,
    groupTitle: true,
    sub: '',
    routing: '',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Accueil',
    icon: 'fa fa-home',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/dashboard',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Mes informations',
    icon: 'fa fa-cutlery',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/information/view',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Categories',
    icon: 'fa fa-braille',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/categories',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Produits',
    icon: 'fa fa-shopping-basket',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/products',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Ventes',
    icon: 'fa fa-briefcase',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/sales',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Clients',
    icon: 'fa fa-users',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/customers',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Commandes',
    icon: 'fa fa-shopping-cart',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/orders',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Gestion employees',
    icon: 'fa fa-black-tie',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/staff',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Notification',
    icon: 'fa fa-pencil',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/notification',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Réglages',
    icon: 'fa fa-cogs',
    active: false,
    groupTitle: false,
    sub: [
      {
        title: 'Tags',
        routing: '/pietech/tags'
      },
      {
        title: 'Zone de livraison',
        routing: '/pietech/delivery'
      },
      {
        title: 'Heures de travail',
        routing: '/pietech/workinghours'
      }
    ],
    routing: '/pietech/tax',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Coupons et offres',
    icon: 'fa fa-gift',
    active: false,
    groupTitle: false,
    sub: [
      {
        title: 'Coupons',
        routing: '/pietech/discount'
      }
    ],
    routing: '/pietech/loyality/configuration',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Chat',
    icon: 'fa fa-comments-o',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/chat',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },


];
