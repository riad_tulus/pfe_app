import {Component} from '@angular/core';
import {MainMenuItem} from './main-menu-item';
import {MainMenuService} from './main-menu.service';

@Component({
  moduleId: module.id,
  selector: 'main-menu',
  templateUrl: 'main-menu.component.html',
  styleUrls: ['main-menu.component.scss'],
  providers: [MainMenuService],
  host: {'class': 'app-menu'}
})
export class MainMenuComponent {
  mainMenuItems: MainMenuItem[];
  user: any;

  constructor(private mainMenuService: MainMenuService) {
    this.user = localStorage.getItem('role');
  }

  getMainMenuItems(): void {
    this.mainMenuService.getMainMenuItems().then(mainMenuItems => this.mainMenuItems = mainMenuItems);
  }

  getManagerMenu(): void {
    this.mainMenuService.getMainMenuItemsRole().then(mainMenuItems => this.mainMenuItems = mainMenuItems);
  }

  ngOnInit(): void {
    if (this.user === 'Owner') {
      this.getMainMenuItems();
    }else if (this.user === 'Manager') {
      this.getManagerMenu();
    } else if (this.user === 'Staff') {
      this.getManagerMenu();
    }
  }

  toggle(event: Event, item: any, el: any) {
    event.preventDefault();

    const items: any[] = el.mainMenuItems;

    if (item.active) {
      item.active = false;
    } else {
      for (let i = 0; i < items.length; i++) {
        items[i].active = false;
      }
      item.active = true;
    }
  }
}
