import {Injectable} from '@angular/core';
import {MainMenuItem} from './main-menu-item';
import {MAINMENUITEMS} from './mock-main-menu-items';
import {MAINMENUITEMS2} from './mock-main-slectedMenu-items'

@Injectable()
export class MainMenuService {
  getMainMenuItems(): Promise<MainMenuItem[]> {
    return Promise.resolve(MAINMENUITEMS);
  }

  getMainMenuItemsRole(): Promise<MainMenuItem[]> {
    return Promise.resolve(MAINMENUITEMS2);

  }

}
