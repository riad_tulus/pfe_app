import {MainMenuItem} from './main-menu-item';
export const MAINMENUITEMS: MainMenuItem[] = [
  {
    title: 'Principale',
    icon: '',
    active: true,
    groupTitle: true,
    sub: '',
    routing: '',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Accueil',
    icon: 'fa fa-home',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/dashboard',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Mes informations',
    icon: 'fa fa-cutlery',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/information/view',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Emplacements',
    icon: 'fa fa-map-marker',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/locations',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Categories',
    icon: 'fa fa-braille',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/categories',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Produits',
    icon: 'fa fa-shopping-basket',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/products',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Paiement',
    icon: 'fa fa-credit-card',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/transactions',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Clients',
    icon: 'fa fa-users',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/customers',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Commandes',
    icon: 'fa fa-shopping-cart',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/orders',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Gestion employees',
    icon: 'fa fa-black-tie',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/staff',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Notification',
    icon: 'fa fa-pencil',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/notification',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Chat',
    icon: 'fa fa-comments-o',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/chat',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Remise & offres',
    icon: 'fa fa-gift',
    active: false,
    groupTitle: false,
    sub: [
      {
        title: 'Programme de fidélité',
        routing: '/pietech/loyality/configuration'
      }
    ],
    routing: '/pietech/loyality/configuration',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Réglages',
    icon: 'fa fa-cogs',
    active: false,
    groupTitle: false,
    sub: [
      {
        title: 'Info Tax',
        routing: '/pietech/tax'
      }
    ],
    routing: '/pietech/tax',
    externalLink: '',
    budge: '',
    budgeColor: ''
  }
];


