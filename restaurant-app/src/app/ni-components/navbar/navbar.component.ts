import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import * as CountAction from '../../pages/restaurantSass/chat/action/count.action';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { UnreadCount } from '../../pages/restaurantSass/chat/action/count';

import { NavBarService } from './navbar.service';
import { SocketSharedService } from '../../services/socketShare.service';

interface CountState {
  count: UnreadCount;
}

interface AppState {
  chatId: string;
}

@Component({
  moduleId: module.id,
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss'],
  providers: [NavBarService],
  host: {
    '[class.app-navbar]': 'true',
    '[class.show-overlay]': 'showOverlay'
  }
})


export class NavbarComponent implements OnInit {
  @Input() title: string;
  @Input() openedSidebar: boolean;
  @Output() sidebarState = new EventEmitter();

  countUpdate: Observable<UnreadCount>;
  showOverlay: boolean;
  public imgUrl: any;
  public unreadChats: any[] = [];
  orderNotify: any = [];
  public unread: any = 0;
  public unreadCount: number;
  public userRole: string;
  public locId: any;
  public userData: any = {
    name: '',
    logo: 'https://res.cloudinary.com/pietechsolutions/image/upload/v1514786142/xmqwpbwli1fgjb02fepz.jpg'
  };
  private chatId$;
  private activeChatId: any;

  constructor(private restService: NavBarService,
    private router: Router,
    private sockectService: SocketSharedService,
    private counter: Store<CountState>,
    private store: Store<AppState>) {
    this.openedSidebar = false;
    this.showOverlay = false;
    this.userRole = localStorage.getItem('role');
    this.imgUrl = localStorage.getItem('img');
    this.locId = localStorage.getItem('locationId');
    // console.log("imgUrl"+ this.imgUrl);
    this.countUpdate = this.counter.select('count');
    this.getImgUrl();
    this.getUnreadMessageList();
    this.getTotalUnreadCount();
    this.getUserData();


    // console.log('locationId' + this.locId);
    if (this.locId) {
      this.getAllUnreadOrders();
    }

  }

  getImgUrl() {
    this.imgUrl = localStorage.getItem('img');
  }

  getUserData() {
    this.restService.userData().subscribe((res: any) => {
      this.userData.name = res.name;
      // console.log(' name ' + res.name);
      if (res.logo !== null) {
        this.imgUrl = res.logo;
      }
      else {
        this.imgUrl = res.restaurantID.logo;
      }
    });
  }

  getAllUnreadOrders() {
    this.unread = 0;
    this.restService.getUnreadNotificationData(this.locId).subscribe((res) => {
      this.orderNotify = res;
      // if(this.orderNotify.length == 0)
      // console.log('notification' + JSON.stringify(res));
      const length = this.orderNotify.length;
      for (let i = 0; i < length; i++) {
        if (this.orderNotify[i].readNotification === false) {
          this.unread++;
        }
      }
    });
  }

  getTotalUnreadCount() {
    this.restService.getUnreadMessageCount().subscribe((res: any) => {
      // console.log('getUnreadMessageCount ' + res);
      this.counter.dispatch(new CountAction.UpdateCount(res));
      this.unreadCount = res;
      // console.log('unreadCount ' + this.unreadCount);
    });
  }

  getUnreadMessageList() {
    this.restService.getUnreadMessageNotificationData().subscribe((res: any) => {
      this.unreadChats = res;
      // console.log('UnreadMessageNotificationData ' + JSON.stringify(res));
    });
  }

  readSingleThread(chatUserId, userType, index) {
    const threadData: any = this.unreadChats[index];
    // console.log('select thread ' + JSON.stringify(threadData));
    if (userType === 'Manager' || userType === 'User') {
      localStorage.setItem('chatType', 'listOne');
      this.restService.readSingleMessageThread(chatUserId).subscribe((res: any) => {
        this.counter.dispatch(new CountAction.DownGradeCount(threadData.count));
        this.counter.dispatch(new CountAction.AssignId(chatUserId));
        this.unreadChats.splice(index, 1);
        return this.router.navigate(['/pietech/chat']);
      });
    } else {
      if (userType === 'Owner' || userType === 'Admin') {
        localStorage.setItem('chatType', 'listTwo');
        this.restService.readSingleMessageThread(chatUserId).subscribe((res: any) => {
          this.counter.dispatch(new CountAction.DownGradeCount(threadData.count));
          this.counter.dispatch(new CountAction.AssignId(chatUserId));
          this.unreadChats.splice(index, 1);
          return this.router.navigate(['/pietech/chat']);
        });
      }
    }

  }

  logout() {
    localStorage.clear();
    return this.router.navigate(['/pages/sign-in']);
  }

  profile() {
    return this.router.navigate(['/pietech/information/view']);
  }

  getSetting() {
    return this.router.navigate(['/pietech/information/edit']);
  }

  ngOnInit() {
    /** Declaring all the method inside ngOnInit block which invoke when observable response
     * getMessageNotification() invoke when new message arrive and socket emit new message;
     * letestOrderInfo() invoke when new Order arrive and socket emit new Order detail;*/
    this.sockectService.getMessageNotification().subscribe((msgNotification: any) => {

      this.chatId$ = this.store.select('chatId');

      this.chatId$.subscribe(() => {
        if ((this.chatId$.actionsObserver._value.payload !== undefined)) {
          this.activeChatId = this.chatId$.actionsObserver._value.payload;
          // console.log('active Chat Id ' + this.activeChatId);

          if (this.activeChatId !== msgNotification._id) {
            const data: any = msgNotification;
            this.playAudio();
            this.counter.dispatch(new CountAction.SetCount(msgNotification.totalcount));

            if (this.unreadChats.length === 0) {
              this.unreadChats.unshift(data);
            } else {
              const index = this.unreadChats.findIndex(x => x._id === data._id);
              if (index >= 0) {
                this.unreadChats.splice(index, 1)
                this.unreadChats.unshift(data)
              } else {
                this.unreadChats.unshift(data);
              }
            }
          } else { // inner If closed
          }
        }// Outer If closed
      });

    });
    ////////////////////////////// getNewOrderNotification ///////////////////////////////
    this.sockectService.letestOrderInfo().subscribe((notification) => {
      // console.log(' get Order Notification' + JSON.stringify(notification));
      this.unread++;
      this.playAudio();
      const data: any = notification;
      if (this.orderNotify.length === 0) {

        this.orderNotify.unshift(data);
      } else {

        const index = this.orderNotify.findIndex(x => x._id === data._id);
        if (index >= 0) {
          this.orderNotify.splice(index, 1)
          this.orderNotify.unshift(data);
        } else {
          this.orderNotify.unshift(data);
        }
      }
    });
  }

  ///////////////////////// Order Notification Update //////////////////

  readNavigate(notificationId, orderId) {
    // console.log('readNavigate ' + notificationId + 'OrderId' + orderId);
    this.updateNotificationAndRoute(notificationId, orderId);
  }

  readNotNavigate(notificationId) {
    // console.log('readNotNavigate ' + notificationId);
    this.updateNotification(notificationId);
  }

  readAllNotification() {
    this.orderNotify = [];
    return this.router.navigate(['/pietech/orders']);
  }

  updateNotification(notificationId) {
    const notify: any = {
      readNotification: true
    }
    // console.log('notificationId ------' + notificationId);
    this.restService.UpdateNotificationData(notify, notificationId).subscribe((res) => {
      this.getAllUnreadOrders();
    });
  }

  updateNotificationAndRoute(notificationId, orderId) {
    const notify: any = {
      readNotification: true
    }
    // console.log('notificationId ' + notificationId);
    this.restService.UpdateNotificationData(notify, notificationId).subscribe((res) => {
      this.getAllUnreadOrders();
      return this.router.navigate(['/pietech/order/view', orderId]);
    });
  }


  open(event) {
    const clickedComponent = event.target.closest('.nav-item');
    const items = clickedComponent.parentElement.children;

    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove('opened');
    }
    clickedComponent.classList.add('opened');

    // Add class 'show-overlay'
    this.showOverlay = true;
  }

  close(event) {
    const clickedComponent = event.target;
    const items = clickedComponent.parentElement.children;

    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove('opened');
    }

    // Remove class 'show-overlay'
    this.showOverlay = false;
  }

  openSidebar() {
    this.openedSidebar = !this.openedSidebar;
    this.sidebarState.emit();
  }
  // invoke when new message or new order arrived
  playAudio() {
    const audio = new Audio();
    audio.src = 'assets/sound/sound.mp3';
    audio.load();
    audio.play();
  }

}
