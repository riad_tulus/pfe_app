import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SelectModule} from 'ng2-select';
import {LaddaModule} from 'angular2-ladda';
import {MaterialModule} from '@angular/material'
import {ChartsModule} from 'ng2-charts';
import {CalendarModule} from 'angular-calendar';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {AgmCoreModule} from 'angular2-google-maps/core';
import {ToastrModule} from 'ngx-toastr';
import {FileUploadModule} from 'ng2-file-upload';
import {CookieModule} from 'ngx-cookie';

import {InfiniteScrollModule} from 'ngx-infinite-scroll';


import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {DefaultLayoutComponent} from './layouts/default/default.component';
import {ExtraLayoutComponent} from './layouts/extra/extra.component';
// import
//A2 Components
import {NavbarComponent} from './ni-components/navbar/navbar.component';
import {SidebarComponent} from './ni-components/sidebar/sidebar.component';
import {LogoComponent} from './ni-components/logo/logo.component';
import {MainMenuComponent} from './ni-components/main-menu/main-menu.component';
import {A2CardComponent} from './ni-components/card/card.component';
import {AlertComponent} from './ni-components/alert/alert.component';
import {BadgeComponent} from './ni-components/badge/badge.component';
import {BreadcrumbComponent} from './ni-components/breadcrumb/breadcrumb.component';
import {FileComponent} from './ni-components/file/file.component';
import {FooterComponent} from './ni-components/footer/footer.component';
import {PageDashboardComponent} from './pages/dashboard/dashboard.component';
// Extra pages
import {PageSignIn1Component} from './pages/extra-pages/sign-in-1/sign-in-1.component';
import {PageSignUp1Component} from './pages/extra-pages/sign-up-1/sign-up-1.component';
import {PageForgotComponent} from './pages/extra-pages/forgot/forgot.component';
import {PageConfirmComponent} from './pages/extra-pages/confirm/confirm.component';
import {Page404Component} from './pages/extra-pages/page-404/page-404.component';
import {Page500Component} from './pages/extra-pages/page-500/page-500.component';
import {PageResetComponent} from './pages/extra-pages/reset-password/reset-password.component';
import {ConstantService} from './services/constant.service';
import {CrudService} from './services/crud.service';
import {CrudBaseService} from './services/base.service';
import {SocketSharedService} from './services/socketShare.service';

import {MyinfoComponent} from './pages/restaurantSass/myinfo/myinfo.component';
import {ViewInfoComponent} from './pages/restaurantSass/myinfo/view-info/view-info.component';

import {AllLocationsComponent} from './pages/restaurantSass/locations/all-locations/all-locations.component';
import {AddLocationsComponent} from './pages/restaurantSass/locations/add-locations/add-locations.component';
import {UpdateLocationsComponent} from './pages/restaurantSass/locations/update-locations/update-locations.component';
import {ViewLocationsComponent} from './pages/restaurantSass/locations/view-locations/view-locations.component';

import {AllCustomersComponent} from './pages/restaurantSass/manage/customers/all-customers/all-customers.component';
import {ViewCustomerComponent} from './pages/restaurantSass/manage/customers/view-customer/view-customer.component';

import {AddProductComponent} from './pages/restaurantSass/manage/products/add-product/add-product.component';
import {AllProductsLocationComponent} from './pages/restaurantSass/manage/products/all-products-location/all-products-location.component';
import {UpdateProductComponent} from './pages/restaurantSass/manage/products/update-product/update-product.component';
import {ViewProductComponent} from './pages/restaurantSass/manage/products/view-product/view-product.component';
import {AllProductsComponent} from './pages/restaurantSass/manage/products/all-products/all-products.component';

import {AddCategoryComponent} from './pages/restaurantSass/manage/categories/all-categories/add-category/add-category.component';
import {CategoriesByLocationComponent} from './pages/restaurantSass/manage/categories/all-categories/categories-by-location/categories-by-location.component';
import {UpdateCategoryComponent} from './pages/restaurantSass/manage/categories/all-categories/update-category/update-category.component';
import {ViewCategoryComponent} from './pages/restaurantSass/manage/categories/all-categories/view-category/view-category.component';
import {AllCategoriesComponent} from './pages/restaurantSass/manage/categories/all-categories/all-categories.component';

import {AllOrdersComponent} from './pages/restaurantSass/manage/orders/all-orders/all-orders.component';
import {ViewOrderComponent} from './pages/restaurantSass/manage/orders/view-order/view-order.component';
import {TagsComponent} from './pages/restaurantSass/settings/tags/tags.component';
import {StaffManagementComponent} from './pages/restaurantSass/staff-management/staff-management.component';
import {AssignedOrdersComponent} from './pages/restaurantSass/staff-management/assigned-orders/assigned-orders.component';

import {NotificationComponent} from './pages/restaurantSass/notification/notification.component';
import {PaymentsComponent} from './pages/restaurantSass/payments/payments.component';

import {LoyalityConfigComponent} from './pages/restaurantSass/loyality-program/loyality-config/loyality-config.component';
import {LoyalityDiscountComponent} from './pages/restaurantSass/loyality-program/loyality-discount/loyality-discount.component';
import {TaxComponent} from './pages/restaurantSass/settings/tax/tax.component';
import {DeliveryAreaComponent} from './pages/restaurantSass/settings/delivery-area/delivery-area.component';
import {WorkingHoursComponent} from './pages/restaurantSass/settings/working-hours/working-hours.component';
import {SalesComponent} from './pages/restaurantSass/sales/sales.component';
import {WalletComponent} from './pages/restaurantSass/payments/wallet/wallet.component';
import {BankComponent} from './pages/restaurantSass/payments/bank/bank.component';
import {WithdrawalComponent} from './pages/restaurantSass/payments/withdrawal/withdrawal.component';
import {ChatComponent} from './pages/restaurantSass/chat/chat.component';
import {ChatBoxComponent} from './pages/restaurantSass/chat/chat-box/chat-box.component';
// ngrx solution
import {StoreModule} from '@ngrx/store';
import {countReducer} from './pages/restaurantSass/chat/action/count.reducer';
// Moment
import {MomentModule} from 'angular2-moment';
import {OrderInvoiceComponent} from './pages/restaurantSass/manage/orders/view-order/order-invoice/order-invoice.component';

// A2 Pages
// common services

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule.forRoot(),
    BrowserAnimationsModule,
    AppRoutingModule,
    FileUploadModule,
    LaddaModule.forRoot({
      spinnerColor: 'black',
      spinnerSize: 60,
      style: 'zoom-in',
    }),
    CookieModule.forRoot(),
    SelectModule,
    ChartsModule,
    ToastrModule.forRoot(),
    CalendarModule.forRoot(),
    NgxDatatableModule,
    MomentModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAU9f7luK3J31nurL-Io3taRKF7w9BItQE',
    }),
    StoreModule.forRoot({count: countReducer}),
    InfiniteScrollModule

  ],
  declarations: [
    AppComponent,
    DefaultLayoutComponent,
    ExtraLayoutComponent,

    NavbarComponent,
    SidebarComponent,
    LogoComponent,
    MainMenuComponent,
    A2CardComponent,
    AlertComponent,
    BadgeComponent,
    BreadcrumbComponent,
    FileComponent,
    FooterComponent,

    PageDashboardComponent,

    PageSignIn1Component,
    PageSignUp1Component,
    PageForgotComponent,
    PageConfirmComponent,
    Page404Component,
    Page500Component,
    PageResetComponent,

    MyinfoComponent,
    ViewInfoComponent,
    AllLocationsComponent,
    AddLocationsComponent,
    UpdateLocationsComponent,
    ViewLocationsComponent,
    AllCustomersComponent,
    ViewCustomerComponent,
    AddProductComponent,
    AllProductsLocationComponent,
    AllProductsComponent,
    UpdateProductComponent,
    ViewProductComponent,
    AddCategoryComponent,
    CategoriesByLocationComponent,
    UpdateCategoryComponent,
    ViewCategoryComponent,
    AllCategoriesComponent,
    AllOrdersComponent,
    ViewOrderComponent,
    TagsComponent,
    StaffManagementComponent,
    NotificationComponent,
    AssignedOrdersComponent,
    PaymentsComponent,
    LoyalityConfigComponent,
    LoyalityDiscountComponent,
    TaxComponent,
    SalesComponent,
    WalletComponent,
    BankComponent,
    WithdrawalComponent,
    ChatComponent,
    ChatBoxComponent,
    DeliveryAreaComponent,
    OrderInvoiceComponent,
    WorkingHoursComponent,
    OrderInvoiceComponent,
  ],

  entryComponents: [],
  providers: [ConstantService, CrudService, CrudBaseService, SocketSharedService],
  bootstrap: [AppComponent]
})

export class AppModule {

}
