import {Component} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-extra-layout',
  templateUrl: 'extra.component.html',
  styleUrls: ['extra.component.scss']
})
export class ExtraLayoutComponent {

}
