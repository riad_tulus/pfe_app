import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DefaultLayoutComponent} from './layouts/default/default.component';
import {ExtraLayoutComponent} from './layouts/extra/extra.component';

import {PageDashboardComponent} from './pages/dashboard/dashboard.component';

import {PageSignIn1Component} from './pages/extra-pages/sign-in-1/sign-in-1.component';
import {PageSignUp1Component} from './pages/extra-pages/sign-up-1/sign-up-1.component';
import {PageForgotComponent} from './pages/extra-pages/forgot/forgot.component';
import {PageConfirmComponent} from './pages/extra-pages/confirm/confirm.component';
import {Page404Component} from './pages/extra-pages/page-404/page-404.component';
import {Page500Component} from './pages/extra-pages/page-500/page-500.component';
import {PageResetComponent} from './pages/extra-pages/reset-password/reset-password.component';


import {MyinfoComponent} from './pages/restaurantSass/myinfo/myinfo.component';
import {ViewInfoComponent} from './pages/restaurantSass/myinfo/view-info/view-info.component';

import {PaymentsComponent} from './pages/restaurantSass/payments/payments.component';
import {WalletComponent} from './pages/restaurantSass/payments/wallet/wallet.component';
import {WithdrawalComponent} from './pages/restaurantSass/payments/withdrawal/withdrawal.component';
import {BankComponent} from './pages/restaurantSass/payments/bank/bank.component';

import {AllLocationsComponent} from './pages/restaurantSass/locations/all-locations/all-locations.component';
import {AddLocationsComponent} from './pages/restaurantSass/locations/add-locations/add-locations.component';
import {UpdateLocationsComponent} from './pages/restaurantSass/locations/update-locations/update-locations.component';
import {ViewLocationsComponent} from './pages/restaurantSass/locations/view-locations/view-locations.component';

import {SalesComponent} from './pages/restaurantSass/sales/sales.component';

import {AllCustomersComponent} from './pages/restaurantSass/manage/customers/all-customers/all-customers.component';
import {ViewCustomerComponent} from './pages/restaurantSass/manage/customers/view-customer/view-customer.component';

import {AddProductComponent} from './pages/restaurantSass/manage/products/add-product/add-product.component';
import {AllProductsLocationComponent} from './pages/restaurantSass/manage/products/all-products-location/all-products-location.component';
import {UpdateProductComponent} from './pages/restaurantSass/manage/products/update-product/update-product.component';
import {ViewProductComponent} from './pages/restaurantSass/manage/products/view-product/view-product.component';
import {AllProductsComponent} from './pages/restaurantSass/manage/products/all-products/all-products.component';

import {AddCategoryComponent} from './pages/restaurantSass/manage/categories/all-categories/add-category/add-category.component';
import {CategoriesByLocationComponent} from './pages/restaurantSass/manage/categories/all-categories/categories-by-location/categories-by-location.component';
import {UpdateCategoryComponent} from './pages/restaurantSass/manage/categories/all-categories/update-category/update-category.component';
import {ViewCategoryComponent} from './pages/restaurantSass/manage/categories/all-categories/view-category/view-category.component';
import {AllCategoriesComponent} from './pages/restaurantSass/manage/categories/all-categories/all-categories.component';
import {OrderInvoiceComponent} from './pages/restaurantSass/manage/orders/view-order/order-invoice/order-invoice.component';

import {AllOrdersComponent} from './pages/restaurantSass/manage/orders/all-orders/all-orders.component';
import {ViewOrderComponent} from './pages/restaurantSass/manage/orders/view-order/view-order.component';

import {ChatComponent} from './pages/restaurantSass/chat/chat.component';

import {StaffManagementComponent} from './pages/restaurantSass/staff-management/staff-management.component';
import {AssignedOrdersComponent} from './pages/restaurantSass/staff-management/assigned-orders/assigned-orders.component';

import {NotificationComponent} from './pages/restaurantSass/notification/notification.component';
import {LoyalityConfigComponent} from './pages/restaurantSass/loyality-program/loyality-config/loyality-config.component';
import {LoyalityDiscountComponent} from './pages/restaurantSass/loyality-program/loyality-discount/loyality-discount.component';

import {TaxComponent} from './pages/restaurantSass/settings/tax/tax.component';
import {TagsComponent} from './pages/restaurantSass/settings/tags/tags.component';
import {DeliveryAreaComponent} from './pages/restaurantSass/settings/delivery-area/delivery-area.component';
import {WorkingHoursComponent} from './pages/restaurantSass/settings/working-hours/working-hours.component';


const defaultRoutes: Routes = [
  {path: 'dashboard', component: PageDashboardComponent},
  {path: 'information/edit', component: MyinfoComponent},
  {path: 'information/view', component: ViewInfoComponent},

  {path: 'transactions', component: PaymentsComponent},
  {path: 'wallet', component: WalletComponent},
  {path: 'withdrawal', component: WithdrawalComponent},
  {path: 'bank', component: BankComponent},

  {path: 'locations', component: AllLocationsComponent},
  {path: 'location/new', component: AddLocationsComponent},
  {path: 'location/edit/:id', component: UpdateLocationsComponent},
  {path: 'location/view/:id', component: ViewLocationsComponent},

  {path: 'customers', component: AllCustomersComponent},
  {path: 'customer/view/:id', component: ViewCustomerComponent},

  {path: 'product/new', component: AddProductComponent},
  {path: 'products', component: AllProductsComponent},
  {path: 'location/product/:id', component: AllProductsLocationComponent},
  {path: 'product/edit/:id', component: UpdateProductComponent},
  {path: 'product/view/:id', component: ViewProductComponent},

  {path: 'category/new', component: AddCategoryComponent},
  {path: 'location/category/:id', component: CategoriesByLocationComponent},
  {path: 'category/edit/:id', component: UpdateCategoryComponent},
  {path: 'category/view/:id', component: ViewCategoryComponent},
  {path: 'categories', component: AllCategoriesComponent},

  {path: 'orders', component: AllOrdersComponent},
  {path: 'order/view/:id', component: ViewOrderComponent},
  {path: 'order/invoice/:id', component: OrderInvoiceComponent},

  {path: 'sales', component: SalesComponent},
  {path: 'sales/:id', component: SalesComponent},

  {path: 'staff', component: StaffManagementComponent},
  {path: 'assigned-order/:id', component: AssignedOrdersComponent},

  {path: 'notification', component: NotificationComponent},

  {path: 'chat', component: ChatComponent},

  {path: 'loyality/configuration', component: LoyalityConfigComponent},
  {path: 'discount', component: LoyalityDiscountComponent},

  {path: 'tax', component: TaxComponent},
  {path: 'tags', component: TagsComponent},
  {path: 'delivery', component: DeliveryAreaComponent},
  {path: 'workinghours', component: WorkingHoursComponent},


  {path: '**', redirectTo: '/pages/page-404'}
];


const extraRoutes: Routes = [
  {path: 'sign-in', component: PageSignIn1Component},
  {path: 'sign-up', component: PageSignUp1Component},
  {path: 'forgot', component: PageForgotComponent},
  {path: 'reset-password', component: PageResetComponent},
  {path: 'confirm', component: PageConfirmComponent},
  {path: 'page-404', component: Page404Component},
  {path: 'page-500', component: Page500Component},
];

const routes: Routes = [
  {
    path: '',
    redirectTo: '/pages/sign-in',
    pathMatch: 'full'
  },
  {
    path: 'pietech',
    component: DefaultLayoutComponent,
    children: defaultRoutes
  },
  {
    path: 'pages',
    component: ExtraLayoutComponent,
    children: extraRoutes
  },
  {
    path: '**',
    redirectTo: '/pages/page-404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
