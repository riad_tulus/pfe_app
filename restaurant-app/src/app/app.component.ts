import {Component} from '@angular/core';
import {SocketSharedService} from './services/socketShare.service';
import {LoginService} from './pages/extra-pages/sign-in-1/login.service';
import {Router} from '@angular/router';
import { Title } from '@angular/platform-browser';
import {ToastrService} from 'ngx-toastr';

@Component({
  moduleId: module.id,
  selector: 'app',
  template: `
    <router-outlet></router-outlet>`,
  styleUrls: ['../assets/sass/style.scss'],
  providers: [LoginService]
})
export class AppComponent {
  constructor(private toaster: ToastrService, private title: Title , private router: Router, private _socketService: SocketSharedService, private loginService: LoginService) {
    /** while project start up and execute app.component constructor function,
      * checking auth token in local storage and define valid variable as a token
      */
    const valid = (localStorage.getItem('token') !== null);
    /** token found in local storage
     * if block execute and invoke a the checkAuthUser function
     to check is token valid or not */
    if (valid) {
      this.checkAuthToken();
    }
  }
  // Initialize a checkAuthToken to check, is token valid or not,
  checkAuthToken() {
    /* accessing getUsersdata member function of loginService,
    * if token is valid so access userInfo the member function of _socketService,
    * userInfo(res._id), emit the unique id of User to server
    * and establishing a secure connection between User and server
    */
    this.loginService.getUsersdata().subscribe((res: any) => {
      this._socketService.userInfo(res._id);
      if (localStorage.getItem('restaurantName')) {
        this.title.setTitle(localStorage.getItem('restaurantName'));
      }
    }, () => {
      localStorage.clear();
      this.router.navigate(['/pages/sign-in']);
      this.toaster.info('login first');
    });
  }
}
