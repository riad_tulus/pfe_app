import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from './login.service'
import {CrudService} from '../../../services/crud.service';
import {ToastrService} from 'ngx-toastr';
import {SocketSharedService} from '../../../services/socketShare.service';
import {Title} from '@angular/platform-browser';
import {CookieService} from 'ngx-cookie';

@Component({
  selector: 'page-sign-in-1',
  templateUrl: './sign-in-1.component.html',
  styleUrls: ['./sign-in-1.component.scss'],
  providers: [LoginService]
})

export class PageSignIn1Component implements OnInit {

  restaurantId: any;
  public isLoading: boolean = false;
  public form: FormGroup;
  public loginData: any = {
    email: '',
    password: '',

  };
  rememberMe: false;
  showCredentials = true;

  constructor(private cookies: CookieService, private title: Title, private socketService: SocketSharedService, private crud: CrudService, private loginService: LoginService, private fb: FormBuilder, private router: Router, private toster: ToastrService) {
    // getting token from local storage if exist
    const valid = (localStorage.getItem('token') !== null);
    if (valid) {
      /* accessing getUsersdata member function of loginService,
      * if token is valid so access userInfo the member function of _socketService,
      * userInfo(res._id), emit the unique id of User to server
      * and establishing a secure connection between User and server
      */
      this.loginService.getUsersdata().subscribe((res: any) => {
        this.router.navigate(['/pietech/dashboard']);
      }, (error) => {
        localStorage.clear();
      });
    }
  }


  onSubmit() { // invoke when user click on log in
    if (this.rememberMe) {
      /*
      * save user credentials inside browser cookies if user check mark on Keep me signed in */
      this.cookies.putObject('user', this.form.value);
    }

    this.loginData.email = this.form.value.email;
    this.loginData.password = this.form.value.password;
    this.isLoading = true;
    this.loginService.loginData(this.loginData)
      .subscribe((res: any) => {
        this.isLoading = false;
        localStorage.clear();
        localStorage.setItem('token', 'Bearer ' + res.token);
        this.crud.initApplication(localStorage.getItem('token'));
        if (localStorage.getItem('token')) {
          this.loginService.getUsersdata().subscribe((response: any) => {
            this.socketService.userInfo(response._id);
            // Setting up useful data inside browser storage
            if (response.role === 'Manager') {
              localStorage.setItem('restaurantName', response.restaurantID.restaurantName);
              localStorage.setItem('locationName', response.locationInfo.locationName);
              localStorage.setItem('locationId', response.locationInfo.locationId);
              localStorage.setItem('Id', response.restaurantID._id);
              if (response.restaurantID.logo) {
                localStorage.setItem('img', response.restaurantID.logo);
              } else {
                localStorage.setItem('img', 'assets/img/manager.png');
              }
              localStorage.setItem('userId', response._id);
            } else if (response.role === 'Staff') {
              localStorage.setItem('userId', response._id);
              localStorage.setItem('restaurantName', response.restaurantName);
              localStorage.setItem('locationName', response.locationInfo.locationName);
              localStorage.setItem('locationId', response.locationInfo.locationId);
              localStorage.setItem('Id', response.restaurantID);
            } else if (response.role === 'Owner') {
              localStorage.setItem('userId', response._id);
              localStorage.setItem('restaurantName', response.restaurantName);
              localStorage.setItem('Id', response._id);
              if (response.logo) {
                localStorage.setItem('img', response.logo);
              } else {
                localStorage.setItem('img', 'assets/img/manager.png');
              }
            }
            localStorage.setItem('role', response.role);
            localStorage.setItem('email', response.email);
            this.title.setTitle(localStorage.getItem('restaurantName'));
            this.toster.success('Connexion réussie!');
            this.router.navigate(['/pietech/dashboard']);

          }, (error) => {
            this.toster.error(error.message, 'ERROR');
          });
        }
      }, (error) => {
        this.isLoading = true;
        this.toster.error(error.message, 'Auth Error');
        this.isLoading = false;
      });
  }


  ngOnInit() {
    this.createLoginForm();
    this.getCookie(); // getting data from Cookies;
  }

  createLoginForm() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  show() {
    this.showCredentials = !(this.showCredentials);
  }

  getCookie() {
    const data: any = this.cookies.getObject('user');
    if (data != null && data !== undefined) {
      this.form.get('email').setValue(data.email);
      this.form.get('password').setValue(data.password);
    }
  }
}
