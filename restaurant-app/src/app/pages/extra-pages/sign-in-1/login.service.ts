'use strict';
import { Injectable } from '@angular/core';
import {Router} from '@angular/router'
import { CrudService } from  '../../../services/crud.service';
import * as io from 'socket.io-client';

@Injectable()
export class LoginService{ 
authToken:string;
constructor(public crud:CrudService,public router:Router) {
this.authToken = localStorage.getItem('token');
}

loginData(login: any) {
        const body = JSON.stringify(login);
        return this.crud.loginPost('auth/local',body).map((data:Response)=>data);
 }

 
getUsersdata (){
 return this.crud.getMyData('users/me').map((data:Response)=>data);

}
//Authentication   
   isAuthenticated() {
    var user = localStorage.getItem('token') !== null;
      if (user) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
