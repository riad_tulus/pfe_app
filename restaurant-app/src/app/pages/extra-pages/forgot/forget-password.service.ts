import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class ForgetPasswordService {

  constructor(public crud: CrudService) {
  }

  sendEmail(userEmail: any) {
    console.log('send Email called');
    const body = JSON.stringify(userEmail);
    return this.crud.post('users/password/otp', body).map((data: Response) => data);
  }

  sendOTP(userOTP: any) {
    console.log('send Email called');
    const body = JSON.stringify(userOTP);
    return this.crud.post('users/password/verification', body).map((data: Response) => data);
  }
}
