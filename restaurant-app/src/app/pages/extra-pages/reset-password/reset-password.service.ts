import { Injectable } from '@angular/core';
import { CrudService } from '../../../services/crud.service';

@Injectable()
export class ResetPasswordService {
    constructor(private crud:CrudService) { }
    resetPassword(data:any){
        return this.crud.post('users/password/reset',data);
    }
}