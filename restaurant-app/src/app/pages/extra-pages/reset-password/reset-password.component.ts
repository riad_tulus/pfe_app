import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ResetPasswordService } from './reset-password.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
@Component({
    selector:'page-reset-password',
    templateUrl:'./reset-password.component.html',
    styleUrls:['./reset-password.component.scss'],
    providers:[ResetPasswordService]
})

export class PageResetComponent implements OnInit {
    @ViewChild('resetPasswordForm') resetPasswordForm:FormGroup;
    newPassword:string;
    confirmPassword:string;
    doesPasswordMatch:boolean;
    constructor(private router:Router, private restService:ResetPasswordService, private toastr:ToastrService) {
    }
    ngOnInit(){
        this.createForm();        
    }

    createForm(){
        this.resetPasswordForm = new FormGroup({
            'password':new FormControl(null,[Validators.required,Validators.minLength(6)]),
            'confirmPassword':new FormControl(null,[Validators.required,Validators.minLength(6)])
        });
    }

    resetPassword(){
        this.newPassword = this.resetPasswordForm.value.password;
        this.confirmPassword = this.resetPasswordForm.value.confirmPassword;
        if(this.confirmPassword !== this.newPassword){
            this.doesPasswordMatch = false;
            //.log('Passwords do not match');
        } else {
            this.doesPasswordMatch = true;
            //.log('Matched....');
            var body = {
                newPass:this.newPassword
            }
            this.restService.resetPassword(body).subscribe(res => {
                if(res !== null){
                    this.toastr.success('Success','Password changed successfully',{timeOut:2000});
                    this.router.navigate(['/pages/sign-in'])
                    localStorage.clear();
                }
            },error => {
                this.toastr.error('Error',error.message,{timeOut:2000});
            });
        }
    }
    
}
