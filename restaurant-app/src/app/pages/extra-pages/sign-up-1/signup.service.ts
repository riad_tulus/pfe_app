import { Injectable } from '@angular/core';
import {Http,Headers,Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {ConstantService} from '../../../services/constant.service';

@Injectable()

export class SignupService {
  constructor(private http: Http,private constantService: ConstantService){ 
  	
}

signUpUser(form){
  	const headers = new Headers();
  	const body = JSON.stringify(form);
  	headers.append('Content-Type','application/json');
  	return this.http.post(this.constantService.API_ENDPOINT+'users/',body,{
  		headers : headers
  	}).map((response: any)=> response.json())
    .catch(this.errorHandler);
  	
}

  private errorHandler(error: any){
    return Observable.throw(error);
  }

}



