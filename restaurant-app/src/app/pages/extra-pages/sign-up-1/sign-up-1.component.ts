import {Component, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {SignupService} from './signup.service';
import {ToastrService} from 'ngx-toastr';


const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'page-sign-up-1',
  templateUrl: './sign-up-1.component.html',
  styleUrls: ['./sign-up-1.component.scss'],
  providers: [SignupService]
})

export class PageSignUp1Component implements OnInit {
  public form: FormGroup;

  @Output() public update: number

  constructor(private fb: FormBuilder, public router: Router, private signUpService: SignupService, private toster: ToastrService) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      restaurantName: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      name: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(30)])],
      email: [null, Validators.compose([Validators.required, CustomValidators.email])],
      password: password,
      confirmPassword: confirmPassword


    });
  }

  onSubmit() {
    let loginData: any = {};
    loginData = this.form.value;
    loginData.role = 'Owner';
    this.signUpService.signUpUser(loginData)
      .subscribe((res: any) => {
        this.toster.success('Register successfully');
        this.router.navigate(['/pages/sign-in/']);
      }, (error) => {
        // console.log(' error ' + error._body.message)
        this.toster.error(error.message, 'ERROR');
      });
  }

}
