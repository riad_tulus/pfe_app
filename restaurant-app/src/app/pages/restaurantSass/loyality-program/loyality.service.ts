import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class LoyaltyService {

  constructor(public crud: CrudService) {
  }

/////////////////////////////////////////// Loyality Config Services////////////////////////////
  getAllLoyaltyData() {
    return this.crud.get('settings').map((data: Response) => data);
  }

  addLoyaltyData(LoyalityData: any) {
    const body = JSON.stringify(LoyalityData);
    return this.crud.post('settings', body).map((data: Response) => data);
  }

  updateLoyaltyData(LoyalityData: any, LoyalityId: any) {
    const body = JSON.stringify(LoyalityData);
    return this.crud.put('settings', body, LoyalityId).map((data: Response) => data);
  }

/////////////////////////////////////////// Coupon Services////////////////////////////

  getAllCouponData() {
    return this.crud.get('coupons').map((data: Response) => data);
  }

  getSingleCouponData(couponId) {
    return this.crud.getOne('coupons/single', couponId).map((data: Response) => data);
  }

  addCouponData(LoyalityData: any) {
    console.log('service called');
    const body = JSON.stringify(LoyalityData);
    return this.crud.post('coupons', body).map((data: Response) => data);
  }

  updateCouponData(LoyalityData: any, LoyalityId: any) {
    const body = JSON.stringify(LoyalityData);
    return this.crud.put('coupons', body, LoyalityId).map((data: Response) => data);
  }

  deleteCouponData(couponId) {
    return this.crud.delete('coupons', couponId).map((data: Response) => data);
  }
}
