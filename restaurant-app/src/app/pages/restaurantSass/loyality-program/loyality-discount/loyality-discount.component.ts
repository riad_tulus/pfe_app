import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {LoyaltyService} from '../loyality.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-loyality-discount',
  templateUrl: './loyality-discount.component.html',
  styleUrls: ['./loyality-discount.component.css'],
  providers: [LoyaltyService]
})
export class LoyalityDiscountComponent implements OnInit {
  public addDialog: boolean = false;
  public updateDialog: boolean = false;
  public couponTable: boolean = true;
  public allCouponsData: Array<any>;
  public addCoupon: any = {
    enable: true,
    couponName: '',
    restaurantID: '',
    location: '',
    offPrecentage: 0,
    applicableFrom: '',
    applicableTo: '',
    description: ''
  };
  public updateCouponData: any = {};
  public loading: boolean = true;
  public dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg: boolean = false;
  public errorImage = './assets/content/error.png';
  public noContent: any = './assets/content/nodata_cloud.png';
  private couponId: any;

  constructor(public restService: LoyaltyService, private toaster: ToastrService) {
    this.addCoupon.restaurantID = localStorage.getItem('Id');
    this.addCoupon.location = localStorage.getItem('locationId');
    this.getAllCouponsData();
  }

  getAllCouponsData() {
    this.restService.getAllCouponData().subscribe((res: any) => {
      this.allCouponsData = res;
      this.loading = false;
      if (this.allCouponsData.length >= 1) {
        this.dataLength = 1;
      } else {
        this.dataLength = 0;
      }
    }, (error) => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  openAddDialog() { // activating add coupon Dialog
    this.closeUpdateDialog();
    this.addDialog = true;
    this.couponTable = false;
  }

  onSaveCouponData() { // on saving new coupon Data
    this.restService.addCouponData(this.addCoupon).subscribe((res: any) => {
      this.getAllCouponsData();
      this.toaster.success('New Coupon Saved ', ' Coupon ');
    }, (error) => {
      this.toaster.error(error.message, 'Error');
    });
    this.closeAddDialog();
    this.couponTable = true;
  }

  closeAddDialog() { // deactivating add coupon Dialog
    this.couponTable = true;
    this.addDialog = false;
    this.addCoupon.couponName = ' ';
    this.addCoupon.offPrecentage = ' ';
    this.addCoupon.applicableFrom = ' ';
    this.addCoupon.applicableTo = ' ';
    this.addCoupon.description = ' ';
  }

  openUpdateDialog(CouponId: any) { // activating update coupon Dialog
    this.couponId = CouponId;
    this.couponTable = false;
    this.restService.getSingleCouponData(this.couponId).subscribe((res: any) => {
      this.addDialog = false;
      this.updateDialog = true;
      this.updateCouponData = res;
      this.couponId = res._id;
    }, (error) => {
      this.toaster.error(error.message, 'Error');
    });
  }

  onUpdateCouponData(form: NgForm) { // on updating coupon data
    this.restService.updateCouponData(this.updateCouponData, this.couponId)
      .subscribe((res: any) => {
        this.getAllCouponsData();
      }, (error) => {
        this.toaster.error(error.message, 'Error');
      });
    this.closeUpdateDialog();
    this.couponTable = true;
  }

  closeUpdateDialog() { // deactivating update coupon Dialog
    this.updateDialog = false;
    this.couponTable = true;
  }

  onDisable(couponId: any) { // on disable coupon
    this.restService.getSingleCouponData(couponId).subscribe((res: any) => {
      res.enable = (!res.enable);
      this.restService.updateCouponData(res, couponId)
        .subscribe((res) => {
          this.toaster.success('Coupon status changed');
        }, (error) => {
          this.toaster.error(error.message, 'Error');
        });
    });
  }


  ngOnInit() {
  }

  onDelete(couponId) { // on delete coupon
    if (confirm('If you want to delete,Press OK button!') == true) {
      this.restService.deleteCouponData(couponId).subscribe((res) => {
        this.getAllCouponsData();
      }, (error) => {
        this.toaster.error('error occurred ..unable to delete');
      });
    }
  }
}
