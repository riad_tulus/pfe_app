import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyalityDiscountComponent } from './loyality-discount.component';

describe('LoyalityDiscountComponent', () => {
  let component: LoyalityDiscountComponent;
  let fixture: ComponentFixture<LoyalityDiscountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoyalityDiscountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoyalityDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
