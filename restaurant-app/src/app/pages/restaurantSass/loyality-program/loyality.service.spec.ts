import { TestBed, inject } from '@angular/core/testing';

import { LoyaltyService } from './loyality.service';
import { CrudService } from '../../../services/crud.service';

describe('LoyaltyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoyaltyService , CrudService]
    });
  });

  it('should ...', inject([LoyaltyService], (service: LoyaltyService) => {
    expect(service).toBeTruthy();
  }));
});
