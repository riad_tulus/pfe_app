import {Component, OnInit} from '@angular/core';
import {LoyaltyService} from '../loyality.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-loyality-config',
  templateUrl: './loyality-config.component.html',
  styleUrls: ['./loyality-config.component.css'],
  providers: [LoyaltyService]
})
export class LoyalityConfigComponent implements OnInit {

  public loyaltyData: any = {
    restaurantID: '',
    loyalityProgram: false,
    minLoyalityPoints: 0,
    minOrdLoyality: 0,
    loyalityPercentage: 0
  };

  public loayaltyData: Array<any>;
  public loading: boolean = true;
  public dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg: boolean = false;
  public errorImage = './assets/content/error.png';
  public noContent: any = './assets/content/nodata_cloud.png';

  constructor(public loyaltyService: LoyaltyService, private toaster: ToastrService) {
    this.getAllLoyaltyData();
    this.loyaltyData.restaurantID = localStorage.getItem('Id');
  }

  getAllLoyaltyData() { // fetch loyalty information from server
    this.loyaltyService.getAllLoyaltyData().subscribe((res: any) => {
      this.loayaltyData = res;
      this.loading = false;
      if (this.loayaltyData.length > 0) {
        this.dataLength = 1;
        this.loyaltyData = this.loayaltyData[0];
      } else {
        this.dataLength = 0;
      }
    }, () => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  ngOnInit() {
  }

  OnSetLoyalty() { // invoked when user want to Add or Update Loyalty program
    console.log(' OnSetLoyalty ' + JSON.stringify(this.loyaltyData));
    if (this.dataLength === 1) {
      const loyaltyDataId = this.loyaltyData._id;
      console.log('update');
      this.loyaltyService.updateLoyaltyData(this.loyaltyData, loyaltyDataId).subscribe((res: any) => {
        this.toaster.info('Data saved ', ' Updated ');
      });
    } else {
      this.loayaltyData.push(this.loyaltyData);
      this.loyaltyService.addLoyaltyData(this.loyaltyData).subscribe((res: any) => {
        this.toaster.success('Data saved ', ' success ');
      });
    }
  }

}
