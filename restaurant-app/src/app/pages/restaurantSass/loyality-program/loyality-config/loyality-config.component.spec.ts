import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyalityConfigComponent } from './loyality-config.component';

describe('LoyalityConfigComponent', () => {
  let component: LoyalityConfigComponent;
  let fixture: ComponentFixture<LoyalityConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoyalityConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoyalityConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
