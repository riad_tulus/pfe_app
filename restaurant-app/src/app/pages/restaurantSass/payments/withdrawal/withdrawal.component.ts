import {Component, OnInit} from '@angular/core';
import {PaymentsService} from '../payments.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.css'],
  providers: [PaymentsService]
})
export class WithdrawalComponent implements OnInit {
  public banks: any [] = [];
  public withdrawalRequest: any = {
    receiverId: '',
    bankDetail: '',
    amount: '',
    transactionStatus: 'Pending'
  }

  public walletAmount = 0;
  public restaurantId: any;

  constructor(private restService: PaymentsService, private toaster: ToastrService, private router: Router) {
    this.restaurantId = localStorage.getItem('Id');
    this.getAllAccountInfo();
    this.getWalletCountData();
    this.withdrawalRequest.receiverId = this.restaurantId;
  }

  getAllAccountInfo() { // fetch all the saved account data from server
    this.restService.getAllAccountData().subscribe((res: any) => {
      this.banks = res;
    });
  }
  getWalletCountData() { // get available wallet amount
    this.restService.getWalletCount(this.restaurantId).subscribe((res: any) => {
      this.walletAmount = res.availableBalance;
    });
  }
  onWithdrawalRequest() { // invoke when user create new withdrawal request
    if (this.withdrawalRequest.amount > this.walletAmount) {
      this.toaster.warning('You don\'t have enough amount', 'Withdrawal');
    } else {
      this.restService.onWithdrawalRequest(this.withdrawalRequest).subscribe((res) => {
        this.router.navigate(['/pietech/wallet']);
      });
    }
  }

  ngOnInit() {
  }

}
