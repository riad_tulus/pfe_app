import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class PaymentsService {

  constructor(public crud: CrudService) {
  }
  // get sales history by restaurant Id
  getLocationSalesHistory(restaurantId) {
    return this.crud.getOne('wallets/collection/details', restaurantId).map((data: Response) => data);
  }
  // get All Saved Account Information
  getAllAccountData() {
    return this.crud.get('accountdetails').map((data: Response) => data);
  }
  // get wallet counts like total earning, total withdrawal and remaining wallet balance
  getWalletCount(restaurantId) {
    return this.crud.getOne('wallets/restaurant/owner', restaurantId).map((data: Response) => data);
  }

  // add new Bank Account Data
  addBankAccountData(accountInfo: any) {
    const body = JSON.stringify(accountInfo);
    return this.crud.post('accountdetails', body).map((data: Response) => data);
  }

  // update specific Bank Account Data
  updateBankAccountData(accountData: any, accountId: any) {
    const body = JSON.stringify(accountData);
    return this.crud.put('accountdetails', body, accountId).map((data: Response) => data);
  }
  // request for a Withdrawal
  onWithdrawalRequest(withdrawalInfo: any) {
    const body = JSON.stringify(withdrawalInfo);
    return this.crud.post('wallets', body).map((data: Response) => data);
  }
  // fetch all withdrawal related information
  getWithdrawalHistory() {
    return this.crud.get('wallets/restaurant/all').map((data: Response) => data);
  }
  // while user want to cancel withdrawal request
  onUpdateWithdrawalRequest(withdrawalInfo: any, docId: any) {
    const body = JSON.stringify(withdrawalInfo);
    return this.crud.put('wallets', body, docId).map((data: Response) => data);
  }


}
