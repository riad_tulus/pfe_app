import {Component, OnInit} from '@angular/core';
import {PaymentsService} from '../payments.service';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css'],
  providers: [PaymentsService]
})
export class BankComponent implements OnInit {
  public showUpdateForm = false;
  public showAddForm = false;
  public showBankDetails = true;
  public bankDetails: any [] = [];
  private updateAccountId: any;

  public addBank: any = {
    accountHolderName: '',
    bankName: '',
    accountNumber: '',
    ifscCode: ''
  }

  public updateBank: any = {
    accountHolderName: '',
    bankName: '',
    accountNumber: '',
    ifscCode: ''
  };

  constructor(private restService: PaymentsService) {
    this.getAllAccountInfo(); // fetching all saved acount from server while component active
  }

  getAllAccountInfo() { // invoke when user want to fetch saved acount from server while component active
    this.restService.getAllAccountData().subscribe((res: any) => {
      this.bankDetails = res;
    });
  }

  openAddForm() { // show add bank form
    this.showAddForm = true;
    this.showBankDetails = false;
    this.showUpdateForm = false;
  }

  onSaveBank() { // invoke when user want to add new bank detail
    this.restService.addBankAccountData(this.addBank).subscribe((res: any) => {
      this.closeAddForm();
      this.getAllAccountInfo();
    }, () => {
      this.closeAddForm();
      this.getAllAccountInfo();
    });
  }

  closeAddForm() { // reset all the variables and hiding add bank block and update block
    this.showAddForm = false;
    this.showBankDetails = true;
    this.showUpdateForm = false;
    this.addBank.accountHolderName = '';
    this.addBank.bankName = '';
    this.addBank.accountNumber = '';
    this.addBank.ifscCode = '';
  }

  openUpdateForm(index) { // invoke when user want to update bank detail
    this.updateBank = this.bankDetails[index];
    this.updateAccountId = this.updateBank._id;
    this.showAddForm = false;
    this.showBankDetails = false;
    this.showUpdateForm = true;
  }

  onUpdateBank() { // invoke when want to update saved bank detail
    this.updateBank.flag = 0;
    this.restService.updateBankAccountData(this.updateBank, this.updateAccountId).subscribe((res: any) => {
      this.closeAddForm();

    }, () => {
      this.closeAddForm();

    });
  }

  ngOnInit() {
  }

}
