import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentsService } from './payments.service';
import { SharedService } from '../../../layouts/shared-service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css'],
  providers: [PaymentsService]
})
export class PaymentsComponent implements OnInit {
  pageTitle = 'Paiement '
  restaurantId: any;
  public walletCounts = {
    totalOrderEarning: 0,
    withdrawlAmount: 0,
    availableBalance: 0,
  };
  public salesHistory: any[] = [];

  constructor(private _sharedService: SharedService, private router: Router, private restService: PaymentsService) {
    this._sharedService.emitChange(this.pageTitle);
    this.restaurantId = localStorage.getItem('Id');
    this.getSalesHistory();
    this.getAllAccountData();
  }

  getSalesHistory() { // fetch sales history of all location by using restaurant Id
    this.restService.getLocationSalesHistory(this.restaurantId).subscribe((res: any) => {
      this.salesHistory = res;
    });
  }

  getAllAccountData() { // fetch Account information like total Order Earning, withdrawal Amount and available Balance
    this.restService.getWalletCount(this.restaurantId).subscribe((res: any) => {
      this.walletCounts = res;
    });
  }

  ngOnInit() {
  }

  onView(locationId) { // navigate to view specific location sales report
    return this.router.navigate(['pietech/sales', locationId]);
  }

  wallet() { // navigate to wallet page
    return this.router.navigate(['pietech/wallet']);
  }

}
