import {Component, OnInit} from '@angular/core';
import {PaymentsService} from '../payments.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css'],
  providers: [PaymentsService]
})
export class WalletComponent implements OnInit {
  private restaurantId: any = '';
  public withdrawalHistory: any [] = [];
  public countData: any = {
    totalOrderEarning: 0,
    withdrawlAmount: 0,
    availableBalance: 0
  };

  constructor(private restService: PaymentsService, private toaster: ToastrService) {
    this.restaurantId = localStorage.getItem('Id');
    this.getCountData();
    this.getWithdrawalHistory();
  }
  // fetch total earning, withdrawal amount and available wallet amount
  getCountData() {
    this.restService.getWalletCount(this.restaurantId).subscribe((res: any) => {
      this.countData = res;
    });
  }
  // fetch all the withdrawal history from server
  getWithdrawalHistory() {
    this.restService.getWithdrawalHistory().subscribe((res: any) => {
      this.withdrawalHistory = res;
    });
  }
  /* * cancel method when user want to cancel the withdrawal request,
     * before Accept or reject by Admin */
  cancel(index) {
    const data = this.withdrawalHistory[index];
    data.transactionStatus = 'Cancelled';
    this.restService.onUpdateWithdrawalRequest(data, data._id).subscribe((res: any) => {
      this.toaster.info('Request Cancelled', 'Withdrawal');
    }, () => {
      this.toaster.error('Something is going wrong', 'Error');
    });
  }

  ngOnInit() {
  }

}
