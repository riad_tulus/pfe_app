import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SharedService} from '../../../../layouts/shared-service';
import {NgForm} from '@angular/forms';
import {MyinfoService} from '../myinfo.service';

@Component({
  selector: 'app-view-info',
  templateUrl: './view-info.component.html',
  styleUrls: ['./view-info.component.css'],
  providers: [MyinfoService]
})
export class ViewInfoComponent implements OnInit {

  loading = true;
  errorImg = false;
  errorImage = './assets/content/error.png';

  form: NgForm;
  restaurantsDetails: any = {
    newAddress: '',
    locationName: '',
    name: '',
    city: '',
    state: '',
    country: '',
    zip: '',
    aboutUs: '',
    contactPerson: '',
    contactNumber: '',
    alternateTelephone: '',
  };
  public myLocation: any
  public user: any;
  public update = false;
  public myRestaurant: any;

  constructor(private _sharedService: SharedService, private router: Router, private restService: MyinfoService) {

    this.restaurantInfo();
    this.myLocation = localStorage.getItem('locationName');
    this.myRestaurant = localStorage.getItem('restaurantName');
    this.user = localStorage.getItem('role');

    if (this.user === 'Manager' || this.user === 'staff') {
      this.update = true;
    }
  }

  onUpdate() {
    return this.router.navigate(['/pietech/information/edit']);
  }

  restaurantInfo() {
    this.restService.getUserInfo().subscribe((res: any) =>{
      this.restaurantsDetails = res;
      this.loading = false;
    }, () => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  back() {
    return this.router.navigate(['/pietech/dashboard']);
  }

  ngOnInit() {

  }

}
