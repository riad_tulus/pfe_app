import {inject, TestBed} from '@angular/core/testing';

import {MyinfoService} from './myinfo.service';

describe('MyinfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyinfoService]
    });
  });

  it('should be created', inject([MyinfoService], (service: MyinfoService) => {
    expect(service).toBeTruthy();
  }));
});
