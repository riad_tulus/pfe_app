import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {MyinfoComponent} from './myinfo.component';
import {MaterialModule} from '@angular/material';
import {FileUploadModule} from 'ng2-file-upload';
import {A2CardComponent} from '../../../ni-components/card/card.component';
import {MyinfoService} from './myinfo.service';
import {ToastrModule} from 'ngx-toastr';
import {ConstantService} from '../../../services/constant.service';
import {CrudService} from '../../../services/crud.service';
import {SharedService} from '../../../layouts/shared-service';

fdescribe('MyinfoComponent', () => {
  let component: MyinfoComponent;
  let fixture: ComponentFixture<MyinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyinfoComponent, A2CardComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [MaterialModule, FormsModule, FileUploadModule, ToastrModule.forRoot(), RouterTestingModule.withRoutes([])],
      providers: [MyinfoService, ConstantService, CrudService, SharedService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
