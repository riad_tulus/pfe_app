import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../layouts/shared-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MyinfoService } from './myinfo.service';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-myinfo',
  templateUrl: './myinfo.component.html',
  styleUrls: ['./myinfo.component.css'],
  providers: [MyinfoService]
})
export class MyinfoComponent implements OnInit {

  pageTitle = 'Restaurant Info';
  loading = true;
  public check = false;
  errorImg = false;
  public myRestaurant: any;
  public myLocation: any;
  public update = false;

  errorImage = './assets/content/error.png';
  url1: any = './assets/content/selectImg.png';
  user: any;
  userInfo: any = {};
  restaurantId: any;

  restaurantDetails: any = {
    _id: '',
    restaurantName: '',
    name: '',
    logo: '',
    publicId: '',
    deletePublicId: '',
    contactPerson: '',
    description: '',
    address: '',
    city: '',
    state: '',
    zip: '',
    country: '',
    locationName: '',
    contactNumber: '',
    alternateTelephone: '',
    currency: '',
    aboutUs: '',
    file: '',
    flag: 0
  };


  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions(this.restService.cloudinarUpload)
  );

  constructor(private _sharedService: SharedService, private route: ActivatedRoute, private router: Router, private restService: MyinfoService, private toster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.restaurantId = localStorage.getItem('Id');
    this.user = localStorage.getItem('role');

    if (this.user !== 'Owner') {
      this.update = true;
      this.myLocation = localStorage.getItem('locationName');
      this.myRestaurant = localStorage.getItem('restaurantName');
    }

    if (this.restaurantId != null) {
      this.userDataInfo();
    }

  }

  userDataInfo() {
    // console.log('My info');
    this.restService.getUserInfo().subscribe((res: any) => {
      this.loading = false;
      if (!res.publicId) {
        this.restaurantDetails = res;
        this.restaurantDetails.deletePublicId = '';
      } else {
        this.restaurantDetails = res;
        this.restaurantDetails.deletePublicId = res.publicId;
        this.url1 = res.logo;
      }
    }, () => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  ngOnInit() {

  }

  readUrl1(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.url1 = event.target.result;
        this.restaurantDetails.flag = 1;
        this.check = true;
      }

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  onUpdateRestaurant() {
    // console.log('before update' + JSON.stringify(this.restaurantDetails));
    if (this.check) {
      this.uploader.uploadAll();
      this.uploader.onSuccessItem = (item: any, response: any, status: number, headers: any): any => {
        const res: any = JSON.parse(response);
        this.restaurantDetails.logo = res.secure_url;
        this.restaurantDetails.publicId = res.public_id;
        this.restService.updateUserInfo(this.restaurantDetails, this.restaurantDetails._id).subscribe((res: any) => {
          this.restaurantDetails = res;
          localStorage.setItem('img', res.logo);
          this.toster.success('updated successfully');
          this.router.navigate(['/pietech/information/view']);


        }, () => {
          this.toster.error('error arrived');
        });
      };
    } else {
      this.restaurantDetails.flag = 0;
      this.restService.updateUserInfo(this.restaurantDetails, this.restaurantDetails._id).subscribe((res: any) => {
        this.restaurantDetails = res;
        // console.log(' after updated data' + JSON.stringify( this.restaurantDetails));
        this.toster.success('updated successfully');
        this.router.navigate(['/pietech/information/view']);
      }, () => {
        this.toster.error('error arrived');
      });
    }
  }

  back() {
    return this.router.navigate(['/pietech/information/view']);
  }

}
