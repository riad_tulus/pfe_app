import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {ConstantService} from '../../../services/constant.service';
import {CrudService} from '../../../services/crud.service';

@Injectable()

export class MyinfoService {
  cloudinarUpload: any;

  constructor(private http: Http, private constantService: ConstantService, public crud: CrudService) {
    this.cloudinarUpload = this.constantService.cloudinarUpload;
  }

// to get user data
  getUserInfo() {
    return this.crud.get('users/me').map((data: Response) => data);
  }

// update user
  updateUserInfo(entityData: any, restaurantId: any) {
    const body = JSON.stringify(entityData);
    return this.crud.put('users', body, restaurantId).map((data: Response) => data);

  }
}
