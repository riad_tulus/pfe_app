import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {NotificationService} from './notification.service';
import {ToastrService} from 'ngx-toastr';
import {SharedService} from '../../../layouts/shared-service';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  providers: [NotificationService]
})
export class NotificationComponent implements OnInit {
  pageTitle = 'Notification'
  form: NgForm
  public notification: any = {}
  public isLoading: boolean = false;
  // "message" is a Configuration object with OneSignal credentials
  message: any = {
    app_id: '05ac6c51-8416-41ab-8779-b48503bbe125',
    contents: {'en': ''},
    headings: {'en': ''},
    included_segments: ['All']
  };

  constructor(private _sharedService: SharedService,
              public router: Router,
              public pushNotification: NotificationService,
              public toster: ToastrService) {

    this._sharedService.emitChange(this.pageTitle);
  }

// sendNotification method invoke when Manager or Owner want sent notification to all users.
  sendNotification() {
    this.isLoading = !(this.isLoading);
    this.pushNotification.sendNotification(this.message).subscribe(res => {
      this.isLoading = !(this.isLoading);
      this.toster.success('Notification Send Successfully!', 'Success!');
    }, (error) => {
      this.isLoading = !(this.isLoading);
      this.toster.error('Error');
    })

  }

  cancel() {
    this.router.navigate(['/coupons/all']);

  }

  ngOnInit() {
  }

}
