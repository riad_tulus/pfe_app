import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class NotificationService {
  url: any = 'https://onesignal.com/api/v1/notifications'; // one signal API
  constructor(private http: Http) {}
  // method "sendNotification" invoke when notification send to all user of Restaurant SASS platform.
  sendNotification(message) {
    const body = JSON.stringify(message);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Basic YzdiOGU3ZWUtMzYxZS00MzVmLThiODctOTg1MDU0ZTBhOTkw');
    return this.http.post(this.url, body, {
      headers: headers
    }).map((data: Response) => data.json())
  }
}
