export interface UnreadCount {
  count: number;
  _chatId: string;
}
