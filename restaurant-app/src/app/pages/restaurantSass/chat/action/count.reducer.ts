import {UnreadCount} from './count';
import * as CountAction from './count.action';


export type Action = CountAction.ALl;

// default appState

const defaultState: UnreadCount = {
  count: +(localStorage.getItem('unreadCount')),
  _chatId: null
}

// Helper function to create new state object

const newState = (state, newData) => {
  return Object.assign({}, state, newData);
}

export function countReducer(state: UnreadCount = defaultState, action: Action) {
  //.log(action.type, state);
  switch (action.type) {
    case CountAction.UPDATE_COUNT :
      return newState(state, {count: state.count + action.payload});

    case CountAction.DOWN_COUNT :
      return newState(state, {count: state.count - action.payload})

    case CountAction.SET :
      return newState(state, {count: action.payload})

    case CountAction.ASSIGN_ID :
      return newState(state, {_chatId: action.payload})
    default:
      return state;
  }
}
