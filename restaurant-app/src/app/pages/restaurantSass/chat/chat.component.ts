import {Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import * as CountAction from './action/count.action';
import {UnreadCount} from './action/count';

import {ChatService} from './chat.service';
import {SocketSharedService} from '../../../services/socketShare.service';
import {userlist} from './chat';

declare var $: any;

interface AppState {
  chatId: string;
}

interface CountState {
  count: UnreadCount;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  providers: [ChatService]
})
export class ChatComponent implements OnInit, OnDestroy {
  count: number = 0; // variable for unread message
  messages$: any; // Observable
  countUpdate: Observable<UnreadCount>; // Observable
  public userlist: userlist[] = [];
  /**if user role "Manager", userList store Customer Data,
   *else if user role "Owner" userList store Manager Data*/
  public adminlist: userlist[] = [];
  /**if user role "Manager", adminlist store Owner Data,
   *else if user role "Owner" adminlist store Admin Data*/
  public managerlistLength: number = 0;
  public adminlistLength: number = 0;
  public warningMessage: any = ''; // warningMessage for, if no one customer contact with Manager
  public userRole: string; // userRole store login user role
  public userId: any; // userId store login user _id
  public username: any = ' '; // username store active chat user name
  public chatData: any[] = []; // chatData store all the chat thread messages
  public renderChatlist: number = 0;
  /** renderChatlist initialize with 0
   *  renderChatlist = 1; while start loading chat Threads;
   *  renderChatlist = 2; when chat Threads loaded successfully */
  public disableScroll: boolean = false; // disableScroll is the property of infinite scroll
  public message: any = {
    message: '',
    sender: '',
    receiver: '',
    receiverRole: ''
  };
  public searchManagerName: string;
  public activateSearch = false;
  private listOrder: string;
  /** there are two types of thread
   *  listOne and listTwo*/
  private storedChatId: any; // storedChatId store active chat thread Id
  private fromConstructor: boolean = false;
  private getChatPageNumber: any = 0; // use for infinite scroll

  constructor(public el: ElementRef,
              private restService: ChatService,
              private SharedService: SocketSharedService,
              private store: Store<AppState>,
              private counter: Store<CountState>) {
    this.userRole = localStorage.getItem('role');
    this.fromConstructor = true;
    this.messages$ = this.store.select('chatId'); // store ChatId from ngRx Store

    this.messages$.subscribe((res: any) => { // check stored chatId is valid or not
      const chatType = localStorage.getItem('chatType');
      if ((this.messages$.actionsObserver._value.payload !== (undefined || null)) && (chatType !== '')) {
        this.storedChatId = this.messages$.actionsObserver._value.payload;
        if (this.storedChatId !== undefined && this.storedChatId.length > 6) {
          this.startConversation(this.storedChatId, chatType); // load chat thread message
        }
      }
    });

    this.countUpdate = this.counter.select('count'); // update thread unread count from ngRx Store
    this.userId = localStorage.getItem('userId');
    this.message.sender = localStorage.getItem('userId');

    if (this.userRole === 'Owner') {
      this.getDataByOwnerRole();
    } else {
      if (this.userRole === 'Manager') {
        this.getDataByManagerRole();
      }
    }
  }

  getDataByManagerRole() {
    this.getUserChatListData(); // retrieve Users list from server
    this.getOwnerChatListData(); // retrieve Owner list from server
  }

  getDataByOwnerRole() {
    this.getManagerChatListData(); // retrieve Managers list from server
    this.getAdminChatListData(); // retrieve Admin list from server
  }
  getManagerChatListData() { // fetching Manager List data from server
    this.restService.getManagerChatList().subscribe((userlist: any) => {
      if (userlist.message === 'You do not have any user who contacted you atleast once.') {
        this.managerlistLength = 1;
        this.warningMessage = userlist.message;
      } else {
        this.userlist = userlist;
        this.managerlistLength = this.userlist.length;
      }
    });
  }

  getAdminChatListData() { // fetching Admin List data from server
    this.restService.getAdminChatList().subscribe((adminlist: any) => {
      if (adminlist.message === 'You do not have any user who contacted you atleast once.') {
      } else {
        this.adminlist = adminlist;
        this.adminlistLength = this.adminlist.length;
      }
    });
  }

  getUserChatListData() { // fetching User List data from server
    this.managerlistLength = 0;
    this.restService.getUserChatList().subscribe((userList: any) => {
      console.log('user list ' + JSON.stringify((userList)));
      if (userList.message) {
        this.managerlistLength = 1;
        this.warningMessage = userList.message;
      } else {
        this.userlist = userList;
        this.managerlistLength = this.userlist.length;
      }
    });
  }

  getOwnerChatListData() { // getOwnerChatList
    this.restService.getOwnerChatList().subscribe((adminlist: any) => {
      if (adminlist.message === 'You do not have any user who contacted you atleast once.') {
      } else {
        this.adminlist = adminlist;
        this.adminlistLength = this.adminlist.length;
      }
    });
  }


  startConversation(userId, listOrder, data?) { // start conversation
    console.log('user Id ' + userId + ' ListOrder ' + listOrder);
    this.listOrder = listOrder;
    if ((data !== undefined) && (data.count > 0)) {
      const updateCount: number = +(data.count);
      this.counter.dispatch(new CountAction.DownGradeCount(updateCount));
    }
    if (userId !== '') {
      this.restService.readSingleMessageThread(userId).subscribe((res: any) => {
        const threadCount = 0;
        if (listOrder === 'listOne') {
          const chatIndex = this.userlist.findIndex(x => x._id === userId);
          this.userlist[chatIndex].count = threadCount;
        } else if (listOrder === 'listTwo') {
          const chatIndex = this.adminlist.findIndex(x => x._id == userId);
          this.adminlist[chatIndex].count = threadCount;
        }
      });
    }
    this.getChatPageNumber = 0;
    if (this.storedChatId === userId && (!this.fromConstructor)) {

    } else {
      this.fromConstructor = false;
      this.disableScroll = false;
      this.storedChatId = userId;
      localStorage.setItem('chatType', listOrder);
      this.counter.dispatch(new CountAction.AssignId(userId));
      if (listOrder === 'listOne' && this.userRole === 'Owner') {
        this.renderChatlist = 1;
        this.message.receiverRole = 'Manager';
        this.restService.getOwnerManagerMessageData(userId, this.getChatPageNumber).subscribe((res: any) => {
          this.chatData = res.messages;
          this.username = res.sender.name;
          this.getChatPageNumber = this.getChatPageNumber + 1;
          this.renderChatlist = 2;
          setTimeout(() => {
            this.scrollToBottom();
          });
        });
        this.message.receiver = userId;

      } else {
        if (listOrder === 'listOne' && this.userRole === 'Manager') {
          this.renderChatlist = 1;
          this.message.receiverRole = 'User' || 'user';
          this.restService.getOwnerManagerMessageData(userId, this.getChatPageNumber).subscribe((res: any) => {
            ;
            this.chatData = res.messages;
            this.username = res.sender.name;
            this.getChatPageNumber = this.getChatPageNumber + 1;
            this.renderChatlist = 2;
            setTimeout(() => {
              this.scrollToBottom();
            });
          });
          this.message.receiver = userId;
        }
      }

      if (listOrder === 'listTwo' && this.userRole === 'Owner') {
        this.renderChatlist = 1;
        this.message.receiverRole = 'Admin';
        this.restService.getOwnerManagerMessageData(userId, this.getChatPageNumber).subscribe((res: any) => {
          this.chatData = res.messages;
          this.username = res.sender.name;
          this.getChatPageNumber = this.getChatPageNumber + 1;
          this.renderChatlist = 2;
          setTimeout(() => {
            this.scrollToBottom();
          });
        });
        this.message.receiver = userId;
      } else {
        if (listOrder === 'listTwo' && this.userRole === 'Manager') {
          this.renderChatlist = 1;
          this.message.receiverRole = 'Owner';
          this.restService.getOwnerManagerMessageData(userId, this.getChatPageNumber).subscribe((res: any) => {
            ;
            this.chatData = res.messages;
            this.username = res.sender.name;
            this.renderChatlist = 2;
            this.getChatPageNumber = res.nextpageno;
            setTimeout(() => {
              this.scrollToBottom();
            });
          });
          this.message.receiver = userId;
        }
      }
    }// outer else close
  }

  onSendMessage() { // when send new message
    if (this.chatData.length < 1) {
      this.chatData = [];
    }
    this.restService.saveMessageData(this.message).subscribe((res: any) => {
      this.chatData.push(res);
      this.message.message = '';
      setTimeout(() => {
        this.scrollToBottom();
      });
    }, (error) => {
    });
  }

  updateCurrentChatReadCount() { // updating thread read/unread count
    this.SharedService.messageRead(this.storedChatId);
  }

  searchManagerChat() { // search chat thread from manager list.
    this.activateSearch = true;
    const index = this.userlist.map((o) => o.name).indexOf(this.searchManagerName);
    if (index > 0) {
      this.activateSearch = false;
      this.userlist.unshift(this.userlist[index]);
      this.userlist.splice(index + 1, 1);
    } else {
      setTimeout(() => this.activateSearch = false, 2500);
    }
  }

  ngOnInit() {
    /** Declaring all the method inside ngOnInit block which invoke when observable response
     * getMessageNotification() invoke when new message arrive and socket emit new message */
    this.SharedService.getMessageNotification().subscribe((message: any) => {
      if (message._id === this.storedChatId) {
        const countZero = 0;
        message.count = countZero;
      }
      if (this.userRole === 'Owner') {
        if (message.senderRole === 'Manager') {
          const data: any = message;
          const index = this.userlist.findIndex(x => x._id === data._id);
          if (index >= 0) {
            this.userlist.splice(index, 1);
            this.userlist.unshift(data);

          } else {
            this.userlist.unshift(data);
          }
        } else {
          if (message.senderRole === 'Admin') {
            const data: any = message;
            const index = this.adminlist.findIndex(x => x._id === data._id);
            if (index >= 0) {
              this.adminlist.splice(index, 1);
              this.adminlist.unshift(data);
            } else {
              this.adminlist.unshift(data);
            }
          }
        }
      } else { // Outer If Close else Start
        if (this.userRole === 'Manager') {
          if (message.senderRole === 'User' || message.senderRole === 'user') {
            const data: any = message;
            const index = this.userlist.findIndex(x => x._id == data._id);

            // console.log("index" + index);
            if (index >= 0) {
              this.userlist.splice(index, 1);
              this.userlist.unshift(data);

            } else {
              this.userlist.unshift(data);
            }
          } else {
            if (message.senderRole == 'Owner') {
              const data: any = message;
              const index = this.adminlist.findIndex(x => x._id == data._id);

              // console.log("index" + index);
              if (index >= 0) {
                this.adminlist.splice(index, 1);
                this.adminlist.unshift(data);

              } else {
                this.adminlist.unshift(data);
              }
            }
          }
        } // elseIf close
      }
    });
    this.SharedService.getMessages(this.userId).subscribe((message) => {
      this.messageUpdated(message);
    });
  }

  messageUpdated(message) { // while new message arrived update on message list
    if (message.sender == this.message.receiver) {
      this.chatData.push(message);
      setTimeout(() => {
        this.scrollToBottom();
      });
    }
  }

  scrollToBottom() { // scroll to bottom when chat message load
    const scrollPane: any = this.el
      .nativeElement.querySelector('.chat-text');
    scrollPane.scrollTop = scrollPane.scrollHeight;
  }

  onScrollUp() { // load chat thread message while user scroll up
    if (this.getChatPageNumber != null) {
      this.restService.getOwnerManagerMessageData(this.message.receiver, this.getChatPageNumber).subscribe((res: any) => {
        const length = res.messages.length;
        if (res.nextpageno === null) {
          this.disableScroll = true;
        }
        for (let i = 0; i < length; i++) {
          this.chatData.unshift(res.messages[i]);
        }
        this.getChatPageNumber = res.nextpageno;
      });
    } else {
    }


  }


  ngOnDestroy() {
    // resetting chatId on NgRx Store
    this.counter.dispatch(new CountAction.AssignId(''));
  }

}

