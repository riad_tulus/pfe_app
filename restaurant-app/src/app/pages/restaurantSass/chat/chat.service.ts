import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";
import {userlist,chatData, userId} from './chat';
import { ConstantService } from  '../../../services/constant.service';


@Injectable()
export class ChatService {
authToken:any;
cloudinarUpload:any;
  constructor(private http: Http, public constantService: ConstantService) { 
        this.authToken=localStorage.getItem('token');
        this.cloudinarUpload=this.constantService.cloudinarUpload;
    }


   getManagerChatList():Observable<userlist[]> { // If user role OWNER
      // console.log("token " + JSON.stringify(this.authToken));
         const headers = new Headers();
         headers.append('Authorization', this.authToken);
         headers.append('Content-Type', 'application/json');
         return this.http.get(this.constantService.API_ENDPOINT+'messages/managers/list',{
            headers:headers
           })
            .map(this.extractUserData)
            .catch(this.handleError);
    } 

    getAdminChatList():Observable<userlist[]> {  // If user role OWNER
      // console.log("token " + JSON.stringify(this.authToken));
         const headers = new Headers();
         headers.append('Authorization', this.authToken);
         headers.append('Content-Type', 'application/json');
         return this.http.get(this.constantService.API_ENDPOINT+'messages/admins/list',{
            headers:headers
           })
            .map(this.extractUserData)
            .catch(this.handleError);
    }

    getUserChatList():Observable<userlist[]> {  // If user role MANAGER 
      // console.log("token " + JSON.stringify(this.authToken));
         const headers = new Headers();
         headers.append('Authorization', this.authToken);
         headers.append('Content-Type', 'application/json');
         return this.http.get(this.constantService.API_ENDPOINT+'messages/users/list/by/manager',{
            headers:headers
           })
            .map(this.extractUserData)
            .catch(this.handleError);
    }
    getOwnerChatList():Observable<userlist[]> {  // If user role MANAGER
      // console.log("token " + JSON.stringify(this.authToken));
         const headers = new Headers();
         headers.append('Authorization', this.authToken);
         headers.append('Content-Type', 'application/json');
         return this.http.get(this.constantService.API_ENDPOINT+'messages/owners/list',{
            headers:headers
           })
            .map(this.extractUserData)
            .catch(this.handleError);
    }


    getOwnerManagerMessageData(userId,chatPageNumber):Observable<chatData> {
      //console.log("token " + JSON.stringify(this.authToken));
         const headers = new Headers();
         headers.append('Authorization', this.authToken);
         headers.append('Content-Type', 'application/json');
        return this.http.get(this.constantService.API_ENDPOINT+'messages/owner/manager/'+userId+'/'+chatPageNumber,{

            headers:headers
           })
            .map(this.extractFollowerData)
            .catch(this.handleError);
    }

    saveMessageData(chatData):Observable<chatData> {
          const body = JSON.stringify(chatData);
         // console.log("body" + body);
        //  console.log("token " + JSON.stringify(this.authToken));
          const headers = new Headers();
          headers.append('Authorization', this.authToken);
          headers.append('Content-Type', 'application/json');
            return this.http.post(this.constantService.API_ENDPOINT + 'messages/', body, {
                headers: headers
            })
            .map((data: Response) => data.json())
     }
    
    searchUsersData(searchKey:any,flag:number){ //Employee as well as users
        const headers = new Headers();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get(this.constantService.API_ENDPOINT+'users/search/chat/'+searchKey+'/'+flag, {
        headers: headers
    })
    .map((data: Response) => data.json())
     .catch(this.handleError);
  }

  readSingleMessageThread(chatUserId){
           const headers = new Headers();
           headers.append('Content-Type', 'application/json');
           let authToken = localStorage.getItem('token');
           headers.append('Authorization', authToken);
           return this.http.get(this.constantService.API_ENDPOINT+'messages/single/thread/read/'+chatUserId, {
          headers: headers
      })
      .map((data: Response) => data.json())
       .catch(this.handleError);
   }

    private extractUserData(res:Response) {
        let body = res.json();
        return body || {};
    }
     private extractFollowerData(res:Response) {
        let body = res.json();
        return body || [];
    }

    private handleError(error:any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
       // console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}
