import { Component, OnInit } from '@angular/core';
import { SalesService } from './sales.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../../layouts/shared-service';


@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css'],
  providers: [SalesService]
})

export class SalesComponent implements OnInit {
  pageTitle = 'Ventes';
  public role = '';
  public salesData: any[] = [];
  public locationData: any[] = [];
  public countData: any = {
    allorders: 0,
    onemonthorders: 0,
    todayorders: 0
  };
  loading = true;
  dataLength = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg = false;
  errorImage = './assets/content/error.png';
  noContent: any = './assets/content/nodata_cloud.png';

  public locationName = 'loading...';
  private locationId: any = '';
  private restaurantId: any;

  constructor(private _sharedService: SharedService, private restService: SalesService, private route: Router, private activeRoute: ActivatedRoute, private toster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    if (localStorage.getItem('role')) {
      this.role = localStorage.getItem('role');
    }
    this.restaurantId = localStorage.getItem('Id');

    this.activeRoute.params
      .map(params => params['id'])
      .subscribe((id) => {
        if (id !== '' && id !== undefined) {
          console.log('Owner'); // this block called if user Login as a Owner
          this.locationId = id;
          this.getSalesData(this.locationId);
          this.getSalesCountsByRestaurantId(this.restaurantId);
          this.getLocationDataInfo();
        } else {
          console.log('Manager'); // this block called if user Login as a Manager
          this.locationId = localStorage.getItem('locationId');
          this.getSalesData(this.locationId);
          this.getSalesCountData(this.locationId);
        }
      });
  }


  getSalesData(locationId) { // fetching sales data of a specific location by location Id
    this.restService.getSalesDataByLocationId(locationId).subscribe((res: any) => {
      this.salesData = res;
      // console.log('sales res ' + JSON.stringify(res));
      if (res.length > 0) {
        this.locationName = this.salesData[0].location.locationName;
        this.loading = false;
        if (this.salesData.length >= 0) {
          this.dataLength = 1;
        } else {
          this.dataLength = 0;
        }
      } else {
        this.loading = false;
        this.dataLength = 0;
      }
    }, () => {
      this.loading = false;
      this.errorImg = true;
    });
  }
  getSalesCountData(locationId) { // invoke when user login as a Manager
    /** fetching number of
      * total orders
      * one month orders
      * today orders from server*/
    this.restService.getSalesCountsByLocationId(locationId).subscribe((res: any) => {
      this.countData = res;
      // console.log("Count Total Orders" + JSON.stringify(res))
    }, () => {
      this.toster.error('something is going wrong please try again', 'sales');
    });
  }

  getSalesCountsByRestaurantId(restaurantId) { // invoke when user login as a Owner
    /** fetching number of
     * total orders
     * one month orders
     * today orders from server*/
    this.restService.getSalesCountsByRestaurantId(restaurantId).subscribe((res: any) => {
      this.countData = res;
    }, () => {
      this.toster.error('something is going wrong please try again', 'sales');
    });
  }

  getLocationDataInfo() { // fetching list of all location from server
    this.restService.getAllLocation(this.restaurantId).subscribe((res: any) => {
      this.locationData = res;
    });
  }

  onSelectLocation(event) {
    this.locationName = '...';
    const locationId = event.value;
    this.getSalesData(locationId);
  }

  onOrderView(id) { // navigating to view order page
    return this.route.navigate(['/pietech/view/', id]);
  }


  ngOnInit() {
  }

}
