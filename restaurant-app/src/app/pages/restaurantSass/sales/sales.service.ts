import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class SalesService {

  constructor(public crud: CrudService) {
  }
  getSalesDataByLocationId(locationId) { // get sales data by location Id
    return this.crud.getOne('orders/delivered/location', locationId).map((data: Response) => data);
  }
  getSalesCountsByLocationId(locationId) { // get Sales Counts data by location Id
    return this.crud.getOne('orders/sales/counts/location', locationId).map((data: Response) => data);
  }
  getSalesCountsByRestaurantId(restaurantId) { // get Sales Counts data by Restaurant Id
    return this.crud.getOne('orders/sales/counts/restaurant', restaurantId).map((data: Response) => data);
  }
  getAllLocation(id: any) { // get all Location data
    return this.crud.getOne('locations/all/data', id).map((data: Response) => data);
  }
}
