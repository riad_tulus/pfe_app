import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../../layouts/shared-service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr'
import { LocationsService } from '../locations.service';

@Component({
  selector: 'app-add-locations',
  templateUrl: './add-locations.component.html',
  styleUrls: ['./add-locations.component.css'],
  providers: [LocationsService]
})
export class AddLocationsComponent implements OnInit {

  pageTitle: string = 'Restaurant Info';
  id: any;

  public disabled: boolean = false;
  public updateManager: any = {
    locationInfo: {},
  };
  public value: any = {};
  restaurant: string;
  username: string;

  locationData: any = {
    restaurantID: '',
    tax: [],
    hasDeliverd: false,
    alwaysReachable: true,
    featured: false,
    taxExist: false,
    contactPerson: '',

  };
  restaurantId: any;
  public manager: Array<any>;
  constructor(private _sharedService: SharedService, private router: Router, private locationService: LocationsService, private toaster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.restaurant = localStorage.getItem('restaurantName');
    this.restaurantId = localStorage.getItem('Id')
    this.username = localStorage.getItem('email');
    this.onGetManagerList(); // get list of unsigned manager of Restaurant
  }
  onAddRestaurentLocation() {
    let updateMember: any = {
      locationId: '',
      locationName: ''
    }
    let updateMemberId: any = '';

    this.locationData.restaurantID = localStorage.getItem('Id');
    // Adding new location
    this.locationService.addNewLocationData(this.locationData).subscribe((res: any) => {
      if (res._id) {
        /** if location added successfully
          * assigning location name and location Id to Manager
          *  and updating manager information
          * */
        updateMemberId = res.contactPerson
        updateMember.locationId = res._id;
        updateMember.locationName = res.locationName;
        this.updateManager.locationInfo = updateMember;
        this.updateManager.location = res._id;
        this.locationService.updateMemberData(this.updateManager, updateMemberId).subscribe((res: any) => {
        }, (error) => {
          this.toaster.error(error.message, 'ERROR');
        });
      }
      this.router.navigate(['pietech/locations']);
      this.toaster.success(' Data Added successfully');

    }), (error) => {
      this.toaster.error(error.message, 'ERROR');
    };
  }


  back() {
    this.router.navigate(['/pietech/locations/']);
  }

  addTax() { // user want to add or update tax
    const check = confirm('do you want to leave this page');
    if (check) {
      this.locationData.taxExist = true;
      this.router.navigate(['/pietech/tax/']);
    }
  }

  public setCurrentPosition() { // set current location latitude and longitude
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.locationData.latitude = position.coords.latitude;
        this.locationData.longitude = position.coords.longitude;
      });
    }
  }


  onGetManagerList() { // getting only unsigned managers data from server
    this.locationService.getUser(this.restaurantId).subscribe((res: any) => {
      this.manager = res;
      console.log("manager list" + JSON.stringify(res))
    }, (error) => {
      this.toaster.error(error.message, 'ERROR');
    });
  }

  onSelectUsers(event) {
    let managerInfo: any = {};
    managerInfo = this.manager[event.value];
    this.locationData.contactPerson = managerInfo._id;
    this.locationData.contactNumber = managerInfo.contactNumber;
    this.locationData.alternateEmail = managerInfo.email;
  }
  ngOnInit() { }

}
