import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LocationsService} from '../locations.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-view-locations',
  templateUrl: './view-locations.component.html',
  styleUrls: ['./view-locations.component.css'],
  providers: [LocationsService]
})
export class ViewLocationsComponent implements OnInit {
  data: any = {};
  public count: any;
  locationId: any;
  public productCount: any;
  product: any = {};

  loadChart: number = 0;
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLegend: boolean = false;


  loading: boolean = true;
  dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg: boolean = false;
  errorImage = './assets/content/error.png';
  noContent: any = './assets/content/nodata_cloud.png';

  public barChartData: any = {
    labels: [],
    borderWidth: 1,
    pointRadius: 1,
    datasets: [{
      data: []

    }]
  };

  constructor(private restService: LocationsService, public route: ActivatedRoute, private toster: ToastrService) {
    this.route.params
      .map(params => params['id'])
      .subscribe((id) => {
        this.locationId = id;
        if (id !== ' ') {
          this.getGraphByLocation(this.locationId);
        }
      });
    }

  getGraphByLocation(locationId: any) {
    this.restService.getGraphDataByLoctionId(this.locationId).subscribe((res: any) => {
        console.log('data' + JSON.stringify(res));
        this.barChartData = res;
        this.barChartData.labels = res.labels;
        this.barChartData.datasets = res.datasets;
        this.loading = false;
        if (this.barChartData.labels.length > 0) {
          this.loadChart = 1;
          this.dataLength = 1;
        }
      },
      (error) => {
        this.loading = false;
        this.errorImg = true;
        this.toster.error(error.message, 'ERROR');
      });
  }
  
  ngOnInit() {}
}
