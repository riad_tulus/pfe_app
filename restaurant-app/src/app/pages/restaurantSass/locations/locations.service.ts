import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()

export class LocationsService {
  constructor(public crud: CrudService) {
  }

  // fetch Location Data By from server by Restaurant Id
  getAllLocation(id: any) {
    return this.crud.getOne('locations/all/data', id).map((data: Response) => data);
  }

  // to add new location
  addNewLocationData(locationData: any) {
    const body = JSON.stringify(locationData);
    return this.crud.post('locations', body).map((data: Response) => data);
  }

  // update the particular location
  updateLocationData(locationId: any, locationData: any) {
    const body = JSON.stringify(locationData);
    return this.crud.put('locations', body, locationId).map((data: Response) => data);
  }

  updateMemberData(memberData: any, userId: any) {
    const body = JSON.stringify(memberData);
    return this.crud.put('users', body, userId).map((data: Response) => data);
  }

  // getting data for particular location
  getSpecificLocation(locationId: any) {
    return this.crud.getOne('locations', locationId).map((data: Response) => data);

  }

  // get all the role a resturant have
  getUser(id: any) {
    return this.crud.get('users/new/manager').map((data: Response) => data);

  }

  // get graph of  and counts data of a location
  getGraphDataByLoctionId(locationId: any) {
    return this.crud.getOne('orders/location/data', locationId).map((data: Response) => data)
  }
}

