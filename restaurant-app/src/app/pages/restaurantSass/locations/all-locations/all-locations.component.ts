import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocationsService } from '../locations.service';
import { SharedService } from '../../../../layouts/shared-service';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-all-locations',
  templateUrl: './all-locations.component.html',
  styleUrls: ['./all-locations.component.css'],
  providers: [LocationsService]
})

export class AllLocationsComponent implements OnInit {
  pageTitle = 'Emplacement';
  public locationData: Array<any>;
  public locationId: any;
  data: any = [];
  id: any;

  loading = true;
  dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg = false;
  errorImage = './assets/content/error.png';
  noContent: any = './assets/content/nodata_cloud.png';
  public updateMember: any = {};

  constructor(private _sharedService: SharedService, private router: Router, private locationService: LocationsService, private toster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.id = localStorage.getItem('Id');
    if (this.id != null) {
      this.getAllLocationInfo(this.id);
    }
  }

  getAllLocationInfo(id) { // getting all location list of a restaurant by restaurant Id
    this.locationService.getAllLocation(id).subscribe((res: any) => {
      this.locationData = res;

      this.loading = false;
      if (this.locationData.length >= 1) {
        this.dataLength = 1;
      } else {
        this.dataLength = 0;
      }
    }, (error) => {
      this.loading = false;
      this.errorImg = true;
      this.toster.error(error.message, 'ERROR');
    });
  }

  addLocation() { // route to add location component
    this.router.navigate(['/pietech/location/new']);

  }

  onSelectCategory(locationId) { // route to Category list component of location by location Id
    this.router.navigate(['/pietech/location/category/' + locationId]);
  }

  onSelectProducts(locationId) { // route to Products list component of location by location Id
    this.router.navigate(['/pietech/location/product/' + locationId]);
  }


  onUpdate(locationId) {
    this.router.navigate(['/pietech/location/edit/' + locationId]);
  }

  updateStatus(locId) { // enable or disable specific location
    this.locationService.getSpecificLocation(locId).subscribe((res: any) => {
      this.updateMember.enable = (!res.enable);
      this.updateMember.flag = 0;
      this.locationService.updateLocationData(locId, this.updateMember).subscribe((res: any) => {
        this.toster.success('your staus has changed successfully');
      }, (error) => {
        this.toster.error(error.message, 'ERROR');
      });
    });
  }

  onView(id: any) {
    this.router.navigate(['/pietech/location/view/' + id])
  }

  ngOnInit() {
  }


}

