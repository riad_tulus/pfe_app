import {Component, OnInit} from '@angular/core';
import {SharedService} from '../../../../layouts/shared-service';
import {ActivatedRoute, Router} from '@angular/router';
import {LocationsService} from '../locations.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-update-locations',
  templateUrl: './update-locations.component.html',
  styleUrls: ['./update-locations.component.css'],
  providers: [LocationsService]

})
export class UpdateLocationsComponent implements OnInit {

  locationData: any = {};
  restaurantName: string;
  locationId: any;
  public value: any = {};
  public disabled: boolean = false;
  loading: boolean = true;
  errorImg: boolean = false;
  errorImage = './assets/content/error.png';

  constructor(private _sharedService: SharedService, private toster: ToastrService, private route: ActivatedRoute, private router: Router, private locationService: LocationsService) {
    this.restaurantName = localStorage.getItem('restaurantName');
    this.route.params
      .map(params => params['id'])
      .subscribe((id) => {
        this.locationId = id;
        if (id !== ' ') {
          this.getLocationData(id);
        }
      });

  }
getLocationData(id) {
  this.locationService.getSpecificLocation(id).subscribe((res: any) => {
    this.locationData = res;
    this.loading = false;
  }, (error) => {
    this.loading = false;
    this.errorImg = true;
  });
}
  onUpdateRestaurent() { // update specific location
    this.locationService.updateLocationData(this.locationId, this.locationData)
      .subscribe((res: any) => {
        this.toster.success(' Data Updated successfully');
        this.router.navigate(['/pietech/locations']);
      }, (error) => {
        this.toster.error('something wrong!..Unable to update', 'Update location');
      });

  }

  back() {
    this.router.navigate(['/pietech/locations']);
  }

  public setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.locationData.latitude = position.coords.latitude;
        this.locationData.longitude = position.coords.longitude;
      }, (error) => {
        this.toster.error(error.message, 'ERROR');
      });
    }
  }
  ngOnInit() {
  }
}
