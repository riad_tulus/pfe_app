import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Rx';
import {ConstantService} from '../../../services/constant.service';
import {CrudService} from '../../../services/crud.service';


@Injectable()
export class StaffManagementService {
  authToken: any;

  constructor(private http: Http, public constantService: ConstantService, public crud: CrudService) {
    this.authToken = localStorage.getItem('token');
  }


  getAllMemberData() {
    const headers = new Headers();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.constantService.API_ENDPOINT + 'users/all/staff/', {
      headers: headers
    })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }

  addNewMemberData(memberData: any) {
    const body = JSON.stringify(memberData);
    const head = new Headers();
    head.append('Content-type', 'application/json');
    return this.http.post(this.constantService.API_ENDPOINT + 'users/', body, {
      headers: head
    })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }

  getSpacificMemberData(Id: any) {                           // SubCategory Id
    const headers = new Headers();
    headers.append('Authorization', this.authToken);
     headers.append('Content-Type', 'application/json');
    return this.http.get(this.constantService.API_ENDPOINT + 'users/data/' + Id, {
      headers: headers
    })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }

  updateMemberData(data: any, Id: any) {
    const body = JSON.stringify(data);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authToken);
    console.log("token"+ this.authToken);
    return this.http.put(this.constantService.API_ENDPOINT + 'users/' + Id, body, {
      headers: headers
    })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }

  // for assigned orders //
  getAssignedOrdersData(emp_id) {
    const headers = new Headers();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.constantService.API_ENDPOINT + 'orders/all/show/' + emp_id, {
      headers: headers
    })
      .map((data: Response) => data.json())
      .catch(this.handleError);

  }


  getStaffDataByLocatoionId(LocationId) {

    return this.crud.getOne('users/location/staff', LocationId).map((data: Response) => data);
  }

  getAllLocationData(id: any) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authToken);
    return this.http.get(this.constantService.API_ENDPOINT + 'locations/all/data/' + id, {
      headers: headers
    })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }

  private handleError(error: any) {
    return Observable.throw(error.json());
  }
}
 