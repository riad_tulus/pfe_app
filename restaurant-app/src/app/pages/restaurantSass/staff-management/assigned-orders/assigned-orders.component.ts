import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StaffManagementService} from '../staff-management.service';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-assigned-orders',
  templateUrl: './assigned-orders.component.html',
  styleUrls: ['./assigned-orders.component.css'],
  providers: [StaffManagementService]
})
export class AssignedOrdersComponent implements OnInit {

  public emp_id: number;
  public todayOrderData: Array<any>;
  public lastSevenDayData: Array<any>;
  public oneMonthData: Array<any>;

  constructor(private restService: StaffManagementService, private router: ActivatedRoute, private route: Router, private toster: ToastrService) {
    this.router.params
      .map(params => params['id'])
      .subscribe((id) => {
        this.emp_id = id;
        if (id !== '') {
          this.getAssignedOrderData(id);
        }
      });
  }

  getAssignedOrderData(emp_id) { // fetch assigned order data of a specific delivery boy by ID
    this.restService.getAssignedOrdersData(emp_id).subscribe((res: any) => {
      // console.log(' getAssignedOrdersData ' + JSON.stringify(res))
      this.todayOrderData = res.todayData;
      this.lastSevenDayData = res.sevenDaysData;
      this.oneMonthData = res.OneMonthData;
    }, () => {
      this.toster.error('error arrived');
    });
  }

  ngOnInit() {
  }

  viewOrderDetails(id) { // invoke when user want to see order details
     this.route.navigate(['/pietech/order/view/', id]);
  }
}
