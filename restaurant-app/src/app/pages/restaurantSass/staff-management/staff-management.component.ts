import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {StaffManagementService} from './staff-management.service';
import {SharedService} from '../../../layouts/shared-service';


@Component({
  selector: 'app-staff-management',
  templateUrl: './staff-management.component.html',
  styleUrls: ['./staff-management.component.css'],
  providers: [StaffManagementService]
})

export class StaffManagementComponent implements OnInit {
  pageTitle = 'Gestion Employées';
  form: NgForm;
  public loading = true;
  public dataLength = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg = false;
  public errorImage = './assets/content/error.png';
  public noContent: any = './assets/content/staff.png';
  public staffData: any[] = [];
  public locationData: Array<any>;
  public updateMember: any = {};
  public addDialog = false;
  public updateDialog = false;
  public staffMemberDialog = true;
  public user: any;
  public addMember: any = {
    name: '',
    address: '',
    contactNumber: '',
    role: '',
    status: true,
    flag: 0,
    email: '',
    restaurantName: '',
    locationInfo: {},
    activationStatus: true,
    totalCount: 0
  };
  public locationName: any;
  locationId: any;
  private memberId: any;

  constructor(private _sharedService: SharedService, private router: Router, private restService: StaffManagementService, private toster: ToastrService) {

    this._sharedService.emitChange(this.pageTitle);
    this.addMember.restaurantID = localStorage.getItem('Id');
    this.addMember.restaurantName = localStorage.getItem('restaurantName');
    this.locationName = localStorage.getItem('locationName');
    this.locationId = localStorage.getItem('locationId');
    // console.log("locationId"+this.locationId);
    this.user = localStorage.getItem('role');
    this.getMemberData();
    this.getLoctionData();
  }

  getMemberData() {
    // if block execute when User logged in as a Manager
    if (this.locationId != null) {
      this.staffData = [];
      this.restService.getStaffDataByLocatoionId(this.locationId).subscribe((res: any) => {
        this.staffData = res;
        this.loading = false;
        if (this.staffData.length > 0) {
          this.dataLength = 1;
        } else {
          this.dataLength = 0;
        }
      }, () => {
        this.loading = false;
        this.errorImg = true;
      });
    } else {
      // else block execute if User logged in as a Owner
      this.restService.getAllMemberData().subscribe((res: any) => {
        this.staffData = res;
        this.loading = false;
        console.log('staff data');
        if (this.staffData.length > 0) {
          this.dataLength = 1;
        } else {
          this.dataLength = 0;
        }
      }, (error) => {
        this.loading = false;
        this.errorImg = true;
      });
    }
  }

  // Invoke when user want to save new Member
  onSaveMember() {
    if (this.locationId !== null) {
      this.addMember.location = this.locationId;
    }
    this.restService.addNewMemberData(this.addMember).subscribe((res: any) => {
      this.toster.success('Member Added Successfully ', 'Success');
      this.getMemberData();
      this.closeAddDialog();
    }, (error) => {
      this.toster.error('ERROR');
    });
  }
  // getLocationData invoke when user logged in as Owner.
  getLoctionData() {
    this.restService.getAllLocationData(this.addMember.restaurantID).subscribe((res: any) => {
      this.locationData = res;

    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  onUpdate(id) {
    this.memberId = id;
    this.restService.getSpacificMemberData(id).subscribe((res: any) => {
      console.log('data' + JSON.stringify(res));
      this.updateMember = res;
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  onUpdateMember() {
    this.updateMember.flag = 0;
    this.restService.updateMemberData(this.updateMember, this.memberId).subscribe((res: any) => {

      this.closeUpdateDialog();

      this.getMemberData();
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  checkAssigned(id) {
    this.router.navigate(['/pietech/assigned-order/', id]);
  }

  onDisable(id: any, index) {
    this.updateMember.activationStatus = (!this.staffData[index].activationStatus)
    this.updateMember.flag = 0;
    this.restService.updateMemberData(this.updateMember, id).subscribe((res: any) => {
      this.toster.success('Member Availability has changed Successfully');
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });

  }

  openAddDialog() {
    this.dataLength = 1;
    this.loading = false;
    this.closeUpdateDialog();
    this.addDialog = true;
    this.staffMemberDialog = false;
    this.updateDialog = false;
  }

  closeAddDialog() {
    this.addDialog = false;
    this.addMember.name = '',
    this.addMember.contactNumber = '',
    this.addMember.email = '',
    this.addMember.password = '',
    this.addMember.address = '',
    this.staffMemberDialog = true;
  }

  openUpdateDialog(staffMemberId: any) {
    this.closeAddDialog();
    this.updateDialog = true;
    this.onUpdate(staffMemberId);
    this.staffMemberDialog = false;
  }

  closeUpdateDialog() {
    this.updateDialog = false;
    this.staffMemberDialog = true;
  }

  onView() {
    this.router.navigate(['/pietech/assigned-order']);
  }

  onSelectLocation(event) {
    const data: any = {
      locationId: '',
      locationName: ''
    }
    const locationData = event.value.split('@');
    data.locationId = locationData[0];
    data.locationName = locationData[1];
    this.addMember.locationInfo = data;
    this.addMember.location = locationData[0];
  }

  onUpdateLocation(event) {
    const data: any = {
      locationId: '',
      locationName: ''
    };
    const locationData = event.value.split('@');
    data.locationId = locationData[0];
    data.locationName = locationData[1];
    this.updateMember.locationInfo = data;
  }
  ngOnInit() {
  }
}
