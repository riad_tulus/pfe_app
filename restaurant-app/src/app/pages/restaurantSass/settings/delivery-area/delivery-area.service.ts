import {Injectable} from '@angular/core';
import {CrudService} from '../../../../services/crud.service';

@Injectable()

export class DeliveryService {
  constructor(public crud: CrudService) {
  }


  addDeliveryData(data, locationId) { // invoke while adding new Delivery location
    const body = {
      id: locationId,
      deliveryInfo: data
    };
    return this.crud.post('locations/add/deliveryinfo/data', body).map((data: Response) => data);
  }

  getSavedDeliveryData() { // invoke while fetching saved Delivery location
    const locationId = localStorage.getItem('locationId');
    return this.crud.getOne('locations', locationId);
  }

}
