import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {DeliveryService} from './delivery-area.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router'
import {json} from 'ng2-validation/dist/json';
import {SharedService} from '../../../../layouts/shared-service';


@Component({
  selector: 'app-delivery-area',
  templateUrl: './delivery-area.component.html',
  styleUrls: ['./delivery-area.component.css'],
  providers: [DeliveryService]
})
export class DeliveryAreaComponent implements OnInit {
  pageTitle = 'Livraison';
  form: NgForm
  public deliveryData: any = {
    deliveryCharges: '',
    freeDelivery: false,
    amountEligibility: 500,
    areaCode: [{}],
    areaAthority: true,
    deliveryTime: '',

  };
  locationId: any;
  addArea = function () { // generating new area row
    if (this.deliveryData.areaCode.length >= 1) {
      const newItemNo = this.deliveryData.areaCode.length + 1;
      this.deliveryData.areaCode.push({});
    }
  };
  removeArea = function (index) { // deleting area row from specific position
    if (this.deliveryData.areaCode.length > 1) {
      const lastItem = this.deliveryData.areaCode.length - 1;
      this.deliveryData.areaCode.splice(index, 1);
    }
  };
  constructor(private _sharedService: SharedService, private restService: DeliveryService, private toaster: ToastrService, private route: Router) {
    this._sharedService.emitChange(this.pageTitle);
    this.locationId = localStorage.getItem('locationId');
    this.getSavedData();
  }

  getSavedData() { // fetching all saved all Delivery information
    this.restService.getSavedDeliveryData().subscribe((res: any) => {
      console.log(' getSavedDeliveryData ' + JSON.stringify(res.deliveryInfo));
      this.deliveryData = res.deliveryInfo.deliveryInfo;
    });
  }

  onSaveDeliveryOption() { // invoke when user save Delivery Options
      this.restService.addDeliveryData(this.deliveryData, this.locationId).subscribe((res: any) => {
      this.route.navigate(['/pietech/dashboard/']);
      this.toaster.success('Delivery Option Updated', 'Delivery');
    }, (error) => {
      this.toaster.error(error.message, 'ERROR');
    });

  }

  ngOnInit() {
  }

}
