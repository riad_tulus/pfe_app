import {Injectable} from '@angular/core';
import {CrudService} from '../../../../services/crud.service';

@Injectable()
export class TaxService {
  constructor(public crud: CrudService) {
  }

  getTaxData(restaurant: any) { // Tax save inside Restaurant Info ;
    return this.crud.getOne('users', restaurant).map((data: Response) => data);
  }

  updateTaxData(TaxData: any, TaxId: any) {
    const body = JSON.stringify(TaxData);
    return this.crud.put('users', body, TaxId).map((data: Response) => data);

  }
}
