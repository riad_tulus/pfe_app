import {Component, OnInit} from '@angular/core';
import {TaxService} from './tax.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {SharedService} from '../../../../layouts/shared-service';


@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.css'],
  providers: [TaxService]
})

export class TaxComponent implements OnInit {
  pageTitle = 'Taxe';
  public loading = true;
  public dataLength = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg = false;
  public errorImage = './assets/content/error.png'
  public noContent: any = './assets/content/nodata_cloud.png';
  public tax: any = {
    taxName: '',
    taxRate: 0
  };
  private restaurantID: any;
  private taxId: any;

  constructor(private _sharedService: SharedService, public taxService: TaxService, private toster: ToastrService, private route: Router) {
    this._sharedService.emitChange(this.pageTitle);
    this.restaurantID = localStorage.getItem('Id');
    this.getTexData();
    this.loading = false;
  }

  getTexData() { // fetching saved tax data from server
    this.taxService.getTaxData(this.restaurantID).subscribe((res: any) => {
      if (res.taxInfo) {
        this.tax = res.taxInfo;
      }
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  onSaveTax() { // invoke when user update tax data

    const taxData: any = {
      taxInfo: {}
    }

    taxData.taxInfo = this.tax;
    this.taxService.updateTaxData(taxData, this.restaurantID).subscribe((res: any) => {
      this.toster.success('Tax added successfully');
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  closeAddDialog() {
    return this.route.navigate(['/pietech/dashboard']);
  }

  ngOnInit() {
  }


}
