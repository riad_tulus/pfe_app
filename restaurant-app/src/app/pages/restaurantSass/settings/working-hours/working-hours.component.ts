import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { WorkingHoursService } from './working-hours.service';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../../../layouts/shared-service';


@Component({
  selector: 'app-working-hours',
  templateUrl: './working-hours.component.html',
  styleUrls: ['./working-hours.component.css'],
  providers: [WorkingHoursService]
})

export class WorkingHoursComponent implements OnInit, OnDestroy {
  pageTitle = 'Horraires de travail';
  form: NgForm;
  days = [
    { value: 'Sunday', viewValue: 'Dimanche' },
    { value: 'Monday', viewValue: 'Lundi' },
    { value: 'Tuesday', viewValue: 'Mardi' },
    { value: 'Wednesday', viewValue: 'Mercredi' },
    { value: 'Thursday', viewValue: 'Jeudi' },
    { value: 'Friday', viewValue: 'Vendredi' },
    { value: 'Saturday', viewValue: 'Samedi' }


  ];
  hours = [
    { value: '00:00', viewValue: '00:00' },
    { value: '01:00', viewValue: '01:00' },
    { value: '02:00', viewValue: '02:00' },
    { value: '03:00', viewValue: '03:00' },
    { value: '04:00', viewValue: '04:00' },
    { value: '05:00', viewValue: '05:00' },
    { value: '06:00', viewValue: '06:00' },
    { value: '07:00', viewValue: '07:00' },
    { value: '08:00', viewValue: '08:00' },
    { value: '09:00', viewValue: '09:00' },
    { value: '10:00', viewValue: '10:00' },
    { value: '11:00', viewValue: '11:00' },
    { value: '12:00', viewValue: '12:00' },
    { value: '13:00', viewValue: '13:00' },
    { value: '14:00', viewValue: '14:00' },
    { value: '15:00', viewValue: '15:00' },
    { value: '16:00', viewValue: '16:00' },
    { value: '17:00', viewValue: '17:00' },
    { value: '18:00', viewValue: '18:00' },
    { value: '19:00', viewValue: '19:00' },
    { value: '20:00', viewValue: '20:00' },
    { value: '21:00', viewValue: '21:00' },
    { value: '22:00', viewValue: '22:00' },
    { value: '23:00', viewValue: '23:00' },
    { value: '24:00', viewValue: '24:00' }
  ];
  data: any = {
    workingHours: {}
  }
  workingHoursData: any = {
    isAlwaysOpen: true,
    daySchedule: [{
      timeSchedule: [{}]
    }]
  };
  addDaySchedule = function () { // create Schedule for s new day
    // this.isDaySchedule = true;
    if (this.workingHoursData.daySchedule.length < 7) {
      this.workingHoursData.daySchedule.push({ timeSchedule: [{}] });
    } else {
      alert('Day limit exceeded');
    }
  };
  deleteDaySchedule = function (index) { // remove Schedule for a new day
    // this.isDaySchedule = true;
    if (this.workingHoursData.daySchedule.length > 1) {
      // let deleteItemNo = this.workingHoursData.daySchedule.length-1;
      this.workingHoursData.daySchedule.splice(index, 1);
    }
  };
  addTimeSchedule = function (index) { // add a timing Schedule for a new day
    // this.isDaySchedule = true;
    if (this.workingHoursData.daySchedule[index].timeSchedule.length < 3) {
      this.workingHoursData.daySchedule[index].timeSchedule.push({});
      this.toaster.success('time set');
    }
  };
  deleteTimeSchedule = function (dayIndex, timeIndex) {
    // removing a specific timing Schedule from a specific new day
    if (this.workingHoursData.daySchedule[dayIndex].timeSchedule.length > 1) {
      this.workingHoursData.daySchedule[dayIndex].timeSchedule.splice(timeIndex, 1);
    }
  };
  private locationId: any;

  constructor(private _sharedService: SharedService, private restService: WorkingHoursService, private toaster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.locationId = localStorage.getItem('locationId');
    this.getWorkingHoursData();
  }

  getWorkingHoursData() { // fetching saved Working Hours Data from server
    this.restService.getWorkingHoursData(this.locationId).subscribe((res: any) => {
      console.log("Working Hours" + JSON.stringify(res))
      if (res.workingHours) {
        if (res.workingHours.isAlwaysOpen === false) {
          this.workingHoursData = res.workingHours;
        }
      }
    }, (error) => {
      this.toaster.error(error.message, 'ERROR');
    });
  }

  saveSchedule() { // invoked when user save Working Hours Data
    if (this.workingHoursData.isAlwaysOpen) {
      /** checking is location open 24 hours for talking orders or not
       * if isAlwaysOpen so assign working Hours as a blank [{}] */
      this.workingHoursData.daySchedule = [{
        timeSchedule: [{}]
      }];
      this.data.workingHours = this.workingHoursData;
      this.restService.updateWorkingHoursData(this.data, this.locationId).subscribe((res: any) => {
        this.toaster.success('schedule set');
      }, (error) => {
        this.toaster.error(error.message, 'ERROR');
      });
    } else {
      // otherwise save Scheduled timing information of a location
      this.data.workingHours = this.workingHoursData;
      this.restService.updateWorkingHoursData(this.data, this.locationId).subscribe((res: any) => {
        this.toaster.success('your schedule set');
      }, (error) => {
        this.toaster.error(error.message, 'ERROR');
      });
    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    alert('if any Changes you made please save first');
  }


}
