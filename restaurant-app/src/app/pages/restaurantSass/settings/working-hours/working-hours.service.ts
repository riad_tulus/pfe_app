import {Injectable} from '@angular/core';
import {CrudService} from '../../../../services/crud.service';

@Injectable()
export class WorkingHoursService {
  constructor(public crud: CrudService) {
  }

  updateWorkingHoursData(data, locationId) {
    return this.crud.put('locations', data, locationId).map((data: Response) => data);
  }

  getWorkingHoursData(locationId) {
    return this.crud.getOne('locations', locationId).map((data: Response) => data);
  }

}
