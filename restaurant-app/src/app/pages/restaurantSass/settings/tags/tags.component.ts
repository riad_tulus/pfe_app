import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TagsService } from './tags.service';
import { ToastrService } from 'ngx-toastr';
import { json } from 'ng2-validation/dist/json';
import { SharedService } from '../../../../layouts/shared-service';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css'],
  providers: [TagsService]
})
export class TagsComponent implements OnInit {
  pageTitle = 'Tags'
  public tagDetails = true;
  public loading = true;
  public dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg = false;
  public errorImage = './assets/content/error.png';
  public noContent: any = './assets/content/nodata_cloud.png';


  public addDialog = false;
  public updateDialog = false;
  public tagsData: Array<any>;

  public addTagData: any = {
    restaurantID: '',
    location: '',
    tag: ''
  };
  public updateTag: any = {};
  public tagId: any;
  private locationId: any;

  constructor(private _sharedService: SharedService, private router: Router, private restService: TagsService, private toster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.locationId = localStorage.getItem('locationId');
    this.addTagData.restaurantID = localStorage.getItem('Id');
    this.addTagData.location = this.locationId;
    //console.log('locationId' + this.locationId);
    this.getTagData();
    // this.addTagData.location = this.locationId;

  }

  getTagData() { // fetching all saved tags list from server
    this.restService.getAllTagData(this.locationId).subscribe((res: any) => {
      this.tagsData = res;
      console.log("tags" + JSON.stringify(res));
      this.loading = false;
      if (this.tagsData.length >= 1) {
        this.dataLength = 1;
      } else {
        this.dataLength = 0;
      }
    }, () => {
      this.loading = false;
      this.errorImg = true;
    });
  }


  openAddDialog() { // invoke when user want to add new tag
    this.loading = false;
    this.dataLength = 1;
    this.closeUpdateDialog();
    this.addDialog = true;
    this.tagDetails = false;
  }

  onSaveTag() { // invoke when user want to save new tag data to server
    this.restService.addNewTagData(this.addTagData).subscribe((res: any) => {
      if (this.tagsData.length > 0) {
        this.tagsData.push(res);
      }
      else {
        this.getTagData();
      }
      this.toster.success(' New Tag Added', 'Success!!!');
      this.closeAddDialog();
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  closeAddDialog() { // after saving tag data reset variables to init state
    this.addDialog = false;
    this.tagDetails = true;
    this.addTagData.tag = '';
  }

  openUpdateDialog(tagId) { // invoke when user want to Update saved tag
    this.addDialog = false;
    this.tagId = tagId;
    this.updateDialog = true;
    this.restService.getSpacificTagData(tagId).subscribe((res: any) => {
      this.updateTag = res;
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  onUpdateSaveTag() { // invoke when user want to update a tag data to server
    this.restService.updateTagData(this.updateTag, this.tagId)
      .subscribe(() => {
        this.getTagData();
        this.closeUpdateDialog();
      }, (error) => {
        this.toster.error(error.message, 'ERROR');
      });
  }

  closeUpdateDialog() { // after update tag data reset variables to init state
    this.updateDialog = false;
  }

  onDisable(id: any) { // invoke when user want to disable a tag
    this.restService.getSpacificTagData(id).subscribe((res: any) => {
      res.enable = (!res.enable);
      this.restService.updateTagData(res, id)
        .subscribe(() => {
          this.getTagData();
        }, (error) => {
          this.toster.error(error.message, 'ERROR');
        });
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  ngOnInit() {
  }
}
