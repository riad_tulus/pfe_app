import {Injectable} from '@angular/core';
import {CrudService} from '../../../../services/crud.service';

@Injectable()
export class TagsService {
  constructor(public crud: CrudService) {
  }

  getAllTagData(locationId) {
    return this.crud.getOne('tags/all', locationId).map((data: Response) => data);
  }

  getSpacificTagData(Id: any) {                           // SubCategory Id
    return this.crud.getOne('tags', Id).map((data: Response) => data);
  }

  addNewTagData(tagData: any) {
    const body = JSON.stringify(tagData);
    return this.crud.post('tags', body).map((data: Response) => data);
  }

  updateTagData(data: any, Id: any) {
    const body = JSON.stringify(data);
    return this.crud.put('tags', body, Id).map((data: Response) => data);
  }

}
