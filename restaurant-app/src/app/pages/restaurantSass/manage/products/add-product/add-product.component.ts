import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {ProductsService} from '../products.service';
import {ToastrService} from 'ngx-toastr';
import {ConstantService} from '../../../../../services/constant.service'
import {CloudinaryOptions, CloudinaryUploader} from 'ng2-cloudinary';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
  providers: [ProductsService, ConstantService]
})

export class AddProductComponent implements OnInit {
  public url1: any = './assets/content/selectImg.png';
  public locationData: Array<any> = [];
  public categoryData: Array<any> = [];
  public tagData: any [] = [];
  public subCategoryData: Array<any> = [];
  public taxOnConfirm: Array<any> = [];
  form: NgForm;


  public product: any = {
    restaurantID: '',
    location: '',
    category: '',
    title: '',
    brand: '',
    description: '',
    tags: [],
    imageUrl: '',
    publicId: '',
    variants: [{}],
    extraIngredients: [{}],
    flag: 0,
    categoryTitle: ''
  };

  noTagMessage: string;
  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions(this.restService.cloudinarUpload)
  );
  public value: any;
  public disabled: boolean = false;
  public user: any;
  public location: any;
  public isManager: boolean = false;
  locationId: any;
  isLocationSelected:boolean = false;

  constructor(private restService: ProductsService, private route: Router, private toster: ToastrService) {
    this.product.restaurantID = localStorage.getItem('Id');
    this.location = localStorage.getItem('locationName');
    this.user = localStorage.getItem('role');
    // showing location  only to owner
    if (this.user === 'Manager' || this.user === 'Staff') {
      this.locationId = localStorage.getItem('locationId');
      this.product.location = this.locationId;
      this.isManager = true;
      this.getAllCategorybyLoctionInfo();
      this.getTagData(this.locationId);
    } else {
      this.isManager = false;
      if (this.product.restaurantID != null) {
        this.getAllLocationDataInfo();
        // this.getProductByLocation();
      }
    }
  }


  onSelectCategory(event) { // while user select category
    this.product.category = event.value;
    const index = this.categoryData.findIndex( i => i._id === event.value);
    this.product.categoryTitle = this.categoryData[index].categoryName;
  }


  onSelectTags(event) {
  }

  readUrl1(event) { // invoke when user select image to upload
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.url1 = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  getAllLocationDataInfo() { // fetch all locations data of a restaurant (for Owner)
    this.restService.getAllLocation(this.product.restaurantID).subscribe((res: any) => {
    // console.log(' data ' + JSON.stringify(res));
      this.locationData = res;
    });
  }


  getTagData(locationId) { // fetch all Tag data of a Location by LocationID
    this.restService.getAllTagData(locationId).subscribe((res: any) => {
      if (res.message) {
        this.noTagMessage = 'Add few tags (0 tags found)';
      } else {
        this.tagData = res;
      }
    }, (error) => {
      this.toster.error('error');
    });
  }


  addVariants = function () { // add new variants Item
    if (this.product.variants.length < 7) {
      this.product.variants.push({});
    }
  };

  addExtraIngredients= function () { // add new ExtraIngredients Item
    if (this.product.extraIngredients.length < 7) {
      this.product.extraIngredients.push({});
    }
  }; 

  removeVariants = function (removeIndex) { // remove Item from Index
    if (this.product.variants.length > 1) {
      this.product.variants.splice(removeIndex, 1);
    }
  }

  removeExtraIngredients = function (removeIndex) { // remove Item from Index
    if (this.product.extraIngredients.length > 1) {
      this.product.extraIngredients.splice(removeIndex, 1);
    }
  }

  calcualteDiscount(index) { // calculating discount
   // console.log(' MRP ' + this.product.variants[index].MRP + ' Discount ' + this.product.variants[index].Discount);
    const price = ((this.product.variants[index].MRP * this.product.variants[index].Discount) / 100);
   // console.log(' price ' + price);
    this.product.variants[index].price = (this.product.variants[index].MRP - price);
  }
  onSaveData(form: NgForm) { // when user add new Product
    this.uploader.uploadAll();
    this.uploader.onSuccessItem = (item: any, response: any, status: number, headers: any): any => {
      const res: any = JSON.parse(response);
      this.product.imageUrl = res.secure_url;
      this.product.publicId = res.public_id;
      this.restService.saveProductData(this.product)
        .subscribe((response) => {
          console.log('new product' +JSON.stringify(this.product));
          this.toster.success('Product Added Successfully!', 'Success!');
          this.route.navigate(['/pietech/products']);
        }, (error) => {
          this.toster.error( 'Error!');
        });
    };
  }

  onSelectLoction(event) { // when user select Location
    this.isLocationSelected = true;
    this.restService.getActiveCategoryByRestaurant(this.product.restaurantID).subscribe((res: any) => {
      this.categoryData = res;
    });
    this.getTagData(this.product.location);
  }

  getAllCategorybyLoctionInfo() {
      this.restService.getActiveCategoryByLocationId(this.product.location
        ).subscribe((res: any) => {
      this.categoryData = res;
    });
    }

  addTag() { // to navigate Tag component
    const check = confirm('Do You Want to Leave This Page?');
    if (check) {
      this.route.navigate(['/pietech/tags']);
    }

  }

  ngOnInit() {
  }
}
