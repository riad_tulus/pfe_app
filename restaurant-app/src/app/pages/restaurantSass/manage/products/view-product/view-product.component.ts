import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductsService} from './../products.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css'],
  providers: [ProductsService]
})
export class ViewProductComponent implements OnInit {
  product: any = {};
  url: any;
  loading = true;
  prdId: any;


  constructor(private route: Router, private router: ActivatedRoute, private restService: ProductsService, private toster: ToastrService) {
    this.router.params
      .map((params) => params['id'])
      .subscribe((id) => {

        this.getProductData(id);
      }, (error) => {
        this.toster.error('error occour');
      });

  }

  getProductData(prdId: any) {
    this.restService.getSpacificProductsData(prdId).subscribe((res: any) => {
        console.log('new product' +JSON.stringify(this.product));
      this.loading = false;
      this.product = res;
      this.url = res.imageUrl;

    }, (error) => {
      this.loading = false;

    });

  }

  ngOnInit() {
  }


}
