import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { ProductsService } from '../products.service';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../../../../layouts/shared-service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css'],
  providers: [ProductsService]
})

export class AllProductsComponent implements OnInit {
  pageTitle = 'Produits'
  public Id: any;
  public locationData: Array<any>;
  public productData: any[] = [];
  public restaurantID: any;
  public productName: any = {
    name: ''
  };
  public restaurantId: any;
  public loading: boolean = true;
  public dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg: boolean = false;
  public errorImage = './assets/content/error.png'
  public noContent: any = './assets/content/nodata_cloud.png';
  role: any;


  constructor(private _sharedService: SharedService, public router: Router, public route: ActivatedRoute, private restService: ProductsService, private toast: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.restaurantId = localStorage.getItem('Id');
    this.role = localStorage.getItem('role');
    this.route.params
      .map((params) => params['id'])
      .subscribe((id) => {
        if (this.role === 'Owner') {
          this.Id = id;
          this.Id = localStorage.getItem('Id')
          this.getProductDataInFo(this.Id);
        }
        else {
          if (this.role === 'Manager')
            this.Id = localStorage.getItem('locationId');
          this.getProductDataInFo(this.Id);
        }
      })
    this.getLocationDataInfo();

  }

  getProductDataInFo(id: any) {
    if (this.role === 'Manager') { // fetching Product data from server by Location Id
      this.getDataByLocation(this.Id);
    } else {
      if (this.role === 'Owner') { // fetching Product data from server by Restaurant Id
        this.getAllProductsData();
      }  // elseIf close
    }  // else colse
  }

  assignData(data: any) { // after fetching data from serve, assigning to array
    this.productData = data.productdata;
    // console.log('product data '+JSON.stringify(this.productData))
    this.loading = false;
    if (this.productData.length >= 0) {
      this.dataLength = 1;
    } else {
      this.dataLength = 0;
    }
  }


  getLocationDataInfo() { // fetching Locations data from server (role: Owner)
    this.restService.getAllLocation(this.restaurantId).subscribe((res: any) => {
      this.locationData = res;
    });
  }

  onSelectLocation(event) { // while selecting location from select List (role: Owner)
    const locationId = event.value;
    if (locationId !== 'allData') {
      this.getDataByLocation(locationId);
    } else {
      if (locationId === 'allData') {
        console.log(locationId);
        this.getAllProductsData();
      }
    }
  }

  getDataByLocation(locationId) {
    this.restService.getProductsDataByLocationId(locationId)
      .subscribe((res: any) => {
        // console.log("product Data"+JSON.stringify(res));
        this.assignData(res);
      }, (error) => {
        this.loading = false;
        this.errorImg = true;
      });
  }

  getAllProductsData() {
    this.restService.getProductsData(this.Id)
      .subscribe((res: any) => {
        console.log("Response of poduct" + JSON.stringify(res))
        this.assignData(res);
      }, (error) => {
        this.loading = false;
        this.errorImg = true;
      });
  }

  ngOnInit() {
  }
  // Invoke when user want to view details a Product
  onView(productId: any) {
    this.router.navigate(['pietech/product/view/' + productId]);
  }

  // Invoke when user want to delete a Product
  onDelete(productId, index) {
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result) {
        this.restService.deleteProductData(productId).subscribe((res: any) => {
          this.productData.splice(index, 1)
          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );
        });
      } else {
        console.log('i am in else' + result.dismiss);
        swal(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        );
      }
    });
  }
  // Invoke when user want to update details a Product
  onUpdate(id) {
    this.router.navigate(['pietech/product/edit/' + id]);
  }

}
