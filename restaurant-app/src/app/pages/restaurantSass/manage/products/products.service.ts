import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {ConstantService} from '../../../../services/constant.service';
import {CrudService} from '../../../../services/crud.service';

@Injectable()
export class ProductsService {

  cloudinarUpload: any;

  constructor(public constantService: ConstantService, public crud: CrudService) {
    this.cloudinarUpload = this.constantService.cloudinarUpload;
  }

  getAllLocation(id: any) {
    return this.crud.getOne('locations/all/data', id).map((data: Response) => data);
  }
// to get all tags by location
  getAllTagData(locationId) {
    return this.crud.getOne('tags/custom/data', locationId)
      .map((data: Response) => data);
  }

// get product by location
  getProductsDataByLocationId(locationId: any) {
    return this.crud.getOne('products/location', locationId).map((data: Response) => data);
  }

// get all category by location
  getCategoriesDataByLocationId(locationId: any) {
    return this.crud.getOne('categories/location', locationId).map((data: Response) => data);
  }


  getProductsData(id: any) {   // get all products from all locations
    return this.crud.getOne('products/restaurant', id).map((data: Response) => data);
  }

// get product details by SubCategory Id
  getSpacificProductsData(Id: any) {
    return this.crud.getOne('products', Id).map((data: Response) => data);
  }

// to save product
  saveProductData(productData: any) {
    const body = JSON.stringify(productData);
    return this.crud.post('products', body).map((data: Response) => data);
  }

// to update product
  updateProductData(productData: any, prdID: any) {
    const body = JSON.stringify(productData);
    return this.crud.put('products', body, prdID).map((data: Response) => data);
  }

// to delete product
  deleteProductData(ProductDataId: any) {
    return this.crud.delete('products', ProductDataId).map((data: Response) => data);
  }

  getActiveCategoryByLocationId(LocationId:any){
    return this.crud.getOne('categories/all/enable/categorylist/bylocation',LocationId).map((data:Response)=>data);
  }

  getActiveCategoryByRestaurant(RestaurantId:any){
    return this.crud.getOne('categories/all/enable/categorylist/byrestaurant',RestaurantId).map((data:Response)=>data);
  }
}



