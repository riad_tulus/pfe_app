import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../products.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {CloudinaryOptions, CloudinaryUploader} from 'ng2-cloudinary';
import {NgForm} from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css'],
  providers: [ProductsService]
})

export class UpdateProductComponent implements OnInit {
  url1: any = '';
  public locationData: Array<any> = [];
  public categoryData: Array<any> = [];
  public tagData: any [] = [];
  form: NgForm
  product: any = {
    restaurantID: '',
    location: '',
    category: '',
    subcategory: '',
    title: '',
    brand: '',
    description: '',
    tags: [],
    imageUrl: '',
    publicId: '',
    variants: [{}],
    extraIngredients: [{}],
    flag: 0,
    categoryTitle: ''
  };
  public value: any = {};
  public disabled = false;
  public check = false;
  public productId: any;

  private locationId: any;

  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions(this.restService.cloudinarUpload)
  );

  constructor(private restService: ProductsService, private route: Router, private router: ActivatedRoute, private toster: ToastrService) {
    this.product.restaurantId = localStorage.getItem('Id');
    this.router.params
      .map((params) => params['id'])
      .subscribe((id) => {
        this.productId = id;
        if (this.productId != null) {
          this.getProductDataInfo(this.productId);
        }
        this.getLocationInfo();
      }, () => {
        this.toster.error('error occur');
      });
  }

  getProductDataInfo(Id) {
    this.restService.getSpacificProductsData(Id).subscribe((res: any) => {
      console.log('data of product'+JSON.stringify(res));
      this.product = res;
      this.url1 = res.imageUrl;
      this.product.flag = 0;
      this.locationId = res.location._id;
      this.getTagData();
      this.getCategoryData();
    }, () => {
      this.toster.error('error occur');
    });
  }
  calcualteDiscount(index) { // calculating discount
    // console.log(' MRP ' + this.product.variants[index].MRP + ' Discount ' + this.product.variants[index].Discount);
    const price = ((this.product.variants[index].MRP * this.product.variants[index].Discount) / 100);
    // console.log(' price ' + price);
    this.product.variants[index].price = (this.product.variants[index].MRP - price);
  }

  addVariants = function () { // add new variants Item
    if (this.product.variants.length < 7) {
      const newItemNo = this.product.variants.length + 1;
      this.product.variants.push({});
    }
  };

  removeVariants = function (index) { // remove Item from Index
    if (this.product.variants.length > 1) {
      const lastItem = this.product.variants.length - 1;
      this.product.variants.splice(index);
    }
  };

  addExtraIngredients= function () { // add new ExtraIngredients Item
    if (this.product.extraIngredients.length < 7) {
      this.product.extraIngredients.push({});
    }
  };

  removeExtraIngredients = function (removeIndex) { // remove Item from Index
    if (this.product.extraIngredients.length > 1) {
      this.product.extraIngredients.splice(removeIndex, 1);
    }
  };

  onSelectsize(event){// Select size/quantity of product full/half
    this.product.variants.size = event.value;
  }

  readUrl1(event) { // invoke when user select image to upload
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (data: any) => {
        this.url1 = data.target.result;
        this.check = true;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  getLocationInfo() { // fetch all locations data of a restaurant (for Owner)
    this.restService.getAllLocation(this.product.restaurantId).subscribe((res: any) => {
      this.locationData = res;
    }, () => {
      this.toster.error('error occur');
    });
  }

  getTagData() { // fetch all Tag data of a Location by LocationID
    this.restService.getAllTagData(this.locationId).subscribe((res: any) => {
      this.tagData = res;
    }, () => {
      this.toster.error('error occur');
    });
  }

  onUpdateForm() { // invoke when user update Product
    console.log("data"+JSON.stringify(this.product));
    if (this.check) {
      this.uploader.uploadAll();
      this.uploader.onSuccessItem = (item: any, response: any, status: number, headers: any): any => {
        const res: any = JSON.parse(response);
        this.product.imageUrl = res.secure_url;
        this.product.publicId = res.public_id;
        this.restService.updateProductData(this.product, this.productId).subscribe((res: any) => {
          console.log("data"+JSON.stringify(res));
          this.toster.success('product updated successfully');
          return this.route.navigate(['/pietech/products']);
        }, () => {
          this.toster.error('error occur');
          return this.route.navigate(['/pietech/products']);
        });
      };
    } else {
      this.restService.updateProductData(this.product, this.productId).subscribe((res: any) => {
        this.toster.success('product updated successfully');
        return this.route.navigate(['/pietech/products']);
      }, () => {
        return this.route.navigate(['/pietech/products']);
      });
    }
  }

  onSelectTags(event) {
  }

  getCategoryData() { // fetch Category from server
    this.restService.getCategoriesDataByLocationId(this.locationId).subscribe((res: any) => {
      this.categoryData = res;
    }, () => {
      this.toster.error('error occur');
    });
  }

  onSelectLoction(event) { // when user select Location
    this.restService.getProductsDataByLocationId(this.product.location).subscribe((res: any) => {
      this.categoryData = res;
    }, () => {
      this.toster.error('error occur');
    });
  }

  onSelectCategory(event) { // when user select Category
    this.product.category = event.value;
    const index = this.categoryData.findIndex( i => i._id === event.value);
    this.product.categoryTitle = this.categoryData[index].categoryName;
  }

  ngOnInit() {
  }


}
