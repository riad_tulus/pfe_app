import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { ToastrService } from 'ngx-toastr';
import swal from 'sweetalert2';

@Component({
  selector: 'app-all-products-location',
  templateUrl: './all-products-location.component.html',
  styleUrls: ['./all-products-location.component.css'],
  providers: [ProductsService]
})
export class AllProductsLocationComponent implements OnInit {

  public productData: any[] = [];
  public loading = true;
  public dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0

  public errorImg = false;
  public errorImage = './assets/content/error.png';

  public noContent: any = './assets/content/nodata_cloud.png';

  constructor(private router: Router, private route: ActivatedRoute, private restService: ProductsService, private toster: ToastrService) {
    this.route.params
      .map((params) => params['id'])
      .subscribe((id) => {
        this.getProductData(id);
      }, (error) => {
        this.toster.error('error occur');
      });
  }

  getProductData(locationId) { // fetch all products from server  by location ID
    this.restService.getProductsDataByLocationId(locationId).subscribe((res: any) => {
      this.productData = res.productdata;

      this.loading = false;
      if (this.productData.length >= 1) {
        this.dataLength = 1;
      } else {
        this.dataLength = 0;
      }
    }, (error) => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  onDelete(productId, index) { // while user want to delete a product
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.restService.deleteProductData(productId).subscribe((res: any) => {
          this.productData.splice(index, 1);
          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        });

      } else {
        swal(
          'Cancelled',
          'Your file is safe :)',
          'error'
        )
      }
    })

  }

  onUpdate(id) { // while user want to update a product
    this.router.navigate(['pietech/product/edit/' + id]);
  }

  onView(productId: any) { // while user want to view a product detail
    this.router.navigate(['pietech/product/view/' + productId]);
  }

  ngOnInit() {
  }

}
