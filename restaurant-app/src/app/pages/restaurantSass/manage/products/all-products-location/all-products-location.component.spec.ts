import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AllProductsLocationComponent} from './all-products-location.component';

describe('AllProductsLocationComponent', () => {
  let component: AllProductsLocationComponent;
  let fixture: ComponentFixture<AllProductsLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AllProductsLocationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllProductsLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
