import { Injectable } from '@angular/core';
import {ConstantService} from '../../../../services/constant.service';
import {CrudService} from '../../../../services/crud.service';
@Injectable()

export class CategoriesService {
  //variable for image uplaod
  cloudinarUpload:any;
  constructor(private constentService:ConstantService,public crud:CrudService) {
  	this.cloudinarUpload =constentService.cloudinarUpload;
   }
//geting complete data from categories
getAllCategoryData(id:any){
	return this.crud.getOne('categories/restaurant',id).map((data:Response)=>data);
}

//add new category
addNewCategoryData(categoryData:any){
  const body = JSON.stringify(categoryData);
	return this.crud.post('categories',body).map((data:Response)=>data);

}
//get all location
getAllLocationsData(id:any){         
       return this.crud.getOne('locations/all/data',id).map((data:Response)=>data);
}

//get selected category
getSpacificCategoryData(CategoryId:any){
   return this.crud.getOne('categories',CategoryId).map((data:Response)=>data);
}

//get category data according to location
getAllCategoryDataByLocation(Id:any){
  return this.crud.getOne('categories/location',Id).map((data:Response)=>data);
}

//update specific category
updateCategoriesData(CategoryData:any,CategoryId:any){
   const body = JSON.stringify(CategoryData);
   return this.crud.put('categories',body,CategoryId).map((data:Response)=>data);
}

//for deleting specific category
onDeleteSingledata(categoryId:any){
  return this.crud.delete('categories',categoryId).map((data:Response)=>data);
}

//get specific location
getSpecificLocation(locationId:any){
  return this.crud.getOne('locations',locationId).map((data:Response)=>data);  
 }

}




    