import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {CloudinaryOptions, CloudinaryUploader} from 'ng2-cloudinary';
import {CategoriesService} from '../../categories.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css'],
  providers: [CategoriesService]
})

export class AddCategoryComponent implements OnInit {
  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions(this.categoriesService.cloudinarUpload)
  );

  form: NgForm;
  locationData: Array<any>
  category = {
    categoryName: '',
    restaurantID: '',
    locationInfo: {
      locationId: '',
      locationName: ''
    },
    sort: 1,
    imageUrl: '',
    publicId: '',
    enable: true,
  };

  public locationName: any;
  url1: any = './assets/content/selectImg.png';
  checked: boolean = true;
  public user: any;
  public isManager: boolean = false;

  constructor(private categoriesService: CategoriesService, private toaster: ToastrService, private route: Router) {
    this.category.restaurantID = localStorage.getItem('Id');
    this.locationName = localStorage.getItem('locationName');
    this.user = localStorage.getItem('role');

    if (this.user === 'Manager') {
      this.isManager = true;
    } else {
      this.isManager = false;
      this.getAllLocationData();
    }
    if (localStorage.getItem('locationId')) {
      this.category.locationInfo.locationId = localStorage.getItem('locationId');
      this.category.locationInfo.locationName = localStorage.getItem('locationName');
    }

  }

  getAllLocationData() { // invoke if user logged in as a Owner
    this.categoriesService.getAllLocationsData(this.category.restaurantID).subscribe((res: any) => {
      this.locationData = res;
    }, (error) => {
      this.toaster.error(error.message, 'ERROR');
    });
  }


  readUrl1(event) { // to display selected image as a thumb
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.url1 = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  onToggle(event) { // when user change category enable status
    this.category.enable = !(this.category.enable);
  }


  onSaveCategoryData() { // onSaveCategoryData method invoke when user Save Category Data
    this.uploader.uploadAll();
    this.uploader.onSuccessItem = (item: any, response: any, status: number, headers: any): any => {
      let res: any = JSON.parse(response);
      this.category.imageUrl = res.secure_url;
      this.category.publicId = res.public_id;
      this.categoriesService.addNewCategoryData(this.category)
        .subscribe((response) => {
          this.toaster.success('Category Added Successfully!', 'Success!');
          this.route.navigate(['pietech/categories']);
        }, (error) => {
          this.toaster.error(error.message, 'ERROR');
        });
    };
  }

  onSelectLocation(event) { // while user logged in as a Owner and select or change Location
    let data: any = {
      locationId: '',
      locationName: ''
    };
    let locationCategory = event.value.split('@');
    data.locationId = locationCategory[0];
    data.locationName = locationCategory[1];
    this.category.locationInfo = data;
  }

  cancel() { // navigate to category list component
    this.route.navigate(['pietech/categories']);
  }

  ngOnInit() {
  }

}
