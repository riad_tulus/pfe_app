import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoriesService } from '../categories.service';
import { SharedService } from '../../../../../layouts/shared-service';
import { ToastrService } from 'ngx-toastr';
import swal from 'sweetalert2';
// import Swal from 'sweetalert2'

@Component({
  selector: 'app-all-categories',
  templateUrl: './all-categories.component.html',
  styleUrls: ['./all-categories.component.css'],
  providers: [CategoriesService]
})

export class AllCategoriesComponent implements OnInit {
  pageTitle = 'Catégorie';
  dataCategories: Array<any>;
  locationData: any[] = [];
  selectLocationData: any;
  loading: boolean = true;
  dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg: boolean = false;
  errorImage = './assets/content/error.png';
  noContent: any = './assets/content/nodata_cloud.png';
  id: any;  // for get data by either locationId or RestaurantId
  public role: string = '';
  restaurantId: any;

  constructor(private _sharedService: SharedService, public router: Router, private route: ActivatedRoute, private restService: CategoriesService, private toaster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.role = localStorage.getItem('role');
    this.restaurantId = localStorage.getItem('Id');
    if (this.role === 'Owner') {
      this.id = localStorage.getItem('Id');
      this.getcategoryData(this.id); // get category data by restaurant Id
    } else {
      if (this.role === 'Manager') {
        this.id = localStorage.getItem('locationId');
        this.getcategoryData(this.id); // get category data by location Id
      }
    }
    this.getAllLocationData();
  }

  getcategoryData(id) { // fetch category data from server according to user
    if (this.role === 'Manager') {
      this.restService.getAllCategoryDataByLocation(id)
        .subscribe((res: any) => {
          console.log("res" + JSON.stringify(res));
          this.assignData(res);
        }, (error) => {
          this.loading = false;
          this.errorImg = true;
        });
    } else {
      if (this.role === 'Owner') {
        this.restService.getAllCategoryData(id)
          .subscribe((res: any) => {

            this.assignData(res);
          }, (error) => {
            this.loading = false;
            this.errorImg = true;
          });
      }  // elseIf close
    }  // else colse
  }

  // invoke when "getcategoryData" method get success response from server
  assignData(data: any) {
    this.dataCategories = data;
    this.loading = false;
    if (this.dataCategories.length >= 1) {
      this.dataLength = 1;
    }
    else {
      this.dataLength = 0;
    }
  }

  // for toggling the status change directly
  updateStatus(index, catId) {
    let updateData: any = {};
    updateData = this.dataCategories[index];
    updateData.flag = 0;
    this.restService.updateCategoriesData(updateData, catId).subscribe((res: any) => {
      this.toaster.success('your Status has changed....');
    }, (error) => {
      this.toaster.error('somthing went wrong');
    });
  }

  onView(categoryId: any) {
    this.router.navigate(['/pietech/category/view/' + categoryId]);
  }

  onUpdate(categoryId: any) {
    this.router.navigate(['/pietech/category/edit/' + categoryId]);
  }

  // for deleting specific category
  onDelete(Id: any, index: any) {
    console.log(Id);
    swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.restService.onDeleteSingledata(Id).subscribe((res: any) => {
          this.dataCategories.splice(index, 1)
          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        });
        // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      } else {
        swal(
          'Cancelled',
          'Your file is safe :)',
          'error'
        )
      }
    })

  }

  getAllLocationData() { // "getAllLocationData" invoke when user logged in as Owner
    this.restService.getAllLocationsData(this.restaurantId).subscribe((res: any) => {
      this.locationData = res;

    }, (error) => {
      this.toaster.error('error');
    });
  }

  selectLocation(event) { // fetching data from server while selecting specific location
    this.selectLocationData = event.value;
    this.restService.getAllCategoryDataByLocation(this.selectLocationData).subscribe((res: any) => {
      this.dataCategories = res;
    }, (error) => {
      // console.log("error");
    });
  }

  ngOnInit() {
  }

}
