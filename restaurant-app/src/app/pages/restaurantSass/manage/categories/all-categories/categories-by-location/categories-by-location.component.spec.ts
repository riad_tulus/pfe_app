import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesByLocationComponent } from './categories-by-location.component';

describe('CategoriesByLocationComponent', () => {
  let component: CategoriesByLocationComponent;
  let fixture: ComponentFixture<CategoriesByLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesByLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesByLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
