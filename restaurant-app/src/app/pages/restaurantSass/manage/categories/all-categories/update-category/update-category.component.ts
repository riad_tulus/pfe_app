import { Component, OnInit } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms'
import { CategoriesService } from '../../categories.service';

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css'],
  providers: [CategoriesService]
})
export class UpdateCategoryComponent implements OnInit {
  locationData: Array<any>;
  public categoryId: number;
  category: any = {
    locationInfo: {},
    restaurantID: ''
  };
  url1: any;
  locationName: any;
  loading: boolean = true;
  errorImg: boolean = false;
  errorImage = './assets/content/error.png';
  user: any;
  update: boolean = false;

  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions(this.restService.cloudinarUpload));

  constructor(private route: ActivatedRoute, private router: Router, private toaster: ToastrService, private restService: CategoriesService) {
    this.category.restaurantID = localStorage.getItem('Id');
    this.locationName = localStorage.getItem('locationName');
    this.user = localStorage.getItem('role');

    this.route.params
      .map(params => params['id'])
      .subscribe((id) => {
        this.categoryId = id;
        if (this.categoryId) {
          this.getSpacificCategoryData();
        }
      });
    if (this.user == 'Owner') {
      this.update = true;
      this.getLoctionData();
    }

  }

  getSpacificCategoryData() {
    this.restService.getSpacificCategoryData(this.categoryId).subscribe((res: any) => {
      this.category = res;
      console.log("Category Data" + JSON.stringify(res));
      this.url1 = res.imageUrl;
      this.category.flag = 0;
      this.loading = false;
    }, (error) => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  onSelectLocation(event) {
    let data: any = {
      locationId: '',
      locationName: ''
    }
    let locationData = event.value.split('@');
    data.locationId = locationData[0];
    data.locationName = locationData[1];
    this.category.locationInfo = data;
  }

  getLoctionData() {
    this.restService.getAllLocationsData(this.category.restaurantID).subscribe((res: any) => {
      this.locationData = res;
    }, (error) => {
      this.toaster.error('error arrived');
    });
  }

  // show selected image as a thumbnil
  readUrl1(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.url1 = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
    this.category.deletePublicId = this.category.publicId;
    this.uploader.uploadAll();
    this.uploader.onSuccessItem = (item: any, response: any, status: number, headers: any): any => {
      let res: any = JSON.parse(response);
      this.category.flag = 1;
      this.category.imageUrl = res.secure_url;
      this.category.publicId = res.public_id;
    };
  }
  // Invoke when user click on update button.
  onUpdateDataCategory(ngForm: NgForm) {
    console.log(JSON.stringify(this.category));
    this.restService.updateCategoriesData(this.category, this.categoryId).subscribe((res: any) => {
      this.toaster.success('Data updated successfully');
      this.router.navigate(['pietech/categories']);
    }, (error) => {
      this.toaster.error('somthing went wrong');
    });
  }

  ngOnInit() {
  }

}



