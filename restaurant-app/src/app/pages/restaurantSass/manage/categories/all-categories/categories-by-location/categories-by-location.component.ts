import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {NgForm} from '@angular/forms';
import {CategoriesService} from '../../categories.service';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-categories-by-location',
  templateUrl: './categories-by-location.component.html',
  styleUrls: ['./categories-by-location.component.css'],
  providers: [CategoriesService]
})
export class CategoriesByLocationComponent implements OnInit {

  public dataCategories: any [] = [];
  public loading: boolean = true;
  public dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg: boolean = false;
  public errorImage = './assets/content/error.png';
  public noContent: any = './assets/content/nodata_cloud.png';

  constructor(private router: Router, private route: ActivatedRoute, private restService: CategoriesService, private toster: ToastrService) {
    this.route.params
      .map((params) => params['id'])
      .subscribe((id) => {
        this.getCategoryData(id);
      });
  }

  getCategoryData(locationId) { // get category data by Location Id
    this.restService.getAllCategoryDataByLocation(locationId).subscribe((res: any) => {
      this.dataCategories = res;
      this.loading = false;
      if (this.dataCategories.length >= 1) {
        this.dataLength = 1;
      }
      else {
        this.dataLength = 0;
      }
    }, (error) => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  onView(id: any) { // navigate to View category page
    this.router.navigate(['/pietech/view/' + id]);
  }

  onUpdate(id: any) { // navigate to update category page
    this.router.navigate(['pietech/edit/' + id]);
  }

  onDelete(id: any, index: any) { // invoke when user want to delete a category data
    const result = confirm('do you realy want to delete');
    if (result) {
      this.restService.onDeleteSingledata(id).subscribe((res: any) => {
        this.dataCategories.splice(index, 1)
        this.toster.success('Data deleted successfully');
      }, (error) => {
        this.toster.error('error occour');
      });
    }

  }


  ngOnInit() {
  }

}
