import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NgForm} from '@angular/forms';
import {CategoriesService} from '../../categories.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.css'],
  providers: [CategoriesService]
})

export class ViewCategoryComponent implements OnInit {
  categories: any = {};
  url: any;
  loading = true;

  constructor(private route: ActivatedRoute, private restService: CategoriesService, private toster: ToastrService) {
    this.route.params
      .map((params) => params['id'])
      .subscribe((id) => {
        this.getCategoryData(id);
      }, (error) => {
        this.toster.error(error.message, 'Error');
      });
  }
  // fetching category data from server by category id
  getCategoryData(dataId: any) {
    this.restService.getSpacificCategoryData(dataId).subscribe((res: any) => {
      this.categories = res;
      this.url = res.imageUrl
      this.loading = false;
    }, (error) => {
      this.loading = false;
      this.toster.error(error.message, 'Error');
    });
  }

  ngOnInit() {
  }

}
