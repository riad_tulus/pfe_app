import {Injectable} from '@angular/core';
import {CrudService} from '../../../../services/crud.service';


@Injectable()
export class CustomersService {

  constructor(public crud: CrudService) {
  }

  // to get all customer list of restaurant
  getAllCustomerByRestaurant(restaurantId: any) {
    return this.crud.getOne('orders/customer/restaurant', restaurantId).map((data: Response) => data);
  }

  // to get customer data by id
  getSpecificCustomerData(id: any) {
    return this.crud.getOne('orders/user', id).map((data: Response) => data);
  }

  // get customer by location id
  getCustomerByLocation(locationId: any) {
    return this.crud.getOne('orders/customer/location', locationId).map((data: Response) => data);

  }


}
