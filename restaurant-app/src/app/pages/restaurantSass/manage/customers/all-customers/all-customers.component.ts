import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CustomersService} from '../customers.service';
import {SharedService} from '../../../../../layouts/shared-service';

import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-all-customers',
  templateUrl: './all-customers.component.html',
  styleUrls: ['./all-customers.component.css'],
  providers: [CustomersService]
})

export class AllCustomersComponent implements OnInit {
  pageTitle = 'Client'
  public restaurantId: any;
  loading = true;
  dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg = false;
  errorImage = './assets/content/error.png';
  noContent: any = './assets/content/nodata_cloud.png';
  public allCustomer: any = {
    locationId: '',
    status: ' ',
    user: '',
    point: 0

  };

  role: any;
  public view = false;

  constructor(private _sharedService: SharedService, public router: Router, private restService: CustomersService, private toaster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.restaurantId = localStorage.getItem('Id');
    this.role = localStorage.getItem('role');
    if (this.role == 'Owner') {
      this.view = true;
    }
    this.getCustomerInfo();
  }

  ngOnInit() {
  }

  OnView(id: any) {
    this.router.navigate(['/pietech/customer/view/' + id]);
  }

  /** fetching customers data from server
   * if user logged in as a Manager so get customer list of a specific location
   * else user logged in as a owner so get customer list of a restaurant(all locations)*/
  getCustomerInfo() {
    const locationId = localStorage.getItem('locationId');
    if (locationId != null) {
      this.restService.getCustomerByLocation(locationId).subscribe((res: any) => {
        console.log('hkkj' + JSON.stringify(res));
        localStorage.setItem('customerId', res.id);
        this.allCustomer = res;
        this.loading = false;
        if (this.allCustomer.length >= 1) {
          this.dataLength = 1;
        } else {
          this.dataLength = 0;
        }

      }, (error) => {
        this.loading = false;
        this.errorImg = true;
      });

    } else {
      this.restService.getAllCustomerByRestaurant(this.restaurantId).subscribe((res: any) => {
        localStorage.setItem('customerId', res.id);
        this.allCustomer = res;
        this.loading = false;
        if (this.allCustomer.length >= 1) {
          this.dataLength = 1;
        } else {
          this.dataLength = 0;
        }

      }, (error) => {
        this.loading = false;
        this.errorImg = true;
      });
    }
  }
}
