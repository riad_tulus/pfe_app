import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router'
import {CustomersService} from '../customers.service';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-view-customer',
  templateUrl: './view-customer.component.html',
  styleUrls: ['./view-customer.component.css'],
  providers: [CustomersService]
})
export class ViewCustomerComponent implements OnInit {
  public customerData: any = {
    name: '',
    address: '',
    contactNumber: '',
    email: ''
  };
  public customerOrders: any [] = [];
  loading = true;
  dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg = false;
  errorImage = './assets/content/error.png';
  noContent: any = './assets/content/nodata_cloud.png';
  custumerID: any;


  constructor(private restService: CustomersService, private router: ActivatedRoute, private toster: ToastrService, private route: Router) {
    this.router.params
      .map((params) => params['id'])
      .subscribe((id) => {
        this.custumerID = id;
        this.getCustomerData();
      });
  }

  ngOnInit() {
  }


  OnView(id: any) { // navigate to view order page
       this.route.navigate(['/pietech/order/view/',id]);
  }

  getCustomerData() { // fetching customer data from the server
    this.restService.getSpecificCustomerData(this.custumerID).subscribe((res: any) => {
      this.customerData = res[0].user;
      this.customerOrders = res;
      this.loading = false;
      if (res.length >= 1) {
        this.dataLength = 1;
      }
      else {
        this.dataLength = 0;
      }
    }, (error) => {
      this.loading = false;
      this.errorImg = true;
    });
  }


}
