import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {OrdersService} from '../orders.service';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {CsvService} from 'angular2-json2csv';
import {ToastrService} from 'ngx-toastr';
import {ConstantService} from '../../../../../services/constant.service';
import {SharedService} from '../../../../../layouts/shared-service';


@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css'],
  providers: [OrdersService, CsvService, ConstantService]
})

export class AllOrdersComponent implements OnInit {
  pageTitle = 'Commande'
  public customerFilter: any = {
    locationId: '',
    status: '',
    orderId: ''
  };
  public users: any = {};
  locationId = 1;
  // customerName: any = {
  //   name: ''
  // };
  flag = false;
  public restaurantID: any;
  // public orderData: Array<any>;
  public allOrderData: any[] = [];
  public locationData: Array<any>;
  // for loader
  loading = true;
  dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg = false;
  errorImage = './assets/content/error.png';
  public noContent: any = './assets/content/nodata_cloud.png';
  public userType: any;
  public searchActive = 0;
  public searchOrderId: any;
  locId: any;

  constructor(private _sharedService: SharedService,
              private constantService: ConstantService,
              private http: Http,
              private router: ActivatedRoute,
              private route: Router,
              private restService: OrdersService,
              public csvService: CsvService,
              private toster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    this.restaurantID = localStorage.getItem('Id');
    this.userType = localStorage.getItem('role');
    this.locId = localStorage.getItem('locationId');

    this.getAllLocationData();
    this.getAllOrderInfo();
  }

  // invoked by constructor() for fetching all saved locations
  getAllLocationData() {
    this.restService.getAllLocationsData(this.restaurantID).subscribe((res: any) => {
      this.locationData = res;
    }, (error) => {
      this.toster.error('error arrived');
    });
  }

  // while user want to view full detail of a order
  onOrderView(id) {
    this.route.navigate(['/pietech/order/view/', id]);
  }

  // get all orders of a location (for role as a Manager) or Restaurant (for role as a Owner)
  getAllOrderInfo() {
    if (this.locId !== null) {
      this.restService.getOrdersDataByLocation(this.locId).subscribe((res: any) => {
        this.allOrderData = res;
        this.loading = false;
        if (this.allOrderData.length >= 1) {
          this.dataLength = 1;
        } else {
          this.dataLength = 0;
        }
      }, (error) => {
        this.loading = false;
        this.errorImg = true;
      });
    } else {
      this.restService.getOrdersDataByRestaurant(this.restaurantID).subscribe((res: any) => {
        this.allOrderData = res;
        this.loading = false;
        if (this.allOrderData.length >= 1) {
          this.dataLength = 1;
        } else {
          this.dataLength = 0;
        }
      }, (error) => {
        this.loading = false;
        this.errorImg = true;
      });
    }
  }

  // Invoke when user want change Order status
  onUpdate(event: any, id: any, preStatus: any, index) {
    if (event.value === preStatus) {
      this.toster.info('Order is already in this status');
    } else {
      let data = {
        status: ''
      };
      data.status = event.value;
      this.restService.updateOrdersData(data, id).subscribe((res: any) => {
        this.allOrderData[index].status = event.value;
        this.toster.success(res.message);
      }, (error) => {
        this.toster.error('Error');
      });
    }


  }

  // invoke when user want to retrieve data in form of CSV sheet
  convertToCSV() {
    const jsonObject = JSON.stringify(this.locationData);
    if (this.locationId !== 1) {
      const data = {flag: 0, location: this.locationId}
      this.restService.onGetCSV(data).subscribe((res: any) => {
        this.ConvertToCSV(res);
      });
    } else {
      const data = {flag: 1, location: 0}
      this.restService.onGetCSV(data).subscribe((res: any) => {
        this.ConvertToCSV(res);
      });
    }
  }

// data on CSV
  ConvertToCSV(res) {
    const data = [];
    for (let i = 0; i <= res.length; i++) {

      if (res[i] !== undefined && res[i].userInfo) {
        const objectData = {
          id: res[i]._id,
          orderID: res[i].orderID,
          restaurantID: res[i].restaurantID,
          paymentOption: res[i].paymentOption,
          orderType: res[i].orderType,
          earnedPoint: res[i].earnedPoint,
          grandTotal: res[i].grandTotal,
          status: res[i].status,
          email: res[i].userInfo.email,
          address: res[i].userInfo.address,
          name: res[i].userInfo.name,
          contactNumber: res[i].userInfo.contactNumber
        }
        data.push(objectData)
      }

    }
    this.csvService.download(data, 'OrderDetails');
  }

  ngOnInit() {
  }

// Filter Orders by Location ( for Owner)
  onSelectLocation(event) {
    this.customerFilter.locationId = event.value;
    this.allOrderData = [];
    this.restService.searchOrderData(this.customerFilter).subscribe((res: any) => {
      this.allOrderData = res;
    }, (error) => {
      this.toster.error('error arrived');
    });

  }

// Filter Orders by order status
  onStatus(event) {
    if (this.locId) {
      this.customerFilter.status = event.value;
      this.allOrderData = [];
      if (event.value === '') {
        this.getAllOrderInfo();
      } else {
        this.restService.searchOrderDataByLocation(this.locId, this.customerFilter).subscribe((res: any) => {
          this.allOrderData = res;
        }, () => {
          this.toster.error('error arrived');
        })
      }

    } else {
      this.customerFilter.status = event.value;
      this.allOrderData = [];
      if (event.value === '') {
        this.getAllOrderInfo();
      } else {
        this.restService.searchOrderData(this.customerFilter).subscribe((res: any) => {
          this.allOrderData = res;

        }, (error) => {
          this.toster.error('error arrived');
        });
      }
    }
  }

// Filter Orders by Order Id
  searchByOrderId(event) {
    if (event.target.value.length > 4) {
      this.searchActive = 1;
      this.customerFilter.orderId = event.target.value;
      this.restService.searchOrderData(this.customerFilter).subscribe((res: any) => {
        if (res != null) {
          this.allOrderData = res;
          this.searchActive = 2;
        } else {
          this.searchActive = 3;
        }
      }, (error) => {
        this.toster.error('something went wrong');

      });
    } else {
      if (event.target.value.length === 0) {
        this.searchActive = 0;
        this.getAllOrderInfo();
      }
    }
  }

// when user clear search field
  clearSearch() {
    this.searchActive = 0;
    this.searchOrderId = '';
    this.getAllOrderInfo();
  }
  // navigate to Invoice Page
  invoicePage(id) {
    this.route.navigate(['/pietech/order/invoice/', id]);
  }

}
