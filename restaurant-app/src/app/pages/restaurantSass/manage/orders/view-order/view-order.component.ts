import {Component, OnInit} from '@angular/core';
import {OrdersService} from '../orders.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css'],
  providers: [OrdersService]
})

export class ViewOrderComponent implements OnInit {

  public myOrder: any = {
    deliveryByName: '',
    assignedDate: Date,
    assigned: false,
    status: '',
    deliveryBy: '',
    activationStatus: true
  };
  loading = true;
  dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg = false;
  errorImage = './assets/content/error.png';
  public locationId: any;
  staffList: any = [];
  public userData: any = {};
  public shippingAddress: any = {};


  constructor(private restService: OrdersService, private route: ActivatedRoute, private toast: ToastrService, private router: Router) {

    this.locationId = localStorage.getItem('locationId');

    this.route.params
      .map((params) => params['id'])
      .subscribe((id) => {
        this.getDataBySpecificId(id);
      }, (error) => {
        this.toast.error('some problem occours');

      });

    if (this.locationId !== null) {
      this.allStaffData();
    }
  }

  ngOnInit() {
  }
// invoked by "constructor()" to fetch specific order details
  getDataBySpecificId(id: any) {
    this.restService.getSpacificOrdersData(id).subscribe((res: any) => {
      this.shippingAddress = res.shippingAddress;
      this.myOrder = res;
      this.userData = res.userInfo;
      this.loading = false;
      if (this.myOrder._id !== null) {
        this.dataLength = 1;
      } else {
        this.dataLength = 0;
      }
    }, (error) => {
      this.loading = false;
      this.errorImg = true;
    });
  }

// invoked when user update order status
  onUpdateStatus(status: any, Id: any, myStatus: any) {
    if (status.value === myStatus) {
      this.toast.info('product is already in this status');
    } else {
      const data = {
        status: '',
      }
      data.status = status.value;

      this.restService.updateOrdersData(data, Id).subscribe((res: any) => {
        this.myOrder.status = status.value;
        this.toast.success(JSON.stringify(res.message));
      }, (error) => {
        this.toast.error(error.message, 'Error');

      });
    }
  }
// invoke when user want to see product information
  productView(productId: any) {
    this.router.navigate(['/pietech/product/view/', productId]);
  }
// invoked by "constructor()" for fetching all the Staff members
  allStaffData() {
    this.restService.getDeliveryPerson(this.locationId).subscribe((res: any) => {
      this.staffList = res;
      console.log('data' + JSON.stringify(res));
    }, (error) => {
      this.toast.error(error.message, 'Error');
    });
  }
// invoked when Manager assign order for Delivery to Staff members
  selectDelivery(event, id) {
    let deliveryBy: any = {};
    deliveryBy = this.staffList[event.value];
    this.myOrder.deliveryByName = deliveryBy.name;
    this.myOrder.deliveryBy = deliveryBy._id;
    this.myOrder.assignedDate = Date.now();
    this.myOrder.assigned = true;
    this.restService.updateOrdersData(this.myOrder, id).subscribe((res: any) => {
      this.toast.success('This order has assigned to  deliveryBoy');
    }, (error) => {
      this.toast.error(error.message, 'Error');
    });
  }
  invoicePage() {
    this.router.navigate(['/pietech/order/invoice/', this.myOrder._id]);
  }
}

