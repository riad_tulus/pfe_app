import { Component, OnInit } from '@angular/core';
import {OrdersService} from '../../orders.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-order-invoice',
  templateUrl: './order-invoice.component.html',
  styleUrls: ['./order-invoice.component.css'],
  providers: [OrdersService]
})
export class OrderInvoiceComponent implements OnInit {
  public myOrder: any = {
    deliveryByName: '',
    assignedDate: Date,
    assigned: false,
    status: '',
    deliveryBy: '',
    activationStatus: true
  };
  loading = true;
  dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  errorImg = false;
  errorImage = './assets/content/error.png';
  public locationId: any;
  public userData: any = {};
  public shippingAddress: any = {};

  constructor(private restService: OrdersService, private route: ActivatedRoute, private toast: ToastrService, private router: Router) {
    this.route.params
      .map((params) => params['id'])
      .subscribe((id) => {
        this.getDataBySpecificId(id);
      }, (error) => {
        this.toast.error('some problem occours');

      });
  }
  getDataBySpecificId(id: any) {
    this.restService.getSpacificOrdersData(id).subscribe((res: any) => {
     // console.log('invoice data ' + JSON.stringify(res));
      this.shippingAddress = res.shippingAddress;
      this.myOrder = res;
      this.userData = res.userInfo;
      this.loading = false;
      if (this.myOrder._id !== null) {
        this.dataLength = 1;
      } else {
        this.dataLength = 0;
      }
    }, (error) => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  ngOnInit() {
  }

}
