
import {Injectable} from '@angular/core';
import {CrudService} from '../../../../services/crud.service';

@Injectable()
export class OrdersService {
  constructor(public crud: CrudService) {
  }

  // get all location
  getAllLocationsData(myId) {
    return this.crud.getOne('locations/all/data', myId).map((data: Response) => data);
  }

  // get all order of restaurant
  getOrdersDataByRestaurant(id) {
    return this.crud.getOne('orders/restaurant', id).map((data: Response) => data);

  }

  // to get all order via location
  getOrdersDataByLocation(locationId) {
    return this.crud.getOne('orders/location', locationId).map((data: Response) => data);
  }

  // get order details via order id
  getSpacificOrdersData(Id: any) {
    return this.crud.getOne('orders', Id).map((data: Response) => data);
  }

  // edit the order data
  updateOrdersData(data: any, Id: any) {
    const body = JSON.stringify(data);
    return this.crud.put('orders', body, Id).map((data: Response) => data);

  }

  // to get data of role by location Id
  getDeliveryPerson(LocationId: any) {
    return this.crud.getOne('users/all/active/staff', LocationId).map((data: Response) => data);
  }

  // filter the customer
  onCustomerSearch(name: any) {
    const body = JSON.stringify(name);
    return this.crud.post('orders/searchByName', body).map((data: Response) => data);

  }

  onGetCSV(data: any) {
    const body = JSON.stringify(data);
    return this.crud.post('orders/custom', body).map((data: Response) => data);
  }

  getAllMemberData() {    // get employees data
    return this.crud.get('users/all/staff').map((data: Response) => data);
  }


  searchOrderData(value: any) {
    const body = JSON.stringify(value);
    return this.crud.post('orders/search', body).map((data: Response) => data);
  }

  searchOrderDataByLocation(locationId: any, value: any,) {
    const body = JSON.stringify(value);
    return this.crud.OrderSearchByLocationId('orders/search/order/bylocation',locationId,body).map((data: Response)=>data);

  }

}
