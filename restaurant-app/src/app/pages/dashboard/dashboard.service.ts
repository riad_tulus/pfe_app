import {Injectable} from '@angular/core';
import {CrudService} from '../../services/crud.service';

@Injectable()
export class DashboardService {
  restauranId: any;

  constructor(private crud: CrudService) {
    this.restauranId = localStorage.getItem('Id');
  }

  getAllDataCount() { // fetch total number of Orders, Products, Categories count
    return this.crud.get('users/countdata/total/data').map((data: Response) => data);
  }

  getAllLocationCount() { // fetch total number of locations related to Restaurant
    return this.crud.getOne('locations/all/countdata', this.restauranId).map((data: Response) => data);
  }

  getDataForGraph() {  // for plot graph if user role Owner
    return this.crud.getOne('orders/dashboard/data', this.restauranId).map((data: Response) => data)
  }

  getDataForLocationGraph(locationId) {  // for plot graph if user role Manager
    return this.crud.getOne('orders/location/data', locationId).map((data: Response) => data)
  }

  getAllDataCountByLocation(id: any) { // fetch total number of Orders, Products, Categories count by Location Id
    return this.crud.getOne('users/countdata/total/data/location', id).map((data: Response) => data);
  }


}
