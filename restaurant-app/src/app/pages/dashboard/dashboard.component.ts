import {Component, OnInit} from '@angular/core';
import {SharedService} from '../../layouts/shared-service';
import {DashboardService} from './dashboard.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-page-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DashboardService]
})

export class PageDashboardComponent implements OnInit {
  countData: any = { // countData object to show number of Orders, category and products count on cards
    restaurantOrderCount: 0,
    categoryCount: 0,
    productCount: 0
  };
  loadChart = 0;
  /** initializing loadChart variable with 0
   * 0 for activating Loader
   * 1 for deactivating Loader
   */
  dataLength = 1;
  /** initializing dataLength variable with 1
   * 1 if Graph data comes from server
   * 0 if Graph data blank comes from server
   */
  errorImg: boolean = false;
  /** errorImg initializing with false
   * If error occurred errorImg will true;*/
  errorImage = './assets/content/error.png';
  noContent: any = './assets/content/nodata_cloud.png';
  pageTitle = 'Accueil';
  locationCount: any = 0; // initializing locationCount variable with 0
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  }
  public barChartLegend = false;

  public barChartData: any = {
    labels: [],
    borderWidth: 1,
    pointRadius: 1,
    datasets: [{
      data: []

    }]
  };
  public userRole: any; // userRole for login user role sometime Manager or Admin
  public myLocation: any; // used if login user role Manager

  constructor(private _sharedService: SharedService, private restService: DashboardService, private toster: ToastrService) {
    this._sharedService.emitChange(this.pageTitle);
    // set user role and user location from localStorage
    this.userRole = localStorage.getItem('role');
    this.myLocation = localStorage.getItem('locationName');
    // if user login as Owner so invoke getAllLocationCountInfo method to get total locations
    if (this.userRole === 'Owner') {
      this.getAllLocationCountInfo();
    }
    this.allDataCount();
    this.getGraphData();
  }
  allDataCount() { // fetch total number of Orders, Products, Categories count from server
    if (this.userRole === 'Owner') {
      this.restService.getAllDataCount().subscribe((res) => {
        this.countData = res;
      });
    } else {
      if (this.userRole === 'Manager') {
        const id = localStorage.getItem('locationId')
        this.restService.getAllDataCountByLocation(id).subscribe((res) => {
          this.countData = res;
        });
      }
    }
  }


  getAllLocationCountInfo() {
    /** invoke if user role "Owner"
     *  get count of total locations of restaurant */
    this.restService.getAllLocationCount().subscribe((res: any) => {
      // console.log('data' + JSON.stringify(res));
      this.locationCount = res;
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  getGraphData() {
    /**
     * if user login as Owner so getDataForGraph() method call and get
      * sales report data of all registered location,
      * data assign to bar chart and generate graph
     * else user login as Manager so getDataForLocationGraph() method call and get
      * sales report data of self location,
      * data assign to bar chart and generate graph
     */
    if (this.userRole === 'Owner') {
      this.restService.getDataForGraph().subscribe((res: any) => {
          // console.log('graph data' + JSON.stringify(res));
          this.loadChart = 1;
          this.barChartData.labels = res.labels;
          this.barChartData.datasets = res.datasets;
          // console.log('length' + this.barChartData.labels.length);
          if (this.barChartData.labels.length > 0) {
            this.dataLength = 1;
          } else {
            this.dataLength = 0;
          }
        },
        (error) => {
          this.toster.error(error.message, 'ERROR');
          this.loadChart = 1;
          this.errorImg = true;
        });
    } else {
      if (this.userRole === 'Manager') {
        // console.log('get graph data for manager')
        const id = localStorage.getItem('locationId')
        this.restService.getDataForLocationGraph(id).subscribe((res: any) => {
            // console.log('data length ' + JSON.stringify(res));
            this.loadChart = 1;
            this.barChartData.labels = res.labels;
            this.barChartData.datasets = res.datasets;
            if (this.barChartData.labels.length > 0) {
            } else {
              this.dataLength = 0;
            }
          },
          (error) => {
            this.toster.error(error.message, 'ERROR');
            this.loadChart = 1;
            this.errorImg = true;
          });
      }

    }
  }


  ngOnInit() {

  }


}
