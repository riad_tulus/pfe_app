'use strict';
import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {Observable} from 'rxjs/Observable';
import {ConstantService} from './constant.service';

@Injectable()
export class SocketSharedService {
  socket;

  constructor(public constService: ConstantService) {
    // initialization of socket
    this.socket = io.connect(this.constService.Socket_Url);
    this.socket.on('connect', function () { //  on if user connected to server
    });

    this.socket.on('disconnect', function () { // on if user disconnect to server
    });

    this.socket.on('error', function (e) { // on if any problem between client and server
    });

    this.socket.emit('info', 'Application is connected !!!'); // emitting info if user connected to server
  }


  getMessages(Uid: any) {
    /** get the new message from server through socket
     * listen event 'message+userId'
     * and send data back to getMessages subscriber */
    const observable = new Observable(observer => {
      this.socket.on('message' + Uid, (data) => {
        observer.next(data);
      });
    });
    return observable;
  }

  userInfo(userId: any) { // Emit user info for establishing socket connection
    const data: any = {
      userId: ''
    };
    data.userId = userId;
    this.socket.emit('storeClientInfo', data);
  }

  messageRead(senderId) { // updating message count by using trigger an "updateUnread" event with sender and receiver Id
    const ids: any = {
      receiverId: '',
      senderId: '',
    };
    ids.receiverId = localStorage.getItem('userId');
    ids.senderId = senderId;
    this.socket.emit('updateUnread', ids);
  }

  getMessageNotification() { // get New Message data
    const observable = new Observable(observer => {
      this.socket.on('notify' + localStorage.getItem('userId'), (data) => {
        observer.next(data);
      });
    });
    return observable;
  }

  letestOrderInfo() { // Get latest Order info through socket
    const observable = new Observable(observer => {
      this.socket.on('orderNotification' + localStorage.getItem('locationId'), (data) => {
        observer.next(data);
      });
    });
    return observable;
  }
}
