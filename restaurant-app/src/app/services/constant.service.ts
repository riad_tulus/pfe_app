import { Injectable } from '@angular/core';

@Injectable()
export class ConstantService {
  // declaration of variables
  API_ENDPOINT: String;
  Login_Auth: String;
  Socket_Url: String;
  cloudinarUpload = { // cloudinary credentials
    cloudName: 'impnolife',
    uploadPreset: 'mspqunld'
  };

  // Server URL
  constructor() {
    this.Login_Auth = 'http://10.188.70.194:8000/';
    this.API_ENDPOINT = 'http://10.188.70.194:8000/api/';
    this.Socket_Url = 'http://10.188.70.194:8000/';
  }
}


