import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

export class CrudBaseService {
  protected data: any = {api: null};
  private obervable: Observable<any>;

  // service executed
  protected extractData(res: Response) {
    this.obervable = null;
    if (res.status >= 400) {
      return 'FAILURE';
    } else if (res.status >= 200 && res.status <= 300) {
      return res.json() || [];
    }
  }

  // while error occur
  protected handleError(error: Response | any) {
    return Observable.throw(error.json());
  }

}
