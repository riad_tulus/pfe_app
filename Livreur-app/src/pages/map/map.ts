import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import mapboxgl from "mapbox-gl/dist/mapbox-gl.js";
import { Geolocation } from "@ionic-native/geolocation";

@IonicPage()
@Component({
  selector: "page-map",
  templateUrl: "map.html",
  providers: [Geolocation]
})
export class MapPage {
  public coordinates: any;
  private position: any;
  public isButtonVisible: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation: Geolocation,
    private http: HttpClient
  ) {
    if (this.navParams.get("position") != null) {
      this.position = this.navParams.get("position");
      console.log("position object found", JSON.stringify(this.position));
    } else {
      this.position = { lat: 12.91, long: 77.586, name: "Current Position" };
    }
  }

  // click on button will execute this method to reveal map
  onShowMap() {
    this.isButtonVisible = !this.isButtonVisible;
    console.log("method call");
    const options = {
      frequency: 3000,
      enableHighAccuracy: true
    };
    this.geolocation.watchPosition(options).subscribe(data => {
      console.log("cnsl pstn--", data);
      this.coordinates = data.coords;
      setTimeout(() => {
        this.executeMap();
      }, 3000);
    });
  }

  // initialize and execute map based on given control
  executeMap() {
    mapboxgl.accessToken =
      "pk.eyJ1Ijoic2FuZGlwa2FrYWRpeWEiLCJhIjoiY2pkMDgyN3pjMndkZDM0cngzNHNyOXRtNiJ9.gC8SBYiOz-bD8xBEc8bHmQ";
    var map = new mapboxgl.Map({
      style: "mapbox://styles/mapbox/streets-v9",
      center: [this.coordinates.longitude, this.coordinates.latitude],
      zoom: 14,
      pitch: 80,
      container: "map"
    });
    map.addControl(
      new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
      })
    );
    let that = this;
    map.on("load", function () {
      that.getRoute(map);
    });
  }

  // to get route between origin and destination
  getRoute(map) {
    console.log("get route method execution started");
    var start = [this.coordinates.longitude, this.coordinates.latitude];
    var end = [this.position.long, this.position.lat];
    var directionsRequest =
      "https://api.mapbox.com/directions/v5/mapbox/cycling/" +
      start[0] +
      "," +
      start[1] +
      ";" +
      end[0] +
      "," +
      end[1] +
      "?steps=true&geometries=geojson&access_token=" +
      mapboxgl.accessToken;
    this.http.get(directionsRequest).subscribe((data: any) => {
      // console.log("wanted response--", data);
      var route = data.routes[0].geometry;
      var instructions = document.getElementById("instructions");
      var steps = data.routes[0].legs[0].steps;
      steps.forEach(function (step) {
        instructions.insertAdjacentHTML(
          "beforeend",
          "<p>" + step.maneuver.instruction + "</p>"
        );
      });
      map.addLayer({
        id: "route",
        type: "line",
        source: {
          type: "geojson",
          data: {
            type: "Feature",
            geometry: route
          }
        },
        paint: {
          "line-width": 5,
          "line-color": "#77C977"
        }
      });
      map.addLayer({
        id: "start",
        type: "circle",
        source: {
          type: "geojson",
          data: {
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: start
            }
          }
        },
        paint: {
          "circle-color": "#33CCEB"
        }
      });
      map.addLayer({
        id: "end",
        type: "circle",
        source: {
          type: "geojson",
          data: {
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: end
            }
          }
        },
        paint: {
          "circle-color": "#EE1111"
        }
      });
      map.addLayer({
        id: "3d-buildings",
        source: "composite",
        "source-layer": "building",
        filter: ["==", "extrude", "true"],
        type: "fill-extrusion",
        minzoom: 2,
        paint: {
          "fill-extrusion-color": "#aaa",
          "fill-extrusion-height": {
            type: "identity",
            property: "height"
          },
          "fill-extrusion-base": {
            type: "identity",
            property: "min_height"
          },
          "fill-extrusion-opacity": 0.6
        }
      });
    });
  }
}
