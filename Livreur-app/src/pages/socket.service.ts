import { Injectable, NgZone } from "@angular/core";
import * as io from "socket.io-client";
import { apis } from "../app/constant.provider";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class SocketService {
  socket: any;
  zone: any;
  clientInfo = {
    userId: ""
  };

  constructor(private http: HttpClient) {
    this.socket = io.connect(apis.API_ENDPOINT);
    this.zone = new NgZone({ enableLongStackTrace: false });

    this.socket.on("connect", function () {
      console.log("connected");
    });
    this.socket.on("disconnect", function () {
      console.log("disconnected");
    });
    this.socket.on("error", function (e) {
      console.log("System", e ? e : "A unknown error occurred");
    });
    if (localStorage.getItem("token") != null) {
      this.establishConnection();
    }
  }

  // retriving user information
  getUser() {
    let authToken = localStorage.getItem("token");
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", authToken);
    return this.http.get(apis.API_ENDPOINT + "api/users/me", {
      headers: headers
    });
  }

  //to establish connection betwwn user and socket
  establishConnection() {
    this.getUser().subscribe(
      (Response: any) => {
        this.clientInfo.userId = Response._id;
        // console.log("user Id" + Response._id);
        this.socket.emit("storeClientInfo", this.clientInfo);
      },
      error => {
        // console.error(error);
        // localStorage.removeItem("token");
      }
    );
  }

  //to emitt current location to server
  emitPosition(positionInfo, orderIds) {
    console.log("socket emitting position--", {
      pos: positionInfo,
      orderIds: orderIds
    });
    this.socket.emit("ordertrack", {
      lat: positionInfo.lat,
      long: positionInfo.long,
      orderIds: orderIds
    });
  }
}
