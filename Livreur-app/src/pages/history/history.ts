import { Component } from "@angular/core";
import { IonicPage, NavController, LoadingController } from "ionic-angular";
import { HomeService } from "./../home/home.service";

@IonicPage()
@Component({
  selector: "page-history",
  templateUrl: "history.html",
  providers: [HomeService]
})
export class HistoryPage {
  public deliveredList: Array<{}> = [];
  public isHistoryAvailable: boolean = false;

  constructor(
    public navCtrl: NavController,
    private homeService: HomeService,
    private loadingCtrl: LoadingController
  ) {
    this.getOrderData();
  }

  // when history page loads this method will get order data
  getOrderData() {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.homeService.getOrderData().subscribe(
      (res: any) => {
        if (res != undefined && res != null) {
          this.isHistoryAvailable = true;
          res.forEach(item => {
            if (item.status == "Delivered") {
              this.deliveredList.push(item);
            }
          });
        } else {
          // console.log("history page--No assigned order found");
        }
        loader.dismiss();
      },
      error => {
        loader.dismiss();
        // console.log("order data error--", error.error.message);
      }
    );
  }
}
