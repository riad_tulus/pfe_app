import { StyledMap } from "./styled-map.directive";
import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { LocationPage } from "./location";
import { AgmCoreModule } from "@agm/core";
import { DirectionsMapDirective } from "./direction.directive";

@NgModule({
  declarations: [LocationPage, StyledMap, DirectionsMapDirective],
  imports: [
    IonicPageModule.forChild(LocationPage),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyBQr_6wlXZS7bJQuwtLYAbvE1IsDiF9Ov0",
      libraries: ["places"]
    })
  ]
})
export class LocationChangerPageModule {}
