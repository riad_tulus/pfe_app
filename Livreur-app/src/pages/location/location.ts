import { Component, NgZone } from "@angular/core";
import { IonicPage, NavParams } from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { MapsAPILoader } from "@agm/core";
import { Observable } from "rxjs/Observable";

declare var google;

@IonicPage()
@Component({
  selector: "page-location",
  templateUrl: "location.html",
  providers: [Geolocation]
})
export class LocationPage {
  origin: { longitude: number; latitude: number } = {
    longitude: 77.6412,
    latitude: 12.9719
  };
  destination: { longitude: number; latitude: number } = {
    longitude: 77.6412,
    latitude: 12.9719
  };

  constructor(
    private geolocation: Geolocation,
    public mapsAPILoader: MapsAPILoader,
    public ngZone: NgZone,
    private navParams: NavParams
  ) { }

  // get origin and destination on view did load + initalize map
  ionViewDidLoad() {
    if (localStorage.getItem("position") != null) {
      const pos = JSON.parse(localStorage.getItem("position"));
      this.origin.latitude = pos.lat;
      this.origin.longitude = pos.long;
    }
    if (this.navParams.get("position") != null) {
      const position = this.navParams.get("position");
      this.destination.latitude = position.lat;
      this.destination.longitude = position.long;
      // console.log("position object found", JSON.stringify(position));
    } else {
      this.destination.longitude = 77.586;
      this.destination.latitude = 12.91;
    }
    this.initMap(); //call to initialize map
  }

  //contains logic that will execute with mao initialization
  initMap() {
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var directionsService = new google.maps.DirectionsService();
    var map = new google.maps.Map(document.getElementById("map"), {
      zoom: 14,
      center: { lat: this.origin.latitude, lng: this.origin.longitude }
    });
    const watchOptions = {
      maxAge: 0,
      enableHighAccuracy: true
    };
    this.geolocation.watchPosition(watchOptions).subscribe(pos => {
      // console.log("watch pos-----", pos);
      this.origin.latitude = pos.coords.latitude;
      this.origin.longitude = pos.coords.longitude;
      directionsDisplay.setMap(map);
    });
    directionsDisplay.setMap(map);
    this.calculateAndDisplayRoute(directionsService, directionsDisplay);
    document.getElementById("mode").addEventListener("change", () => {
      this.calculateAndDisplayRoute(directionsService, directionsDisplay);
    });
  }

  // calculation of route between origin and destination
  calculateAndDisplayRoute(directionsService, directionsDisplay) {
    const selectedMode: any = document.getElementById("mode");
    directionsService.route(
      {
        origin: {
          lat: this.origin.latitude,
          lng: this.origin.longitude
        },
        destination: {
          lat: this.destination.latitude,
          lng: this.destination.longitude
        },
        travelMode: google.maps.TravelMode[selectedMode.value]
      },
      function (response, status) {
        if (status == "OK") {
          directionsDisplay.setDirections(response);
        } else {
          window.alert("Directions request failed due to " + status);
        }
      }
    );
  }
}
