import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { apis } from "../../app/constant.provider";

@Injectable()
export class ProfileService {
  public token: string;
  constructor(public http: HttpClient) {
    this.token = localStorage.getItem("token");
  }

  // api call to get user profile data
  getUserDetails() {
    let authToken = localStorage.getItem("token");
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", authToken);
    return this.http.get(apis.API_ENDPOINT + "api/users/me", {
      headers: headers
    });
  }

  // api call to update user profie
  UpdateUserProfile(address: any, userId: any) {
    const body = address;
    let authToken = localStorage.getItem("token");
    // console.log("authToen" + authToken);

    const headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", authToken);

    return this.http.put(apis.API_ENDPOINT + "api/users/" + userId, body, {
      headers: headers
    });
  }
}
