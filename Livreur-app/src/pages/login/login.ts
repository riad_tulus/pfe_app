import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  LoadingController,
  ToastController,
  Events
} from "ionic-angular";
import { LoginService } from "./login.service";
import { ProfileService } from "./../profile/profile.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html",
  providers: [LoginService, ProfileService]
})
export class LoginPage {
  public login: FormGroup;

  constructor(
    public navCtrl: NavController,
    private loginService: LoginService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private profileService: ProfileService,
    private events: Events
  ) { }

  //form initialization with default value
  ngOnInit() {
    this.login = new FormGroup({
      email: new FormControl("foodworldstaff@gmail.com", Validators.required),
      password: new FormControl("123456", Validators.required)
    });
  }

  //submit login credential inputed by user to service  
  onLogin() {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    let user = {
      email: this.login.value.email,
      password: this.login.value.password
    };
    this.loginService.onLogin(user).subscribe(
      (res: any) => {
        localStorage.setItem("token", "bearer " + res.token);
        this.showToaster("Connexion réussie");
        this.profileService.getUserDetails().subscribe(
          (res: any) => {
            // console.log("User Detail" + JSON.stringify(res));
            this.events.publish("userInfo", res);
            this.navCtrl.setRoot("HomePage");
            loader.dismiss();
          },
          error => {
            loader.dismiss();
          }
        );
      },
      error => {
        this.showToaster(error.error.message);
        loader.dismiss();
      }
    );
  }


  // to show toast on screen
  showToaster(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  //route to home page
  goToHome() {
    this.navCtrl.setRoot("HomePage");
  }
}
