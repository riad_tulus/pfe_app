import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { apis } from "../../app/constant.provider";
import "rxjs/add/operator/catch";

@Injectable()
export class LoginService {
  constructor(public http: HttpClient) { }

  // api call to login user
  onLogin(body) {
    const headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    return this.http.post(apis.API_ENDPOINT + "auth/local", body, {
      headers: headers
    });
  }
}
