import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { apis } from "../../app/constant.provider";
import "rxjs/add/operator/catch";

@Injectable()
export class HomeService {
  constructor(public http: HttpClient) { }

  // api call to get all assigned order details
  getOrderData() {
    let authToken = localStorage.getItem("token");
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", authToken);

    return this.http.get(
      apis.API_ENDPOINT + "api/ordertracks/delivery/assigned/",
      {
        headers: headers
      }
    );
  }

  // api call to change order status 
  changeOrderStatus(body, id) {
    let authToken = localStorage.getItem("token");
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", authToken);
    return this.http.put(apis.API_ENDPOINT + "api/orders/" + id, body, {
      headers: headers
    });
  }
}
