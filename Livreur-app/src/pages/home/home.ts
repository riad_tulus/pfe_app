import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  LoadingController,
  Events
} from "ionic-angular";
import { HomeService } from "./home.service";
import { SocketService } from "../socket.service";

@IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html",
  providers: [HomeService, SocketService]
})
export class HomePage {
  public orders: string = "pending"; //required for html segment
  public pendingList: Array<{}> = [];
  public onGoingList: Array<{}> = [];
  public deliveredList: Array<{}> = [];
  public cancelledList: Array<{}> = []; //for later use
  public OnTheWayOrdersIdList: Array<string> = [];
  public btnsVisible = true;
  // public searchBoxVisible = false;
  constructor(
    public navCtrl: NavController,
    private homeService: HomeService,
    private loadingCtrl: LoadingController,
    private socketService: SocketService,
    private event: Events
  ) {
    this.getOrderData();
  }
  // searchVisible() {
  //   this.searchBoxVisible = true;
  //   this.btnsVisible = false;
  // }
  // btnVisible() {
  //   this.searchBoxVisible = false;
  //   this.btnsVisible = true;
  // }

  // to get all assigned order
  getOrderData() {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.homeService.getOrderData().subscribe(
      (res: any) => {
        this.cancelledList = [];
        this.deliveredList = [];
        this.onGoingList = [];
        this.OnTheWayOrdersIdList = [];
        this.pendingList = [];
        if (res != undefined && res != null) {
          res.forEach(item => {
            if (item.status == "Pending") {
              this.pendingList.push(item);
            } else if (item.status == "On the Way") {
              this.onGoingList.push(item);
              this.OnTheWayOrdersIdList.push(item._id);
            } else if (item.status == "Delivered") {
              this.deliveredList.push(item);
            } else if (
              item.status == "Cancelled" ||
              item.status == "Canceled"
            ) {
              this.cancelledList.push(item);
            }
          });
          this.event.subscribe("position", pos => {
            this.socketService.emitPosition(pos, this.OnTheWayOrdersIdList);
          });
          // console.log("order data response----", JSON.stringify(res));
          // console.log("order data length", res.length);
        } else {
          // console.log("No assigned order found");
        }
        loader.dismiss();
      },
      error => {
        loader.dismiss();
        // console.log("order data error--", error.error.message);
      }
    );
  }

  /* to accept assigned order for delivery. 
  It will change order status to 'On the Way'
  */
  onClickAccept(orderId) {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    const body: { status: string } = { status: "On the Way" };
    this.homeService.changeOrderStatus(body, orderId).subscribe(
      res => {
        // console.log("status chnage response--", res);
        this.getOrderData();
        loader.dismiss();
      },
      error => {
        // console.error("status change error--", error);
        loader.dismiss();
      }
    );
  }

  //to chenge orders status to 'Delivered'
  onClickDelivered(orderId) {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    const body: { status: string } = { status: "Delivered" };
    this.homeService.changeOrderStatus(body, orderId).subscribe(
      res => {
        // console.log("status chnage response--", res);
        this.getOrderData();
        loader.dismiss();
      },
      error => {
        // console.error("status change error--", error);
        loader.dismiss();
      }
    );
  }

  // route to location page
  goToMap(position) {
    this.navCtrl.push("LocationPage", { position: position });
  }
}
