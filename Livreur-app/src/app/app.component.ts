import { Component, ViewChild } from "@angular/core";
import { Nav, Platform, Events } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Geolocation } from "@ionic-native/geolocation";
import { SocketService } from "./../pages/socket.service";
import { ProfileService } from "./../pages/profile/profile.service";

@Component({
  templateUrl: "app.html",
  providers: [Geolocation, ProfileService, SocketService]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public rootPage: string;
  public username: string = "Guest";
  public imageUrl: string = "assets/img/profile.jpg";

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private event: Events,
    private geoLocation: Geolocation,
    private profileService: ProfileService,
    private socketService: SocketService
  ) {
    this.getProfileInfo();
    this.getCurrentPosition();
    this.initializeApp();
  }

  /* to establish socket connection and get userinfo published from here
    ,login page and profile page
  */
  private getProfileInfo() {
    if (localStorage.getItem("token") != null) {
      this.socketService.establishConnection();
      this.profileService.getUserDetails().subscribe((res: any) => {
        this.event.publish("userInfo", res);
      });
      this.event.subscribe("userInfo", res => {
        // console.log("REsponse of userInfo" + JSON.stringify(res));
        this.username = res.name;
        if (res.logo != null) {
          this.imageUrl = res.logo;
        }
      });
    }
  }

  /* 1) to initilize app required for ionic to work set
     2) set root page */
  initializeApp() {
    if (this.isAuthenticated()) {
      this.rootPage = "HomePage";
    } else {
      this.rootPage = "LoginPage";
    }
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }


  // methd will watch current position of delivery man and publish it 
  private getCurrentPosition() {
    const options = {
      frequency: 3000,
      enableHighAccuracy: true
    };
    this.geoLocation.watchPosition(options).subscribe(
      position => {
        var pos = {
          lat: position.coords.latitude,
          long: position.coords.longitude
        };
        console.log("position from app componenet---", pos);
        localStorage.setItem("position", JSON.stringify(pos));
        this.event.publish("position", pos);
      },
      error => {
        console.log("position error--", error);
      }
    );
  }

  // to check user is logged in or not
  isAuthenticated() {
    return localStorage.getItem("token") != null;
  }

  // to logout user by removing token from localstorage
  logout() {
    localStorage.removeItem("token");
    this.openPage("LoginPage", true);
  }

  /* to route to other page
  if setRoot value is true page will set as root page
  */
  openPage(page: string, setRoot: boolean) {
    if (setRoot) {
      this.nav.setRoot(page);
    } else {
      this.nav.push(page);
    }
  }
}
