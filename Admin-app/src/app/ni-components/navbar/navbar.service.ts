'use strict';
import { Injectable,OnInit } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";
import { ConstantService } from  '../../services/constant.service';
import { CrudService } from '../../services/crud.service';

@Injectable()
export class NavBarService {
    authToken:any;
    url: any = 'https://onesignal.com/api/v1/notifications';
    constructor(public crudService:CrudService, private http: Http, public constantService: ConstantService) {
       this.authToken=localStorage.getItem('token');
    }



   readAllNotifications(){
    return this.crudService.get('notifications/all/read').map((data:Response)=>data);
   }

    getUnreadNotificationData(){
     return this.crudService.get('notifications/unread/all').map((data:Response)=>data);
    }

   getUnreadMessageNotificationData(){
      return this.crudService.get('messages/unread/list').map((data:Response)=>data);
    }

    userData() {
      return this.crudService.getMyData('users/me').map((data:Response)=>data);
    }

    UpdateNotificationData(data, notifyId: any) {
       const body = JSON.stringify(data);
    return this.crudService.put('notifications',body,notifyId).map((data:Response)=>data);
   }

   readSingleMessageThread(chatUserId){
           const headers = new Headers();
           headers.append('Content-Type', 'application/json');
           let authToken = localStorage.getItem('token');
           headers.append('Authorization', authToken);
           return this.http.get(this.constantService.API_ENDPOINT+'messages/single/thread/read/'+chatUserId, {
          headers: headers
      })
      .map((data: Response) => data.json())
       .catch(this.handleError);
   }

   readSingleAllMessageThread(){
      return this.crudService.get('messages/mark/read').map((data:Response)=>data);
    }

   getUnreadMessageCount(){
      return this.crudService.get('messages/total/unread/count').map((data:Response)=>data);
    }


 private handleError (error: any) {
   return Observable.throw(error);
}

}
