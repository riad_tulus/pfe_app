import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as CountAction from '../../pages/restaurant/chat/action/count.action';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { UnreadCount } from '../../pages/restaurant/chat/action/count';

import { NavBarService } from './navbar.service';
import { SocketSharedService }  from '../../services/socketShare.service';

interface CountState {
   count:UnreadCount;
}
interface AppState {
   chatId:string;
}

@Component({
  moduleId: module.id,
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss'],
  providers: [ NavBarService ],
  host: {
    '[class.app-navbar]': 'true',
    '[class.show-overlay]': 'showOverlay'
  }
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  @Input() openedSidebar: boolean;
  @Output() sidebarState = new EventEmitter();

  countUpdate : Observable<UnreadCount>;
  showOverlay: boolean;
  public imgUrl:any;
  public unreadChats:any [] = [];
  orderNotify:any=[];
  unread:any = 0;
  private chatId$;
  private activeChatId:any;
  public unreadCount:number;
  public userRole:string;
  public userData:any = {
    name:'',
    logo:'https://res.cloudinary.com/pietechsolutions/image/upload/v1514786142/xmqwpbwli1fgjb02fepz.jpg'
  };

  constructor(private restService:NavBarService,
              private router:Router,
              private toastr:ToastrService,
              private sockectService:SocketSharedService,
              private counter:Store<CountState>,
              private store:Store<AppState>) {
    this.openedSidebar = false;
    this.showOverlay = false;

    this.userRole = localStorage.getItem('role');
    this.countUpdate = this.counter.select('count');

    this.getUnreadMessageList();
    this.getTotalUnreadCount();
    this.getUserData();
  }

  getUserData(){
    // console.log("User Data in NavBar");
    this.restService.userData().subscribe((res:any)=>{
      this.userData.name = res.name;
      // console.log("User Data in NavBar "+JSON.stringify(res));
    }, (error) => {
      // console.log(error.message);
      if (error.message === 'Unexpected token < in JSON at position 0') {
        localStorage.clear();
        this.router.navigate(['/pages/sign-in']);
      }
    });
  }


  getTotalUnreadCount(){
   this.restService.getUnreadMessageCount().subscribe((res:any)=>{
     // console.log('getUnreadMessageCount '+res);
     this.counter.dispatch(new CountAction.UpdateCount(res));
     this.unreadCount = res;
     // console.log("unreadCount "+this.unreadCount);
   })
  }

  getUnreadMessageList(){
    this.restService.getUnreadMessageNotificationData().subscribe((res:any)=>{
      this.unreadChats = res;
      // console.log("UnreadMessageNotificationData "+JSON.stringify(res));
    })
  }

  readSingleThread(chatUserId,userType,index){
    //(userType == 'user')?userType='User':userType='User';
    // console.log("chat user ID "+chatUserId+' userType '+userType);

    // console.log("index "+index);
    let threadData:any  = this.unreadChats[index];
    // console.log("select thread "+JSON.stringify(threadData));
       localStorage.setItem('chatType','listOne');
       this.restService.readSingleMessageThread(chatUserId).subscribe((res:any)=>{
       this.counter.dispatch(new CountAction.DownGradeCount(threadData.count));
       this.counter.dispatch(new CountAction.AssignId(chatUserId));
       this.unreadChats.splice(index,1);
       //this.getTotalUnreadCount();
       this.router.navigate(['/pietech/chat']);
      })
    // console.log("called");
  }

  open(event) {
    let clickedComponent = event.target.closest('.nav-item');
    let items = clickedComponent.parentElement.children;

    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove('opened');
    }
    clickedComponent.classList.add('opened');

    //Add class 'show-overlay'
    this.showOverlay = true;
  }

  close(event) {
    let clickedComponent = event.target;
    let items = clickedComponent.parentElement.children;

    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove('opened');
    }

    //Remove class 'show-overlay'
    this.showOverlay = false;
  }

  openSidebar() {
    this.openedSidebar = !this.openedSidebar;
    this.sidebarState.emit();
  }

  logOut(){
    localStorage.clear();
    this.toastr.success('Logout successful','Success',{timeOut:2000});
    this.router.navigate(['/pages/sign-in']);
  }

  ngOnInit() {
     this.sockectService.getUserNotification().subscribe((msgNotification:any)=>{
      // console.log(" socket msg "+ JSON.stringify(msgNotification));
       this.chatId$ = this.store.select('chatId');

       this.chatId$.subscribe((res:any)=>{
               if((this.chatId$.actionsObserver._value.payload != undefined)){
                 this.activeChatId = this.chatId$.actionsObserver._value.payload;
                 // console.log("active Chat Id "+this.activeChatId);

                 if(this.activeChatId != msgNotification._id){
                   var data:any = msgNotification;
                   this.playAudio();
                    //this.counter.dispatch(new CountAction.UpdateCount(data.count));
                    this.counter.dispatch(new CountAction.SetCount(msgNotification.totalcount));

                    if(this.unreadChats.length == 0){
                      this.unreadChats.unshift(data);
                    }else{
                      let index = this.unreadChats.findIndex(x=>x._id==data._id);
                      if(index >=0){
                          this.unreadChats.splice(index,1)
                          this.unreadChats.unshift(data)
                        }
                        else{
                        this.unreadChats.unshift(data);
                        }
                     }
                    }// inner If closed
                  else{}
               }// Outer If closed
             })

          // console.log("message Notification "+JSON.stringify(msgNotification));

        })
  }

  playAudio(){
      // console.log("play audio");
      let audio = new Audio();
      audio.src = "assets/sound/sound.mp3";
      audio.load();
      audio.play();
  }
}
