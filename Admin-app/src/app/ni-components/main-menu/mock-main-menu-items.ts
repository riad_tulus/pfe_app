import { MainMenuItem } from './main-menu-item';

export const MAINMENUITEMS: MainMenuItem[] = [
   {
    title: 'Accueil',
    icon: 'fa fa-home',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/dashboard',
    externalLink: '',
    budge: '',
    budgeColor: '#f44236'
  },
  {
    title: 'Restaurant',
    icon: 'fa fa-cutlery',
    active: false,
    groupTitle : false,
    sub: '',
    routing: '/pietech/restaurants',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  {
    title: 'Chat',
    icon: 'fa fa-comments-o',
    active: false,
    groupTitle: false,
    sub: '',
    routing: '/pietech/chat',
    externalLink: '',
    budge: '',
    budgeColor: ''
  },
  // {
  //   title: 'Cuisines',
  //   icon: 'fa fa-heart-o',
  //   active: false,
  //   groupTitle: false,
  //   sub: '',
  //   routing: '/pietech/cuisines',
  //   externalLink: '',
  //   budge: '',
  //   budgeColor: ''
  // },
  //  {
  //   title: 'Withdrawal',
  //   icon: 'fa fa-credit-card-alt',
  //   active: false,
  //   groupTitle : false,
  //   sub: '',
  //   routing: '/pietech/payment/withdrawal',
  //   externalLink: '',
  //   budge: '',
  //   budgeColor: ''
  // },
];