import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, Http} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SelectModule} from 'ng2-select';
import {CookieModule} from 'ngx-cookie';
import {MaterialModule} from '@angular/material';
import {ChartsModule} from 'ng2-charts';
import {CalendarModule} from 'angular-calendar';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {AgmCoreModule} from 'angular2-google-maps/core';
import {ToastrModule} from 'ngx-toastr';
import {FileUploadModule} from 'ng2-file-upload';
import {Ng2CloudinaryModule} from 'ng2-cloudinary';
import {NgxPaginationModule} from 'ngx-pagination';
import {AppRoutingModule} from './app-routing.module';

import {InfiniteScrollModule} from 'ngx-infinite-scroll';

import {AppComponent} from './app.component';
import {DefaultLayoutComponent} from './layouts/default/default.component';
import {ExtraLayoutComponent} from './layouts/extra/extra.component';

//A2 Components
import {NavbarComponent} from './ni-components/navbar/navbar.component';
import {SidebarComponent} from './ni-components/sidebar/sidebar.component';
import {LogoComponent} from './ni-components/logo/logo.component';
import {MainMenuComponent} from './ni-components/main-menu/main-menu.component';
import {A2CardComponent} from './ni-components/card/card.component';
import {AlertComponent} from './ni-components/alert/alert.component';
import {BadgeComponent} from './ni-components/badge/badge.component';
import {BreadcrumbComponent} from './ni-components/breadcrumb/breadcrumb.component';
import {FileComponent} from './ni-components/file/file.component';
import {FooterComponent} from './ni-components/footer/footer.component';
//A2 Pages

import {PageDashboard2Component} from './pages/dashboard-2/dashboard-2.component';

//Extra pages
import {PageSignIn1Component} from './pages/extra-pages/sign-in-1/sign-in-1.component';
import {PageSignUp1Component} from './pages/extra-pages/sign-up-1/sign-up-1.component';
import {PageForgotComponent} from './pages/extra-pages/forgot/forgot.component';
import {PageConfirmComponent} from './pages/extra-pages/confirm/confirm.component';
import {Page404Component} from './pages/extra-pages/page-404/page-404.component';
import {Page500Component} from './pages/extra-pages/page-500/page-500.component';
import {PageResetComponent} from './pages/extra-pages/reset-password/reset-password.component';
import {UserProfileSettings} from './pages/user-profile/settings/settings.component';
//common services

import {ConstantService} from './services/constant.service';
import {CrudService} from './services/crud.service';
import {CrudBaseService} from './services/base.service';
import {SocketSharedService} from './services/socketShare.service';

import {RestaurantComponent} from './pages/restaurant/restaurant.component';
import {AddRestaurantComponent} from './pages/restaurant/add-restaurant/add-restaurant.component';
import {CategoriesComponent} from './pages/restaurant/categories/categories.component';
import {OrderComponent} from './pages/restaurant/order/order.component';
import {ProductsComponent} from './pages/restaurant/products/products.component';
import {UsersComponent} from './pages/restaurant/users/users.component';
import {ViewRestaurantComponent} from './pages/restaurant/view-restaurant/view-restaurant.component';
import {LocationComponent} from './pages/restaurant/location/location.component';
import {PaymentComponent} from './pages/restaurant/payment/payment.component';
import {WalletComponent} from './pages/restaurant/payment/wallet/wallet.component';
import {BankComponent} from './pages/restaurant/payment/bank/bank.component';
import {WithdrawalComponent} from './pages/restaurant/payment/withdrawal/withdrawal.component';
import {ProfileComponent} from './pages/user-profile/profile/profile.component';
import {UpdateProfileComponent} from './pages/user-profile/profile/update-profile/update-profile.component';

import {ChatComponent} from './pages/restaurant/chat/chat.component';
import {ChatBoxComponent} from './pages/restaurant/chat/chat-box/chat-box.component';

// ngrx solution
import {StoreModule} from '@ngrx/store';
import {countReducer} from './pages/restaurant/chat/action/count.reducer';

// Moment
import {MomentModule} from 'angular2-moment';
import { CuisinesComponent } from './pages/restaurant/cuisines/cuisines.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule.forRoot(),
    BrowserAnimationsModule,
    AppRoutingModule,
    Ng2CloudinaryModule,
    FileUploadModule,
    HttpModule,
    SelectModule,
    ChartsModule,
    CookieModule.forRoot(),
    ToastrModule.forRoot(),
    CalendarModule.forRoot(),
    NgxDatatableModule,
    NgxPaginationModule,
    MomentModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAU9f7luK3J31nurL-Io3taRKF7w9BItQE',
    }),
    StoreModule.forRoot({count: countReducer}),
    InfiniteScrollModule
  ],
  declarations: [
    AppComponent,
    DefaultLayoutComponent,
    ExtraLayoutComponent,

    NavbarComponent,
    SidebarComponent,
    LogoComponent,
    MainMenuComponent,
    A2CardComponent,
    AlertComponent,
    BadgeComponent,
    BreadcrumbComponent,
    FileComponent,
    FooterComponent,

    PageDashboard2Component,

    PageSignIn1Component,
    PageSignUp1Component,
    PageForgotComponent,
    PageConfirmComponent,
    Page404Component,
    Page500Component,
    PageResetComponent,

    RestaurantComponent,
    UserProfileSettings,
    AddRestaurantComponent,
    CategoriesComponent,
    OrderComponent,
    ProductsComponent,
    UsersComponent,
    ViewRestaurantComponent,
    LocationComponent,
    PaymentComponent,
    WalletComponent,
    BankComponent,
    WithdrawalComponent,
    ProfileComponent,
    UpdateProfileComponent,
    ChatComponent,
    ChatBoxComponent,
    CuisinesComponent
  ],

  entryComponents: [],
  providers: [ConstantService, CrudService, CrudBaseService, SocketSharedService],
  bootstrap: [AppComponent]
})

export class AppModule {

}
