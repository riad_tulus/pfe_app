import { Injectable } from '@angular/core';
import { CrudService } from '../../../services/crud.service';
@Injectable()
export class SettingsService {
    constructor(private crud:CrudService) { }

    getLoggedInUserDetails(){
        return this.crud.get('users/me');
    }

    updatePassword(data,id){
        return this.crud.put('users/password/update',data,id)
    }

    updateEmail(data,id){
        return this.crud.put('users/email/update',data,id);
    }
}