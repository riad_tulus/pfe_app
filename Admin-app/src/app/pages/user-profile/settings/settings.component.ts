import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {SettingsService} from './settings.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'user-profile-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  providers: [SettingsService]
})

export class UserProfileSettings implements OnInit {
  @ViewChild('changePasswordForm') changePasswordForm: FormGroup;
  @ViewChild('changeEmailForm') changeEmailForm: FormGroup;
  userDetails: any = {};
  hasUserData: boolean = false;
  loading: boolean = true;
  isChangePassword: boolean = false;
  isChangeEmail: boolean = false;
  newPassword: string;
  confirmPassword: string;
  doesPasswordMatch: boolean;
  passwordBoxChecked: boolean;
  emailBoxChecked: boolean;

  constructor(private restService: SettingsService, private toastr: ToastrService, private router: Router) {
    this.getCurrentUser();
  }

  ngOnInit() {
    this.createChangePasswordForm();
    this.createChangeEmailForm();
  }

  createChangePasswordForm() {
    this.changePasswordForm = new FormGroup({
      'oldPassword': new FormControl(null, Validators.required),
      'newPassword': new FormControl(null, [Validators.required, Validators.minLength(6)]),
      'confirmPassword': new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
  }

  createChangeEmailForm() {
    this.changeEmailForm = new FormGroup({
      'oldEmail': new FormControl(null, [Validators.required, Validators.email]),
      'newEmail': new FormControl(null, [Validators.required, Validators.email])
    });
  }

  getCurrentUser() {
    this.restService.getLoggedInUserDetails().subscribe(res => {
      if (res !== null) {
        this.hasUserData = true;
        this.userDetails = res;
        // console.log(this.userDetails);
        this.loading = false;
      }
    }, (error) => {
      //// console.log(error.message);
      if (error.message === 'Unexpected token < in JSON at position 0') {
        localStorage.clear();
        this.router.navigate(['/pages/sign-in']);
      }
    });
  }

  changePasswordEvent(event) {
    this.isChangePassword = !this.isChangePassword;
  }

  changeEmailEvent($event) {
    this.isChangeEmail = !this.isChangeEmail;
  }

  changePassword() {
    this.newPassword = this.changePasswordForm.value.newPassword;
    this.confirmPassword = this.changePasswordForm.value.confirmPassword;
    if (this.confirmPassword !== this.newPassword) {
      this.doesPasswordMatch = false;
      // console.log('Passwords do not match');
    } else {
      this.doesPasswordMatch = true;
      // console.log('Matched...');
      var body = {
        oldPassword: this.changePasswordForm.value.oldPassword,
        newPassword: this.changePasswordForm.value.newPassword
      }
      // console.log(body);
      let id = this.userDetails._id;
      this.restService.updatePassword(body, id).subscribe(res => {
        // console.log(res);
        this.toastr.success('Success', 'Password Changed Successfully', {timeOut: 2000});
        this.changePasswordForm.reset();
        this.passwordBoxChecked = false;
        this.isChangePassword = false;
      }, error => {
        this.toastr.error('Error', error.message, {timeOut: 2000});
      });
    }
  }

  changeEmail() {
    // console.log(this.changeEmailForm.value);
    let id = this.userDetails._id;
    this.restService.updateEmail(this.changeEmailForm.value, id).subscribe(res => {
      this.toastr.success('Success', 'Email updated successfully', {timeOut: 2000});
      this.changeEmailForm.reset();
      this.emailBoxChecked = false;
      this.isChangeEmail = false;
    }, error => {
      this.toastr.error('Error', error.message, {timeOut: 2000});
    });
  }

}
