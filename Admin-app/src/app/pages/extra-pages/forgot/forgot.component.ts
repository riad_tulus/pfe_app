import { Component, OnInit } from '@angular/core';
import { ForgetPasswordService } from './forget-password.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import {CrudService} from '../../../services/crud.service';

@Component({
  selector: 'page-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss'],
  providers: [ForgetPasswordService]
})
export class PageForgotComponent implements OnInit {
  user = {
  	email:''
  }
  constructor( private router:Router,private crud:CrudService, private restService:ForgetPasswordService,private toastr:ToastrService ) { }

  onSubmit(){
  	this.restService.sendEmail(this.user).subscribe((res:any)=>{
  		localStorage.setItem('token','Bearer ' +res.token)
        this.crud.initApplication(localStorage.getItem('token'));
  		this.toastr.success( 'OTP sent ', 'Success');
        this.router.navigate(['/pages/confirm']);
  	},(error)=>{
  	    this.toastr.success( error.message , 'Error');
  	})
  }

  ngOnInit() { }
}
