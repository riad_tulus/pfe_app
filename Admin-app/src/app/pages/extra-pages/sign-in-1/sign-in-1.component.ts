import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SignIn1Service } from './sign-in-1.service';
import { CrudService } from  '../../../services/crud.service';
import { SocketSharedService } from '../../../services/socketShare.service';

@Component({
  selector: 'page-sign-in-1',
  templateUrl: './sign-in-1.component.html',
  styleUrls: ['./sign-in-1.component.scss'],
  providers: [SignIn1Service]
})
export class PageSignIn1Component implements OnInit {
  @ViewChild('loginForm') loginForm: FormGroup;
  rememberUser = false;
  constructor(private socketService: SocketSharedService, private crud: CrudService,private router: Router, private restService:SignIn1Service, private fb:FormBuilder, private cookieService:CookieService, private toastr:ToastrService) {
    // getting token from local storage if exist
    const valid = (localStorage.getItem('token') !== null);
    if (valid) {
      /* accessing getUsersdata member function of loginService,
      * if token is valid so access userInfo the member function of _socketService,
      * userInfo(res._id), emit the unique id of User to server
      * and establishing a secure connection between User and server
      */
      this.restService.getUser().subscribe((res: any) => {
        this.router.navigate(['/pietech/dashboard']);
      }, (error) => {
        localStorage.clear();
      });
    }
  }

  ngOnInit() {
    this.createLoginForm();
    this.getCookie();
  }

  createLoginForm() { // login form
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  getCookie() { // getting data from Cookies;
    const data:any = this.cookieService.getObject('user');
    if(data != null && data !== undefined){
      this.loginForm.get('email').setValue(data.email);
      this.loginForm.get('password').setValue(data.password);
    }
  }

  rememberMe(val) {
    /** save user credentials inside browser cookies if user check mark on Keep me signed in */
    if (val.checked === true){
      this.rememberUser = true;
      this.cookieService.putObject('user',this.loginForm.value);
    } else {
      this.rememberUser = false;
    }
  }

  onSubmit() { // invoke when user click on log in
    this.restService.loginWithEmailAndPassword(this.loginForm.value).subscribe(res => {
      localStorage.setItem('token','bearer ' + res.token);
      this.crud.initApplication(localStorage.getItem('token'));
      this.toastr.success('Success', 'Connexion réussie', {timeOut: 2000});
      this.restService.getUser().subscribe(res => {
        this.socketService.userInfo(res._id);
        localStorage.setItem('id',res._id);
        localStorage.setItem('role',res.role);
        this.router.navigate(['/pietech/dashboard']);
      }, (error) => {
      });
    }, (error) => {
      this.toastr.error(error.message, 'Error',{timeOut: 2000});
    });
  }
}
