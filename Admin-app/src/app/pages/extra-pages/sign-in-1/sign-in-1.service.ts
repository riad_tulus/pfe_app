import {Injectable} from '@angular/core';
import {ConstantService} from '../../../services/constant.service';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class SignIn1Service {
  constructor(private constantService: ConstantService, private http: Http, private crud: CrudService) {
  }

  loginWithEmailAndPassword(data: string) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.constantService.Login_Auth + 'auth/local', data, {
      headers: headers
    }).map(this.handleResponse).catch(this.handleError);
  }

  getUser() { // getting all save information of user from server
    const token = localStorage.getItem('token');
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.get(this.constantService.API_ENDPOINT + 'users/me', {
      headers: headers
    }).map(this.handleResponse).catch(this.handleError);
  }

  private handleResponse(res: Response) {
    return res.json();
  }

  private handleError(error: any) {
    return Observable.throw(error.json());
  }
}
