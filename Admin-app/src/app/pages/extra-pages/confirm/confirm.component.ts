import { Component, OnInit } from '@angular/core';
import { ForgetPasswordService } from '../forgot/forget-password.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import {CrudService} from '../../../services/crud.service';

@Component({
  selector: 'page-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
  providers: [ForgetPasswordService]
})
export class PageConfirmComponent implements OnInit {
  user = {
  	otp:0
  }
  constructor( private router:Router,private crud:CrudService, private restService:ForgetPasswordService,private toastr:ToastrService ) { }

  onSubmit(){
  	this.restService.sendOTP(this.user).subscribe((res:any)=>{
  		localStorage.setItem('token','Bearer ' +res.token)
        this.crud.initApplication(localStorage.getItem('token'));
  		this.toastr.success( 'OTP Verified ', 'Success');
        this.router.navigate(['/pages/reset-password']);
  	},(error)=>{
  	    this.toastr.success( error.message , 'Error');
  	})
  }

  ngOnInit() { }
}
