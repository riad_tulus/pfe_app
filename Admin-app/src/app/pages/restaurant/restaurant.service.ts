import {Injectable} from '@angular/core';
import {CrudService} from '../../services/crud.service';

@Injectable()
export class RestaurantService {
  constructor(private crud: CrudService) {
  }

  getRestaurants() { // get All Restaurant data
    return this.crud.get('users/owner/list');
  }

  updateRestaurant(id, data) { // update Restaurant Active status by restaurant id
    return this.crud.put('users', data, id);
  }
}
