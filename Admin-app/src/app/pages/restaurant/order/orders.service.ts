import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class OrdersService {
  constructor(private crud: CrudService) {
  }

  getOrders(id: string) {
    return this.crud.getOne('orders/restaurant', id);
  }

  getRestaurantDetails(id: string) {
    return this.crud.getOne('users', id);
  }
}
