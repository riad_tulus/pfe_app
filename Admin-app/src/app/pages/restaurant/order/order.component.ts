import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SharedService} from '../../../layouts/shared-service';
import {OrdersService} from './orders.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  providers: [OrdersService]
})
export class OrderComponent implements OnInit {
  p: number = 1;
  loading: boolean = true;
  restaurantId: string;
  breadcrumb: any = [
    {
      title: 'Restaurants',
      link: '/pietech/restaurants',
      icon: ''
    },
    {
      title: 'View Restaurants',
      link: '/pietech/restaurant/view/' + this.restaurantId,
      icon: ''
    },
    {
      title: 'Orders',
      link: '',
      icon: ''
    },
  ];
  ordersData: Array<any>;
  ordersLength: number;
  pageTitle: string = 'Orders';
  userDetails: any = {};

  constructor(private toaster: ToastrService, private _sharedService: SharedService, private route: ActivatedRoute, private restService: OrdersService) {
    this._sharedService.emitChange(this.pageTitle);
    let uid = localStorage.getItem('id');
    if (uid !== null) {
      this.route.params.map(p => p['id']).subscribe(Id => {
        if (Id !== null) {
          this.restaurantId = Id;
          this.getOrders(this.restaurantId); // get Orders data by restaurant Id
          this.getRestaurantDetils(this.restaurantId); // get Restaurant data by restaurant Id
        }
      });
    }
  }

  getOrders(id: string) { // fetching Orders data by restaurant Id
    this.restService.getOrders(id).subscribe(res => {
      this.ordersData = res;
      this.ordersLength = this.ordersData.length;
      this.loading = false;
    }, (error) => {
      this.toaster.error(error.message, 'error');
    });
  }

  getRestaurantDetils(id: string) { // fetching Restaurant Data by restaurant Id
    this.restService.getRestaurantDetails(id).subscribe(res => {
      this.userDetails = res;
    }, (error) => {
      this.toaster.error(error.message, 'error');
    });
  }

  ngOnInit() {
  }

}
