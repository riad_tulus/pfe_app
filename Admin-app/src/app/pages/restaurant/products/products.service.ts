import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class ProductsService {
  constructor(private crud: CrudService) {
  }

  getProducts(id: string) { // get Products data by restaurant id
    return this.crud.getOne('products/restaurant', id);
  }

  getRestaurantDetails(id: string) { // get Restaurant Details by restaurant id
    return this.crud.getOne('users', id);
  }
}
