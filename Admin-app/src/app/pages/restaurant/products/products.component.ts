import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SharedService} from '../../../layouts/shared-service';
import {ProductsService} from './products.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  providers: [ProductsService]
})
export class ProductsComponent implements OnInit {
  loading: boolean = true;
  p = 1;
  pageTitle = 'Produits';
  restuarantID: string;
  restaurantDetails: any = {};
  productData: Array<any>;
  productDataLength: number;
  breadcrumb: any = [
    {
      title: 'Restaurants',
      link: '/pietech/restaurants',
      icon: ''
    },
    {
      title: 'View Restaurants',
      link: '/pietech/restaurant/view',
      icon: ''
    },
    {
      title: 'Products',
      link: '',
      icon: ''
    },
  ];

  constructor(private toaster: ToastrService, private _sharedService: SharedService, private route: ActivatedRoute, private restService: ProductsService) {
    this._sharedService.emitChange(this.pageTitle);
    const id = localStorage.getItem('id');
    if (id != null) {
      this.route.params.map(params => params['id']).subscribe(Id => {
        if (Id != null) {
          this.restuarantID = Id;
          this.getRestaurantDetails(this.restuarantID); // get Restaurant Details by restaurant Id
          this.getProduct(this.restuarantID); // get Product data by restaurant Id
        }
      });
    }
  }

  ngOnInit() {}

  getRestaurantDetails(id: string) { // fetching Orders data by restaurant Id
    return this.restService.getRestaurantDetails(id).subscribe(res => {
      this.restaurantDetails = res;
    });
  }

  getProduct(id: string) { // fetching Product data by restaurant Id
    this.restService.getProducts(id).subscribe(res => {
      this.productData = res.productdata;
      this.productDataLength = this.productData.length;
      this.loading = false;
    }, (error: any) => {
      this.toaster.error('Quelque chose ne va pas : ' + error.message , 'Error');
    });
  }
}
