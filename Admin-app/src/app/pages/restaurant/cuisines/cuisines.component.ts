import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { json } from 'ng2-validation/dist/json';
import { CuisinesService } from './cuisines.service';

@Component({
  selector: 'app-cuisines',
  templateUrl: './cuisines.component.html',
  styleUrls: ['./cuisines.component.css'],
  providers: [CuisinesService]
})
export class CuisinesComponent implements OnInit {

  public cusineDetails = true;
  public loading = true;
  public dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg = false;
  public errorImage = './assets/content/error.png';
  public noContent: any = './assets/content/nodata_cloud.png';


  public addDialog = false;
  public updateDialog = false;
  public cuisineData: Array<any>;

  public addCuisine: any = {
    cuisineName: ''
  };
  public updateCuisine: any = {};
  public cuisineId: string;


  constructor(private toster: ToastrService, private restService: CuisinesService) {
    this.getCuisineData();

  }

  getCuisineData() { // fetching all saved cuisine list from server
    this.restService.getAllCuisineData().subscribe((res: any) => {
      this.cuisineData = res;
      this.loading = false;
      if (this.cuisineData.length >= 1) {
        this.dataLength = 1;
      } else {
        this.dataLength = 0;
      }
    }, () => {
      this.loading = false;
      this.errorImg = true;
    });
  }


  openAddDialog() { // invoke when user want to add new cuisine
    this.loading = false;
    this.dataLength = 1;
    this.closeUpdateDialog();
    this.addDialog = true;
    this.cusineDetails = false;
  }

  onSaveCuisine() { // invoke when user want to save new cuisine data to server
    this.restService.addNewCuisineData(this.addCuisine).subscribe((res: any) => {
      if (this.cuisineData.length > 0) {
        this.cuisineData.push(res);
      }
      else {
        this.getCuisineData();
      }
      this.toster.success(' New cuisine Added', 'Success!!!');
      this.closeAddDialog();
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  closeAddDialog() { // after saving cuisine data reset variables to init state
    this.addDialog = false;
    this.cusineDetails = true;
    this.addCuisine.cuisineName = '';
  }

  openUpdateDialog(cuisineId) { // invoke when user want to Update saved cuisine
    this.addDialog = false;
    this.cuisineId = cuisineId
    this.updateDialog = true;
    this.restService.getSpacificCuisineData(cuisineId).subscribe((res: any) => {
      this.updateCuisine = res;
      console.log("Specific cousine " + JSON.stringify(res));
    }, (error) => {
      this.toster.error(error.message, 'ERROR');
    });
  }

  onUpdateSaveCuisine() { // invoke when user want to update a cuisine data to server
    this.restService.updateCuisineData(this.cuisineId, this.updateCuisine)
      .subscribe(() => {
        this.toster.success('Cusine updated successfully');
        this.getCuisineData();
        this.closeUpdateDialog();
      }, (error) => {
        this.toster.error(error.message, 'ERROR');
      });
  }

  closeUpdateDialog() { // after update cuisine data reset variables to init state
    this.updateDialog = false;
  }

  onDelete(id, index) { // to delete cuisine data from server
    let result = confirm("Do yo want to delete")
    if (result) {
      this.restService.deleteCuisine(id).subscribe((res) => {
        this.cuisineData.splice(index, 1);
        this.toster.success('cuisine deleted successfully');
      }, (error) => {
        this.toster.error('unable to delete');
      })
    }
  }


  ngOnInit() {
  }
}
