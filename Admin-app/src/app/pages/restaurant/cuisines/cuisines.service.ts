import { Injectable } from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class CuisinesService {

  constructor(private crud:CrudService) { }

  // save cuisine to server
  addNewCuisineData(body :string){
    return this.crud.post('cuisines',body);
  }
  // get list of all cuisine save by admin
  getAllCuisineData(){
    return this.crud.get('cuisines');
  }

  // get particular cuisine by cuisine id from server
  getSpacificCuisineData(Id){
    return this.crud.getOne('cuisines',Id);
  }

  // update/edit cuisine on server via admin only
  updateCuisineData(Id:string,data:any){
    const body = JSON.stringify(data);
      return this.crud.put('cuisines',body,Id);
  }

  // delete particular cuisine from server
   deleteCuisine(Id:string){
      return this.crud.delete('cuisines',Id);
   } 
}
