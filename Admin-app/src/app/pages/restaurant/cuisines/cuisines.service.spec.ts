import { TestBed, inject } from '@angular/core/testing';

import { CuisinesService } from './cuisines.service';

describe('CuisinesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuisinesService]
    });
  });

  it('should be created', inject([CuisinesService], (service: CuisinesService) => {
    expect(service).toBeTruthy();
  }));
});
