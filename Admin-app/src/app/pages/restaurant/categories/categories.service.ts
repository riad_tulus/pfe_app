import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class CategoriesService {
  constructor(private crud: CrudService) {
  }
  // get category data according to restaurant
  getCategories(id: string) {
    return this.crud.getOne('categories/restaurant', id);
  }
  // get restaurant info from server
  getRestaurantData(id: string) {
    return this.crud.getOne('users', id);
  }
}
