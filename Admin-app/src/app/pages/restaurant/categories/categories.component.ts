import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SharedService} from '../../../layouts/shared-service';
import {CategoriesService} from './categories.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [CategoriesService]
})
export class CategoriesComponent implements OnInit {
  loading = true
  pageTitle = ' Categories';
  restaurantID: string;
  breadcrumb: any = [
    {
      title: 'Restaurants',
      link: '/pietech/restaurants',
      icon: ''
    },
    {
      title: 'View Restaurants',
      link: '/pietech/restaurant/view/' + this.restaurantID,
      icon: ''
    },
    {
      title: 'Categories',
      link: '',
      icon: ''
    },
  ];
  categoriesData: Array<any>;
  userDetails: any = {};
  categoryLength: number;
  p = 1;

  constructor(private toaster: ToastrService, private _sharedService: SharedService, private route: ActivatedRoute, private restService: CategoriesService) {
    this._sharedService.emitChange(this.pageTitle);
    const uid = localStorage.getItem('id');
    if (uid !== null) {
      this.route.params.map(params => params['id']).subscribe(Id => {
        if (Id != null) {
          this.restaurantID = Id;
          this.getCategories(this.restaurantID); // get category data by restaurant Id
          this.getRestaurantData(this.restaurantID); // get Restaurant Data by restaurant Id
        }
      });
    }
  }

  getCategories(id: string) { // fetching category data by restaurant Id
    this.restService.getCategories(id).subscribe(res => {
      this.categoriesData = res;
      this.categoryLength = this.categoriesData.length;
      // console.log(this.categoriesData);
      this.loading = false;
    }, (error: any) => {
      this.toaster.error('Quelque chose ne va pas : ' + error.message , 'Error');
    });
  }

  getRestaurantData(id: string) { // fetching Restaurant Data by restaurant Id
    this.restService.getRestaurantData(id).subscribe(res => {
      this.userDetails = res;
    });
  }

  ngOnInit() {
  }

}
