import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class UsersService {
  constructor(private crud: CrudService) {
  }

  getUsers(id: string) { // get Users data by restaurant id
    return this.crud.getOne('orders/customer/restaurant', id);
  }

  getRestaurantDetails(id: string) { // get Restaurant Details by restaurant id
    return this.crud.getOne('users', id);
  }
}
