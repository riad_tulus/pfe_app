import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SharedService} from '../../../layouts/shared-service';
import {UsersService} from './users.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [UsersService]
})
export class UsersComponent implements OnInit {
  loading = true;
  restaurantId: string;
  userData: Array<any>;
  userDataLength: number;
  pageTitle = 'Utilisateurs';
  restaurantDetails: any = {};
  breadcrumb: any = [
    {
      title: 'Restaurants',
      link: '/pietech/restaurants',
      icon: ''
    },
    {
      title: 'View Restaurants',
      link: '/pietech/restaurant/view',
      icon: ''
    },
    {
      title: 'Users',
      link: '',
      icon: ''
    },
  ];

  constructor(private toaster: ToastrService, private _sharedService: SharedService, private route: ActivatedRoute, private restService: UsersService) {
    this._sharedService.emitChange(this.pageTitle);
    const uid = localStorage.getItem('id');
    if (uid !== null) {
      this.route.params.map(p => p['id']).subscribe(id => {
        if (id !== null) {
          this.restaurantId = id;
          this.getUsers(this.restaurantId); // get Users Details by restaurant Id
          this.getRestaurantDetails(this.restaurantId); // get Restaurant Details by restaurant Id
        }
      });
    }
  }

  getUsers(id: string) { // invoked by constructor fetching User data by restaurant Id
    this.restService.getUsers(id).subscribe(res => {
      // console.log(res);
      this.userData = res;
      this.userDataLength = this.userData.length;
      this.loading = false;
    }, (error: any) => {
      this.toaster.error('Quelque chose ne va pas : ' + error.message , 'Error');
    });
  }

  getRestaurantDetails(id: string) { // invoked by constructor fetching Product data by restaurant Id
    this.restService.getRestaurantDetails(id).subscribe(res => {
      // console.log(res);
      this.restaurantDetails = res;
    }, (error: any) => {
      this.toaster.error('Quelque chose ne va pas : ' + error.message , 'Error');
    });
  }

  ngOnInit() {
  }

}
