import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class ViewRestaurantService {
  constructor(private crud: CrudService) {
  }

  getRestaurant(id: string) { // get Restaurant data by restaurant id
    return this.crud.getOne('users', id);
  }

  getRestaurantData(id: string) { // get orders data from collection by restaurant id
    return this.crud.getOne('orders/collection', id);
  }

  getWalletCount(restaurantId) { // get Wallet Count restaurant id
    return this.crud.getOne('wallets/restaurant/owner', restaurantId).map((data: Response) => data);
  }
}
