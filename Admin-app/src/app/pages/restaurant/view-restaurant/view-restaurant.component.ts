import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {SharedService} from '../../../layouts/shared-service';
import {ViewRestaurantService} from './view-restaurant.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-view-restaurant',
  templateUrl: './view-restaurant.component.html',
  styleUrls: ['./view-restaurant.component.scss'],
  providers: [ViewRestaurantService]
})
export class ViewRestaurantComponent implements OnInit {
  loading: boolean = true;
  restaurantDetails: any = {
    restaurantName: ''
  };
  showProfile: boolean = false;
  isRestaurantNull: boolean = true;
  pageTitle: string = 'View Restaurant';
  productCount: number = 0
  categoryCount: number = 0;
  orderCount: number = 0;
  userCount: number = 0;
  locationCount: number = 0;
  barChartLabels: Array<any>;
  barChartData: Array<any> = [];
  chartReady: boolean = false;
  restaurantData: any = {};
  public walletCounts = {
    totalOrderEarning: 0,
    withdrawlAmount: 0,
    availableBalance: 0,
  };
  restaurantID: string;
  // barChart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    responsiveAnimationDuration: 500,
    barPercentage: 0.2
  };
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartColors: any[] = [
    {
      backgroundColor: 'rgba(226,50,55,0.2)',
      borderColor: '#e23237'
    },
  ];
  breadcrumb = [
    {
      title: 'Restaurants',
      link: '/pietech/restaurants',
      icon: ''
    },
    {
      title: 'View Restaurants',
      link: '',
      icon: ''
    }
  ];
  constructor(private router: Router,private toaster: ToastrService, private _sharedService: SharedService, private route: ActivatedRoute, private restService: ViewRestaurantService) {
    this._sharedService.emitChange(this.pageTitle);
    this.route.params.map(params => params['id']).subscribe(Id => {
      if (Id != null) {
        this.restaurantID = Id;
        this.getRestaurant(Id);
        this.getRestaurantData(Id);
        this.getAllWalletCountData(Id);
      }
    });
  }

  getAllWalletCountData(restaurantId) { // invoked by constructor fetching wallet amount details by restaurant Id
    this.restService.getWalletCount(restaurantId).subscribe((res: any) => {
      this.walletCounts = res;
    });
  }

  getRestaurant(id: string) { // invoked by constructor fetching restaurant details by restaurant Id
    this.restService.getRestaurant(id).subscribe(res => {
      this.restaurantDetails = res;
      // console.log(' restaurantDetails ' + JSON.stringify(this.restaurantDetails));
    }, (error: any) => {
      this.toaster.error('Quelque chose ne va pas : ' + error.message , 'Error');
    });
  }

  getRestaurantData(id: string) {
    /* invoked by constructor fetching Restaurant count data by restaurant Id
     * like : product Count, category Count, order Count, user Count, location Count */
    this.restService.getRestaurantData(id).subscribe(res => {
      if (res != null) {
        this.isRestaurantNull = false;
        this.restaurantData = res;
        // console.log(this.restaurantData);
        this.productCount = this.restaurantData.productcount;
        this.categoryCount = this.restaurantData.categorycount;
        this.orderCount = this.restaurantData.ordercount;
        this.userCount = this.restaurantData.usercount;
        this.locationCount = this.restaurantData.locationcount;
        this.loading = false;
        if (this.restaurantData.datasets.length > 0 && this.restaurantData.labels.length > 0) {
          this.barChartLabels = this.restaurantData.labels;
          this.barChartData.push({label: 'Sales', data: this.restaurantData.datasets, borderWidth: 2});
          this.chartReady = true;
        }
      } else {
      }
    }, (error) => {
      this.toaster.error(' Quelque chose ne va pas ', 'Error');
    });
  }

  ngOnInit() {
  }

  viewProfile() { // when Admin click on show profile button (restaurant project)
    this.showProfile = !(this.showProfile);
  }

  product() { // navigate to product by restaurant ID
    this.router.navigate(['/pietech/restaurant/product/' + this.restaurantID]);
  }

  order() { // navigate to order by restaurant ID
    this.router.navigate(['/pietech/restaurant/order/' + this.restaurantID]);
  }

  categories() { // navigate to categories by restaurant ID
    this.router.navigate(['/pietech/restaurant/categories/' + this.restaurantID]);
  }

  users() { // navigate to users by restaurant ID
    this.router.navigate(['/pietech/restaurant/users/' + this.restaurantID]);
  }

// breadcrumb menu

  location() { // navigate to location by restaurant ID
    this.router.navigate(['/pietech/restaurant/location/' + this.restaurantID]);
  }

}

