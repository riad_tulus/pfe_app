import {Injectable} from '@angular/core';
import {CrudService} from '../../../../services/crud.service';

@Injectable()
export class WithdrawalService {
  constructor(private crud: CrudService) {
  }

  getWithdrawalRequests() { // get Withdrawal Requests from server
    return this.crud.get('wallets');
  }

  updateWallet(id, data) { // Update Withdrawal Requests
    return this.crud.put('wallets', data, id);
  }
}
