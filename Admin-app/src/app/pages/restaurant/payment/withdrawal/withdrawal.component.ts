import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {WithdrawalService} from './withdrawal.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.css'],
  providers: [WithdrawalService]
})
export class WithdrawalComponent implements OnInit {
  p: number = 1;
  loading: boolean = true;

  public dataLength: number = -1; // init by -1; if any data present value will be 1; if no any data value will be 0
  public errorImg: boolean = false;
  public errorImage = './assets/content/error.png'
  public noContent: any = './assets/content/nodata_cloud.png';


  withdrawlsData: Array<any>;
  rejectedRequest: boolean = false;
  confirmedRequest: boolean = false;
  rejectedId: string;
  confirmedId: string;
  status: string;

  constructor(private restService: WithdrawalService, private toastr: ToastrService) {
    this.getWithdrawals(); // fetching all the Withdrawal request
  }

  ngOnInit() {
  }

  getWithdrawals() { // invoked by constructor (fetching all the Withdrawal request)
    this.restService.getWithdrawalRequests().subscribe(res => {
      this.withdrawlsData = res;
      // console.log(' withdrawlsData ' + JSON.stringify(res));
      this.loading = false;
      if (this.withdrawlsData.length > 0) {
        this.dataLength = 1;
      } else {
        this.dataLength = 0;
      }
    }, () => {
      this.loading = false;
      this.errorImg = true;
    });
  }

  confirmRequest(obj: any) { // when admin confirm the withdrawal request
    swal({
      title: 'Are you sure',
      text: 'Do you want confirm the withdrawal request',
      type: 'warning',
      background: '#fff',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes, Confirm',
      cancelButtonText: 'No',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-danger'
    }).then(function () {
      this.status = 'Confirmed';
      let id = obj._id;
      this.confirmedId = id;
      obj.transactionStatus = this.status;
      this.restService.updateWallet(id, obj).subscribe(res => {
        this.toastr.success('Success', 'Withdrawal request is confirmed', {timeOut: 2000});
        this.confirmedRequest = true;
      }, error => {
        this.toastr.error('Error', error.message, {timeOut: 2000});
      });
    }.bind(this), function (dismiss) {
      if (dismiss === 'cancel') {
        swal('Cancelled', 'Request status is not updated');
      }
    });
  }

  rejectRequest(obj: any) { // when admin reject the withdrawal request

    swal({
      title: 'Are you sure',
      text: 'Do you want reject the withdrawal request',
      type: 'warning',
      background: '#fff',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes, Reject',
      cancelButtonText: 'No',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-danger'
    }).then(function () {
      this.rejectedRequest = true;
      this.status = 'Rejected';
      obj.transactionStatus = this.status;
      this.rejectedId = obj._id;
      this.restService.updateWallet(this.rejectedId, obj).subscribe(res => {
        this.toastr.success('Success', 'Withdrawal request is rejected', {timeOut: 2000});
      }, error => {
        this.toastr.error('Error', error.message, {timeOut: 2000});
      });
    }.bind(this), function (dismiss) {
      if (dismiss === 'Cancel') {
        swal('Cancelled', 'Request status is not updated');
      }
    });
  }

}
