import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SharedService} from '../../../layouts/shared-service';
import {ToastrService} from 'ngx-toastr';
import {AddRestaurantService} from './add-restaurant.service';

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.scss'],
  providers: [AddRestaurantService]
})
export class AddRestaurantComponent implements OnInit {
  @ViewChild('restaurantForm') restaurantForm: FormGroup;
  pageTitle: string = 'Add Restaurant';
  addRestaurant = {
    name: null,
    restaurantName: null,
    activationStatus: null,
    email: null,
    role: null,
    password: null,
    contactNumber: null
  };

  constructor(private _sharedService: SharedService, private toastr: ToastrService, private restService: AddRestaurantService, private router: Router) {
    this._sharedService.emitChange(this.pageTitle);
  }

  ngOnInit() {
    this.createRestaurantForm();
  }

  createRestaurantForm() { // form for registration
    this.restaurantForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'restaurantName': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
      'contactNumber': new FormControl(null, Validators.required)
    });
  }

  onRestaurant() { // invoke when user click on register button
    this.addRestaurant.name = this.restaurantForm.value.name;
    this.addRestaurant.restaurantName = this.restaurantForm.value.restaurantName;
    this.addRestaurant.email = this.restaurantForm.value.email;
    this.addRestaurant.contactNumber = this.restaurantForm.value.contactNumber;
    this.addRestaurant.password = this.restaurantForm.value.password;
    this.addRestaurant.role = 'Owner';
    this.addRestaurant.activationStatus = true;
    this.restService.addRestaurant(this.addRestaurant).subscribe(res => {
     // console.log(res);
      this.toastr.success('Restaurant created successfully', 'Success', {timeOut: 2000});
      this.router.navigate(['/pietech/restaurants']);
    }, error => {
      this.toastr.error(error.message, 'Error', {timeOut: 2000});
    });
  }

}
