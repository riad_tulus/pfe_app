import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class AddRestaurantService {
  constructor(private crud: CrudService) {
  }

  addRestaurant(data: any) { // post Restaurant information to server
    return this.crud.post('users', data);
  }
 }
