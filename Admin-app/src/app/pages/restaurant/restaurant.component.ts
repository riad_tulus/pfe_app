import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../../layouts/shared-service';
import { ToastrService } from 'ngx-toastr';
import { RestaurantService } from './restaurant.service';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss'],
  providers: [RestaurantService]
})
export class RestaurantComponent implements OnInit {
  loading: boolean = true;
  pageTitle: string = 'Restaurant';
  restaurantData: any = [];
  public p: number = 1;
  breadcrumb: any = [
    {
      title: 'Home',
      link: '/pietech/dashboard',
      icon: ''
    },
    {
      title: 'Restaurants',
      link: '',
      icon: ''
    },
  ];

  constructor(private _sharedService: SharedService, private restService: RestaurantService, private toastr: ToastrService, private router: Router) {
    this._sharedService.emitChange(this.pageTitle);
    this.getUsers(); // fetching all Restaurants list
  }

  ngOnInit() { }

  getUsers() { // invoked by constructor, fetching Restaurants list
    this.restService.getRestaurants().subscribe(res => {
      this.restaurantData = res;
      this.loading = false;
    }, (error: any) => {
      this.toastr.error('Something is going wrong : ' + error.message, 'Error');
    });
  }

  enableDisableChanged(data, event) { // invoked by admin when admin active or de-active restaurant
    const id = data._id;
    data.status = event.checked;
    data.flag = 0;
    this.restService.updateRestaurant(id, data).subscribe(res => {
      this.toastr.success('Restaurant updated successfully', 'Success', { timeOut: 2000 });
    }, error => {
      this.toastr.error(error.message, 'Error', { timeOut: 2000 });
    });
  }

  viewRestaurant(restaurantId: string) { // invoked by admin when admin want to see restaurant information
    this.router.navigate(['pietech/restaurant/view/' + restaurantId]);
  }

}
