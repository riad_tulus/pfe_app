export interface userlist {
    name:string;
    _id:number;
    image:string;
    email:string;
    count:number;
    lastmsgTime:string;
    lastMessage:string;
}


export interface chatData {
	sender:{
		name:string,
        id:string,
        email:string
	};
    receiver:{
        id:string,
        name:string,
        email:string
    };
    messages:any;
}

export interface showChat {
  id: string;
}
export interface userId {
  id: string;
}