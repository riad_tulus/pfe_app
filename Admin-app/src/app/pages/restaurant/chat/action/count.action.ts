import { Action } from '@ngrx/store';

export const UPDATE_COUNT = '[Count] Update';
export const DOWN_COUNT = '[Count] Down';
export const SET = '[Count] Set';
export const ASSIGN_ID = '[Count] AssignId';

export class UpdateCount implements Action {
	readonly type = UPDATE_COUNT;
    constructor(public payload:any){}
}

export class DownGradeCount implements Action {
	readonly type = DOWN_COUNT;
    constructor(public payload:any){}
}

export class SetCount implements Action {
	readonly type = SET;
    constructor(public payload:any){}
}

export class AssignId implements Action {
	readonly type = ASSIGN_ID;
    constructor(public payload:any){}
}

export type ALl = UpdateCount|DownGradeCount|AssignId|SetCount;