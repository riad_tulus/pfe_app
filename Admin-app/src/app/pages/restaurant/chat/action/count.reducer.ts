 import { UnreadCount } from './count';
import * as CountAction from './count.action';

// export function countData(state: number=0, action: Action){
// 	console.log('action type '+action.type+ "  "+ state);

// 	switch (action.type) {
// 		case "INCREMENT":
// 			return state = state+1;

// 	    case "DECREMENT" :
// 		    return state = state-1 ;
		    
// 		default:
// 		    return state;	 
// 	} 
// }

// export interface UnreadCount{
// 	count:number
// }

export type Action = CountAction.ALl;

// default appState

const defaultState: UnreadCount = {
  count : +(localStorage.getItem('unreadCount')),
 _chatId : null 
}

// Helper function to create new state object

const newState = (state, newData) => {
	return Object.assign({}, state, newData);
}

export function countReducer( state: UnreadCount = defaultState, action:Action){
	console.log(action.type, state);
	switch (action.type) {
		case CountAction.UPDATE_COUNT :
			return newState(state, { count: state.count + action.payload });

	    case CountAction.DOWN_COUNT :
			return newState(state, { count: state.count - action.payload })
		
		case CountAction.SET :
			return newState(state, { count: action.payload })

		case CountAction.ASSIGN_ID :
			return newState(state, { _chatId: action.payload })
		default:
			return state;
	}
}