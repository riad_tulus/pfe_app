import {Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import * as CountAction from './action/count.action';
import {UnreadCount} from './action/count';

import {ChatService} from './chat.service';
import {SocketSharedService} from '../../../services/socketShare.service';
import {userlist} from './chat';
// import * as fromApp from '../../../store/app.reducer';

declare var $: any;

interface AppState {
  chatId: string;
}

interface CountState {
  count: UnreadCount;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  providers: [ChatService]
})
export class ChatComponent implements OnInit, OnDestroy {
  count: number = 9;
  messages$: any;
  countUpdate: Observable<UnreadCount>;
  public userlist: userlist[] = [];
  public managerlistLength = 0;
  public warningMessage: any = '';
  public userRole: string;
  public userId: any;
  public username: any = '';
  public chatData: any[] = []; //
  public renderChatlist = 0;
  public disableScroll = false;
  public message: any = {
    message: '',
    sender: '',
    receiver: '',
    receiverRole: ''
  };
  public searchName: string;
  public searchData = 0;
  public length = 0;
  private storedChatId: any; // storedChatId store active chat thread I
  private fromConstructor = false;
  private getChatPageNumber: any = 0;  // use for infinite scroll

  constructor(public el: ElementRef,
              private restService: ChatService,
              private SharedService: SocketSharedService,
              private store: Store<AppState>,
              private counter: Store<CountState>) {
    this.userRole = localStorage.getItem('role');
    this.fromConstructor = true;
    this.messages$ = this.store.select('chatId'); // store ChatId from ngRx Store
    this.messages$.subscribe((res: any) => { // check stored chatId is valid or not
      const chatType = localStorage.getItem('chatType');
      if ((this.messages$.actionsObserver._value.payload !== (undefined || null)) && (chatType !== '')) {
        this.storedChatId = this.messages$.actionsObserver._value.payload;
        if (this.storedChatId !== undefined && this.storedChatId.length > 6) {
          this.startConversation(this.storedChatId, chatType);
        }
      }
    });

    this.countUpdate = this.counter.select('count');
    this.userId = localStorage.getItem('id');
    this.message.sender = localStorage.getItem('userId');

    this.getRestaurantChatList(); // retrieve Restaurant Chat List from server

  }

  getRestaurantChatList() { // fetching Restaurant Chat List
    this.managerlistLength = 0;
    this.restService.getRestaurantChatList().subscribe((userList: any) => {
      console.log('User list' + JSON.stringify(userList.message));
      if (userList.message == 'You do not have any user who have contacted you atleast once.') {
        this.managerlistLength = 1;
        this.warningMessage = userList.message;
        console.log('message warningMessage ' + this.warningMessage);
      }
      else {
        this.userlist = userList;
        console.log(' userList ' + JSON.stringify(this.userlist));
        this.managerlistLength = this.userlist.length;
      }
    })
  }


  startConversation(userId, listOrder, data?) { // start conversation
    console.log('chatIndex ' + JSON.stringify(data));
    if ((data != undefined) && (data.count > 0)) {
      let updateCount: number = +(data.count);
      this.counter.dispatch(new CountAction.DownGradeCount(data.count));
    }
    this.restService.readSingleMessageThread(userId).subscribe((res: any) => {
      const threadCount: number = 0;
      if (listOrder === 'listOne') {
        const chatIndex = this.userlist.findIndex(x => x._id == userId);
        this.userlist[chatIndex].count = threadCount;
      }
    });
    this.getChatPageNumber = 0;
    if (this.storedChatId == userId && (!this.fromConstructor)) {

    } else {
      this.fromConstructor = false;
      this.disableScroll = false;
      this.storedChatId = userId;
      localStorage.setItem('chatType', listOrder);
      this.counter.dispatch(new CountAction.AssignId(userId));
      if (listOrder === 'listOne' && this.userRole === 'Admin') {
        this.renderChatlist = 1;
        this.message.receiverRole = 'Owner';
        this.restService.getOwnerManagerMessageData(userId, this.getChatPageNumber).subscribe((res: any) => {
          console.log('conversation chat data ' + JSON.stringify(res));
          this.chatData = res.messages;
          this.username = res.sender.restaurantName;
          this.getChatPageNumber = this.getChatPageNumber + 1;
          this.renderChatlist = 2;
          setTimeout(() => {
            this.scrollToBottom();
          });
        });
        this.message.receiver = userId;
      }
      else {

      }
    }// outer else close
  }

  onSendMessage() {  // when send new message
    if (this.chatData.length < 1) {
      this.chatData = [];
    }
    this.restService.saveMessageData(this.message).subscribe((res: any) => {
      console.log('response onSendMessage  ' + JSON.stringify(res));
      this.chatData.push(res);
      this.message.message = '';
      setTimeout(() => {
        this.scrollToBottom();
      });
    }, (error) => {
      //  console.log("error "+JSON.stringify(error));
    });
  }

  updateCurrentChatReadCount() {  // updating thread read/unread count
    this.SharedService.messageRead(this.storedChatId);
  }

  searchChat(val) { // search chat thread from Restaurant list.
    if (val.length == 0) {
      this.clearSearch();
    }
    const key = isNaN(val);
    if (key == true) {
      this.searchData = 1;
      const totalCount = val.length;
      if (totalCount >= 3) {
        this.restService.searchUsersData(this.searchName, 0).subscribe(res => {
          this.searchData = 2;
          this.userlist = res;
          this.length = this.userlist.length;
        });
      }
    } else if (key == false) {
      const totalCount = val.length;
      console.log('totalCount' + totalCount);
      if (totalCount > 8) {
        this.restService.searchUsersData(this.searchName, 1).subscribe(res => {
          this.userlist = res;
          this.length = this.userlist.length;
        });
      }
    }
  }


  ngOnInit() {
    /** Declaring all the method inside ngOnInit block which invoke when observable response
     * getUserNotification() invoke when new message arrive and socket emit new message */
    this.SharedService.getUserNotification().subscribe((message: any) => {
      console.log('ssss getUserNotification' + JSON.stringify(message));
      if (message._id === this.storedChatId) {
        const countZero = 0;
        console.log('chat is going on...');
        message.count = countZero;
      }
      const data: any = message;
      const index = this.userlist.findIndex(x => x._id === data._id);
      if (index >= 0) {
        this.userlist.splice(index, 1);
        this.userlist.unshift(data)

      } else {
        this.userlist.unshift(data);
      }
    });
      this.SharedService.getMessages(this.userId).subscribe((message) => {
      this.messageUpdated(message);
    });

    this.SharedService.getCountNotification().subscribe((message) => {
      console.log('ssss getCountNotification');
    });
  }

  messageUpdated(message) { // while new message arrived update on message list
    if (message.sender == this.message.receiver) {
      this.chatData.push(message);
      setTimeout(() => {
        this.scrollToBottom();
      });
    }
  }

  scrollToBottom() { // scroll to bottom when chat message load
    const scrollPane: any = this.el
      .nativeElement.querySelector('.chat-text');
    scrollPane.scrollTop = scrollPane.scrollHeight;
  }

  onScrollUp() { // load chat thread message while user scroll up
    if (this.getChatPageNumber != null) {
      this.restService.getOwnerManagerMessageData(this.message.receiver, this.getChatPageNumber).subscribe((res: any) => {
        const length = res.messages.length;
        if (res.nextpageno === null) {
          this.disableScroll = true;
        }
        for (let i = 0; i < length; i++) {
          this.chatData.unshift(res.messages[i]);
        }
        this.getChatPageNumber = res.nextpageno;
      });
    } else { }


  }

  clearSearch() {
    console.log(' clearSearch called ');
    this.searchName = '';
    this.searchData = 0;
    this.getRestaurantChatList();
  }


  ngOnDestroy() {
    // resetting chatId on NgRx Store
    this.counter.dispatch(new CountAction.AssignId(''));
  }

}

