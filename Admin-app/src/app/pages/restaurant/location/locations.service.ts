/**/
import {Injectable} from '@angular/core';
import {CrudService} from '../../../services/crud.service';

@Injectable()
export class LocationsService {
  constructor(private crud: CrudService) {
  }

  getLocations(id: string) { // get Locations data by restaurant Id
    return this.crud.getOne('locations/all/data', id);
  }

  getRestaurantDetails(id: string) { // get Restaurant Details
    return this.crud.getOne('users', id);
  }
}
