import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LocationsService} from './locations.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
  providers: [LocationsService]
})
export class LocationComponent implements OnInit {
  loading = true;
  restaurantId: string;
  locationsData: Array<any>;
  restaurantData: any = {};
  p = 1;
  locationDataLength: number;
  breadcrumb: any = [
    {
      title: 'Restaurants',
      link: '/pietech/restaurants',
      icon: ''
    },
    {
      title: 'View Restaurants',
      link: '/pietech/restaurant/view',
      icon: ''
    },
    {
      title: 'Locations',
      link: '',
      icon: ''
    },
  ];

  constructor(private toaster: ToastrService, private route: ActivatedRoute, private restService: LocationsService) {
    const uid = localStorage.getItem('id');
    if (uid !== null) {
      this.route.params.map(params => params['id']).subscribe(Id => {
        this.restaurantId = Id;
        this.getLocations(this.restaurantId); // get Locations data by restaurant Id
        this.getRestaurantData(this.restaurantId); // get Restaurant Data by restaurant Id
      });
    }
  }

  getLocations(id: string) { // invoked from constructor
    this.restService.getLocations(id).subscribe(res => {
      this.locationsData = res;
      this.locationDataLength = this.locationsData.length;
      this.loading = false;
    }, (error: any) => {
      this.toaster.error('Quelque chose ne va pas : ' + error.message , 'Error');
    });
  }

  getRestaurantData(id: string) { // invoked from constructor
    this.restService.getRestaurantDetails(id).subscribe(res => {
      this.restaurantData = res;
    }, (error: any) => {
      this.toaster.error('Quelque chose ne va pas : ' + error.message , 'Error');
    });
  }

  ngOnInit() {
  }

}
