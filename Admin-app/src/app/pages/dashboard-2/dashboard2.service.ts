import {Injectable} from '@angular/core';
import {CrudService} from '../../services/crud.service';

@Injectable()
export class Dashboard2Service {
  restauranId: any;

  constructor(private crud: CrudService) {
    this.restauranId = localStorage.getItem('Id');
  }

  getWalletcount() { // fetching wallet available amount from server
    return this.crud.get('wallets/admin/all/commission').map((data: Response) => data);
  }

  getDataForGraph() { // fetching sales data from server
    return this.crud.get('orders/collections/restaurants');
  }


}
