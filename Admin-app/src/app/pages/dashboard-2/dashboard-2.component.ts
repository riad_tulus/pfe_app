import {Component, OnInit} from '@angular/core';
import {SharedService} from '../../layouts/shared-service';
import {Dashboard2Service} from './dashboard2.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'page-dashboard-2',
  templateUrl: './dashboard-2.component.html',
  styleUrls: ['./dashboard-2.component.scss'],
  providers: [Dashboard2Service]
})

export class PageDashboard2Component implements OnInit {
  loading: boolean = true;
  pageTitle: string = 'Accueil';
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: Array<any>;
  public barChartType: any = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: Array<any> = [];
  public user: any;
  public myLocation: any;
  public update = false;
  public userCount = 0;
  public ownerCount = 0;
  public orderCount = 0;
  public walletCount = 0;
  public totalCollection = 0;
  public hasChartData = false;

  constructor(private router: Router, private _sharedService: SharedService, private restService: Dashboard2Service, private toster: ToastrService) {
    this.user = localStorage.getItem('role');
    this.myLocation = localStorage.getItem('locationName');
    if (this.user === 'Owner') {
      this.update = true;
    }

    this._sharedService.emitChange(this.pageTitle);
    this.getGraphData();
    this.getWalletAmount();
  }

  getWalletAmount() { // fetching available wallet amount from server
    this.restService.getWalletcount().subscribe((res: any) => {
      this.walletCount = res[0].data;
    });
  }

  getGraphData() { // fetching sales data from server, for plotting graph
    const role = localStorage.getItem('role');
    if (role === 'Admin') {
      this.restService.getDataForGraph().subscribe(res => {
        console.log(res);
        this.userCount = res.usercount;
        this.ownerCount = res.ownercount;
        this.orderCount = res.ordercount;
        this.barChartLabels = res.labels;
        res.datasets.forEach(data => {
          this.totalCollection += data;
        });
        this.barChartData.push({label: 'Sales', data: res.datasets, borderWidth: 2});
        this.hasChartData = true;
        this.loading = false;
      }, (error) => {
        console.log(error.message);
        if (error.message === 'Unexpected token < in JSON at position 0') {
          localStorage.clear();
          this.router.navigate(['/pages/sign-in']);
        }
      });
    } else {
      console.log('You are not authorized');
    }
  }


  ngOnInit() {
  }


}
