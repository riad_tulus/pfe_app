import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {CrudService} from './services/crud.service';
import {SocketSharedService} from './services/socketShare.service';

@Component({
  moduleId: module.id,
  selector: 'app',
  template: `
    <router-outlet></router-outlet>`,
  styleUrls: ['../assets/sass/style.scss'],
  providers: [CrudService]
})
export class AppComponent {
  constructor(private _socketService: SocketSharedService, private router: Router, private crud: CrudService) {
    const token = localStorage.getItem('token');
    const uid = localStorage.getItem('id');
    if (token !== null && uid !== null) {
      this.crud.get('users/me').subscribe((res: any) => {
          this._socketService.userInfo(res._id);
        },
        () => {
          localStorage.removeItem('token');
          localStorage.removeItem('id');
          localStorage.removeItem('role');
        });
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('id');
      localStorage.removeItem('role');
      this.router.navigate(['/pages/sign-in']);
    }
  }
}
