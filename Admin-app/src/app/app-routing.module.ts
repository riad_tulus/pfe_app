import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DefaultLayoutComponent} from './layouts/default/default.component';
import {ExtraLayoutComponent} from './layouts/extra/extra.component';

import {PageDashboard2Component} from './pages/dashboard-2/dashboard-2.component';

import {PageSignIn1Component} from './pages/extra-pages/sign-in-1/sign-in-1.component';
import {PageSignUp1Component} from './pages/extra-pages/sign-up-1/sign-up-1.component';
import {PageForgotComponent} from './pages/extra-pages/forgot/forgot.component';
import {PageConfirmComponent} from './pages/extra-pages/confirm/confirm.component';
import {Page404Component} from './pages/extra-pages/page-404/page-404.component';
import {Page500Component} from './pages/extra-pages/page-500/page-500.component';

import {RestaurantComponent} from './pages/restaurant/restaurant.component';
import {AddRestaurantComponent} from './pages/restaurant/add-restaurant/add-restaurant.component';
import {ViewRestaurantComponent} from './pages/restaurant/view-restaurant/view-restaurant.component';
import {UsersComponent} from './pages/restaurant/users/users.component';
import {OrderComponent} from './pages/restaurant/order/order.component';
import {ProductsComponent} from './pages/restaurant/products/products.component';
import {CategoriesComponent} from './pages/restaurant/categories/categories.component';
import {LocationComponent} from './pages/restaurant/location/location.component';
import {PaymentComponent} from './pages/restaurant/payment/payment.component';
import {WithdrawalComponent} from './pages/restaurant/payment/withdrawal/withdrawal.component';
import {PageResetComponent} from './pages/extra-pages/reset-password/reset-password.component';
import {UserProfileSettings} from './pages/user-profile/settings/settings.component';
import {ProfileComponent} from './pages/user-profile/profile/profile.component';
import {UpdateProfileComponent} from './pages/user-profile/profile/update-profile/update-profile.component';
import {CuisinesComponent} from './pages/restaurant/cuisines/cuisines.component';

import {ChatComponent} from './pages/restaurant/chat/chat.component';

const defaultRoutes: Routes = [
  {path: 'dashboard', component: PageDashboard2Component},
  {path: 'restaurants', component: RestaurantComponent},
  {path: 'restaurant/add', component: AddRestaurantComponent},
  {path: 'restaurant/view/:id', component: ViewRestaurantComponent},
  {path: 'restaurant/users/:id', component: UsersComponent},
  {path: 'restaurant/product/:id', component: ProductsComponent},
  {path: 'restaurant/categories/:id', component: CategoriesComponent},
  {path: 'restaurant/order/:id', component: OrderComponent},
  {path: 'restaurant/location/:id', component: LocationComponent},
  {path: 'payment', component: PaymentComponent},
  {path: 'payment/withdrawal', component: WithdrawalComponent},
  {path: 'settings', component: UserProfileSettings},
  {path: 'profile', component: ProfileComponent},
  {path: 'profile-update', component: UpdateProfileComponent},
  {path: 'chat', component: ChatComponent},
  {path:'cuisines',component:CuisinesComponent},
  {path: '**', redirectTo: '/pages/page-404'},
];


const extraRoutes: Routes = [
  {path: 'sign-in', component: PageSignIn1Component},
  {path: 'sign-up', component: PageSignUp1Component},
  {path: 'forgot', component: PageForgotComponent},
  {path: 'reset-password', component: PageResetComponent},
  {path: 'confirm', component: PageConfirmComponent},
  {path: 'page-404', component: Page404Component},
  {path: 'page-500', component: Page500Component},
];


const routes: Routes = [
  {
    path: '',
    redirectTo: '/pietech/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'pietech',
    component: DefaultLayoutComponent,
    children: defaultRoutes
  },
  {
    path: 'pages',
    component: ExtraLayoutComponent,
    children: extraRoutes
  },
  {
    path: '**',
    redirectTo: '/pages/page-404',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
