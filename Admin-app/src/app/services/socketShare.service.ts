'use strict';
import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {ConstantService} from './constant.service';

@Injectable()
export class SocketSharedService {
  socket;

  constructor(public constService: ConstantService) {
    // initialization of socket
    console.log('Inside Service Chat')
    this.socket = io.connect(this.constService.Socket_Url);
    this.socket.on('connect', function () {
      console.log('connected');
    });

    this.socket.on('disconnect', function () {
      console.log('disconnected');
    });

    this.socket.on('error', function (e) {
      console.log('System', e ? e : 'A unknown error occurred');
    });

    this.socket.emit('info', 'MicraApp is connected !!!');
  }


  getMessages(Uid: any) {
    /** get the new message from server through socket
     * listen event 'message+userId'
     * and send data back to getMessages subscriber */
    console.log('userId inside service ' + Uid);
    let observable = new Observable(observer => {
      this.socket.on('message' + Uid, (data) => {
        console.log('message in socket Service' + JSON.stringify(data));
        observer.next(data);
      });
    })
    return observable;
  }

  getCountNotification() { // copy of messageUpdatedCount()
    const observable = new Observable(observer => {
      console.log(localStorage.getItem('userId'));
      this.socket.on('updatedCount' + localStorage.getItem('userId'), (data) => {
        console.log('getCountNotification Count......' + JSON.stringify(data));
        observer.next(data);
      });
    })
    return observable;
  }

  userInfo(userId: any) { // Emit user info for socket connection
    let data: any = {
      userId: ''
    };
    data.userId = userId;
    this.socket.emit('storeClientInfo', data);
  }

  messageRead(senderId) { // count update
    console.log('messageRead ' + senderId);
    let ids: any = {
      receiverId: '', // receiverId, login User ID // senderId, other person
      senderId: ''
    };
    ids.receiverId = localStorage.getItem('id');
    ids.senderId = senderId;
    this.socket.emit('updateUnread', ids);
  }

  getUserNotification() { // get New Message
    let observable = new Observable(observer => {
      this.socket.on('notify' + localStorage.getItem('id'), (data) => {
        console.log('getUserNotification onente' + JSON.stringify(data));
        observer.next(data);
      });
    })
    return observable;
  }

 }
