import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

export class CrudBaseService {
  protected data:any ={api: null};
  protected record:any = {api:null};
  private obervable: Observable<any>;

  protected callCache(res:Response,post?:boolean){
    this.record.api = res.json();
    (post)? this.data.api.push(res.json()): this.data.api=null//invalid cache if not inserting
  }
  // service excuted
  protected extractData(res:Response){
    this.obervable = null;
    if(res.status>= 400){
      return "FAILURE";
    }else if (res.status>=200 && res.status<= 300){
      return res.json() || [];
    }
  }
  // while error occour
  protected handleError(error: Response| any){
    return Observable.throw(error.json());
  }

}
