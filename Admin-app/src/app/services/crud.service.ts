import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {ConstantService} from './constant.service';
import {CrudBaseService} from './base.service';

@Injectable()
export class CrudService extends CrudBaseService {
  public authToken = localStorage.getItem('token');
  private headers = new Headers({'Content-Type': 'application/json', 'Authorization': this.authToken});

  constructor(private constService: ConstantService, private http: Http) {
    super(); // invoking super class constructor method
  }

  initApplication(token) { // reset token and header information and re-initApplication
    this.authToken = token;
    this.headers = new Headers({'Content-Type': 'application/json', 'Authorization': this.authToken});

  }

  // get data from server Authentication
  get(api: string): Observable<any> {
    const headers = new Headers({'Content-Type': 'application/json', 'Authorization': this.authToken});
    return this.http.get(this.constService.API_ENDPOINT + api + '/', {headers: headers})
      .map(this.extractData)
      .catch(this.handleError);
  }
// get user data
  getMyData(api: string): Observable<any> {
    return this.http.get(this.constService.API_ENDPOINT + api + '/', {headers: this.headers})
      .map(this.extractData)
      .catch(this.handleError);
  }

  // to get data by perticular id from server
  getOne(api: string, id: string): Observable<any> {
    const headers = new Headers();
    return this.http.get(`${this.constService.API_ENDPOINT}${api}/${id}`, {headers: this.headers})
      .map(this.extractData)
      .catch(this.handleError);
  }

  // to save data on server
  post(api: string, body: any): Observable<any> {
    const headers = new Headers();

    return this.http.post(this.constService.API_ENDPOINT + api + '/', body, {headers: this.headers})
      .map(this.extractData)
      .catch(this.handleError);
  }

  // to  update data on server
  put(api: string, body: any, id: string): Observable<any> {
    return this.http.put(`${this.constService.API_ENDPOINT}${api}/${id}`, body, {headers: this.headers})
      .map(this.extractData)
      .catch(this.handleError);
  }

  // to delete data from server
  delete(api: string,id:string): Observable<any>{
    return this.http.delete(`${this.constService.API_ENDPOINT}${api}/${id}`, {  headers: this.headers}) 
      .map(this.extractData)
      .catch(this.handleError);
   }
}